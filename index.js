/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './src';
import { name as appName } from './app.json';

/**
 * Test library
 */

//Screenshoot Automation
// import App from './src/testlibs/Viewshot'

//Sketch Assign
// import App from './src/testlibs/Sketchview'

//RN Element and NextgIcon
// import App from './src/testlibs/Elementsicon'

// import App from './src/testlibs/DatePicker'

// import App from './src/contents/screens/asuransi/jiwa/msigsinarmas/BaseMsig'

// import App from './src/testlibs/FileSystemTest'

// import App from './src/testlibs/CacheScreenshoot'

AppRegistry.registerComponent(appName, () => App);
