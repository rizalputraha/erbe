export const SLIDER_DASHBOARD = 'slider_dashboard'
export const CATEGORY_DASHBOARD = 'category_dashboard'
export const PRODUCT_ASURANSI = 'product_asuransi'

export const STORAGE_DOWNLINE_MEMBER = 'downline_member'

/**
 * Constant Master Data Use Case
 */
//Asuransi Jiwa MSIG
export const STORAGE_MSIG_MASTER_BANK = 'msig_master_bank'
export const STORAGE_MSIG_MASTER_COUNTRY = 'msig_master_country'
export const STORAGE_MSIG_MASTER_GENDER = 'msig_master_gender'
export const STORAGE_MSIG_MASTER_GRADUATE = 'msig_master_graduate'
export const STORAGE_MSIG_MASTER_GREENCARD = 'msig_master_greencard'
export const STORAGE_MSIG_MASTER_IDENTITY = 'msig_master_identity'
export const STORAGE_MSIG_MASTER_MARRIAGE_STATUS = 'msig_master_marriage_status'
export const STORAGE_MSIG_MASTER_RELIGION = 'msig_master_religion'
export const STORAGE_MSIG_MASTER_PEKERJAAN = 'msig_master_pekerjaan'
export const STORAGE_MSIG_MASTER_PENDAPATAN = 'msig_master_pendapatan'
export const STORAGE_MSIG_MASTER_RELATION = 'msig_master_relation'
export const STORAGE_MSIG_MASTER_RELATION_AHLI_WARIS = 'msig_master_relation_ahli_waris'
export const STORAGE_MSIG_MASTER_RELATION_PEMBAYAR = 'msig_master_relation_pembayar'
export const STORAGE_MSIG_MASTER_SUMBER_DANA = 'msig_master_sumber_dana'
export const STORAGE_MSIG_MASTER_SUMBER_PENDAPATAN_BULAN = 'msig_master_sumber_pendapatan_bulan'
export const STORAGE_MSIG_MASTER_SUMBER_PENDAPATAN_TAHUN = 'msig_master_sumber_pendapatan_tahun'
export const STORAGE_MSIG_MASTER_TUJUAN = 'msig_master_tujuan'
export const STORAGE_MSIG_MASTER_TITLE = 'msig_master_title'
export const STORAGE_MSIG_MASTER_WILAYAH = 'msig_master_wilayah'
export const STORAGE_MSIG_MASTER_QUESTIONER = 'msig_master_questioner'
export const STORAGE_MSIG_MASTER_PROFILE_RESIKO = 'msig_master_profile_resiko'


//Asuransi Jiwa Simasjiwa
export const STORAGE_SIMASJIWA_MASTER_BANK = 'simasjiwa_master_bank'
export const STORAGE_SIMASJIWA_MASTER_WILAYAH = 'simasjiwa_master_wilayah'
export const STORAGE_SIMASJIWA_MASTER_IDENTITY = 'simasjiwa_master_identity'
export const STORAGE_SIMASJIWA_MASTER_GENDER = 'simasjiwa_master_gender'
export const STORAGE_SIMASJIWA_MASTER_MARRIAGE_STATUS = 'simasjiwa_master_marriage_status'
export const STORAGE_SIMASJIWA_MASTER_RELIGION = 'simasjiwa_master_religion'
export const STORAGE_SIMASJIWA_MASTER_GRADUATE = 'simasjiwa_master_graduate'
export const STORAGE_SIMASJIWA_MASTER_QUESTIONER = 'simasjiwa_master_questioner'
export const STORAGE_SIMASJIWA_MASTER_COUNTRY = 'simasjiwa_master_country'
export const STORAGE_SIMASJIWA_MASTER_RELATION = 'simasjiwa_master_relation'