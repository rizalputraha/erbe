/**
 * const management file msig
 */
export const MSIG_SIGNATURE_PP = 'msig_signature_pp_'
export const MSIG_SIGNATURE_TT = 'msig_signature_tt_'
export const MSIG_FILE_ATTACHMENT_PP = 'msig_file_attachment_pp_'
export const MSIG_FILE_ATTACHMENT_TT = 'msig_file_attachment_tt_'
export const MSIG_FILE_ATTACHMENT_CP = 'msig_file_attachment_cp_'
export const MSIG_SS_SCREEN_PP = 'msig_ss_screen_pp_'
export const MSIG_SS_SCREEN_TT = 'msig_ss_screen_tt_'
export const MSIG_SS_SCREEN_CP = 'msig_ss_screen_cp_'
export const MSIG_SS_SCREEN_UA = 'msig_ss_screen_ua_'
export const MSIG_SS_SCREEN_DI = 'msig_ss_screen_di_'
export const MSIG_SS_SCREEN_DA = 'msig_ss_screen_da_'
export const MSIG_SS_SCREEN_QU = 'msig_ss_screen_qu_'
export const MSIG_SS_SCREEN_PR = 'msig_ss_screen_pr_'
export const MSIG_SS_SCREEN_PN = 'msig_ss_screen_pn_'
export const MSIG_SS_SCREEN_SP = 'msig_ss_screen_sp_'
export const MSIG_SS_SCREEN_ST = 'msig_ss_screen_st_'
export const MSIG_SS_SCREEN_FD = 'msig_ss_screen_fd_'

/**
 * const management file simasjiwa
 */
export const SIMASJIWA_SIGNATURE_PEMEGANGPOLIS = 'simasjiwa_signature_pemegangpolis_'
export const SIMASJIWA_SIGNATURE_TERTANGGUNG = 'simasjiwa_signature_tertanggung_'
export const SIMASJIWA_FILE_PEMEGANGPOLIS = 'simasjiwa_file_pemegangpolis_'
export const SIMASJIWA_FILE_TERTANGGUNG = 'simasjiwa_file_tertanggung_'