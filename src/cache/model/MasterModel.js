import AsyncStorage from "@react-native-community/async-storage";
import { Alert } from "react-native";

export const saveCacheDownlineMember = async (key, data) => {
    try {
        await AsyncStorage.setItem(key, JSON.stringify(data));
    } catch (error) {
        Alert.alert('Error', error.message)
        // __DEV__ && console.log('Error saveCacheDownlineMember', error)
    }
}

export const loadCacheStorage = async (key) => {
    try {
        const storage = await AsyncStorage.getItem(key)
        return JSON.parse(storage)
    } catch (error) {
        Alert.alert('Error', error.message)
        // console.log("Error", err.message);
    }
}

/**
 * 
 * @param {String} storageKey Constant of asyncstorage name
 * @param {Array} oldCache From existing asyncstorage
 * @param {Array} newCache From resp api
 */
export const saveCacheMasterData = async (storageKey, oldCache, newCache) => {
    const saveItem = async (key, value) => {
        await AsyncStorage.setItem(key, JSON.stringify({ ...value }));
    }

    try {
        if(newCache.error === 0 && newCache.data.length > 0) {
            if(oldCache === null) {
                //Put in storage if not available
                await saveItem(storageKey, newCache)
                // __DEV__ && console.log('Execute create new saveCacheMasterData', {storageKey, oldCache, newCache})
            } else if (newCache.md5_checksum !== oldCache.md5_checksum) {
                await saveItem(storageKey, newCache)
                // __DEV__ && console.log('Execute update saveCacheMasterData', {storageKey, oldCache, newCache})
            } else {
                // __DEV__ && console.log('Execute no update saveCacheMasterData', {storageKey, oldCache, newCache})
            }
            
        }
    } catch (error) {
        Alert.alert('Error', error.message)
    }
}


export const loadCacheMasterData = async (key) => {
    try {
        const storage = await AsyncStorage.getItem(key)
        return JSON.parse(storage)
    } catch (err) {
        Alert.alert('Error', error.message)
    }
}

/**
 * 
 * @param {String} key
 * save data to storage like { error: 0, data: [], md5_checksum: ...} 
 */

export const loadDataMaster = async (key) => {
    try {
        const storage = await AsyncStorage.getItem(key)
        const json = JSON.parse(storage)
        return json.data
    } catch (err) {
        Alert.alert('Error', error.message)
    }
}