import { SLIDER_DASHBOARD, PRODUCT_ASURANSI } from '../constant/MasterAsuransi'
import AsyncStorage from '@react-native-community/async-storage'
import { Dashboardv2, Insurance } from '../../redux/api'

export const cacheProductInsurance = async (token) => {
    try {
        const resp = await Insurance.reqProductInsurance(token);
        console.log('Request cacheProductInsurance', {token, resp})
        if (resp.error == 0) {
            await AsyncStorage.setItem(PRODUCT_ASURANSI, JSON.stringify(resp.data));
        }
    } catch (error) {
        console.log("Error Set Menu", error);
    }
}

export const loadCacheProductInsurance = async () => {
    return await AsyncStorage.getItem(PRODUCT_ASURANSI).then(data => JSON.parse(data)).then(data => data).catch(error => error);
}


export const cacheSliderDashboard = async (token) => {
    try {
        const resp = await Dashboardv2.sliderReqv2(token);
        console.log('Request cacheSliderDashboard', {token, resp})
        if (resp.error == 0) {
            await AsyncStorage.setItem(SLIDER_DASHBOARD, JSON.stringify(resp.data));
        }
    } catch (error) {
        __DEV__ && console.log("Error Set Slider", error);
    }
}

export const loadCacheSlider = async (key) => {
    const slider = await AsyncStorage.getItem(key);
    const data = JSON.parse(slider);
    return data;
} 