import AsyncStorage from "@react-native-community/async-storage"
import { loadDataMaster } from './MasterModel'
import * as JSON_WILAYAH_MSIG from 'erbejsonfile/wilayah_msig.json'
import {
    STORAGE_MSIG_MASTER_BANK,
    STORAGE_MSIG_MASTER_COUNTRY,
    STORAGE_MSIG_MASTER_GENDER,
    STORAGE_MSIG_MASTER_GRADUATE,
    STORAGE_MSIG_MASTER_GREENCARD,
    STORAGE_MSIG_MASTER_IDENTITY,
    STORAGE_MSIG_MASTER_MARRIAGE_STATUS,
    STORAGE_MSIG_MASTER_RELIGION,
    STORAGE_MSIG_MASTER_PEKERJAAN,
    STORAGE_MSIG_MASTER_PENDAPATAN,
    STORAGE_MSIG_MASTER_RELATION,
    STORAGE_MSIG_MASTER_RELATION_AHLI_WARIS,
    STORAGE_MSIG_MASTER_RELATION_PEMBAYAR,
    STORAGE_MSIG_MASTER_SUMBER_DANA,
    STORAGE_MSIG_MASTER_SUMBER_PENDAPATAN_BULAN,
    STORAGE_MSIG_MASTER_SUMBER_PENDAPATAN_TAHUN,
    STORAGE_MSIG_MASTER_TUJUAN,
    STORAGE_MSIG_MASTER_TITLE,
    STORAGE_MSIG_MASTER_QUESTIONER,
    STORAGE_MSIG_MASTER_PROFILE_RESIKO
}
from '../constant/MasterAsuransi'

/**
 * 
 * @param {String} nameStorage 
 * @param {Any} state 
 */
export const saveCacheMsig = async (nameStorage, state) => {
    console.log('saveCacheMsig', {nameStorage, state})
    try {
        const keyStorage = state.keyStorage

        const data = await AsyncStorage.getItem(nameStorage);

        const found = (iData, kStorage) => {
            const res = iData.filter(item => item.keyStorage == kStorage)
            return res.length > 0
        }

        if(data !== null) {
            let extdata = JSON.parse(data)
            const cek = found(extdata, keyStorage)
            if(cek) {
                const obj = []
                extdata.map((item, index) => {
                    if(item.keyStorage !== keyStorage) obj.push(item)
                    if(item.keyStorage == keyStorage) obj.unshift(state)
                })
                extdata = obj
            } else {
                extdata.unshift(state)
            }
            await AsyncStorage.setItem(nameStorage, JSON.stringify(extdata));
            return extdata
        } else {
            let newdata = []
            newdata.push(state)
            await AsyncStorage.setItem(nameStorage, JSON.stringify(newdata));
            return newdata
        }
        return null;
    } catch (error) {
        return null;
        console.log("Error Set Proposal", error);
    }
}

export const loadCacheMsig = async (nameStorage) => {
    try {
        const data = await AsyncStorage.getItem(nameStorage);
        return JSON.parse(data);
    } catch (err) {
        console.log("Error", err.message);
    }
}

export const removeCacheMsig = async (nameStorage, state) => {
    try {
        const keyStorage = state.keyStorage

        const data = await AsyncStorage.getItem(nameStorage);

        if(data !== null) {
            let extdata = JSON.parse(data)
            const found = (iData, kStorage) => {
                const res = iData.filter(item => item.keyStorage !== kStorage)
                return res
            }
            const newdata = found(extdata, keyStorage)
            if(newdata.length > 0) {
                extdata = newdata
                await AsyncStorage.setItem(nameStorage, JSON.stringify(extdata));
            } else {
                await AsyncStorage.removeItem(nameStorage);
            }
        } else {
            await AsyncStorage.removeItem(nameStorage);
        }
    } catch (error) {
        console.log('Error', error.message)
    }
}

export const masterBank = async() => {
    const resp = await loadDataMaster(STORAGE_MSIG_MASTER_BANK)
    return resp
}

export const masterCountry = async() => {
    const resp = await loadDataMaster(STORAGE_MSIG_MASTER_COUNTRY)
    return resp
}

export const masterGender = async() => {
    const resp = await loadDataMaster(STORAGE_MSIG_MASTER_GENDER)
    return resp
}

export const masterGraduate = async() => {
    const resp = await loadDataMaster(STORAGE_MSIG_MASTER_GRADUATE)
    return resp
}

export const masterGreenCard = async() => {
    const resp = await loadDataMaster(STORAGE_MSIG_MASTER_GREENCARD)
    return resp
}

export const masterIdentity = async() => {
    const resp = await loadDataMaster(STORAGE_MSIG_MASTER_IDENTITY)
    return resp
}

export const masterMarriageStatus = async() => {
    const resp = await loadDataMaster(STORAGE_MSIG_MASTER_MARRIAGE_STATUS)
    return resp
}

export const masterReligion = async() => {
    const resp = await loadDataMaster(STORAGE_MSIG_MASTER_RELIGION)
    return resp
}

export const masterPekerjaan = async() => {
    const resp = await loadDataMaster(STORAGE_MSIG_MASTER_PEKERJAAN)
    return resp
}

export const masterPendapatan = async() => {
    const resp = await loadDataMaster(STORAGE_MSIG_MASTER_PENDAPATAN)
    return resp
}

export const masterRelation = async() => {
    const resp = await loadDataMaster(STORAGE_MSIG_MASTER_RELATION)
    return resp
}

export const masterRelationAhliWaris = async() => {
    const resp = await loadDataMaster(STORAGE_MSIG_MASTER_RELATION_AHLI_WARIS)
    return resp
}

export const masterRelationPembayar = async() => {
    const resp = await loadDataMaster(STORAGE_MSIG_MASTER_RELATION_PEMBAYAR)
    return resp
}

export const masterSumberDana = async() => {
    const resp = await loadDataMaster(STORAGE_MSIG_MASTER_SUMBER_DANA)
    return resp
}

export const masterSumberPendapatanBulan = async() => {
    const resp = await loadDataMaster(STORAGE_MSIG_MASTER_SUMBER_PENDAPATAN_BULAN)
    return resp
}

export const masterSumberPendapatanTahun = async() => {
    const resp = await loadDataMaster(STORAGE_MSIG_MASTER_SUMBER_PENDAPATAN_TAHUN)
    return resp
}

export const masterTujuan = async() => {
    const resp = await loadDataMaster(STORAGE_MSIG_MASTER_TUJUAN)
    return resp
}

export const masterTitle = async() => {
    const resp = await loadDataMaster(STORAGE_MSIG_MASTER_TITLE)
    return resp
}

export const masterWilayah = async() => {
    const resp = JSON_WILAYAH_MSIG.data;
    return resp;
}

export const masterQuestioner = async() => {
    const resp = await loadDataMaster(STORAGE_MSIG_MASTER_QUESTIONER)
    return resp
}

export const masterProfileResiko = async() => {
    const resp = await loadDataMaster(STORAGE_MSIG_MASTER_PROFILE_RESIKO)
    return resp
}