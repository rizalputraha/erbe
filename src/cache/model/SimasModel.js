import AsyncStorage from "@react-native-community/async-storage";
import { loadDataMaster } from './MasterModel'
import {
    STORAGE_SIMASJIWA_MASTER_BANK,
    STORAGE_SIMASJIWA_MASTER_WILAYAH,
    STORAGE_SIMASJIWA_MASTER_IDENTITY,
    STORAGE_SIMASJIWA_MASTER_GENDER,
    STORAGE_SIMASJIWA_MASTER_MARRIAGE_STATUS,
    STORAGE_SIMASJIWA_MASTER_RELIGION,
    STORAGE_SIMASJIWA_MASTER_GRADUATE,
    STORAGE_SIMASJIWA_MASTER_QUESTIONER,
    STORAGE_SIMASJIWA_MASTER_COUNTRY,
    STORAGE_SIMASJIWA_MASTER_RELATION
} from '../constant/MasterAsuransi'

/**
 * Save cache as array when save spaj form entry data to storage
 * @param {String} keyStorage 
 * @param {Array} data
 */

export const saveCacheStorage = async (key, value) => {
    console.log('save storage key value', { key, value });
    try {
        await AsyncStorage.setItem(key, JSON.stringify({ ...value }));
    } catch (error) {
        console.log("Error Set Proposal", error);
    }
}

export const loadCacheSimas = async (key) => {
    try {
        const data = await AsyncStorage.getItem(key);
        return JSON.parse(data);
    } catch (err) {
        console.log("Error", err.message);
    }
}

export const removeStorage = async (key) => {
    try {
        await AsyncStorage.removeItem(key);
    } catch (error) {
        console.log('Error', error.message)
    }
}

/**
 * 
 * @param {String} nameStorage 
 * @param {Any} state 
 */
export const saveCacheSimas = async (nameStorage, state) => {
    console.log('saveCacheSimas', { nameStorage, state })
    try {
        const keyStorage = state.keyStorage

        const data = await AsyncStorage.getItem(nameStorage);

        const found = (iData, kStorage) => {
            const res = iData.filter(item => item.keyStorage == kStorage)
            return res.length > 0
        }

        if (data !== null) {
            let extdata = JSON.parse(data)
            const cek = found(extdata, keyStorage)
            if (cek) {
                const obj = []
                extdata.map((item, index) => {
                    if (item.keyStorage !== keyStorage) obj.push(item)
                    if (item.keyStorage == keyStorage) obj.unshift(state)
                })
                extdata = obj
            } else {
                extdata.unshift(state)
            }
            await AsyncStorage.setItem(nameStorage, JSON.stringify(extdata));
            return extdata
        } else {
            let newdata = []
            newdata.push(state)
            await AsyncStorage.setItem(nameStorage, JSON.stringify(newdata));
            return newdata
        }
        return null;
    } catch (error) {
        return null;
        console.log("Error Set Proposal", error);
    }
}

export const masterBank = async () => {
    const resp = await loadDataMaster(STORAGE_SIMASJIWA_MASTER_BANK)
    return resp
}

export const masterWilayah = async () => {
    const resp = await loadDataMaster(STORAGE_SIMASJIWA_MASTER_WILAYAH)
    return resp
}

export const masterIdentity = async () => {
    const resp = await loadDataMaster(STORAGE_SIMASJIWA_MASTER_IDENTITY)
    return resp
}

export const masterGender = async () => {
    const resp = await loadDataMaster(STORAGE_SIMASJIWA_MASTER_GENDER)
    return resp
}

export const masterRelation = async () => {
    const resp = await loadDataMaster(STORAGE_SIMASJIWA_MASTER_RELATION)
    return resp
}

export const masterMarriageStatus = async () => {
    const resp = await loadDataMaster(STORAGE_SIMASJIWA_MASTER_MARRIAGE_STATUS)
    return resp
}

export const masterReligion = async () => {
    const resp = await loadDataMaster(STORAGE_SIMASJIWA_MASTER_RELIGION)
    return resp
}

export const masterGraduate = async () => {
    const resp = await loadDataMaster(STORAGE_SIMASJIWA_MASTER_GRADUATE)
    return resp
}

export const masterQuestioner = async () => {
    const resp = await loadDataMaster(STORAGE_SIMASJIWA_MASTER_QUESTIONER)
    return resp
}

export const masterCountry = async () => {
    const resp = await loadDataMaster(STORAGE_SIMASJIWA_MASTER_COUNTRY)
    return resp
}