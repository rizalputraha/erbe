export const submitProposalPemegangPolis = (state) => {
    if (state.usiaPp < 18) return { result: false, message: 'Umur pemegang polis tidak boleh kurang dari 18 tahun' }
    if (state.usiaPp > 80) return { result: false, message: 'Umur pemegang polis tidak boleh lebih dari 80 tahun' }
    if (state.dobPp === null) return { result: false, message: 'Tentukan tanggal lahir pemegang polis' }
    if (state.genderPp === null) return { result: false, message: 'Pilih Jenis Kelamin' }
    return { result: true, message: 'Validation Success' }
}

export const submitTertanggung = (state) => {
    if (state.namaTt == '') return { result: false, message: 'Nama tertanggung harus dilengkapi' }
    if (state.usiaTt < 1) return { result: false, message: 'Umur tertanggung minimal 15 hari setelah lahir' }
    if (state.usiaTt > 50) return { result: false, message: 'Umur tertanggung tidak boleh lebih dari 50 tahun' }
    if (state.dobTt === false) return { result: false, message: 'Tentukan tanggal lahir tertanggung' }
    if (state.genderTt === false) return { result: false, message: 'Pilih jenis kelamin' }
    return { result: true, message: 'Validation Success' }
}

export const submitFormPP = (state) => {
    if (state.namaPp == '') return { result: false, message: 'Lengkapi nama pemegang polis' };
    if (state.namaIbuPp == '') return { result: false, message: 'Lengkapi nama ibu pemegang polis' };
    if (state.religionPp === false) return { result: false, message: 'Tentukan agama pemegang polis' };
    if (state.statusPp === false) return { result: false, message: 'Tentukan status pemegang polis' };
    if (state.tptLahirPp == '') return { result: false, message: 'Lengkapi kota kelahiran' };
    if (state.pendidikanPp == false) return { result: false, message: 'Lengkapi Pendidikan Terakhir' };
    if (state.dobPp == '') return { result: false, message: 'Lengkapi data tanggal lahir' };
    if (state.buktiIdentitasPp == '') return { result: false, message: 'Tentukan bukti identitas' };
    if (state.identitasPp == '') return { result: false, message: 'Lengkapi no identitas' };
    if (state.jobDescPp == '') return { result: false, message: 'Lengkapi uraian pekerjaan' };
    if (state.countryPp === false) return { result: false, message: 'Lengkapi Negara Asal' };
    if (state.addressPp == '') return { result: false, message: 'Lengkapi alamat rumah' };
    if (state.kdPosPp == '') return { result: false, message: 'Lengkapi Kode Pos' };
    if (state.noHpPp == '') return { result: false, message: 'Lengkapi nomor handphone' };
    if (validateCountCharacter(state.noHpPp, 10, 12) !== true) return { result: false, message: 'Maximum nomor hp 12 digit' };
    if (state.isValidEmail === false) return { result: false, message: 'Format input email tidak valid' };
    if (state.relationPp === false) return { result: false, message: 'Tentukan hubungan pemegang polis' };
    if (state.bankIdPp === false) return { result: false, message: 'Tentukan bank' };
    if (state.branchIdPp == '') return { result: false, message: 'Tentukan cabang bank' };
    if (state.noRekPp == '') return { result: false, message: 'Lengkapi nomor rekening' };
    if (state.namaRekeningPp == '') return { result: false, message: 'Lengkapi nama pemegang rekening' };
    if (validateCountCharacter(state.noRekPp, 3, 16) !== true) return { result: false, message: 'Maximum nomor rekening adalah ' + 16 + ' digit' };
    return { result: true, message: 'Validation Success' };
}

export const submitFormTT = (state) => {
    if (state.namaTt == '') return { result: false, message: 'Lengkapi data nama lengkap' };
    if (state.tptLahirTt == '') return { result: false, message: 'Lengkapi data kota kelahiran' };
    if (state.namaIbuTt == '') return { result: false, message: 'Lengkapi nama ibu tertanggung' };
    if (state.religionTt === false) return { result: false, message: 'Tentukan agama tertanggung' };
    if (state.statusTt === false) return { result: false, message: 'Tentukan status tertanggung' };
    if (state.dobTt == '') return { result: false, message: 'Lengkapi data tangal lahir' };
    if (state.buktiIdentitasTt == '') return { result: false, message: 'Tentukan bukti identitas' };
    if (state.identitasTt == '') return { result: false, message: 'Lengkapi no identitas' };
    if (state.jobDescTt == '') return { result: false, message: 'Lengkapi uraian pekerjaan' };
    if (state.countryTt === false) return { result: false, message: 'Lengkapi Negara Asal' };
    if (state.alamatrumah_tt === false) return { result: false, message: 'Tentukan opsi alamat rumah' };
    if (state.addressTt == '') return { result: false, message: 'Lengkapi alamat rumah (jika sesuai KTP)' };
    if (state.noHpTt == '') return { result: false, message: 'Lengkapi nomor handphone' };
    if (validateCountCharacter(state.noHpTt, 10, 12) !== true) return { result: false, message: 'Maximum nomor hp 12 digit' };
    if (state.isValidEmail === false) return { result: false, message: 'Format input email tidak valid' };
    if (state.kdPosTt == '') return { result: false, message: 'Lengkapi data kodepos' };
    return { result: true, message: 'Validation Success' };
}

export const submitFormPenerimaManfaat = (state) => {
    if (state.nama == '') return { result: false, message: 'Lengkapi form nama lengkap' };
    if (state.genderAhliWaris === false) return { result: false, message: 'Lengkapi form jenis kelamin' };
    if (state.tptLahir == '') return { result: false, message: 'Lengkapi form kota ' };
    if (state.dobAhliWaris == false) return { result: false, message: 'Lengkapi form tanggal lahir penerima manfaat' };
    if (state.hubunganAhliWaris == '') return { result: false, message: 'Lengkapi form hubungan dengan tertanggung' };
    return { result: true, message: 'Validation Success' };
}

/**
     * 
     * @param {*} data 
     */

export const validateRequired = (data) => {
    if (data === '' || data === undefined || data === null) return false;
    //Valid required condition
    return true;
}

export const getMaxCharBank = (value, obj) => {
    let resp;
    obj.forEach(function (item, index) {
        if (item.id == value) resp = item.maxrek;
    })
    return resp;
}

export const validateCountCharacter = (data, min, max) => {
    // console.log('validateCountCharacter', {data,min,max});
    if (min === undefined) return false;
    if (max === undefined) return false;
    if (data === undefined) return false;
    if (data.length < min) return false;
    if (data.length > max) return false;
    return true;
}

export const formatPhoneNumber = (data) => {
    var arr = data.split("");
    if (arr[0] == '0' && arr[1] == '0') {
        return false;
    }
    if (arr[0] == '0' && arr[1] == '0' && arr[2] == '0') {
        return false;
    }
    if (arr[0] == '0' && arr[1] == '0' && arr[2] == '0' && arr[3] == '0') {
        return false;
    }
    return true;
}