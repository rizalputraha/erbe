/**
 * Validation ProposalPemegangPolis screen
 * @param {Any} state 
 */
export const validationProposalPp = (state) => {
    if (state.usiaPp < 18) return { result: false, message: 'Umur pemegang polis tidak boleh kurang dari 18 tahun' }
    if (state.usiaPp > 80) return { result: false, message: 'Umur pemegang polis tidak boleh lebih dari 80 tahun' }
    if (state.dobPp === null) return { result: false, message: 'Tentukan tanggal lahir pemegang polis' }
    if (state.genderPp === null) return { result: false, message: 'Pilih Jenis Kelamin' }
    return { result: true, message: 'Validation Success' }
}

/**
 * Validation ProposalTertanggung screen
 * @param {Any} state 
 */
export const validationProposalTt = (state) => {
    if (state.namaTt == '') return { result: false, message: 'Nama tertanggung harus dilengkapi' }
    if (state.namaTt.length > 30) return { result: false, message: 'Nama tertanggung tidak boleh melebihi 30 karakter' }
    if (state.usiaTt < 1) return { result: false, message: 'Umur tertanggung minimal 15 hari setelah lahir' }
    if (state.usiaTt > 50) return { result: false, message: 'Umur tertanggung tidak boleh lebih dari 50 tahun' }
    if (state.dobTt === false) return { result: false, message: 'Tentukan tanggal lahir tertanggung' }
    if (state.genderTt === false) return { result: false, message: 'Pilih jenis kelamin' }
    return { result: true, message: 'Validation Success' }
}

/**
 * Validation FormPemegangPolis screen
 * @param {Any} state 
 */
export const validationSpajPp = (state) => {
    if(state.namaIbuPp == '') return { result: false, message: 'Nama ibu kandung harus dilengkapi' }
    if(state.namaIbuPp.length > 30) return { result: false, message: 'Nama ibu kandung tidak boleh melebihi 30 karakter' }
    if(state.tptLahirPp == '') return { result: false, message: 'Kota Kelahiran harus dilengkapi'}
    if(state.agamaPp === false) return { result: false, message: 'Agama harus dilengkapi'}
    if(state.statusPp === false) return { result: false, message: 'Status harus dilengkapi'}
    if(state.identitasPp == '') return { result: false, message: 'No Bukti Identitas harus dilengkapi'}
    if(state.tglBerlakuIdentitasPp === false) return { result: false, message: 'Tanggal berlaku identitas harus dilengkapi'}
    if(state.hubunganPp == '') return { result: false, message: 'Hubungan Dengan Tertanggung harus dilengkapi'}
    if(state.addressPp == '') return { result: false, message: 'Alamat Rumah harus dilengkapi'}
    if(state.propinsiIdPp == '') return { result: false, message: 'Tentukan Propinsi'}
    if(state.kotaIdPp == '') return { result: false, message: 'Tentukan kota'}
    if(state.kecamatanIdPp == '') return { result: false, message: 'Tentukan kecamatan'}
    if(state.kelurahanIdPp == '') return { result: false, message: 'Tentukan kelurahan'}
    if(state.kdPosPp == '') return { result: false, message: 'Kode Pos harus dilengkapi'}
    if(state.nmPerusahaanPp == '') return { result: false, message: 'Nama Lembaga Tempat Bekerja harus dilengkapi'}
    if(state.klasifikasiPekerjaanPp == '') return { result: false, message: 'Pekerjaan harus dilengkapi'}
    if(state.jobDescPp == '') return { result: false, message: 'Uraian Pekerjaan harus dilengkapi'}
    if(state.npwpPp == '') return { result: false, message: 'Tentukan anda mempunyai NPWP atau tidak'}
    if(state.bankIdPp == '') return { result: false, message: 'Bank harus dilengkapi'}
    if(state.branchIdPp == '') return { result: false, message: 'Tentukan cabang bank'}
    if(state.branchIdPp == '') return { result: false, message: 'Pilih cabang bank'}
    if(state.namaRekPp == '') return { result: false, message: 'Nama Rekening harus dilengkapi'}
    if(state.noRekPp == '') return { result: false, message: 'No Rekening harus dilengkapi'}
    return { result: true, message: 'Validation Success'}
}

/**
 * Validation FormTertanggung screen
 * @param {any} state 
 */
export const validationSpajTt = (state) => {
    if(state.namaIbuTt == '') return { result: false, message: 'Nama ibu kandung harus dilengkapi' }
    if(state.namaIbuTt.length > 30) return { result: false, message: 'Nama ibu kandung tidak boleh melebihi 30 karakter' }
    if(state.tptLahirTt == '') return { result: false, message: 'Kota Kelahiran harus dilengkapi'}
    if(state.agamaTt === false) return { result: false, message: 'Agama harus dilengkapi'}
    if(state.statusTt === false) return { result: false, message: 'Status harus dilengkapi'}
    if(state.identitasTt == '') return { result: false, message: 'No Bukti Identitas harus dilengkapi'}
    if(state.tglBerlakuIdentitasTt === false) return { result: false, message: 'Tanggal berlaku identitas harus dilengkapi'}
    if(state.addressTt == '') return { result: false, message: 'Alamat rumah harus di lengkapi'}
    if(state.propinsiIdTt == '') return { result: false, message: 'Tentukan Propinsi'}
    if(state.kotaIdTt == '') return { result: false, message: 'Tentukan kota'}
    if(state.kecamatanIdTt == '') return { result: false, message: 'Tentukan kecamatan'}
    if(state.kelurahanIdTt == '') return { result: false, message: 'Tentukan kelurahan'}
    if(state.kdPosTt == '') return { result: false, message: 'Kode Pos harus dilengkapi'}
    if(state.nmPerusahaanTt == '') return { result: false, message: 'Nama Lembaga Tempat Bekerja harus dilengkapi'}
    if(state.klasifikasiPekerjaanTt == '') return { result: false, message: 'Pekerjaan harus dilengkapi'}
    if(state.jobDescTt == '') return { result: false, message: 'Uraian Pekerjaan harus dilengkapi'}
    if(state.noHpTt == '') return { result: false, message: 'No Handphone harus dilengkapi'}
    if(state.emailTt == '') return { result: false, message: 'Email harus dilengkapi'}
    return { result: true, message: 'Validation Success'}
}

/**
 * Validation FormPenerimaManfaat screen
 * @param {any} state 
 */
export const validationSpajPm = (state) => {
    if(state.nama == '') return { result: false, message: 'Nama harus dilengkapi' }
    if(state.nama.length > 30) return { result: false, message: 'Nama tidak boleh melebihi 30 karakter' }
    if(state.jekel === false) return { result: false, message: 'Jenis Kelamin harus dilengkapi' }
    if(state.kota_lahir == '') return { result: false, message: 'Kota Lahir harus dilengkapi' }
    if(state.ttl == '') return { result: false, message: 'Tanggal Lahir harus dilengkapi' }
    if(state.usia == '') return { result: false, message: 'Usia harus dilengkapi' }
    if(state.hub_dgcalon_tt == '') return { result: false, message: 'Hubungan Dengan Tertanggung harus dilengkapi' }
    return { result: true, message: 'Validation Success'}
}
/**
 * 
 * @param {Any} state from state reducers questioner
 */
export const validationSpajKu = (state) => {
    var res = { result: true, message: 'Validation Success'}
    if(state.length > 0) {
        state.map(item => {
            if(item.isOption === false) {
                item.answerList.map((q, i) => {
                    if(i == 0 && q.answerText == '') res = { result: false, message: 'Tinggi Badan harus dilengkapi' }
                    if(i == 1 && q.answerText == '') res = { result: false, message: 'Berat Badan harus dilengkapi' }
                })
            } else {
                if (item.answerOption.optionYa === true && item.answerOption.optionTidak === false) res = { result: false, message: 'Pertanyaan harus di jawab tidak !' }
            }
        })
    }
    return res;
}

/**
 * 
 * @param {Any} state from state reducers signature
 * @param {String} screen pemegangPolis or tertanggung screen
 */
export const validationSignature = (state, screen) => {
    if(screen == 'pemegangPolis' && state.pemegangPolis == '') return { result: false, message: 'Tanda tangan harus dilengkapi' }
    if(screen == 'pemegangPolis' && state.isAssignedPp === false) return { result: false, message: 'Tanda tangan tidak boleh blank' }
    if(screen == 'tertanggung' && state.tertanggung == '') return { result: false, message: 'Tanda tangan harus dilengkapi' }
    if(screen == 'tertanggung' && state.isAssignedTt === false) return { result: false, message: 'Tanda tangan tidak boleh blank' }
    return { result: true, message: 'Validation Success'}
}

/**
 * 
 * @param {Any} state from state reducers fileAttachment
 */
export const validationFileAttachment = (state) => {
    if(state.pemegangPolis == '') return { result: false, message: 'KTP Pemegang Polis harus dilengkapi !' }
    if(state.tertanggung == '') return { result: false, message: 'KTP Tertanggung harus dilengkapi !' }
    if(state.calonPembayar == '') return { result: false, message: 'KTP Calon Pembayar harus dilengkapi !' }
    return { result: true, message: 'Validation Success'}
}