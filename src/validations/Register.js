import { validateEmail } from './Helper'

/**
 * Validation getreferal on register process
 * @param {referal} String 
 */

export function validationGetReferal(state) {
    if(state.referal === '' || state.referal === undefined || state.referal === true || state.referal === false) {
        return { error: 201, message: 'Referal tidak boleh kosong' }
    }
    return { error: 0, data: state }
}

/**
 * Data validation
 * username, nama, hp, email, alamat, kota, password, confirm_password
 */

export function validationRegister(state) {
    let isValidEmail = false;
    if(state.hp === '' || state.hp === undefined || state.hp === true || state.hp === false) {
        return { error: 201, message: 'No handphone tidak boleh kosong' }
    }
    if(state.email === '' || state.email === undefined || state.email === true || state.email === false) {
        return { error: 201, message: 'Email address tidak boleh kosong' }
    }
    if(state.email) {
        let isValidEmail = validateEmail(state.email);
        if(isValidEmail === false) {
            return { error: 201, message: 'Email address tidak valid' }
        }
    }
    if(state.password === '' || state.password === undefined || state.password === true || state.password === false) {
        return { error: 201, message: 'Password tidak boleh kosong' }
    }
    if(state.confirm_password === '' || state.confirm_password === undefined || state.confirm_password === true || state.confirm_password === false) {
        return { error: 201, message: 'Confirm Password tidak boleh kosong' }
    }
    if(state.password !== state.confirm_password) {
        return { error: 201, message: 'Password dan Confirm Password tidak sama' }
    }
    return { error: 0, data: state }
}