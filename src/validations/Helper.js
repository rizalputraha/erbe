
import moment from 'moment'

export function validateEmail(email) {
    let data = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{1,3})+$/;
    if (data.test(String(email).toLowerCase()) === false) return false;
    return true;
}

export function validateRequired(data) {
    if (data === '' || data === undefined || data === null) return false;
    return true;
}

export function validateCountCharacter(data, min, max) {
    if (min === undefined) return false;
    if (max === undefined) return false;
    if (data === undefined) return false;
    if (data.length < min) return false;
    if (data.length > max) return false;
    return true;
}

export function formatPhoneNumber(data) {
    var arr = data.split("");
    if (arr[0] == '0' && arr[1] == '0') {
        return false;
    }
    if (arr[0] == '0' && arr[1] == '0' && arr[2] == '0') {
        return false;
    }
    if (arr[0] == '0' && arr[1] == '0' && arr[2] == '0' && arr[3] == '0') {
        return false;
    }
    return true;
}

export const remapMobilePhone = (noHp) => {
    var totalArr = noHp.length
    var firstDigit = noHp.substr(0, 1)
    if(firstDigit == '0') {
        console.log('remapMobilePhone', {firstDigit})
        var phoneNumber = noHp.substr(1, totalArr)
        return `62${phoneNumber}`
    }
    console.log('remapMobilePhone', {noHp})
    return noHp
}

export const remapKodePhone = (noHp) => {
    var firs2Digit = noHp.substr(0, 2)
    if(firs2Digit == '62') {
        console.log('remapMobilePhone', {firs2Digit})
        return `0${noHp.substr(2, 2)}`
    } else {
        var first3Digit = noHp.substr(0, 3)
        console.log('remapMobilePhone', {first3Digit})
        return first3Digit
    }
}

export function countRumus(data) {
    let resp = null;
    let maxAge = data.rumus.maxAge;
    let operator = data.rumus.math;
    let param = this.state.umur_tt;
    switch (operator) {
        case '-':
            resp = (maxAge - param)
            break;
        default:
            resp = null
            break;
    }
    return resp;
}

export function formatCurrency(num) {
    if (num !== undefined) {
        let NumberFormat = num.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
        return 'Rp.' + NumberFormat + ',00';
    }
    return 'Rp.0,00';
}

export function getLevelAgen(level) {
    if (level == 0) return 'Partner';
    else if (level == 1) return 'Manager';
    else return 'Director';
}

export function getIconSource(iconName) {
    switch (iconName) {
        case 'jiwa':
            return require('../assets/icon/jiwa.jpg');
            break;
        case 'kendaraan':
            return require('../assets/icon/kendaraan.jpg');
            break;
        case 'perjalanan':
            return require('../assets/icon/perjalanan.jpg');
            break;
        case 'rumah':
            return require('../assets/icon/rumah.jpg');
            break;
        case 'kecelakaan':
            return require('../assets/icon/kecelakaan.jpg');
            break;
        case 'kesehatan':
            return require('../assets/icon/kesehatan.jpg');
            break;
        default:
            return require('../assets/icon/jiwa.jpg');
    }
}

export function getCompanyInsuranceImages(imgName) {
    switch (imgName) {
        case 'msig.png':
            return require('../assets/imgproduct/msig.png')
            break;

        case 'simasjiwa.png':
            return require('../assets/imgproduct/simasjiwa.png')
            break;
        default:
            return require('../assets/imgproduct/erbe.png')
            break;
    }
}

export const setDateFormat = (date) => {
    return moment(date).format('DD MM YYYY, hh:mm:ss');
}