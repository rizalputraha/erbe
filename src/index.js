import React, { Component } from 'react';
import { PersistGate } from 'redux-persist/integration/react';
import configureStore from './redux/store';
import { Provider } from 'react-redux';
import MainApp from './App';
import { persistStore } from 'redux-persist';


const store = configureStore()
let persistor = persistStore(store);

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <MainApp />
                </PersistGate>
            </Provider>
        )
    }
}
