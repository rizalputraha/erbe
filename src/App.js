import React, { Component } from 'react'
import { View, Text, StatusBar, SafeAreaView } from 'react-native'
import FlashMessage from 'react-native-flash-message'
import AsyncStorage from '@react-native-community/async-storage'
import AuthNavigation from 'navigation/AuthNavigation'
import MainNavigation from 'navigation/MainNavigation'
import { setNavigator } from 'erbeservice/AppNavigation'
import { setAuthUser, setDeviceParams, cacheMasterDataMsig, cacheMasterDataSimasJiwa, cacheDownlineMember, showFirstLoading, hideFirstLoading } from 'erberedux/actions'
import deviceReader from 'erberedux/api/DeviceReader'
import { connect } from 'react-redux'
import Style from 'stylescreen/Home'
import Theme from 'style/Theme'
import jwt from 'jwt-decode'
import { cacheProductInsurance, cacheSliderDashboard } from "./cache/model/HomeModel";
import Netinfo from "@react-native-community/netinfo";
import { cameraPermission, phoneStatePermission } from './service/DevicePermission';
import { LoadingScreen } from 'content/screens/base'

class MainApp extends Component {
    constructor(props) {
        super(props)
        this.state = { ...this.state }
    }

    async componentDidMount() {
        await cameraPermission();
        await phoneStatePermission();
        AsyncStorage.getItem('token').then(async authToken => {
            if (authToken) {
                this.props.showFirstLoading()
                const udata = jwt(authToken)
                const authData = { token: JSON.parse(authToken), user_data: JSON.parse(udata.data) }
                await this.props.setAuthUser(authData)
                Netinfo.fetch().then(async (state) => {
                    if (state.isConnected === true) {
                        await cacheProductInsurance(authToken);
                        await cacheSliderDashboard(authToken);
                        await this.props.cacheMasterDataMsig()
                        await this.props.cacheMasterDataSimasJiwa()
                        await this.props.cacheDownlineMember(authToken, { data: {} })
                    } else {
                        this.props.hideFirstLoading()
                    }
                });

            }
        });
        deviceReader().then(params => {
            this.props.setDeviceParams(params)
        })
    }

    renderAuthNavigation() {
        return <AuthNavigation ref={nav => setNavigator(nav)} />
    }

    renderMainNavigation() {
        return <MainNavigation ref={nav => setNavigator(nav)} />
    }

    renderLoading() {
        return <LoadingScreen />
    }

    renderApp() {
        const { authUser, showAppLoading } = this.props;
        return (!showAppLoading ? (authUser ? this.renderMainNavigation() : this.renderAuthNavigation()) : this.renderLoading())
    }

    render() {
        return (
            <SafeAreaView style={Style.container}>
                <StatusBar backgroundColor={Theme.statusBar.backgroundColor} barStyle={Theme.statusBar.barStyle} />
                {this.renderApp()}
                <FlashMessage position="bottom" autoHide={false} />
            </SafeAreaView>
        )
    }
}

const mtp = ({ Auth, ErrorHandling }) => {
    const { authUser } = Auth
    const { errorMessage, showErrorMessage, showAppLoading } = ErrorHandling
    return { authUser, errorMessage, showErrorMessage, showAppLoading }
}

export default connect(mtp, { setAuthUser, setDeviceParams, cacheMasterDataMsig, cacheMasterDataSimasJiwa, cacheDownlineMember, showFirstLoading, hideFirstLoading })(MainApp)