import { PermissionsAndroid } from 'react-native'

export const cameraPermission = async () => {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA, {
            'title': 'Erbe apps need access your camera',
            'message': 'To upload documents KTP you can use your camera'
            }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            __DEV__ && console.log("You can use the camera")
        } else {
            __DEV__ && console.log("Camera permission denied")
        }
    } catch (err) {
        __DEV__ && console.log('Error cameraPermission', {})
    }
}

export const phoneStatePermission = async () => {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE, {
            'title': 'Erbe apps need access your imei',
            'message': 'To crate spaj we need your valid imei as your identity'
            }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            __DEV__ && console.log("You can use the camera")
        } else {
            __DEV__ && console.log("Camera permission denied")
        }
    } catch (err) {
        __DEV__ && console.log('Error cameraPermission', {})
    }
}