import { NavigationActions, StackActions } from 'react-navigation';
import { Share } from 'react-native';

const config = {};

export function setNavigator(nav) {
    if (nav) {
        config.navigator = nav;
    }
}

export function navigate(routeName, params) {
    if (config.navigator && routeName) {
        let action = NavigationActions.navigate({ routeName, params });
        config.navigator.dispatch(action);
    }
}

export function goBack() {
    if (config.navigator) {
        let action = NavigationActions.back({});
        config.navigator.dispatch(action);
    }
}

export function reset(index, routeName, params) {
    if(config.navigator) {
        let actions = NavigationActions.navigate(routeName, params)
        let resetAction = StackActions.reset({ index, actions })
        config.navigator.dispatch(resetAction)
    }
}

export function push(routeName, params) {
    if(config.navigator) {
        let pushAction = StackActions.push({ routeName, params})
        config.navigator.dispatch(pushAction)
    }
}

export function pop(n) {
    if(config.navigator) {
        let popAction = StackActions.pop({ n })
        config.navigator.dispatch(popAction)
    }
}

export function popToTop() {
    if(config.navigator) {
        config.navigator.dispatch(StackActions.popToTop())
    }
}

export function share(content,contentOptions){
    Share.share(content,contentOptions);
}