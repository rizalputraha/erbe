import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'
import AsyncStorage from '@react-native-community/async-storage';
import { MSIG_PERSIST_REDUCER } from '../constants'

export const MsigPersistedConfig = {
    timeout: null,
    key: MSIG_PERSIST_REDUCER,
    storage: AsyncStorage,
    stateReconciler: autoMergeLevel2
}