import AsyncStorage from '@react-native-community/async-storage';
import { ROOT_PERSIST_REDUCER } from '../constants'
export const RootPersistedConfig = {
    timeout: null,
    key: ROOT_PERSIST_REDUCER,
    storage: AsyncStorage,
    whitelist: [
        ''
    ],
    blacklist: [
        ''
    ]
}