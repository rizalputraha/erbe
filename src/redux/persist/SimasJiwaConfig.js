import AsyncStorage from '@react-native-community/async-storage';
import { SIMASJIWA_PERSIST_REDUCER } from '../constants'

export const SimasJiwaPersistedConfig = {
    timeout: null,
    key: SIMASJIWA_PERSIST_REDUCER,
    storage: AsyncStorage,
    blacklist: ['isPartner']
}