import DeviceInfo from 'react-native-device-info';
import Imei from "react-native-imei";

export default readDevice = async () => {
    const imeiDevice = await Imei.getImei()
    const params = {
        applicationName: DeviceInfo.getApplicationName(),
        batteryLevel: await DeviceInfo.getBatteryLevel(),
        brand: DeviceInfo.getBrand(),
        buildNumber: DeviceInfo.getBuildNumber(),
        bundleId: DeviceInfo.getBundleId(),
        carrier: await DeviceInfo.getCarrier(),
        deviceId: DeviceInfo.getDeviceId(),
        deviceName: await DeviceInfo.getDeviceName(),
        macAddr: await DeviceInfo.getMacAddress(),
        manufacturer: await DeviceInfo.getManufacturer(),
        model: DeviceInfo.getModel(),
        phoneNumber: await DeviceInfo.getPhoneNumber(),
        buildId: await DeviceInfo.getBuildId(),
        uniqueId: DeviceInfo.getUniqueId(),
        userAgent: await DeviceInfo.getUserAgent(),
        version: DeviceInfo.getVersion(),
        isTablet: DeviceInfo.isTablet(),
        isLandscape: await DeviceInfo.isLandscape(),
        deviceType: DeviceInfo.getDeviceType(),
        isLocationEnable: await DeviceInfo.isLocationEnabled(),
        availLocationProvs: await DeviceInfo.getAvailableLocationProviders(),
        systemAvailFeature: await DeviceInfo.getSystemAvailableFeatures(),
        systemName: DeviceInfo.getSystemName(),
        systemVersion: DeviceInfo.getSystemVersion(),
        readableVersion: DeviceInfo.getReadableVersion(),
        imei: imeiDevice[0]
    }
    return params;
}