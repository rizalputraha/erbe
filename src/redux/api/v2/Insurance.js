import req from '../Caller';
import { BASE_URL } from 'erberedux/constants';
import AsyncStorage from '@react-native-community/async-storage'

const removeToken = async () => {
    await AsyncStorage.removeItem('token');
}

const reqProductInsurance = async (token) => {
    let resp = await req.appGet(`${BASE_URL}v2/insurance/product_asuransi?data=`, token);
    return resp;
}

/**
 * Upload file attachment KTP pemegangPolis, tertanggung, calonPembayar
 * @param {String} imgBase64 Base64 images from cache files
 * @param {String} token token from props Auth
 */
const apiFileAttachmentPp = async (imgBase64, token) => {
    const data = { data: { file: imgBase64 } }
    const url = `${BASE_URL}v2/insurance/upload_ktp_pp`
    let resp = await req.appPost(url, data, token);
    return resp;
}

const apiFileAttachmentTt = async (imgBase64, token) => {
    const data = { data: { file: imgBase64 } }
    const url = `${BASE_URL}v2/insurance/upload_ktp_tt`
    let resp = await req.appPost(url, data, token);
    return resp;
}

const apiFileAttachmentCp = async (imgBase64, token) => {
    const data = { data: { file: imgBase64 } }
    const url = `${BASE_URL}v2/insurance/upload_ktp_cp`
    let resp = await req.appPost(url, data, token);
    return resp;
}

/**
 * Upload signature pemegangPolis and tertanggung
 * @param {String} imgBase64 Base64 images from cache files
 * @param {String} token token from props Auth
 */
const apiSignaturePp = async (imgBase64, token) => {
    const data = { data: { file: imgBase64 } }
    const url = `${BASE_URL}v2/insurance/upload_assign_pp`
    let resp = await req.appPost(url, data, token);
    return resp;
}

const apiSignatureTt = async (imgBase64, token) => {
    const data = { data: { file: imgBase64 } }
    const url = `${BASE_URL}v2/insurance/upload_assign_tt`
    let resp = await req.appPost(url, data, token);
    return resp;
}

/**
 * Upload screenshoot pemegangPolis, tertanggung, calonPembayar, etc
 * Only use for asuransi jiwa Msig
 ** @param {String} imgBase64 Base64 images from cache files
 * @param {String} token token from props Auth
 */
const apiScreenShootPp = async (imgBase64, token) => {
    const data = { data: { file: imgBase64 } }
    const url = `${BASE_URL}v2/insurance/upload_ss_pp`
    let resp = await req.appPost(url, data, token);
    return resp;
}

//ScreenShoot Tertanggung
const apiScreenShootTt = async (imgBase64, token) => {
    const data = { data: { file: imgBase64 } }
    const url = `${BASE_URL}v2/insurance/upload_ss_tt`
    let resp = await req.appPost(url, data, token);
    return resp;
}

//ScreenShoot Calon Pembayar
const apiScreenShootCp = async (imgBase64, token) => {
    const data = { data: { file: imgBase64 } }
    const url = `${BASE_URL}v2/insurance/upload_ss_cp`
    let resp = await req.appPost(url, data, token);
    return resp;
}

//ScreenShoot Usulan Asuransi
const apiScreenShootUa = async (imgBase64, token) => {
    const data = { data: { file: imgBase64 } }
    const url = `${BASE_URL}v2/insurance/upload_ss_ua`
    let resp = await req.appPost(url, data, token);
    return resp;
}

//ScreenShoot Investasi
const apiScreenShootDi = async (imgBase64, token) => {
    const data = { data: { file: imgBase64 } }
    const url = `${BASE_URL}v2/insurance/upload_ss_di`
    let resp = await req.appPost(url, data, token);
    return resp;
}

//ScreenShoot Detil Agen
const apiScreenShootDa = async (imgBase64, token) => {
    const data = { data: { file: imgBase64 } }
    const url = `${BASE_URL}v2/insurance/upload_ss_da`
    let resp = await req.appPost(url, data, token);
    return resp;
}

//ScreenShoot Quisioner
const apiScreenShootQu = async (imgBase64, token) => {
    const data = { data: { file: imgBase64 } }
    const url = `${BASE_URL}v2/insurance/upload_ss_qu`
    let resp = await req.appPost(url, data, token);
    return resp;
}

//ScreenShoot Profile Resiko
const apiScreenShootPr = async (imgBase64, token) => {
    const data = { data: { file: imgBase64 } }
    const url = `${BASE_URL}v2/insurance/upload_ss_pr`
    let resp = await req.appPost(url, data, token);
    return resp;
}

//ScreenShoot Persetujuan Nasabah
const apiScreenShootPn = async (imgBase64, token) => {
    const data = { data: { file: imgBase64 } }
    const url = `${BASE_URL}v2/insurance/upload_ss_pn`
    let resp = await req.appPost(url, data, token);
    return resp;
}

//ScreenShoot Signature Pemegang Polis
const apiScreenShootSp = async (imgBase64, token) => {
    const data = { data: { file: imgBase64 } }
    const url = `${BASE_URL}v2/insurance/upload_ss_sp`
    let resp = await req.appPost(url, data, token);
    return resp;
}

//ScreenShoot Signature Tertanggung
const apiScreenShootSt = async (imgBase64, token) => {
    const data = { data: { file: imgBase64 } }
    const url = `${BASE_URL}v2/insurance/upload_ss_st`
    let resp = await req.appPost(url, data, token);
    return resp;
}

//ScreenShoot Signature File Document
const apiScreenShootFd = async (imgBase64, token) => {
    const data = { data: { file: imgBase64 } }
    const url = `${BASE_URL}v2/insurance/upload_ss_fd`
    let resp = await req.appPost(url, data, token);
    return resp;
}

//Create Proposal Api
const apiCreateProposal = async (state, token) => {
    const data = { data: { ...state } }
    const url = `${BASE_URL}v2/insurance/create_proposal`
    let resp = await req.appPost(url, data, token)
    return resp
}

//Create SPAJ Api
const apiCreateSpaj = async (state, token) => {
    const data = { data: { ...state } }
    const url = `${BASE_URL}v2/insurance/create_spaj`
    let resp = await req.appPost(url, data, token)
    return resp
}

export default {
    reqProductInsurance,
    apiFileAttachmentPp,
    apiFileAttachmentTt,
    apiFileAttachmentCp,
    apiSignaturePp,
    apiSignatureTt,
    apiScreenShootPp,
    apiScreenShootTt,
    apiScreenShootCp,
    apiScreenShootUa,
    apiScreenShootDi,
    apiScreenShootDa,
    apiScreenShootQu,
    apiScreenShootPr,
    apiScreenShootPn,
    apiScreenShootSp,
    apiScreenShootSt,
    apiScreenShootFd,
    apiCreateProposal,
    apiCreateSpaj
}