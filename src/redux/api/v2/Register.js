import req from '../Caller';
import { BASE_URL } from 'erberedux/constants';

const referalReq = async (referal) => {
    __DEV__ && console.log('getReferalRequest payload', referal);
    let resp = await req.appGet(`${BASE_URL}v2/auth/referal?id=${referal}`);
    __DEV__ && console.log('getReferalRequest response', resp);
    /**
     * Resp format:
     * {
     *  {"error":0,"data":{"upline_id":"1","upline_user":"erbe","upline_nama":"YEREMIA KURNIAWAN","reff_id":"123098"}}
     * }
     */
    return resp
}

const registerReq = async (data) => {
    /**
     * Payload send to server:
     * {upline, user}
     */
    let resp = await req.appPost(`${BASE_URL}v2/auth/register`, data)
    return resp
}

const verifyReq = async (data) => {
    let resp = await req.appPost(`${BASE_URL}v2/auth/verify`, data)
    return resp
}

export default { referalReq, registerReq, verifyReq }