import req from '../Caller';
import { BASE_URL } from 'erberedux/constants';
import jwt from "jwt-decode";

const accountRequest = async (account) => {
    let resp = await req.appPost(`${BASE_URL}v2/auth/account`, {account_login: account.account_login});
    return resp;
}

const signInRequest = async (accountId, loginId, pwd) => {
    let resp = await req.appPost(`${BASE_URL}v2/auth/signin`, { account_id: accountId, login_id: loginId, password: pwd });
    return resp;
}

const signinVerifyRequest = async (accountId, loginId, pwd, kodeVerify) => {
    let resp = await req.appPost(`${BASE_URL}v2/auth/signin_verify`, { account_id: accountId, login_id: loginId, password: pwd, kode_verifikasi: kodeVerify });
    return resp;
}

const singOutRequest = async (token) => {
    let resp = await req.appPost(`${BASE_URL}v2/auth/signout`, {data: {}}, token);
    return resp;
}

const forgotRequest = async (payload) => {
    let resp = await req.appPost(`${BASE_URL}v2/auth/forgot_password`, payload);
    __DEV__ && console.log('forgotRequest api v2 resp', resp);
    return resp;
}

const verfifyForgotRequest = async (accountId, kodeVerifikasi, password = false) => {
    let data = {}
    data.id = accountId
    data.kode_verifikasi = kodeVerifikasi

    if(password !== false) {
        data.password = password
    }

    let resp = await req.appPost(`${BASE_URL}v2/auth/verify_forgot_password`, {...data})
    return resp;
}

export default { accountRequest, signInRequest, signinVerifyRequest, singOutRequest, forgotRequest, verfifyForgotRequest };
