import req from '../Caller'
import { BASE_URL } from 'erberedux/constants'

const sliderReqv2 = async (token) => {
    let resp = await req.appGet(`${BASE_URL}v2/dashboard/slider?data=`,token)
    return resp
}

const referalReqV2 = async (token) => {
    let resp = await req.appGet(`${BASE_URL}v1/dashboard/referal?data=`, token)
    return resp
}

export default { sliderReqv2, referalReqV2 }