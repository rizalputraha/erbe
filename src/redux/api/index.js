import Auth from './v2/Auth'
import Report from './v1/Report'
import Register from './v2/Register'
import Dashboard from './v1/Dashboard'
import Dashboardv2 from './v2/Dashboardv2'
import Invoice from './v1/Invoice'
import Insurance from './v2/Insurance'
import Supplier from './v1/Supplier'
module.exports = {
    Auth,
    Report,
    Register,
    Dashboard,
    Dashboardv2,
    Invoice,
    Insurance,
    Supplier
};