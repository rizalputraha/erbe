import req from '../Caller';
import { BASE_URL } from 'erberedux/constants';

const downlineByUserId = async (token,data) => {
    __DEV__ && console.log('memberDownline payload', data);
    let resp = await req.appPost(`${BASE_URL}v1/member/downline`, data,token);
    __DEV__ && console.log('memberDownline response', resp);
    /**
     * Resp format:
     * {
     *  error: 0,
     *  data: [   
     *      id: String,
     *      username: Object
     *      nama: String
     *      no_telp: String
     *      downline: String
     *      kota: String
     *      email: String
     *      level: String
     *      sponsor: String
     *  ]
     * }
     */
    return resp;
}

const downlineByUsername = async (token,data) => {
    __DEV__ && console.log('memberDownline payload', data);
    let resp = await req.appPost(`${BASE_URL}v1/member/downline_by_uname`, data,token);
    __DEV__ && console.log('memberDownline response', resp);
    /**
     * Resp format:
     * {
     *  error: 0,
     *  data: {   
     *      token: String,
     *      user_data: Object
     *  }
     * }
     */
    return resp;
}

const getKomisi = async (token) => {
    __DEV__ && console.log('memberDownline payload', token);
    let resp = await req.appGet(`${BASE_URL}v1/report/komisi_bonus?data=`,token);
    __DEV__ && console.log('memberDownline response', resp);
    /**
     * Resp format:
     * {
     *  error: 0,
     *  data: {   
     *      token: String,
     *      user_data: Object
     *  }
     * }
     */
    return resp;
}

export default { getKomisi,downlineByUserId, downlineByUsername };
