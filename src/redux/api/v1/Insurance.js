import req from '../Caller'
import { BASE_URL } from 'erberedux/constants'

const categoryReq = async (token) => {
    let resp = await req.appGet(`${BASE_URL}v1/insurance/kategori?data=`, token)
    return resp
}

const insuranceCompanyReq = async (data, token) => {
    let resp = await req.appPost(`${BASE_URL}v1/insurance/asuransi`, {data}, token)
    return resp
}

const getProfilePP = async (data,token) => {
    let resp = await req.appGet(`${BASE_URL}v1/insurance/profile_pp?data=`+{data}, token)
    return resp;
}

const insuranceProductReq = async (data,token) => {
    let resp = await req.appPost(`${BASE_URL}v1/insurance/product`, {data}, token)
    return resp
}

const insurancePeriodeBayarReq = async (data,token) => {
    let resp = await req.appPost(`${BASE_URL}v1/insurance/periode_bayar`, {data}, token)
    return resp
}

const productPricingReq = async (data,token) => {
    let resp = await req.appPost(`${BASE_URL}v1/insurance/product_pricing`, {data}, token)
    return resp
}

const productLinkReq = async (data,token) => {
    let resp = await req.appPost(`${BASE_URL}v1/insurance/product_link`, {data}, token)
    return resp
}

const createProposalReq = async (data,token) => {
    let resp = await req.appPost(`${BASE_URL}v1/insurance/create_proposal`, {data}, token)
    return resp
}

const viewPdfProposalReq = async (data,token) => {
    let resp = await req.appGet(`${BASE_URL}v1/insurance/view_pdf_proposal?data=`+data, token)
    return resp
}



export default { categoryReq, insuranceCompanyReq, insuranceProductReq,insurancePeriodeBayarReq}