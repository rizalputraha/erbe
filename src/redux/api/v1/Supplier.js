import req from '../Caller';
import { SUPPLIER_URL } from 'erberedux/constants'

//Asuransi Jiwa MSIG
const reqBankMsig = async () => await req.appGet(`${SUPPLIER_URL}v1/msig/bank`)
const reqCountryMsig = async () => await req.appGet(`${SUPPLIER_URL}v1/msig/country`)
const reqGenderMsig = async () => await req.appGet(`${SUPPLIER_URL}v1/msig/gender`)
const reqGraduateMsig = async () => await req.appGet(`${SUPPLIER_URL}v1/msig/graduate`)
const reqGreenCardMsig = async () => await req.appGet(`${SUPPLIER_URL}v1/msig/greenCard`)
const reqIdentityMsig = async () => await req.appGet(`${SUPPLIER_URL}v1/msig/identity`)
const reqMarriageStatusMsig = async () => await req.appGet(`${SUPPLIER_URL}v1/msig/marriageStatus`)
const reqReligionMsig = async () => await req.appGet(`${SUPPLIER_URL}v1/msig/religion`)
const reqPekerjaanMsig = async () => await req.appGet(`${SUPPLIER_URL}v1/msig/pekerjaan`)
const reqPendapatanMsig = async () => await req.appGet(`${SUPPLIER_URL}v1/msig/pendapatan`)
const reqRelationMsig = async () => await req.appGet(`${SUPPLIER_URL}v1/msig/relation`)
const reqRelationAhliWarisMsig = async () => await req.appGet(`${SUPPLIER_URL}v1/msig/relationAhliWaris`)
const reqRelationPembayarMsig = async () => await req.appGet(`${SUPPLIER_URL}v1/msig/relationPembayar`)
const reqSumberDanaMsig = async () => await req.appGet(`${SUPPLIER_URL}v1/msig/sumberDana`)
const reqSumberPendapatanBulanMsig = async () => await req.appGet(`${SUPPLIER_URL}v1/msig/sumberPendapatanBulan`)
const reqSumberPendapatanTahunMsig = async () => await req.appGet(`${SUPPLIER_URL}v1/msig/sumberPendapatanTahun`)
const reqTujuanMsig = async () => await req.appGet(`${SUPPLIER_URL}v1/msig/tujuan`)
const reqTitleMsig = async () => await req.appGet(`${SUPPLIER_URL}v1/msig/title`)
const reqQuestionerMsig = async () => await req.appGet(`${SUPPLIER_URL}v1/msig/questioner`)
const reqProfileResikoMsig = async () => await req.appGet(`${SUPPLIER_URL}v1/msig/profileResiko`)


//Asuransi Jiwa Simasjiwa
const reqBankSimasJiwa = async () => await req.appGet(`${SUPPLIER_URL}v1/simasjiwa/allBank`)
const reqWilayahSimasJiwa = async () => await req.appGet(`${SUPPLIER_URL}v1/simasjiwa/wilayah`)
const reqIdentitySimasJiwa = async () => await req.appGet(`${SUPPLIER_URL}v1/simasjiwa/identity`)
const reqGenderSimasJiwa = async () => await req.appGet(`${SUPPLIER_URL}v1/simasjiwa/gender`)
const reqMarriageStatusSimasJiwa = async () => await req.appGet(`${SUPPLIER_URL}v1/simasjiwa/marriageStatus`)
const reqReligionSimasJiwa = async () => await req.appGet(`${SUPPLIER_URL}v1/simasjiwa/religion`)
const reqGraduateSimasJiwa = async () => await req.appGet(`${SUPPLIER_URL}v1/simasjiwa/graduate`)
const reqQuestionerSimasJiwa = async () => await req.appGet(`${SUPPLIER_URL}v1/simasjiwa/questioner`)
const reqCountrySimasJiwa = async () => await req.appGet(`${SUPPLIER_URL}v1/simasjiwa/country`)
const reqRelationSimasJiwa = async () => await req.appGet(`${SUPPLIER_URL}v1/simasjiwa/relation`)

export default {
    reqBankMsig,
    reqCountryMsig,
    reqGenderMsig,
    reqGraduateMsig,
    reqGreenCardMsig,
    reqIdentityMsig,
    reqMarriageStatusMsig,
    reqReligionMsig,
    reqPekerjaanMsig,
    reqPendapatanMsig,
    reqRelationMsig,
    reqRelationAhliWarisMsig,
    reqRelationPembayarMsig,
    reqSumberDanaMsig,
    reqSumberPendapatanBulanMsig,
    reqSumberPendapatanTahunMsig,
    reqTujuanMsig,
    reqTitleMsig,
    reqQuestionerMsig,
    reqProfileResikoMsig,
    reqBankSimasJiwa,
    reqWilayahSimasJiwa,
    reqIdentitySimasJiwa,
    reqGenderSimasJiwa,
    reqMarriageStatusSimasJiwa,
    reqReligionSimasJiwa,
    reqGraduateSimasJiwa,
    reqQuestionerSimasJiwa,
    reqCountrySimasJiwa,
    reqRelationSimasJiwa
}