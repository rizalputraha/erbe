import req from '../Caller';
import { BASE_URL } from 'erberedux/constants';

const getTagihan = async (token) => {
    __DEV__ && console.log('invoiceTagihan payload', token);
    let resp = await req.appGet(`${BASE_URL}v1/invoice/tagihan?data=`,token);
    __DEV__ && console.log('invoiceTagihan response', resp);
    /**
     * Resp format:
     * {
     *  error: 0,
     *  data: [   
     *      id: String,
     *      username: Object
     *      nama: String
     *      no_telp: String
     *      downline: String
     *      kota: String
     *      email: String
     *      level: String
     *      sponsor: String
     *  ]
     * }
     */
    return resp;
}

export default { getTagihan };
