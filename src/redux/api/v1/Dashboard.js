import req from '../Caller'
import { BASE_URL } from 'erberedux/constants'

const sliderReq = async (token) => {
    let resp = await req.appGet(`${BASE_URL}v1/dashboard/slider?data=`, token)
    return resp
}

const referalReq = async (token) => {
    let resp = await req.appGet(`${BASE_URL}v1/dashboard/referal?data=`, token)
    return resp
}

export default { sliderReq, referalReq }