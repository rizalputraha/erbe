import req from '../Caller';
import { BASE_URL } from 'erberedux/constants';
import jwt from "jwt-decode";

const signInRequest = async (user, password) => {
    __DEV__ && console.log('signInRequest payload', user, password);
    let resp = await req.appPost(`${BASE_URL}v1/auth/login`, { username: user, password: password });
    /**
     * Resp format:
     * {
     *  error: 0,
     *  data: {
     *      token: String,
     *      user_data: Object
     *  }
     * }
     */
    return resp;
}

const singOutRequest = async (token) => {
    // let resp = await req.appPost(`${BASE_URL}v1/auth/signout`, {}, token);
    __DEV__ && console.log('singOutRequest', token);
    
    let resp = {
        error: 0,
        data: []
    }    
    return resp;
}

export default { signInRequest, singOutRequest };
