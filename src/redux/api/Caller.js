import DeviceInfo from 'react-native-device-info';
let DeviceId = DeviceInfo.getDeviceId()
const appPost = async (url, data, token = '', header = {}) => {
    console.log('url', url);
    console.log('data', data);
    console.log('token', token);
    
    let resp = await fetch(url, {
        body: JSON.stringify(data),
        method: 'POST',
        headers: {
            ...header,
            "Auth-Token": token,
            "content-type": 'application/json',
            'User-Agent': 'Erbe.app',
            'Develop-By': 'PT. Generasi Baru Internasional',
            'DeviceId': DeviceId
        }
    })
    console.log('data resp di api', resp);
    return await resp.json()
}

const appGet = async (url, token = '', header = {}) => {
    let resp = await fetch(url, {
        method: 'GET',
        headers: {
            ...header,
            "Auth-Token": token,
            "content-type": 'application/json',
            'User-Agent': 'Erbe.app',
            'Develop-By': 'PT. Generasi Baru Internasional',
            'DeviceId': DeviceId
        }
    });

    return await resp.json()
}

const appDownload = async (url, token = '', header = {}) => {
    let resp = await fetch(url, {
        method: 'GET',
        headers: {
            ...header,
            "Auth-Token": token,
            "content-type": 'application/json',
            'User-Agent': 'Erbe.app',
            'Develop-By': 'PT. Generasi Baru Internasional',
            'DeviceId': DeviceId
        }
    })
    console.log(resp.headers.get('content-type'));
    if (resp.headers.get('content-type') === 'application/json') return resp.json();
    let blob = await resp.blob();
    var aurl = window.URL.createObjectURL(blob);
    var a = document.createElement('a');
    a.href = aurl;
    a.download = resp.headers.get('content-filename');
    document.body.appendChild(a);
    a.click();
    a.remove();
    return { error: 0, message: 'Success Download' }
}

const appUpload = async (url, data, token = '', header = {}) => {
    console.log(url, data, token);

    let resp = await fetch(url, {
        method: 'POST',
        body: data,
        headers: {
            ...header,
            "Auth-Token": token,
            'User-Agent': 'Erbe.app',
            'Develop-By': 'PT. Generasi Baru Internasional',
            'DeviceId': DeviceId
        }
    })
    return resp.json()
}

export default { appGet, appPost, appDownload, appUpload }