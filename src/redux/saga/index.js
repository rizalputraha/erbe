import { all } from 'redux-saga/effects'
import report from "./Report";
import auth from './Auth';
import register from './Register';
import invoice from './Invoice';
import dashboard from './Dashboard';
import insurance from './Insurance';
import navigation from './Navigation';
import supplier from './Supplier';
import msig from './Msig';
import simasjiwa from './Simasjiwa';

export default function* () {
    yield all([
        auth(),
        register(),
        report(),
        invoice(),
        dashboard(),
        insurance(),
        navigation(),
        supplier(),
        msig(),
        simasjiwa()
    ]);
}