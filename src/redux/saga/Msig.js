/**
 * Saga Msig 
 * Use to side effect trough moving screen route navigation and async with api submit data and uploading files
 * version 19.01.25
 */
import { all, call, fork, put, delay, takeEvery, select, cancel, cancelled } from 'redux-saga/effects'
import { Insurance } from 'erberedux/api'

import {
    SAGA_PROPOSAL_CATEGORY_MSIG,
    SAGA_NEXT_SCREEN_MSIG,
    SAGA_SUBMIT_FORM_MSIG,
    SAGA_SAVE_SUBMIT_FORM_MSIG,
    STORAGE_SPAJ_MSIG
} from 'erberedux/constants'
import {
    saveCacheMsig,
    loadCacheMsig,
    removeCacheMsig
} from 'erbecache/model/MsigModel'

import { writeFile, loadFile } from 'erbecache/model/FileModel'

import {
    showButtonLoading,
    hideButtonLoading,
    showLoadingBtnNasabahMsig,
    hideLoadingBtnNasabahMsig,
    showLoadingBtnAgenMsig,
    hideLoadingBtnAgenMsig,
    showLoadingUploadDocumentMsig,
    hideLoadingUploadDocumentMsig,
    showLoadingGenerateProposalMsig,
    hideLoadingGenerateProposalMsig,
    showLoadingSubmitSpajMsig,
    hideLoadingSubmitSpajMsig,
    showErrorMessage,
    pushToScreen,
    navigatePopToRoot,
    setStateFromCacheMsig,
    setStatePartnerMsig,
    setStateProfileFromAuthUserMsig,
    setStateProposalIsTertanggungMsig,
    setStateSpajSignatureKeyMsig,
    setStateScreenshootMsig,
    setStateSpajIsTertanggungMsig,
    setStateSpajTertanggungMsig,
    setStateSpajCalonPembayarMsig,
    setStateUriResponseMsig,
    resetCacheStateMsig
} from 'erberedux/actions'

import {
    validationProposalPp,
    validationProposalTt,
    validationSpajPp,
    validationSpajTt,
    validationSpajPm,
    validationSpajKu,
    validationSignature,
    validationFileAttachment
} from 'erbevalidation/Msig'
import { setStateRootMsig } from '../actions'

import {
    MSIG_SS_SCREEN_PP,
    MSIG_SS_SCREEN_TT,
    MSIG_SS_SCREEN_CP,
    MSIG_SS_SCREEN_UA,
    MSIG_SS_SCREEN_DI,
    MSIG_SS_SCREEN_DA,
    MSIG_SS_SCREEN_QU,
    MSIG_SS_SCREEN_PR,
    MSIG_SS_SCREEN_PN,
    MSIG_SS_SCREEN_SP,
    MSIG_SS_SCREEN_ST,
    MSIG_SS_SCREEN_FD
}
from 'erbecache/constant/MasterFile'
/**
 * @param {String} buttonType 
 */
function* nextProposalCategory({payload}) {
    const { buttonType } = payload
    if(buttonType == 'nasabah') {
        yield put(showLoadingBtnNasabahMsig())
        try {
            const cacheForm = yield call(loadCacheMsig, STORAGE_SPAJ_MSIG)
            console.log('cacheForm Nasabah', cacheForm)
            if(cacheForm !== null) {
                const nasabah = cacheForm.filter(item => item.isPartner === false)
                if(nasabah.length !== parseInt(0)) {
                    yield put(pushToScreen('MsigCache', { type: 'nasabah', data: nasabah }))
                } else {
                    yield put(pushToScreen('MsigStarter'))
                }
            } else {
                yield put(pushToScreen('MsigStarter'))
            }
        } catch (error) {
            yield put(showErrorMessage(error.message))
        }
        yield delay(300)
        yield put(hideLoadingBtnNasabahMsig())
    } else if (buttonType == 'agen') {
        yield put(showLoadingBtnAgenMsig())
        try {
            const cacheForm = yield call(loadCacheMsig, STORAGE_SPAJ_MSIG)
            if(cacheForm !== null) {
                const agen = cacheForm.filter(item => item.isPartner === true)
                console.log('cache data Agen', agen)
                if(agen.length !== parseInt(0)) {
                    yield put(pushToScreen('MsigCache', { type: 'agen', data: agen}))
                } else {
                    yield put(pushToScreen('MsigDownline'))
                }
            } else {
                yield put(pushToScreen('MsigDownline'))
            }
        } catch (error) {
            yield put(showErrorMessage(error.message))
        }
        yield delay(300)
        yield put(hideLoadingBtnAgenMsig())
    } else {
        yield put(showErrorMessage('Type button undefined'))
    }
    
}

function* nextScreen({payload}) {
    const {prevRouteName, nextRouteName, state, authUser, device, capture } = payload
    const captureBase64 = `data:image/jpg;base64, ${capture}`
    const {isTertanggung, screenShoot} = yield select(state => state.Msig)

    switch (prevRouteName) {
        case 'MsigCache':
            yield put(showButtonLoading())
            try {
                yield put(setStateFromCacheMsig(state))
                yield put(pushToScreen(nextRouteName))
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(100)
            yield put(hideButtonLoading())
        break;

        case 'MsigDownline':
            yield put(showButtonLoading())
            try {
                yield put(setStatePartnerMsig(state))
                yield put(pushToScreen(nextRouteName))
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(100)
            yield put(hideButtonLoading())
        break;

        case 'MsigStarter':
            yield put(showButtonLoading())
            try {
                yield put(pushToScreen(nextRouteName))
                const {isPartner} = yield select(state => state.Msig)
                if(isPartner === false) {
                    yield put(setStateProfileFromAuthUserMsig(authUser))
                }
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(300)
            yield put(hideButtonLoading())
        break;
        
        case 'MsigProposalPp':
            yield put(showButtonLoading())
            const valStateProposalPp = yield call(validationProposalPp, state)
            try {
                if(valStateProposalPp.result === false) {
                    yield put(showErrorMessage(valStateProposalPp.message))
                } else {
                    if(nextRouteName == 'MsigProduk') {
                        yield put(setStateProposalIsTertanggungMsig(state))
                    }
                    yield put(pushToScreen(nextRouteName))
                }
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(300)
            yield put(hideButtonLoading())
        break;

        case 'MsigProposalTt':
            yield put(showButtonLoading())
            try {
                const valStateProposalTt = yield call(validationProposalTt, state)
                if(valStateProposalTt.result === false) {
                    yield put(showErrorMessage(valStateProposalTt.message))
                } else {
                    yield put(pushToScreen(nextRouteName))
                }
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(300)
            yield put(hideButtonLoading())
        break;

        case 'MsigProduk':
            yield put(showButtonLoading())
            try {
                yield put(pushToScreen(nextRouteName))
                yield put(setStateSpajSignatureKeyMsig(device.imei, device.uniqueId))
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(300)
            yield put(hideButtonLoading())
        break;

        case 'MsigPemegangPolis':
            yield put(showButtonLoading())
            try {
                const valStatePemegangPolis = yield call(validationSpajPp, state)
                if(valStatePemegangPolis.result === false) {
                    yield put(showErrorMessage(valStatePemegangPolis.message))
                } else {
                    //If Is Tertanggung True, get state and conditions
                    if(isTertanggung === true) {
                        //Set Screenshoot tertanggung from pemegang polis
                        const ssTt = yield call(writeFile, screenShoot.screenTt, MSIG_SS_SCREEN_TT, captureBase64)
                        yield put(setStateScreenshootMsig('screenTt', ssTt))
                        //Set State tertanggung from pemegang polis
                        yield put(setStateSpajIsTertanggungMsig(state))
                    }
                    //Set Screenshoot pemegangPolis
                    const ssPp = yield call(writeFile, screenShoot.screenPp, MSIG_SS_SCREEN_PP, captureBase64)
                    yield put(setStateScreenshootMsig('screenPp', ssPp))

                    yield put(setStateSpajTertanggungMsig(state))
                    yield put(setStateSpajCalonPembayarMsig(state))
                    yield put(pushToScreen(nextRouteName))
                }
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(300)
            yield put(hideButtonLoading())
        break;

        case 'MsigTertanggung':
            yield put(showButtonLoading())
            try {
                const valStateTertanggung = yield call(validationSpajTt, state)
                if(valStateTertanggung.result === false) {
                    yield put(showErrorMessage(valStateTertanggung.message))
                } else {
                    //Set Screenshoot tertanggung
                    const ssTt = yield call(writeFile, screenShoot.screenTt, MSIG_SS_SCREEN_TT, captureBase64)
                    yield put(setStateScreenshootMsig('screenTt', ssTt))

                    yield put(pushToScreen(nextRouteName))
                }
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(300)
            yield put(hideButtonLoading())
        break;

        case 'MsigCalonPembayar':
            yield put(showButtonLoading())
            try {
                const ssCp = yield call(writeFile, screenShoot.screenCp, MSIG_SS_SCREEN_CP, captureBase64)
                yield put(setStateScreenshootMsig('screenCp', ssCp))
                yield put(pushToScreen(nextRouteName))
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(300)
            yield put(hideButtonLoading())
        break;

        case 'MsigUsulanAsuransi':
            yield put(showButtonLoading())
            try {
                const ssUa = yield call(writeFile, screenShoot.screenUa, MSIG_SS_SCREEN_UA, captureBase64)
                yield put(setStateScreenshootMsig('screenUa', ssUa))
                yield put(pushToScreen(nextRouteName))
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(300)
            yield put(hideButtonLoading())
        break;

        case 'MsigPenerimaManfaat':
            yield put(showButtonLoading())
            try {
                const valStatePm = yield call(validationSpajPm, state)
                if(valStatePm.result === false) {
                    yield put(showErrorMessage(valStatePm.message))
                } else {
                    const ssDi = yield call(writeFile, screenShoot.screenDi, MSIG_SS_SCREEN_DI, captureBase64)
                    yield put(setStateScreenshootMsig('screenDi', ssDi))
                    yield put(pushToScreen(nextRouteName))
                }
            } catch (error) {
                yield put(showErrorMessage(error.message))                
            }
            yield delay(300)
            yield put(hideButtonLoading())
        break;

        case 'MsigDetilAgen':
            yield put(showButtonLoading())
            try {
                const ssDa = yield call(writeFile, screenShoot.screenDa, MSIG_SS_SCREEN_DA, captureBase64)
                yield put(setStateScreenshootMsig('screenDa', ssDa))
                yield put(pushToScreen(nextRouteName))
            } catch (error) {
                yield put(showErrorMessage(error.message))                
            }
            yield delay(300)
            yield put(hideButtonLoading())
        break;

        case 'MsigKuisioner':
            yield put(showButtonLoading())
            try {
                const valStateKu = yield call(validationSpajKu, state)
                if(valStateKu.result === false) {
                    yield put(showErrorMessage(valStateKu.message))
                } else {
                    const ssQu = yield call(writeFile, screenShoot.screenQu, MSIG_SS_SCREEN_QU, captureBase64)
                    yield put(setStateScreenshootMsig('screenQu', ssQu))
                    yield put(pushToScreen(nextRouteName))
                }
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(300)
            yield put(hideButtonLoading())
        break;

        case 'MsigProfileResiko':
            yield put(showButtonLoading())
            try {
                const ssPr = yield call(writeFile, screenShoot.screenPr, MSIG_SS_SCREEN_PR, captureBase64)
                yield put(setStateScreenshootMsig('screenPr', ssPr))
                yield put(pushToScreen(nextRouteName))
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(300)
            yield put(hideButtonLoading())
        break;

        case 'MsigPersetujuanNasabah':
            yield put(showButtonLoading())
            try {
                const ssPn = yield call(writeFile, screenShoot.screenPn, MSIG_SS_SCREEN_PN, captureBase64)
                yield put(setStateScreenshootMsig('screenPn', ssPn))
                yield put(pushToScreen(nextRouteName))
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(300)
            yield put(hideButtonLoading())
        break;

        case 'MsigSignaturePp':
            yield put(showButtonLoading())
            try {
                const valStateSigPp = yield call(validationSignature, state, 'pemegangPolis')
                if(valStateSigPp.result === false) {
                    yield put(showErrorMessage(valStateSigPp.message))
                } else {
                    const ssSp = yield call(writeFile, screenShoot.screenSp, MSIG_SS_SCREEN_SP, captureBase64)
                    yield put(setStateScreenshootMsig('screenSp', ssSp))
                    yield put(pushToScreen(nextRouteName))
                }
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(300)
            yield put(hideButtonLoading())
        break;

        case 'MsigSignatureTt':
            yield put(showButtonLoading())
            try {
                const valStateSigTt = yield call(validationSignature, state, 'tertanggung')
                if(valStateSigTt.result === false) {
                    yield put(showErrorMessage(valStateSigTt.message))
                } else {
                    const ssSt = yield call(writeFile, screenShoot.screenSt, MSIG_SS_SCREEN_ST, captureBase64)
                    yield put(setStateScreenshootMsig('screenSt', ssSt))
                    yield put(pushToScreen(nextRouteName))
                }
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(300)
            yield put(hideButtonLoading())
        break;

        case 'MsigFileUpload':
            yield put(showButtonLoading())
            try {
                const valStateFile = yield call(validationFileAttachment, state)
                if(valStateFile.result === false) {
                    yield put(showErrorMessage(valStateFile.message))
                } else {
                    const ssFd = yield call(writeFile, screenShoot.screenFd, MSIG_SS_SCREEN_FD, captureBase64)
                    yield put(setStateScreenshootMsig('screenFd', ssFd))
                    yield put(pushToScreen(nextRouteName))
                }
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(300)
            yield put(hideButtonLoading())
        break;

        default:
            yield put(showButtonLoading())
            try {
                yield put(pushToScreen(nextRouteName))
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(300)
            yield put(hideButtonLoading())
        break;
    }
}

/**
 * Api uploading KTP
 * @param {String} imgBase64 Base64 images from cache files
 * @param {String} token token from props Auth
 */
const uploadFileAttachmentPp = async (imgBase64, token) => 
    await Insurance.apiFileAttachmentPp(imgBase64, token)
    .then(resp => resp).catch(err => err)

const uploadFileAttachmentTt = async (imgBase64, token) => 
    await Insurance.apiFileAttachmentTt(imgBase64, token)
    .then(resp => resp).catch(err => err)
    
const uploadFileAttachmentCp = async (imgBase64, token) => 
    await Insurance.apiFileAttachmentCp(imgBase64, token)
    .then(resp => resp).catch(err => err)

/**
 * Api uploading KTP
 * @param {String} imgBase64 Base64 images from cache files
 * @param {String} token token from props Auth
 */
const uploadSignaturePp = async (imgBase64, token) => 
    await Insurance.apiSignaturePp(imgBase64, token)
    .then(resp => resp).catch(err => err)

const uploadSignatureTt = async(imgBase64, token) =>
    await Insurance.apiSignatureTt(imgBase64, token)
    .then(resp => resp).catch(err => err)

/**
 * Api uploading ScreenShoot
 * @param {String} imgBase64 Base64 images from cache files
 * @param {String} token token from props Auth
 */
const uploadScreenShootPp = async(imgBase64, token) => 
    await Insurance.apiScreenShootPp(imgBase64, token)
    .then(resp => resp).catch(err => err)

const uploadScreenShootTt = async(imgBase64, token) => 
    await Insurance.apiScreenShootTt(imgBase64, token)
    .then(resp => resp).catch(err => err)

const uploadScreenShootCp = async(imgBase64, token) => 
    await Insurance.apiScreenShootCp(imgBase64, token)
    .then(resp => resp).catch(err => err)

const uploadScreenShootUa = async(imgBase64, token) => 
    await Insurance.apiScreenShootUa(imgBase64, token)
    .then(resp => resp).catch(err => err)

const uploadScreenShootDi = async(imgBase64, token) => 
    await Insurance.apiScreenShootDi(imgBase64, token)
    .then(resp => resp).catch(err => err)

const uploadScreenShootDa = async(imgBase64, token) => 
    await Insurance.apiScreenShootDa(imgBase64, token)
    .then(resp => resp).catch(err => err)

const uploadScreenShootQu = async(imgBase64, token) => 
    await Insurance.apiScreenShootQu(imgBase64, token)
    .then(resp => resp).catch(err => err)

const uploadScreenShootPr = async(imgBase64, token) => 
    await Insurance.apiScreenShootPr(imgBase64, token)
    .then(resp => resp).catch(err => err)

const uploadScreenShootPn = async(imgBase64, token) => 
    await Insurance.apiScreenShootPn(imgBase64, token)
    .then(resp => resp).catch(err => err)

const uploadScreenShootSp = async(imgBase64, token) => 
    await Insurance.apiScreenShootSp(imgBase64, token)
    .then(resp => resp).catch(err => err)

const uploadScreenShootSt = async(imgBase64, token) => 
    await Insurance.apiScreenShootSt(imgBase64, token)
    .then(resp => resp).catch(err => err)

const uploadScreenShootFd = async(imgBase64, token) => 
    await Insurance.apiScreenShootFd(imgBase64, token)
    .then(resp => resp).catch(err => err)

//Task One uploading documents
function* uploadDocuments() {
    const {authUser} = yield select(state => state.Auth)
    const token = authUser.token
    const {uriResponse, fileAttachment, signature, screenShoot} = yield select(state => state.Msig)
    //Set Success File Upload if all the files document uploaded successfull
    const uriA = uriResponse.imgFileAttachment.pemegangPolis
    const uriB = uriResponse.imgFileAttachment.tertanggung
    const uriC = uriResponse.imgFileAttachment.calonPembayar

    const uriD = uriResponse.imgSignature.pemegangPolis
    const uriE = uriResponse.imgSignature.tertanggung

    const uriF = uriResponse.imgScreenShoot.screenPp
    const uriG = uriResponse.imgScreenShoot.screenTt
    const uriH = uriResponse.imgScreenShoot.screenCp
    const uriI = uriResponse.imgScreenShoot.screenUa
    const uriJ = uriResponse.imgScreenShoot.screenDi
    const uriK = uriResponse.imgScreenShoot.screenDa
    const uriL = uriResponse.imgScreenShoot.screenQu
    const uriM = uriResponse.imgScreenShoot.screenPr
    const uriN = uriResponse.imgScreenShoot.screenPn
    const uriO = uriResponse.imgScreenShoot.screenSp
    const uriP = uriResponse.imgScreenShoot.screenSt
    const uriQ = uriResponse.imgScreenShoot.screenFd

    try {
        if(uriResponse.imgFileAttachment.pemegangPolis == '') {
            const base64IdentityPp = yield call(loadFile, fileAttachment.pemegangPolis)
            const resIdentityPp = yield call(uploadFileAttachmentPp, base64IdentityPp, token)
            if(resIdentityPp.error !== 0)
                return false
            if(resIdentityPp.error == 0) 
                yield put(setStateUriResponseMsig('imgFileAttachment', 'pemegangPolis', resIdentityPp.data))
        }
            
        if(uriResponse.imgFileAttachment.tertanggung == '') {
            const base64IdentityTt = yield call(loadFile, fileAttachment.tertanggung)
            const resIdenityTt = yield call(uploadFileAttachmentTt, base64IdentityTt, token)
            if(resIdenityTt.error !== 0)
                return false
            if(resIdenityTt.error == 0)
                yield put(setStateUriResponseMsig('imgFileAttachment', 'tertanggung', resIdenityTt.data))
        }

        if(uriResponse.imgFileAttachment.calonPembayar == '') {
            const base64IdentityCp = yield call(loadFile, fileAttachment.calonPembayar)
            const resIdenityCp = yield call(uploadFileAttachmentCp, base64IdentityCp, token)
            if(resIdenityCp.error !== 0)
                return false
            if(resIdenityCp.error == 0)
                yield put(setStateUriResponseMsig('imgFileAttachment', 'calonPembayar', resIdenityCp.data))
        }

        if(uriResponse.imgSignature.pemegangPolis == '') {
            const base64SignaturePp = yield call(loadFile, signature.pemegangPolis)
            const resSignaturePp = yield call(uploadSignaturePp, base64SignaturePp, token)
            if(resSignaturePp.error !== 0)
                return false
            if(resSignaturePp.error == 0)
                yield put(setStateUriResponseMsig('imgSignature', 'pemegangPolis', resSignaturePp.data))
        }

        if(uriResponse.imgSignature.tertanggung == '') {
            const base64SignatureTt = yield call(loadFile, signature.tertanggung)
            const resSignatureTt = yield call(uploadSignatureTt, base64SignatureTt, token)
            if(resSignatureTt.error !== 0)
                return false
            if(resSignatureTt.error == 0)
                yield put(setStateUriResponseMsig('imgSignature', 'tertanggung', resSignatureTt.data))
        }

        if(uriResponse.imgScreenShoot.screenPp == '') {
            const base64ScreenShootPp = yield call(loadFile, screenShoot.screenPp)
            const resScreenShootPp = yield call(uploadScreenShootPp, base64ScreenShootPp, token)
            if(resScreenShootPp.error !== 0)
                return false
            if(resScreenShootPp.error == 0)
                yield put(setStateUriResponseMsig('imgScreenShoot', 'screenPp', resScreenShootPp.data))
        }

        if(uriResponse.imgScreenShoot.screenTt == '') {
            const base64ScreenShootTt = yield call(loadFile, screenShoot.screenTt)
            const resScreenShootTt = yield call(uploadScreenShootTt, base64ScreenShootTt, token)
            if(resScreenShootTt.error !== 0)
                return false
            if(resScreenShootTt.error == 0)
                yield put(setStateUriResponseMsig('imgScreenShoot', 'screenTt', resScreenShootTt.data))
        }

        if(uriResponse.imgScreenShoot.screenCp == '') {
            const base64ScreenShootCp = yield call(loadFile, screenShoot.screenCp)
            const resScreenShootCp = yield call(uploadScreenShootCp, base64ScreenShootCp, token)
            if(resScreenShootCp.error !== 0)
                return false
            if(resScreenShootCp.error == 0)
                yield put(setStateUriResponseMsig('imgScreenShoot', 'screenCp', resScreenShootCp.data))
        }

        if(uriResponse.imgScreenShoot.screenUa == '') {
            const base64ScreenShootUa = yield call(loadFile, screenShoot.screenUa)
            const resScreenShootUa = yield call(uploadScreenShootUa, base64ScreenShootUa, token)
            if(resScreenShootUa.error !== 0)
                return false
            if(resScreenShootUa.error == 0)
                yield put(setStateUriResponseMsig('imgScreenShoot', 'screenUa', resScreenShootUa.data))
        }

        if(uriResponse.imgScreenShoot.screenDi == '') {
            const base64ScreenShootDi = yield call(loadFile, screenShoot.screenDi)
            const resScreenShootDi = yield call(uploadScreenShootDi, base64ScreenShootDi, token)
            if(resScreenShootDi.error !== 0)
                return false
            if(resScreenShootDi.error == 0)
                yield put(setStateUriResponseMsig('imgScreenShoot', 'screenDi', resScreenShootDi.data))
        }

        if(uriResponse.imgScreenShoot.screenDa == '') {
            const base64ScreenShootDa = yield call(loadFile, screenShoot.screenDa)
            const resScreenShootDa = yield call(uploadScreenShootDa, base64ScreenShootDa, token)
            if(resScreenShootDa.error !== 0)
                return false
            if(resScreenShootDa.error == 0)
                yield put(setStateUriResponseMsig('imgScreenShoot', 'screenDa', resScreenShootDa.data))
        }

        if(uriResponse.imgScreenShoot.screenQu == '') {
            const base64ScreenShootQu = yield call(loadFile, screenShoot.screenQu)
            const resScreenShootQu = yield call(uploadScreenShootQu, base64ScreenShootQu, token)
            if(resScreenShootQu.error !== 0)
                return false
            if(resScreenShootQu.error == 0)
                yield put(setStateUriResponseMsig('imgScreenShoot', 'screenQu', resScreenShootQu.data))
        }

        if(uriResponse.imgScreenShoot.screenPr == '') {
            const base64ScreenShootPr = yield call(loadFile, screenShoot.screenPr)
            const resScreenShootPr = yield call(uploadScreenShootPr, base64ScreenShootPr, token)
            if(resScreenShootPr.error !== 0)
                return false
            if(resScreenShootPr.error == 0)
                yield put(setStateUriResponseMsig('imgScreenShoot', 'screenPr', resScreenShootPr.data))
        }

        if(uriResponse.imgScreenShoot.screenPn == '') {
            const base64ScreenShootPn = yield call(loadFile, screenShoot.screenPn)
            const resScreenShootPn = yield call(uploadScreenShootPn, base64ScreenShootPn, token)
            if(resScreenShootPn.error !== 0)
                return false
            if(resScreenShootPn.error == 0)
                yield put(setStateUriResponseMsig('imgScreenShoot', 'screenPn', resScreenShootPn.data))
        }

        if(uriResponse.imgScreenShoot.screenSp == '') {
            const base64ScreenShootSp = yield call(loadFile, screenShoot.screenSp)
            const resScreenShootSp = yield call(uploadScreenShootSp, base64ScreenShootSp, token)
            if(resScreenShootSp.error !== 0)
                return false
            if(resScreenShootSp.error == 0)
                yield put(setStateUriResponseMsig('imgScreenShoot', 'screenSp', resScreenShootSp.data))
        }

        if(uriResponse.imgScreenShoot.screenSt == '') {
            const base64ScreenShootSt = yield call(loadFile, screenShoot.screenSt)
            const resScreenShootSt = yield call(uploadScreenShootSt, base64ScreenShootSt, token)
            if(resScreenShootSt.error !== 0)
                return false
            if(resScreenShootSt.error == 0)
                yield put(setStateUriResponseMsig('imgScreenShoot', 'screenSt', resScreenShootSt.data))
        }

        if(uriResponse.imgScreenShoot.screenFd == '') {
            const base64ScreenShootFd = yield call(loadFile, screenShoot.screenFd)
            const resScreenShootFd = yield call(uploadScreenShootFd, base64ScreenShootFd, token)
            if(resScreenShootFd.error !== 0)
                return false
            if(resScreenShootFd.error == 0)
                yield put(setStateUriResponseMsig('imgScreenShoot', 'screenFd', resScreenShootFd.data))
        }

        if(uriA !== '' && uriB !== '' && uriC !== '' && uriD !== '' 
            && uriE !== '' && uriF !== '' && uriG !== ''&& uriH !== '' 
            && uriI !== '' && uriJ !== '' && uriK !== '' && uriL !== '' 
            && uriM !== '' && uriN !== '' && uriO !== '' && uriP !== '' && uriQ !== '') {
            yield put(setStateRootMsig('successFileUpload', true))
            return true
        } else {
            return false
        }
    } catch (error) {
        yield put(showErrorMessage(error.message))
        return false
    }
}

const uploadProposal = async(state, token) =>
    await Insurance.apiCreateProposal(state, token)
    .then(resp => resp).catch(err => err)

function* generateProposal() {
    try {
        const {authUser} = yield select(state => state.Auth)
        const token = authUser.token
        const {Msig} = yield select(state => state)
        const resp = yield call(uploadProposal, Msig, token)
        yield delay(100)
        yield put(setStateRootMsig('successGenerateProposal', true))
        return resp
    } catch (error) {
        yield put(showErrorMessage(error.message))
        return false
    }
}

function* generateSpaj() {
    try {
        const {authUser} = yield select(state => state.Auth)
        const token = authUser.token
        const {Msig} = yield select(state => state)
        yield delay(100)
        yield put(setStateRootMsig('successSubmitSpaj', true))
        return true
    } catch (error) {
        yield put(showErrorMessage(error.message))
        return false
    }
}

function* submitSpaj() {
    try {
        yield* showLoadingTaskOne()
        let i = 1
        let j = 1
        let k = 1
        while (true) {
            //Start Uploading Document Files
            const taskOne = yield* uploadDocuments()
            if(taskOne === false) {
                i ++
                if(i > 5 && taskOne === false) {
                    yield put(showErrorMessage('Upload documents failed, please try again !'))
                    yield cancel()
                    break
                }
            } else {
                //Start Generate Proposal
                yield* showLoadingTaskTwo()
                const taskTwo = yield* generateProposal()
                if(taskTwo === false) {
                    j++
                    if(j > 5 && taskTwo === false) {
                        yield put(showErrorMessage('Generate Proposal failed, please try again !'))
                        yield cancel()
                        break
                    }
                } else {
                    //Start Generate SPAJ
                    yield* showLoadingTaskThree()
                    const taskTree = yield* generateSpaj()
                    if(taskTree === false) {
                        k++
                        if(k > 5 && taskTree === false) {
                            yield put(showErrorMessage('Submit SPAJ failed, please try again !'))
                            yield cancel()
                            break
                        }
                    } else {
                        yield put(showErrorMessage('Successfull submit spaj'))
                        yield put(navigatePopToRoot())
                        yield cancel()
                        break
                    }
                }
            }
        }
    } catch (error) {
        yield put(showErrorMessage(error.message))
    }  finally {
        if(yield cancelled())
        yield* hideLoadingTaskOne()
        yield* hideLoadingTaskTwo()
        yield* hideLoadingTaskTree()
    }
}

function* showLoadingTaskOne() {
    yield put(showButtonLoading())
    yield put(showLoadingUploadDocumentMsig())
}

function* hideLoadingTaskOne() {
    yield put(hideButtonLoading())
    yield put(hideLoadingUploadDocumentMsig())
}

function* showLoadingTaskTwo() {
    yield put(showButtonLoading())
    yield put(showLoadingGenerateProposalMsig())
}

function* hideLoadingTaskTwo() {
    yield put(hideButtonLoading())
    yield put(hideLoadingGenerateProposalMsig())
}

function* showLoadingTaskThree() {
    yield put(showButtonLoading())
    yield put(showLoadingSubmitSpajMsig())
}

function* hideLoadingTaskTree() {
    yield put(hideButtonLoading())
    yield put(hideLoadingSubmitSpajMsig())
}

function* cacheSpajForm() {
    yield put(showButtonLoading())
    const {Msig} = yield select(state => state)
    try {
        const save = yield call(saveCacheMsig, STORAGE_SPAJ_MSIG, Msig)
        //Back to root navigation and reset cache state Msig Reducer
        //This condition only when save cache success with return no equal null
        if(save !== null) {
            yield put(resetCacheStateMsig())
            yield put(navigatePopToRoot())
            yield put(showErrorMessage('SPAJ berhasil di simpan di gadget anda'))
        }
        console.log('save cache state', save)
    } catch (error) {
        yield put(showErrorMessage(error.message))
    }
    yield delay(300)
    yield put(hideButtonLoading())
    
    console.log('Saga cache form spaj', Msig)
}

export function* doNextProposalCategory () {
    yield takeEvery(SAGA_PROPOSAL_CATEGORY_MSIG, nextProposalCategory)
}

export function* doNextScreen() {
    yield takeEvery(SAGA_NEXT_SCREEN_MSIG, nextScreen)
}

export function* doSubmitSpaj() {
    yield takeEvery(SAGA_SUBMIT_FORM_MSIG, submitSpaj)
}

export function* doSaveSubmitSpaj() {
    yield takeEvery(SAGA_SAVE_SUBMIT_FORM_MSIG, cacheSpajForm)
}

export default function* rootSaga(){
    yield all([
        fork(doNextProposalCategory),
        fork(doNextScreen),
        fork(doSubmitSpaj),
        fork(doSaveSubmitSpaj)
    ])
}
