import { all,call,fork,put,takeEvery } from 'redux-saga/effects'
import { GET_TAGIHAN } from '../constants'
import { Invoice } from '../api'
import { 
  getTagihanSuccess,
  showGlobalLoading,
  showErrorMessage,
  hideErrorMessage,
  hideGlobalLoading
} from '../actions'

const reqTagihan = async (token) => 
    await Invoice.getTagihan(token)
    .then(resp => resp)
    .catch(error => error)

function* getTagihan({payload}) {
    __DEV__ && console.log('Saga payload function getTagihan', payload)
    const {token} = payload;
    yield put(showGlobalLoading())
    try {
        let resp = yield call(reqTagihan, token)
        __DEV__ && console.log('Saga payload function getTagihan', resp)
        
        if(resp.message) {
            yield put(showErrorMessage('Fetch Data Error', resp.message))
        } else {
            yield put(hideErrorMessage())
            yield put(getTagihanSuccess(resp.data))
        }
    } catch (error) {
        yield put(showErrorMessage('System Error', error))
    }
    yield put(hideGlobalLoading())
}

export function* doGetTagihan(){
    yield takeEvery(GET_TAGIHAN, getTagihan);
}

export default function* rootSaga(){
    yield all([
        fork(doGetTagihan),
    ])
}
