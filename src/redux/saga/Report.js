import { all, call, fork, put, takeEvery, throttle, select } from 'redux-saga/effects'
import { GET_KOMISI, CACHE_DOWNLINE, GET_DOWNLINE, SET_DOWNLINE_SEARCH_KEY } from '../constants'
import { Report } from '../api'
import {
    showGlobalLoading,
    hideGlobalLoading,
    showErrorMessage,
    hideErrorMessage,
    getKomisiSuccess,
    showFirstLoading,
    hideFirstLoading,
    setDownlineSuccess,
    showRefreshControl,
    hideRefreshControl,
    setDownlineFilter
} from '../actions'
import { STORAGE_DOWNLINE_MEMBER } from '../../cache/constant/MasterAsuransi'
import { saveCacheDownlineMember } from '../../cache/model/MasterModel'
const reqDownlineUserId = async (token, data) =>
    await Report.downlineByUserId(token, data)
        .then(resp => resp)
        .catch(error => error)

const reqDownlineUsername = async (token, data) =>
    await Report.downlineByUsername(token, data)
        .then(resp => resp)
        .catch(error => error)

const reqKomisi = async (token, data) =>
    await Report.getKomisi(token, data)
        .then(resp => resp)
        .catch(error => error)

function* getDownline({ payload }) {
    __DEV__ && console.log('Saga payload function downline', payload)
    const { data, token } = payload;

    let data_uid = {
        'data': {
            "user_id": data
        }
    }
    try {
        yield put(showRefreshControl())
        let resp = yield call(reqDownlineUserId, token, data_uid)
        __DEV__ && console.log('Saga payload function downline', payload)
        if (resp.message) {
            yield put(showErrorMessage('Fetch Data Error', resp.message.toString()))
        } else {
            if (resp.data.length > 0) {
                yield put(setDownlineSuccess(resp.data))
            } else {
                yield showErrorMessage('Error', 'User Tidak Mempunyai Downline')
            }
            yield put(hideRefreshControl())
        }
        yield put(hideRefreshControl())
    } catch (error) {
        yield put(hideGlobalLoading())
        yield put(showErrorMessage('System Error', error.message))
    }

}

function* getKomisi({ payload }) {
    __DEV__ && console.log('Saga payload function getReport', payload)
    const { token } = payload;

    try {
        yield put(showGlobalLoading())
        let resp = yield call(reqKomisi, token)
        __DEV__ && console.log('Saga payload function getReport', resp)

        if (resp.error != 0) {
            yield put(getKomisiSuccess(0))
            yield put(hideGlobalLoading())
        } else {
            yield put(hideErrorMessage())
            yield put(getKomisiSuccess(resp))
            yield put(hideGlobalLoading())
        }
    } catch (error) {
        yield put(showErrorMessage('System Error', error))
        yield put(hideGlobalLoading())
    }
}


function* cacheDownlineMember({ payload }) {
    try {
        yield put(showFirstLoading())
        const { data, token } = payload;
        let resp = yield call(reqDownlineUserId, token, data)
        if (resp.error == 0) {
            yield call(saveCacheDownlineMember, STORAGE_DOWNLINE_MEMBER, resp.data)
        }
        yield put(hideFirstLoading())
    } catch (error) {
        yield put(hideFirstLoading())
        __DEV__ && console.log('Error Resport Saga cacheDownlineMember', error)
    }
}

function* setSearchKeyData({ payload }) {
    try {
        const { authUser } = yield select(state => state.Auth);
        const { downlineUserId } = yield select(state => state.Report);
        const token = authUser.token;
        const uid = downlineUserId == '' ? authUser.user_data.id : downlineUserId
        console.log('downline User Id', { uid, token });
        if (payload == '') {
            let data_uid = {
                'data': {
                    "user_id": uid
                }
            }
            try {
                yield put(showRefreshControl())
                let resp = yield call(reqDownlineUserId, token, data_uid)
                __DEV__ && console.log('Saga payload function downline', payload)
                if (resp.message) {
                    yield put(showErrorMessage('Fetch Data Error', resp.message.toString()))
                } else {
                    yield put(setDownlineSuccess(resp.data))
                    yield put(hideRefreshControl())
                }
                yield put(hideRefreshControl())
            } catch (error) {
                yield put(hideGlobalLoading())
                yield put(showErrorMessage('System Error', error.message))
            }
        }
        yield put(setDownlineFilter(payload))
        __DEV__ && console.log('select authUser', authUser);
    } catch (error) {

    }
}

export function* doGetDownline() {
    yield takeEvery(GET_DOWNLINE, getDownline)
}

export function* doReqKomisi() {
    yield takeEvery(GET_KOMISI, getKomisi);
}

export function* doCacheDownline() {
    yield takeEvery(CACHE_DOWNLINE, cacheDownlineMember)
}

export function* doDownlineSearchKey() {
    yield throttle(1000, SET_DOWNLINE_SEARCH_KEY, setSearchKeyData)
}


export default function* rootSaga() {
    yield all([
        fork(doGetDownline),
        fork(doReqKomisi),
        fork(doCacheDownline),
        fork(doDownlineSearchKey)
    ])
}
