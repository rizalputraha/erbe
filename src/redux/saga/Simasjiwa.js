import { all, call, fork, put, takeEvery, delay, select, cancel, cancelled } from 'redux-saga/effects'
import { Insurance } from 'erberedux/api'

import {
    SAGA_PROPOSAL_CATEGORY_SIMAS,
    SAGA_NEXT_SCREEN_SIMAS,
    STORAGE_SPAJ_SIMAS,
    SAGA_SUBMIT_FORM_SIMAS
} from 'erberedux/constants'

import { saveCacheSimas, loadCacheSimas } from 'erbecache/model/SimasModel';
import { loadFile } from 'erbecache/model/FileModel'

import {
    navigateToScreen,
    showLoadingBtnAgenSimas,
    showLoadingBtnNasabahSimas,
    hideLoadingBtnAgenSimas,
    hideLoadingBtnNasabahSimas,
    showLoadingUploadDocumentSimas,
    hideLoadingUploadDocumentSimas,
    showLoadingGenerateProposalSimas,
    hideLoadingGenerateProposalSimas,
    showLoadingSubmitSpajSimas,
    hideLoadingSubmitSpajSimas,
    showButtonLoading,
    hideButtonLoading,
    showErrorMessage,
    hideErrorMessage,
    setStateProfileFromAuthUserSimas,
    setProposalPPtoTTSimas,
    setSpajPPtoTTSimas,
    setStateDownlineSimas,
    setStateSpajPPSimas,
    setStateSpajTTSimas,
    setProposalIsPartner,
    navigatePopToRoot,
    setKeyRootSimas,
    resetCacheStateSimas,
    setStateUriResponseSimas,
    setStateFromCacheSimas
} from '../actions'

import {
    submitProposalPemegangPolis,
    submitTertanggung,
    submitFormPP,
    submitFormTT,
    submitFormPenerimaManfaat
} from 'erbevalidation/SimasJiwa'
import { SAGA_SAVE_SUBMIT_FORM_SIMAS } from '../constants';


function* nextProposalCategorySimas({ payload }) {
    const { buttonType } = payload
    if (buttonType == 'nasabah') {
        yield put(showLoadingBtnNasabahSimas())
        try {
            const cacheForm = yield call(loadCacheSimas, STORAGE_SPAJ_SIMAS)
            if (cacheForm !== null) {
                const nasabah = cacheForm.filter(item => item.isPartner === false)
                if (nasabah.length !== parseInt(0)) {
                    yield put(navigateToScreen('SimasCache', { type: 'nasabah', data: nasabah }))
                } else {
                    yield put(navigateToScreen('SimasStarter'))
                }
            } else {
                yield put(navigateToScreen('SimasStarter'))
            }
        } catch (error) {
            yield put(showErrorMessage(error.message))
        }
        yield delay(300)
        yield put(hideLoadingBtnNasabahSimas())
    } else if (buttonType == 'agen') {
        yield put(showLoadingBtnAgenSimas())
        try {
            const cacheForm = yield call(loadCacheSimas, STORAGE_SPAJ_SIMAS)
            if (cacheForm !== null) {
                const agen = cacheForm.filter(item => item.isPartner === false)
                if (agen.length !== parseInt(0)) {
                    yield put(navigateToScreen('SimasCache', { type: 'agen', data: agen }))
                } else {
                    yield put(navigateToScreen('SimasStarter'))
                }
            } else {
                yield put(navigateToScreen('SimasStarter'))
            }
        } catch (error) {
            yield put(showErrorMessage(error.message))
        }
        yield delay(300)
        yield put(hideLoadingBtnNasabahSimas())
    } else {
        yield put(showErrorMessage('Type button undefined'))
    }
}

function* nextScreen({ payload }) {
    const { prevRouteName, nextRouteName, state, authUser, device, capture } = payload
    const { isPartner } = yield select(state => state.SimasJiwa);
    switch (prevRouteName) {
        case 'SimasCache':
            yield put(showButtonLoading())
            try {
                yield put(setStateFromCacheSimas(state))
                yield put(navigateToScreen(nextRouteName))
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(100)
            yield put(hideButtonLoading())
            break;

        case 'SimasDownline':
            yield put(showButtonLoading())
            try {
                yield put(navigateToScreen(nextRouteName))
                yield put(setStateDownlineSimas(state))
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(500)
            yield put(hideButtonLoading())
            break;

        case 'SimasStarter':
            yield put(showButtonLoading())
            try {
                yield put(navigateToScreen(nextRouteName))
                __DEV__ && console.log('state', state);
                if (isPartner) {
                    yield put(setProposalIsPartner(state))
                } else {
                    yield put(setStateProfileFromAuthUserSimas(authUser))
                }
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(500)
            yield put(hideButtonLoading())
            break;

        case 'SimasPP':
            yield put(showButtonLoading())
            const valStateProposalPp = yield call(submitProposalPemegangPolis, state)
            __DEV__ && console.log('state simas', state);

            try {
                if (valStateProposalPp.result === false) {
                    yield put(showErrorMessage(valStateProposalPp.message))
                } else {
                    if (nextRouteName == 'SimasProduk') {
                        yield put(setProposalPPtoTTSimas(state))

                        yield put(setStateSpajPPSimas('namaPp', state.namaPp));
                        yield put(setStateSpajPPSimas('dobPp', state.dobPp.replace(/-/g, '/')));
                        yield put(setStateSpajPPSimas('genderPp', state.genderPp));
                        yield put(setStateSpajPPSimas('usiaPp', state.usiaPp));
                    }
                    yield put(setStateSpajPPSimas('namaPp', state.namaPp));
                    yield put(setStateSpajPPSimas('dobPp', state.dobPp.replace(/-/g, '/')));
                    yield put(setStateSpajPPSimas('genderPp', state.genderPp));
                    yield put(setStateSpajPPSimas('usiaPp', state.usiaPp));
                    yield put(navigateToScreen(nextRouteName))
                }
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(500)
            yield put(hideButtonLoading())
            break;

        case 'SimasTT':
            yield put(showButtonLoading())
            try {
                const valStateProposalTt = yield call(submitTertanggung, state)
                __DEV__ && console.log('state tertanggung', state);
                if (valStateProposalTt.result === false) {
                    yield put(showErrorMessage(valStateProposalTt.message))
                } else {
                    yield put(setStateSpajTTSimas('usiaTt', state.usiaTt))
                    yield put(setStateSpajTTSimas('namaTt', state.namaTt));
                    yield put(setStateSpajTTSimas('dobTt', state.dobTt));
                    yield put(setStateSpajTTSimas('genderTt', state.genderTt));
                    yield put(navigateToScreen(nextRouteName))
                }
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(500)
            yield put(hideButtonLoading())
            break;

        case 'SimasProduk':
            yield put(showButtonLoading())
            try {
                yield put(navigateToScreen(nextRouteName))
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(500)
            yield put(hideButtonLoading())
            break;

        case 'SimasFormPP':
            yield put(showButtonLoading())
            try {
                const valStateSpajPemegangPolis = yield call(submitFormPP, state)
                if (valStateSpajPemegangPolis.result === false) {
                    yield put(showErrorMessage(valStateSpajPemegangPolis.message))
                } else {
                    if (nextRouteName == 'SimasPenerimaManfaat') {
                        yield put(setSpajPPtoTTSimas(state))
                    }
                    yield put(navigateToScreen(nextRouteName))
                }
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(500)
            yield put(hideButtonLoading())
            break;

        case 'SimasFormTT':
            yield put(showButtonLoading())
            try {
                const valStateSpajTertanggung = yield call(submitFormTT, state)
                if (valStateSpajTertanggung.result === false) {
                    yield put(showErrorMessage(valStateSpajTertanggung.message))
                } else {
                    yield put(navigateToScreen(nextRouteName))
                }
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(500)
            yield put(hideButtonLoading())
            break;

        case 'SimasPenerimaManfaat':
            yield put(showButtonLoading())
            try {
                const valStateSpajPM = yield call(submitFormPenerimaManfaat, state)
                if (valStateSpajPM.result === false) {
                    yield put(showErrorMessage(valStateSpajPM.message))
                } else {
                    yield put(navigateToScreen(nextRouteName))
                }
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(500)
            yield put(hideButtonLoading())
            break;

        default:
            yield put(showButtonLoading())
            try {
                yield put(navigateToScreen(nextRouteName))
            } catch (error) {
                yield put(showErrorMessage(error.message))
            }
            yield delay(500)
            yield put(hideButtonLoading())
            break;
    }
}

/**
 * Api uploading KTP
 * @param {String} imgBase64 Base64 images from cache files
 * @param {String} token token from props Auth
 */
const uploadFileAttachmentPp = async (imgBase64, token) =>
    await Insurance.apiFileAttachmentPp(imgBase64, token)
        .then(resp => resp).catch(err => err)

const uploadFileAttachmentTt = async (imgBase64, token) =>
    await Insurance.apiFileAttachmentTt(imgBase64, token)
        .then(resp => resp).catch(err => err)

/**
 * Api uploading KTP
 * @param {String} imgBase64 Base64 images from cache files
 * @param {String} token token from props Auth
 */
const uploadSignaturePp = async (imgBase64, token) =>
    await Insurance.apiSignaturePp(imgBase64, token)
        .then(resp => resp).catch(err => err)

const uploadSignatureTt = async (imgBase64, token) =>
    await Insurance.apiSignatureTt(imgBase64, token)
        .then(resp => resp).catch(err => err)

function* uploadDocuments() {
    const { authUser } = yield select(state => state.Auth)
    const token = authUser.token
    const { uriResponse, fileAttachment, signature } = yield select(state => state.SimasJiwa)

    const uriA = uriResponse.imgFileAttachment.pemegangPolis
    const uriB = uriResponse.imgFileAttachment.tertanggung

    const uriC = uriResponse.imgSignature.pemegangPolis
    const uriD = uriResponse.imgSignature.tertanggung

    try {
        if (uriResponse.imgFileAttachment.pemegangPolis == '') {
            const base64IdentityPp = yield call(loadFile, fileAttachment.pemegangPolis)
            const resIdentityPp = yield call(uploadFileAttachmentPp, base64IdentityPp, token)
            __DEV__ && console.log('resIdentity : ', resIdentityPp);
            if (resIdentityPp.error !== 0)
                return false
            if (resIdentityPp.error == 0)
                yield put(setStateUriResponseSimas('imgFileAttachment', 'pemegangPolis', resIdentityPp.data))
        }
        if (uriResponse.imgFileAttachment.tertanggung == '') {
            const base64IdentityTt = yield call(loadFile, fileAttachment.tertanggung)
            const resIdenityTt = yield call(uploadFileAttachmentTt, base64IdentityTt, token)
            if (resIdenityTt.error !== 0)
                return false
            if (resIdenityTt.error == 0)
                yield put(setStateUriResponseSimas('imgFileAttachment', 'tertanggung', resIdenityTt.data))
        }

        if (uriResponse.imgSignature.pemegangPolis == '') {
            const base64SignaturePp = yield call(loadFile, signature.pemegangPolis)
            const resSignaturePp = yield call(uploadSignaturePp, base64SignaturePp, token)
            if (resSignaturePp.error !== 0)
                return false
            if (resSignaturePp.error == 0)
                yield put(setStateUriResponseSimas('imgSignature', 'pemegangPolis', resSignaturePp.data))
        }

        if (uriResponse.imgSignature.tertanggung == '') {
            const base64SignatureTt = yield call(loadFile, signature.tertanggung)
            const resSignatureTt = yield call(uploadSignatureTt, base64SignatureTt, token)
            if (resSignatureTt.error !== 0)
                return false
            if (resSignatureTt.error == 0)
                yield put(setStateUriResponseSimas('imgSignature', 'tertanggung', resSignatureTt.data))
        }

        if (uriA !== '' && uriB !== '' && uriC !== '' && uriD !== '') {
            yield put(setKeyRootSimas('successFileUpload', true))
            return true
        } else {
            return false
        }
    } catch (error) {
        yield put(showErrorMessage(error.message))
        return false
    }
}

const uploadProposal = async (state, token) =>
    await Insurance.apiCreateProposal(state, token)
        .then(resp => resp).catch(err => err)

const uploadSpaj = async (state, token) =>
    await Insurance.apiCreateSpaj(state, token)
        .then(resp => resp).catch(err => err)

function* generateProposal() {
    try {
        const { authUser } = yield select(state => state.Auth)
        const token = authUser.token
        const { SimasJiwa } = yield select(state => state)
        const resp = yield call(uploadProposal, SimasJiwa, token)
        __DEV__ && console.log('respon pdfDocument', resp);
        if (resp.error !== 0) {
            return false
        }
        if (resp.error === 0) {
            yield put(setStateUriResponseSimas('pdfDocument', 'proposal', resp.data))
        }
        yield delay(100)
        yield put(setKeyRootSimas('successGenerateProposal', true))
        return resp
    } catch (error) {
        yield put(showErrorMessage(error.message))
        return false
    }
}

function* generateSpaj() {
    try {
        const { authUser } = yield select(state => state.Auth)
        const token = authUser.token
        const { SimasJiwa } = yield select(state => state)
        yield delay(100)
        yield put(setKeyRootSimas('successSubmitSpaj', true))
        return true
    } catch (error) {
        yield put(showErrorMessage(error.message))
        return false
    }
}

function* submitSpaj() {
    try {
        yield* showLoadingTaskOne()
        let i = 1
        let j = 1
        let k = 1
        while (true) {
            //Start Uploading Document Files
            const taskOne = yield* uploadDocuments()
            if (taskOne === false) {
                i++
                if (i > 5 && taskOne === false) {
                    yield put(showErrorMessage('Upload documents failed, please try again !'))
                    yield cancel()
                    break
                }
            } else {
                //Start Generate Proposal
                yield* showLoadingTaskTwo()
                const taskTwo = yield* generateProposal()
                if (taskTwo === false) {
                    j++
                    if (j > 5 && taskTwo === false) {
                        yield put(showErrorMessage('Generate Proposal failed, please try again !'))
                        yield cancel()
                        break
                    }
                } else {
                    //Start Generate SPAJ
                    yield* showLoadingTaskThree()
                    const taskTree = yield* generateSpaj()
                    if (taskTree === false) {
                        k++
                        if (k > 5 && taskTree === false) {
                            yield put(showErrorMessage('Submit SPAJ failed, please try again !'))
                            yield cancel()
                            break
                        }
                    } else {
                        yield put(showErrorMessage('Successfull submit spaj'))
                        yield put(navigatePopToRoot())
                        yield cancel()
                        break
                    }
                }
            }
        }
    } catch (error) {
        yield put(showErrorMessage(error.message))
    } finally {
        if (yield cancelled())
            yield* hideLoadingTaskOne()
        yield* hideLoadingTaskTwo()
        yield* hideLoadingTaskTree()
    }
    console.log('Saga submit spaj', proposal)
}

function* showLoadingTaskOne() {
    yield put(showButtonLoading())
    yield put(showLoadingUploadDocumentSimas())
}

function* hideLoadingTaskOne() {
    yield put(hideButtonLoading())
    yield put(hideLoadingUploadDocumentSimas())
}

function* showLoadingTaskTwo() {
    yield put(showButtonLoading())
    yield put(showLoadingGenerateProposalSimas())
}

function* hideLoadingTaskTwo() {
    yield put(hideButtonLoading())
    yield put(hideLoadingGenerateProposalSimas())
}

function* showLoadingTaskThree() {
    yield put(showButtonLoading())
    yield put(showLoadingSubmitSpajSimas())
}

function* hideLoadingTaskTree() {
    yield put(hideButtonLoading())
    yield put(hideLoadingSubmitSpajSimas())
}

function* cacheSpajForm() {
    yield put(showButtonLoading())
    const { SimasJiwa } = yield select(state => state)
    try {
        const save = yield call(saveCacheSimas, STORAGE_SPAJ_SIMAS, SimasJiwa)
        //Back to root navigation and reset cache state Simas Reducer
        //This condition only when save cache success with return no equal null

        const data = yield call(loadCacheSimas, STORAGE_SPAJ_SIMAS)
        console.log('data storage simas', data);

        if (save !== null) {
            yield put(resetCacheStateSimas())
            yield put(navigatePopToRoot())
            yield put(showErrorMessage('SPAJ berhasil di simpan di gadget anda'))
        }
        console.log('save cache state', save)
    } catch (error) {
        yield put(showErrorMessage(error.message))
    }
    yield delay(300)
    yield put(hideButtonLoading())

    console.log('Saga cache form spaj', SimasJiwa)
}

export function* doNextScreenCategory() {
    yield takeEvery(SAGA_PROPOSAL_CATEGORY_SIMAS, nextProposalCategorySimas)
}

export function* doNextScreen() {
    yield takeEvery(SAGA_NEXT_SCREEN_SIMAS, nextScreen)
}

export function* doSubmitSpaj() {
    yield takeEvery(SAGA_SUBMIT_FORM_SIMAS, submitSpaj)
}

export function* doSaveSubmitSpaj() {
    yield takeEvery(SAGA_SAVE_SUBMIT_FORM_SIMAS, cacheSpajForm)
}

export default function* rootSaga() {
    yield all([
        fork(doNextScreenCategory),
        fork(doNextScreen),
        fork(doSubmitSpaj),
        fork(doSaveSubmitSpaj)
    ])
}