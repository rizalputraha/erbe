import { all, fork, debounce, } from 'redux-saga/effects'
import { NAVIGATE_PUSH, NAVIGATE_SHARE, NAVIGATE_ROUTE, NAVIGATE_POP, NAVIGATE_POP_TO_ROOT } from '../constants'
import { push, share, navigate, popToTop, pop } from 'erbeservice/AppNavigation'

function* pushScreen({ payload }) {
    const { screen, param } = payload;
    push(screen, param)
}

function* navigateRoute({ payload }) {
    const { routeName, params } = payload;
    navigate(routeName, params);
}

function* shareScreen({ payload }) {
    const { content, contentOptions } = payload;
    share(content, contentOptions);
}

function* navigatePopToRoot({ payload }) {
    const { params } = payload
    popToTop();
}

function* navigatePop({ payload }) {
    const { n } = payload
    pop(n)
}

function* debouncePush() {
    yield debounce(250, NAVIGATE_PUSH, pushScreen)
    yield debounce(100, NAVIGATE_SHARE, shareScreen)
    yield debounce(200, NAVIGATE_ROUTE, navigateRoute)
    yield debounce(10, NAVIGATE_POP, navigatePop)
    yield debounce(500, NAVIGATE_POP_TO_ROOT, navigatePopToRoot)
}

export default function* rootSaga() {
    yield all([
        fork(debouncePush),
    ])
}
