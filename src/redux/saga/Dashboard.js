import { all,call,fork,put,takeEvery } from 'redux-saga/effects'
import { 
    SAGA_GET_DATA_SLIDER,
    SAGA_GET_SPAJ_REFERAL
} from 'erberedux/constants'

import {
    setDataSlider,
    setSpajReferal,
    showButtonLoading,
    hideButtonLoading,
    showErrorMessage,
    hideErrorMessage
} from '../actions'

import { Dashboardv2 } from '../api'

const sliderRequest = async (token) =>
    await Dashboardv2.sliderReqv2(token)
    .then(resp => resp)
    .catch(error => error)

const referalRequest = async (token) => 
    await Dashboardv2.referalReqV2(token)
    .then(resp => resp)
    .catch(error => error)

function* getSlider({payload}) {
    __DEV__ && console.log('Saga payload function getSlider', payload)
    yield put(showButtonLoading())
    try {
        let resp = yield call(sliderRequest, payload.token)
        if(resp.error !== 0) {
            yield put(showErrorMessage(resp.message))
        } else {
            yield put(setDataSlider(resp.data))
            yield put(hideErrorMessage())
        }
    } catch (error) {
        yield put(showErrorMessage('System Error', error));
    }
    yield put(hideButtonLoading())
}

function* getSpajReferal({ payload }) {
    yield put(showButtonLoading())
    try {
        let resp = yield call(referalRequest, payload.token)
        if(resp.error == 0) {
            yield put(setSpajReferal(resp.data))
        }
    } catch (error) {
        __DEV__ && console.log('Saga throw function getSpajReferal', error)
        yield put(showErrorMessage('System Error', error));
    }
    yield put(hideButtonLoading())
}

export function* doGetSlider() {
    yield takeEvery(SAGA_GET_DATA_SLIDER, getSlider)
}

export function* doGetSpajReferal() {
    yield takeEvery(SAGA_GET_SPAJ_REFERAL, getSpajReferal)
}

export default function* rootSaga() {
    yield all([
        fork(doGetSlider),
        fork(doGetSpajReferal)
    ])
}