import jwt from 'jwt-decode'
import { all, call, fork, put, takeEvery } from 'redux-saga/effects'
import AsyncStorage from '@react-native-community/async-storage'
import {
    SAGA_AUTH_ACCOUNT,
    SAGA_SIGN_IN,
    SAGA_SIGN_OUT,
    SAGA_SIGN_IN_VERIFY,
    SAGA_FORGOT_PASSWORD,
    SAGA_VERIFY_FORGOT_PASSWORD,
    ASYNC_USER_FORGOT_PASSWORD
} from '../constants'
import { Auth } from '../api'
import {
    sagaSignIn,
    setAuthAccount,
    signInSuccess,
    signOutSuccess,
    setUserForgotPassword,
    showErrorMessage,
    hideErrorMessage,
    showButtonLoading,
    hideButtonLoading,
    cacheMasterDataMsig,
    cacheMasterDataSimasJiwa,
    cacheDownlineMember,
} from '../actions'
import { cacheProductInsurance, cacheSliderDashboard } from "erbecache/model/HomeModel";
import { navigate, reset } from 'erbeservice/AppNavigation'

const signInRequest = async (accountId, loginId, password) =>
    await Auth.signInRequest(accountId, loginId, password)
        .then(resp => resp)
        .catch(error => error)

const signinVerifyRequest = async (accountId, loginId, pwd, kodeVerifikasi) =>
    await Auth.signinVerifyRequest(accountId, loginId, pwd, kodeVerifikasi)
        .then(resp => resp)
        .catch(error => error)

const signOutRequest = async (token) =>
    await Auth.singOutRequest(token)
        .then(resp => resp)
        .catch(error => error)

const accountRequest = async (account_login) =>
    await Auth.accountRequest(account_login)
        .then(resp => resp)
        .catch(error => error)

const forgotPasswordRequest = async (payload) =>
    await Auth.forgotRequest(payload)
        .then(resp => resp)
        .catch(error => error)

const verifyForgotPasswordRequest = async (accountId, kodeVerifikasi, password) =>
    await Auth.verfifyForgotRequest(accountId, kodeVerifikasi, password)
        .then(resp => resp)
        .catch(error => error)

function* authAccount({ payload }) {
    const { account_login } = payload
    __DEV__ && console.log('Saga payload function authAccount', account_login)
    yield put(showButtonLoading())
    try {
        let resp = yield call(accountRequest, payload)
        if (resp.error !== 0) {
            yield put(showErrorMessage('Login Error', resp.message))
        } else {
            yield put(setAuthAccount(resp.data))
        }
    } catch (error) {
        yield put(showErrorMessage('System Error', error))
    }
    yield put(hideButtonLoading())
}

function* signin({ payload }) {

    const { account_id, login_id, password } = payload;
    __DEV__ && console.log('Saga payload function signin', { account_id, login_id, password })

    try {
        yield put(showButtonLoading())
        let resp = yield call(signInRequest, account_id, login_id, password)
        __DEV__ && console.log('Saga resp function signin', resp)
        if (resp.message) {
            yield put(showErrorMessage('Login Error', resp.message))
            if (resp.error == 402) {
                navigate('LoginVerify', { account_id, login_id, password })
                yield put(hideButtonLoading())
            }
            yield put(hideButtonLoading())
        } else {
            yield put(hideErrorMessage())
            AsyncStorage.setItem('token', JSON.stringify(resp.data))
            const udata = jwt(resp.data)
            __DEV__ && console.log('Saga udata function signin', udata)
            const authData = { token: resp.data, user_data: JSON.parse(udata.data) }
            yield all([
                call(cacheProductInsurance, resp.data),
                call(cacheSliderDashboard, resp.data)
            ])
            yield put(signInSuccess(authData))
            yield put(cacheDownlineMember(resp.data, { data: {} }))
            yield put(cacheMasterDataMsig())
            yield put(cacheMasterDataSimasJiwa())
            yield put(hideButtonLoading())
        }
    } catch (error) {
        yield put(showErrorMessage('System Error', error.message))
        yield put(hideButtonLoading())
    }
}

function* signinVerify({ payload }) {
    const { account_id, login_id, password, kode_verifikasi } = payload;
    __DEV__ && console.log('Saga payload function signinVerify', payload)

    try {
        yield put(showButtonLoading())
        let resp = yield call(signinVerifyRequest, account_id, login_id, password, kode_verifikasi)
        if (resp.error !== 0) {
            yield put(hideButtonLoading())
            yield put(showErrorMessage('Login Error', resp.message))
        } else {
            yield put(hideButtonLoading())
            let login = { account_id, login_id, password }
            yield put(sagaSignIn(login))
        }
        __DEV__ && console.log('Saga resp function signinVerify', resp)
    } catch (error) {
        yield put(hideButtonLoading())
        yield put(showErrorMessage('System Error', error))
    }

}

function* singout({ payload }) {
    __DEV__ && console.log('Saga payload function signout', payload)
    yield put(showButtonLoading())
    try {
        let resp = yield call(signOutRequest, payload)
        console.log('resp saga signout', resp);
        if (resp.message) {
            yield put(showErrorMessage('Logout Error', resp.message))
        } else {
            AsyncStorage.removeItem('token');
            yield put(signOutSuccess())
            yield put(hideErrorMessage())
        }
    } catch (error) {
        yield put(showErrorMessage('System Error', error))
    }
    yield put(hideButtonLoading())
}

function* forgotPassword({ payload }) {
    __DEV__ && console.log('Saga payload function forgotPassword', payload)
    yield put(showButtonLoading())
    try {
        let resp = yield call(forgotPasswordRequest, payload)
        __DEV__ && console.log('Resp saga function forgotPassword', resp)
        if (resp.error !== 0) {
            yield put(showErrorMessage('Forgot Password Error', resp.message))
        } else {
            yield put(hideErrorMessage())
            AsyncStorage.setItem(ASYNC_USER_FORGOT_PASSWORD, JSON.stringify(resp.data))
            navigate('ForgotPasswordVerify', { ...resp.data })
        }
    } catch (error) {
        __DEV__ && console.log('Error Saga payload function forgotPassword', error)
    }
    yield put(hideButtonLoading())
}

function* verifyForgotPassword({ payload }) {
    const { id, kode_verifikasi, password } = payload;
    __DEV__ && console.log('Saga payload function verifyForgotPassword', payload)
    yield put(showButtonLoading())
    try {
        let resp = yield call(verifyForgotPasswordRequest, id, kode_verifikasi, password);
        if (resp.error !== 0) {
            yield put(showErrorMessage('Verifikasi Forgot Password', resp.message))
        } else {
            yield put(setUserForgotPassword(resp.data));
            if (resp.data.step === 1) {
                AsyncStorage.removeItem(ASYNC_USER_FORGOT_PASSWORD)
                navigate('Login')
            }
        }
        __DEV__ && console.log('Resp Saga function verifyForgotPassword', resp)
    } catch (error) {
        yield put(showErrorMessage('System Error', error))
    }
    yield put(hideButtonLoading())
}

export function* doAccount() {
    yield takeEvery(SAGA_AUTH_ACCOUNT, authAccount)
}

export function* doSignIn() {
    yield takeEvery(SAGA_SIGN_IN, signin);
}

export function* doSignInVerify() {
    yield takeEvery(SAGA_SIGN_IN_VERIFY, signinVerify)
}

export function* doSignOut() {
    yield takeEvery(SAGA_SIGN_OUT, singout);
}

export function* doForgotPassword() {
    yield takeEvery(SAGA_FORGOT_PASSWORD, forgotPassword)
}

export function* doVerifyForgotPassword() {
    yield takeEvery(SAGA_VERIFY_FORGOT_PASSWORD, verifyForgotPassword)
}

export default function* rootSaga() {
    yield all([
        fork(doAccount),
        fork(doSignIn),
        fork(doSignInVerify),
        fork(doSignOut),
        fork(doForgotPassword),
        fork(doVerifyForgotPassword)
    ])
}
