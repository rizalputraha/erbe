import { all,call,fork, put, takeEvery} from 'redux-saga/effects'
import { CACHE_MASTER_DATA_MSIG, CACHE_MASTER_DATA_SIMASJIWA } from '../constants'
import { showFirstLoading, hideFirstLoading } from 'erberedux/actions'
import {
    STORAGE_MSIG_MASTER_BANK,
    STORAGE_MSIG_MASTER_COUNTRY,
    STORAGE_MSIG_MASTER_GENDER,
    STORAGE_MSIG_MASTER_GRADUATE,
    STORAGE_MSIG_MASTER_GREENCARD,
    STORAGE_MSIG_MASTER_IDENTITY,
    STORAGE_MSIG_MASTER_MARRIAGE_STATUS,
    STORAGE_MSIG_MASTER_RELIGION,
    STORAGE_MSIG_MASTER_PEKERJAAN,
    STORAGE_MSIG_MASTER_PENDAPATAN,
    STORAGE_MSIG_MASTER_RELATION,
    STORAGE_MSIG_MASTER_RELATION_AHLI_WARIS,
    STORAGE_MSIG_MASTER_RELATION_PEMBAYAR,
    STORAGE_MSIG_MASTER_SUMBER_DANA,
    STORAGE_MSIG_MASTER_SUMBER_PENDAPATAN_BULAN,
    STORAGE_MSIG_MASTER_SUMBER_PENDAPATAN_TAHUN,
    STORAGE_MSIG_MASTER_TUJUAN,
    STORAGE_MSIG_MASTER_TITLE,
    STORAGE_MSIG_MASTER_QUESTIONER,
    STORAGE_MSIG_MASTER_PROFILE_RESIKO,
    STORAGE_SIMASJIWA_MASTER_BANK,
    STORAGE_SIMASJIWA_MASTER_WILAYAH,
    STORAGE_SIMASJIWA_MASTER_IDENTITY,
    STORAGE_SIMASJIWA_MASTER_GENDER,
    STORAGE_SIMASJIWA_MASTER_MARRIAGE_STATUS,
    STORAGE_SIMASJIWA_MASTER_RELIGION,
    STORAGE_SIMASJIWA_MASTER_GRADUATE,
    STORAGE_SIMASJIWA_MASTER_QUESTIONER,
    STORAGE_SIMASJIWA_MASTER_COUNTRY,
    STORAGE_SIMASJIWA_MASTER_RELATION
} from 'erbecache/constant/MasterAsuransi'

import {
    loadCacheMasterData,
    saveCacheMasterData
} from 'erbecache/model/MasterModel'
import { Supplier } from '../api'

const getBankMsig = async () => await Supplier.reqBankMsig() .then(resp => resp) .catch (err=>err)
const getCountryMsig = async () => await Supplier.reqCountryMsig() .then(resp => resp) .catch (err=>err)
const getGenderMsig = async () => await Supplier.reqGenderMsig() .then(resp => resp) .catch (err=>err)
const getGraduateMsig = async () => await Supplier.reqGraduateMsig() .then(resp => resp) .catch (err=>err)
const getGreenCardMsig = async () => await Supplier.reqGreenCardMsig() .then(resp => resp) .catch (err=>err)
const getIdentityMsig = async () => await Supplier.reqIdentityMsig() .then(resp => resp) .catch (err=>err)
const getMarriageStatusMsig = async () => await Supplier.reqMarriageStatusMsig() .then(resp => resp) .catch (err=>err)
const getReligionMsig = async () => await Supplier.reqReligionMsig() .then(resp => resp) .catch (err=>err)
const getPekerjaanMsig = async () => await Supplier.reqPekerjaanMsig() .then(resp => resp) .catch (err=>err)
const getPendapatanMsig = async () => await Supplier.reqPendapatanMsig() .then(resp => resp) .catch (err=>err)
const getRelationMsig = async () => await Supplier.reqRelationMsig() .then(resp => resp) .catch (err=>err)
const getRelationAhliWarisMsig = async () => await Supplier.reqRelationAhliWarisMsig() .then(resp => resp) .catch (err=>err)
const getRelationPembayarMsig = async () => await Supplier.reqRelationPembayarMsig() .then(resp => resp) .catch (err=>err)
const getSumberDanaMsig = async () => await Supplier.reqSumberDanaMsig() .then(resp => resp) .catch (err=>err)
const getSumberPendapatanBulanMsig = async () => await Supplier.reqSumberPendapatanBulanMsig() .then(resp => resp) .catch (err=>err)
const getSumberPendapatanTahunMsig = async () => await Supplier.reqSumberPendapatanTahunMsig() .then(resp => resp) .catch (err=>err)
const getTujuanMsig = async () => await Supplier.reqTujuanMsig() .then(resp => resp) .catch (err=>err)
const getTitleMsig = async () => await Supplier.reqTitleMsig() .then(resp => resp) .catch (err=>err)
const getQuestionerMsig = async () => await Supplier.reqQuestionerMsig() .then(resp => resp) .catch (err=>err)
const getProfileResiko = async () => await Supplier.reqProfileResikoMsig() .then(resp => resp) .catch (err=>err)

export function* doCacheMsig() {
    try {
        yield put(showFirstLoading())

        const [
            newBank, 
            newCountry, 
            newGender, 
            newGraduate, 
            newGreenCard,
            newIdentity,
            newMarriageStatus,
            newReligion,
            newPekerjaan,
            newPendapatan,
            newRelation,
            newRelationAhliWaris,
            newRelationPembayar,
            newSumberDana,
            newSumberPendapatanBulan,
            newSumberPendapatanTahun,
            newTujuan,
            newTitle,
            newQuestioner,
            newProfileResiko
        ] = yield all([
            call(getBankMsig),
            call(getCountryMsig),
            call(getGenderMsig),
            call(getGraduateMsig),
            call(getGreenCardMsig),
            call(getIdentityMsig),
            call(getMarriageStatusMsig),
            call(getReligionMsig),
            call(getPekerjaanMsig),
            call(getPendapatanMsig),
            call(getRelationMsig),
            call(getRelationAhliWarisMsig),
            call(getRelationPembayarMsig),
            call(getSumberDanaMsig),
            call(getSumberPendapatanBulanMsig),
            call(getSumberPendapatanTahunMsig),
            call(getTujuanMsig),
            call(getTitleMsig),
            call(getQuestionerMsig),
            call(getProfileResiko)
        ])

        const [
            oldBank,
            oldCountry,
            oldGender,
            oldGraduate,
            oldGreenCard,
            oldIdentity,
            oldMarriageStatus,
            oldReligion,
            oldPekerjaan,
            oldPendapatan,
            oldRelation,
            oldRelationAhliWaris,
            oldRelationPembayar,
            oldSumberDana,
            oldSumberPendapatanBulan,
            oldSumberPendapatanTahun,
            oldTujuan,
            oldTitle,
            oldQuestioner,
            oldProfileResiko
        ] = yield all([
            call(loadCacheMasterData, STORAGE_MSIG_MASTER_BANK),
            call(loadCacheMasterData, STORAGE_MSIG_MASTER_COUNTRY),
            call(loadCacheMasterData, STORAGE_MSIG_MASTER_GENDER),
            call(loadCacheMasterData, STORAGE_MSIG_MASTER_GRADUATE),
            call(loadCacheMasterData, STORAGE_MSIG_MASTER_GREENCARD),
            call(loadCacheMasterData, STORAGE_MSIG_MASTER_IDENTITY),
            call(loadCacheMasterData, STORAGE_MSIG_MASTER_MARRIAGE_STATUS),
            call(loadCacheMasterData, STORAGE_MSIG_MASTER_RELIGION),
            call(loadCacheMasterData, STORAGE_MSIG_MASTER_PEKERJAAN),
            call(loadCacheMasterData, STORAGE_MSIG_MASTER_PENDAPATAN),
            call(loadCacheMasterData, STORAGE_MSIG_MASTER_RELATION),
            call(loadCacheMasterData, STORAGE_MSIG_MASTER_RELATION_AHLI_WARIS),
            call(loadCacheMasterData, STORAGE_MSIG_MASTER_RELATION_PEMBAYAR),
            call(loadCacheMasterData, STORAGE_MSIG_MASTER_SUMBER_DANA),
            call(loadCacheMasterData, STORAGE_MSIG_MASTER_SUMBER_PENDAPATAN_BULAN),
            call(loadCacheMasterData, STORAGE_MSIG_MASTER_SUMBER_PENDAPATAN_TAHUN),
            call(loadCacheMasterData, STORAGE_MSIG_MASTER_TUJUAN),
            call(loadCacheMasterData, STORAGE_MSIG_MASTER_TITLE),
            call(loadCacheMasterData, STORAGE_MSIG_MASTER_QUESTIONER),
            call(loadCacheMasterData, STORAGE_MSIG_MASTER_PROFILE_RESIKO)
        ])

        yield call(saveCacheMasterData, STORAGE_MSIG_MASTER_BANK, oldBank, newBank)
        yield call(saveCacheMasterData, STORAGE_MSIG_MASTER_COUNTRY, oldCountry, newCountry)
        yield call(saveCacheMasterData, STORAGE_MSIG_MASTER_GENDER, oldGender, newGender)
        yield call(saveCacheMasterData, STORAGE_MSIG_MASTER_GRADUATE, oldGraduate, newGraduate)
        yield call(saveCacheMasterData, STORAGE_MSIG_MASTER_GREENCARD, oldGreenCard, newGreenCard)
        yield call(saveCacheMasterData, STORAGE_MSIG_MASTER_IDENTITY, oldIdentity, newIdentity)
        yield call(saveCacheMasterData, STORAGE_MSIG_MASTER_MARRIAGE_STATUS, oldMarriageStatus, newMarriageStatus)
        yield call(saveCacheMasterData, STORAGE_MSIG_MASTER_RELIGION, oldReligion, newReligion)
        yield call(saveCacheMasterData, STORAGE_MSIG_MASTER_PEKERJAAN, oldPekerjaan, newPekerjaan)
        yield call(saveCacheMasterData, STORAGE_MSIG_MASTER_PENDAPATAN, oldPendapatan, newPendapatan)
        yield call(saveCacheMasterData, STORAGE_MSIG_MASTER_RELATION, oldRelation, newRelation)
        yield call(saveCacheMasterData, STORAGE_MSIG_MASTER_RELATION_AHLI_WARIS, oldRelationAhliWaris, newRelationAhliWaris)
        yield call(saveCacheMasterData, STORAGE_MSIG_MASTER_RELATION_PEMBAYAR, oldRelationPembayar, newRelationPembayar)
        yield call(saveCacheMasterData, STORAGE_MSIG_MASTER_SUMBER_DANA, oldSumberDana, newSumberDana)
        yield call(saveCacheMasterData, STORAGE_MSIG_MASTER_SUMBER_PENDAPATAN_BULAN, oldSumberPendapatanBulan, newSumberPendapatanBulan)
        yield call(saveCacheMasterData, STORAGE_MSIG_MASTER_SUMBER_PENDAPATAN_TAHUN, oldSumberPendapatanTahun, newSumberPendapatanTahun)
        yield call(saveCacheMasterData, STORAGE_MSIG_MASTER_TUJUAN, oldTujuan, newTujuan)
        yield call(saveCacheMasterData, STORAGE_MSIG_MASTER_TITLE, oldTitle, newTitle)
        yield call(saveCacheMasterData, STORAGE_MSIG_MASTER_QUESTIONER, oldQuestioner, newQuestioner)
        yield call(saveCacheMasterData, STORAGE_MSIG_MASTER_PROFILE_RESIKO, oldProfileResiko, newProfileResiko)
        
        yield put(hideFirstLoading())
    } catch (error) {
        yield put(hideFirstLoading())
        __DEV__ && console.log('Error Supplier Saga doCacheMsig', error)
    }
}

const getBankSimasJiwa = async () => await Supplier.reqBankSimasJiwa() .then(resp => resp) .catch (err=>err)
const getWilayahSimasJiwa = async () => await Supplier.reqWilayahSimasJiwa() .then(resp => resp) .catch (err=>err)
const getIdentitySimasJiwa = async () => await Supplier.reqIdentitySimasJiwa() .then(resp => resp) .catch (err=>err)
const getGenderSimasJiwa = async () => await Supplier.reqGenderSimasJiwa() .then(resp => resp) .catch (err=>err)
const getMarriageStatusSimasJiwa = async () => await Supplier.reqMarriageStatusSimasJiwa() .then(resp => resp) .catch (err=>err)
const getReligionSimasJiwa = async () => await Supplier.reqReligionSimasJiwa() .then(resp => resp) .catch (err=>err)
const getGraduateSimasJiwa = async () => await Supplier.reqGraduateSimasJiwa() .then(resp => resp) .catch (err=>err)
const getQuestionerSimasJiwa = async () => await Supplier.reqQuestionerSimasJiwa() .then(resp => resp) .catch (err=>err)
const getCountrySimasJiwa = async () => await Supplier.reqCountrySimasJiwa() .then(resp => resp) .catch (err=>err)
const getRelationSimasJiwa = async () => await Supplier.reqRelationSimasJiwa() .then(resp => resp) .catch (err=>err)


export function* doCacheSimasJiwa() {
    try {
        yield put(showFirstLoading())
        const [
            newBank,
            newWilayah,
            newIdentity,
            newGender,
            newMarriageStatus,
            newReligion,
            newGraduate,
            newQuestioner,
            newCountry,
            newRelation
        ] = yield all([
            call(getBankSimasJiwa),
            call(getWilayahSimasJiwa),
            call(getIdentitySimasJiwa),
            call(getGenderSimasJiwa),
            call(getMarriageStatusSimasJiwa),
            call(getReligionSimasJiwa),
            call(getGraduateSimasJiwa),
            call(getQuestionerSimasJiwa),
            call(getCountrySimasJiwa),
            call(getRelationSimasJiwa)
        ])

        const [
            oldBank,
            oldWilayah,
            oldIdentity,
            oldGender,
            oldMarriageStatus,
            oldReligion,
            oldGraduate,
            oldQuestioner,
            oldCountry,
            oldRelation
        ] = yield all([
            call(loadCacheMasterData, STORAGE_SIMASJIWA_MASTER_BANK),
            call(loadCacheMasterData, STORAGE_SIMASJIWA_MASTER_WILAYAH),
            call(loadCacheMasterData, STORAGE_SIMASJIWA_MASTER_IDENTITY),
            call(loadCacheMasterData, STORAGE_SIMASJIWA_MASTER_GENDER),
            call(loadCacheMasterData, STORAGE_SIMASJIWA_MASTER_MARRIAGE_STATUS),
            call(loadCacheMasterData, STORAGE_SIMASJIWA_MASTER_RELIGION),
            call(loadCacheMasterData, STORAGE_SIMASJIWA_MASTER_GRADUATE),
            call(loadCacheMasterData, STORAGE_SIMASJIWA_MASTER_QUESTIONER),
            call(loadCacheMasterData, STORAGE_SIMASJIWA_MASTER_COUNTRY),
            call(loadCacheMasterData, STORAGE_SIMASJIWA_MASTER_RELATION)
        ])

        yield call(saveCacheMasterData, STORAGE_SIMASJIWA_MASTER_BANK, oldBank, newBank)
        yield call(saveCacheMasterData, STORAGE_SIMASJIWA_MASTER_WILAYAH, oldWilayah, newWilayah)
        yield call(saveCacheMasterData, STORAGE_SIMASJIWA_MASTER_IDENTITY, oldIdentity, newIdentity)
        yield call(saveCacheMasterData, STORAGE_SIMASJIWA_MASTER_GENDER, oldGender, newGender)
        yield call(saveCacheMasterData, STORAGE_SIMASJIWA_MASTER_MARRIAGE_STATUS, oldMarriageStatus, newMarriageStatus)
        yield call(saveCacheMasterData, STORAGE_SIMASJIWA_MASTER_RELIGION, oldReligion, newReligion)
        yield call(saveCacheMasterData, STORAGE_SIMASJIWA_MASTER_GRADUATE, oldGraduate, newGraduate)
        yield call(saveCacheMasterData, STORAGE_SIMASJIWA_MASTER_QUESTIONER, oldQuestioner, newQuestioner)
        yield call(saveCacheMasterData, STORAGE_SIMASJIWA_MASTER_COUNTRY, oldCountry, newCountry)
        yield call(saveCacheMasterData, STORAGE_SIMASJIWA_MASTER_RELATION, oldRelation, newRelation)

        yield put(hideFirstLoading())
    } catch (error) {
        yield put(hideFirstLoading())
        __DEV__ && console.log('Error Supplier Saga doCacheSimasJiwa', error)
    }
}

export function* doMasterMsig() {
    yield takeEvery(CACHE_MASTER_DATA_MSIG, doCacheMsig)
}

export function* doMasterSimasJiwa() {
    yield takeEvery(CACHE_MASTER_DATA_SIMASJIWA, doCacheSimasJiwa)
}

export default function* rootSaga() {
    yield all([
        fork(doMasterMsig),
        fork(doMasterSimasJiwa)
    ])
}