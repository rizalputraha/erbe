import { all, call, fork, put, takeEvery, delay, debounce } from 'redux-saga/effects'
import {
    ASYNC_GET_PRODUCT_INSURANCE,
    SAGA_PERUSAHAAN_ASURANSI
} from 'erberedux/constants'

import {
    setCacheInsurance,
    setPerusahaanAsuransi,
    navigateToScreen,
    hideLoadingBtnAgenSimas,
    hideLoadingBtnNasabahSimas,
    pushToScreen,
    showGlobalLoading,
    hideGlobalLoading,
    showErrorMessage,
    setSelectedPerusahaan
} from '../actions'

import { loadCacheProductInsurance } from '../../cache/model/HomeModel'
import { SAGA_SELECTED_PERUSAHAAN } from '../constants';

function* getProductInsurance() {
    yield put(hideLoadingBtnAgenSimas())
    yield put(hideLoadingBtnNasabahSimas())
    yield put(showGlobalLoading())
    try {
        let resp = yield call(loadCacheProductInsurance)
        yield put(setCacheInsurance(resp))
    } catch (error) {
        yield put(showErrorMessage('System Error', error.message));
    }
    yield put(hideGlobalLoading())
}

function* getCompanyInsurance({ payload }) {
    const { asuransi } = payload
    yield put(showGlobalLoading())
    try {
        yield put(setPerusahaanAsuransi(asuransi));
        yield put(pushToScreen('CompanyInsurance'));
    } catch (error) {
        yield put(showErrorMessage('System Error', error.message));
    }
    yield put(hideGlobalLoading())
}

function* getSelectedCompany({ payload }) {
    yield put(showGlobalLoading())
    try {
        yield put(setSelectedPerusahaan(payload));
        yield put(pushToScreen(payload.route, payload.name))

    } catch (error) {
        yield put(showErrorMessage('System Error', error.message))
    }
    yield put(hideGlobalLoading())
}

export function* doGetProductInsurance() {
    yield takeEvery(ASYNC_GET_PRODUCT_INSURANCE, getProductInsurance)
}

export function* doGetCompanyInsurance() {
    yield takeEvery(SAGA_PERUSAHAAN_ASURANSI, getCompanyInsurance)
}

export function* doGetSelectedCompany() {
    yield takeEvery(SAGA_SELECTED_PERUSAHAAN, getSelectedCompany)
}

export default function* rootSaga() {
    yield all([
        fork(doGetProductInsurance),
        fork(doGetCompanyInsurance),
        fork(doGetSelectedCompany)
    ])
}