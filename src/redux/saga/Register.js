/**
 * Async storage use register
 * REGISTER: {
 * 
 * }
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects'
import AsyncStorage from '@react-native-community/async-storage'
import { Register } from '../api'
import {
    SAGA_REGISTER_GET_REFERAL,
    SAGA_REGISTER_USER,
    SAGA_REGISTER_VERIFY,
    ASYNC_REGISTER_REFERAL,
    ASYNC_REGISTER_UPLINE,
    ASYNC_REGISTER_USER
}
from '../constants' 

import {
    setRegisterReferal,
    setRegisterUser,
    sagaSignIn,
    showButtonLoading,
    hideButtonLoading,
    showErrorMessage,
    hideErrorMessage
} from '../actions'

import { navigate, reset } from 'erbeservice/AppNavigation'

const referalRequest = async(referal) =>
    await Register.referalReq(referal)
    .then(resp => resp)
    .catch(error => error)

const registerRequest = async (data) => 
    await Register.registerReq(data)
    .then(resp => resp)
    .catch(error => error)

const registerVerifyRequest = async (data) =>
    await Register.verifyReq(data)
    .then(resp => resp)
    .catch(error => error)

function* getReferal({payload}) {
    __DEV__ && console.log('saga getReferal payload', payload)
    yield put(showButtonLoading())
    try {
        let resp = yield call(referalRequest, payload.referal)
        __DEV__ && console.log('saga getReferal resp', resp)
        if(resp.error !== 0) {
            yield put(showErrorMessage(resp.message))
        } else {
            yield put(hideErrorMessage())
            yield put(setRegisterReferal(resp.data))
            AsyncStorage.setItem(ASYNC_REGISTER_REFERAL, payload.referal)
            AsyncStorage.setItem(ASYNC_REGISTER_UPLINE, JSON.stringify(resp.data))
            navigate('RegisterUser', {...resp.data})
        }
        __DEV__ && console.log('resp getReferal', resp)
    } catch (error) {
        yield put(showErrorMessage('System Error', error))
    }
    yield put(hideButtonLoading())
}

function* registerUser({payload}) {
    yield put(showButtonLoading())
    try {
        let resp = yield call(registerRequest, payload)
        if(resp.error !== 0) {
            yield put(showErrorMessage(resp.message));
        } else {
            yield put(hideErrorMessage())
            yield put(setRegisterUser(payload.user))
            AsyncStorage.setItem(ASYNC_REGISTER_USER, JSON.stringify(payload.user))
            navigate('RegisterVerify', {id: resp.data})
        }
        __DEV__ && console.log('Resp registerUser', resp)
    } catch (error) {
        yield put(showErrorMessage('System Error', error))
    }
    yield put(hideButtonLoading())
}

function* registerVerify({payload}) {
    yield put(showButtonLoading())
    try {
        let resp = yield call(registerVerifyRequest, payload)
        if(resp.error !== 0) {
            yield put(showErrorMessage(resp.message));
        } else {
            let login = {
                account_id: payload.user.email,
                login_id: 'mail',
                password: payload.user.password
            }
            yield put(sagaSignIn(login))
        }
        
    } catch (error) {
        yield put(showErrorMessage('System Error', error));
    }
    yield put(hideButtonLoading())
}

export function* doGetReferal() {
    yield takeEvery(SAGA_REGISTER_GET_REFERAL, getReferal)
}

export function* doRegister() {
    yield takeEvery(SAGA_REGISTER_USER, registerUser)
}

export function* doRegisterVerify() {
    yield takeEvery(SAGA_REGISTER_VERIFY, registerVerify)
}

export default function* rootSaga() {
    yield all([
        fork(doGetReferal),
        fork(doRegister),
        fork(doRegisterVerify)
    ])
}