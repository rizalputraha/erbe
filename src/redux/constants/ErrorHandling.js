/**
 * Action types use for loading process data async global of screen, button, and parent screen
 */

//Global loading screen handle by modal component
export const SHOW_GLOBAL_LOADING = 'show_global_loading'
export const HIDE_GLOBAL_LOADING = 'hide_global_loading'

//Button component when click show loading if process are running
export const SHOW_BUTTON_LOADING = 'show_button_loading'
export const HIDE_BUTTON_LOADING = 'hide_button_loading'

//Set loading on first app load
export const SHOW_FIRST_LOADING = 'show_first_loading'
export const HIDE_FIRST_LOADING = 'hide_first_loading'

//Show message on error handling
export const SHOW_ERROR_MESSAGE = 'show_error_message'
export const HIDE_ERROR_MESSAGE = 'hide_error_message'