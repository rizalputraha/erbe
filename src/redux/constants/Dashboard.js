//Reducers
export const SET_DATA_SLIDER = 'set_data_slider'
export const SET_SPAJ_REFERAL = 'set_spaj_referal'

//Dashboard SAGA side effect
export const SAGA_GET_DATA_SLIDER = 'saga_get_data_slider'
export const SAGA_GET_SPAJ_REFERAL = 'saga_get_spaj_referal'