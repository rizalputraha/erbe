export const GET_DOWNLINE = 'get_downline';
export const SET_DOWNLINE_SUCCESS = 'set_downline_success';

export const SET_DOWNLINE_SEARCH_KEY = 'set_downline_search_key';
export const SET_DOWNLINE_FILTER = 'set_downline_filter'

export const SET_ACTIVE_DOWNLINE = 'set_active_downline';
export const SAGA_ACTIVE_DOWNLINE = 'saga_active_downline';

export const HIDE_REFRESH_CONTROL = 'hide_refresh_control';
export const SHOW_REFRESH_CONTROL = 'show_refresh_control';

export const SET_DOWNLINE_ACTIVE = 'set_downline_active';

export const GET_KOMISI = 'get_komisi';
export const GET_KOMISI_SUCCESS = 'get_komisi_success';

export const CACHE_DOWNLINE = 'cache_downline';