export const SET_KEY_ROOT_SIMAS = 'set_key_root_simas';
export const RESET_CACHE_SIMAS = 'reset_cache_data';
export const SAGA_NEXT_SCREEN_SIMAS = 'saga_next_screen_simas'

export const SET_STATE_PROFILE_FROM_AUTHUSER_SIMAS = 'set_state_profile_from_authuser_simas'

// Storage
export const STORAGE_SPAJ_SIMAS = 'storage_spaj_simas'
export const SAGA_SUBMIT_FORM_SIMAS = 'saga_submit_form_simas'
export const SAGA_SAVE_SUBMIT_FORM_SIMAS = 'saga_save_submit_form_simas'

//Proposal
export const SET_STATE_PROPOSAL_PP_SIMAS = 'set_state_proposal_pp_simas'
export const SET_STATE_PROPOSAL_TT_SIMAS = 'set_state_proposal_tt_simas'
export const SET_STATE_PROPOSAL_PRODUCT_SIMAS = 'set_state_proposal_product_simas'
export const SET_KEY_PROPOSAL_SIMAS = 'set_key_proposal_simas';

export const SET_PROPOSAL_PP_TO_TT_SIMAS = 'set_proposal_pp_to_tt_simas'

// Spaj
export const SET_STATE_SPAJ_PP_SIMAS = 'set_state_spaj_pp_simas';
export const SET_STATE_SPAJ_TT_SIMAS = 'set_state_spaj_tt_simas';
export const SET_STATE_SPAJ_PM_SIMAS = 'set_state_spaj_pm_simas';

export const SET_SPAJ_PP_TO_TT_SIMAS = 'set_spaj_pp_to_tt_simas';

//Signature
export const SET_STATE_SIGNATURE_SIMAS = 'set_state_signature_simas'
export const SET_SIGNATURE_SIMAS = 'set_signature_simas'

// Questionare
export const LOAD_STORAGE_QUESTIONER_SIMAS = 'load_storage_questioner_simas'
export const SET_ANSWER_HEALTH_QUESTIONER_OPTION_SIMAS = 'set_state_answer_questioner_option_simas'
export const SET_ANSWER_HEALTH_QUESTIONER_TEXT_SIMAS = 'set_state_answer_questioner_text_simas'
export const SET_ANSWER_PROFILE_RESIKO_SIMAS = 'set_answer_profile_resiko_simas'

export const SAGA_PROPOSAL_CATEGORY_SIMAS = 'saga_proposal_category_simas'

// File Attahcment
export const SET_STATE_FILE_ATTACHMENT_SIMAS = 'set_state_file_attachment_simas'

// Loading Button
export const SHOW_LOADING_BUTTON_NASABAH_SIMAS = 'show_loading_button_nasabah_simas'
export const HIDE_LOADING_BUTTON_NASABAH_SIMAS = 'hide_loading_button_nasabah_simas'
export const SHOW_LOADING_BUTTON_AGEN_SIMAS = 'show_loading_button_agen_simas'
export const HIDE_LOADING_BUTTON_AGEN_SIMAS = 'hide_loading_button_agen_simas'
export const SHOW_LOADING_UPLOAD_DOCUMENT_SIMAS = 'show_loading_upload_document_simas'
export const HIDE_LOADING_UPLOAD_DOCUMENT_SIMAS = 'hide_loading_upload_document_simas'
export const SHOW_LOADING_GENERATE_PROPOSAL_SIMAS = 'show_loading_generate_proposal_simas'
export const HIDE_LOADING_GENERATE_PROPOSAL_SIMAS = 'hide_loading_generate_proposal_simas'
export const SHOW_LOADING_SUBMIT_SPAJ_SIMAS = 'show_loading_submit_spaj_simas'
export const HIDE_LOADING_SUBMIT_SPAJ_SIMAS = 'hide_loading_submit_spaj_simas'

// Set Downline
export const SET_STATE_DOWNLINE_SIMAS = 'set_state_downline_simas'
export const SET_PROPOSAL_IS_PARTNER = 'set_proposal_is_partner'

export const SET_STATE_URI_RESPONSE_SIMAS = 'set_state_uri_response_simas'
export const SET_STATE_FROM_CACHE_SIMAS = 'set_state_from_cache_simas'
export const SET_STATE_SPAJ_CACHE_STORAGE_SIMAS = 'set_state_spaj_cache_storage_simas'