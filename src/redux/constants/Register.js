//Need save referal when receive deep or app link when hit api get referal
export const SET_REGISTER_REFERAL = 'set_register_referal'
export const SET_REGISTER_USER = 'set_register_user'
export const SET_REGISTER_VERIFY = 'set_register_verify'

//Need some action type for side effect
export const SAGA_REGISTER_GET_REFERAL= 'saga_register_get_referal'
export const SAGA_REGISTER_USER = 'saga_register_user'
export const SAGA_REGISTER_VERIFY = 'saga_register_verify'

//Caching with async storage to save upline, and new user from save referal
export const ASYNC_REGISTER_REFERAL = 'async_register_referal'
export const ASYNC_REGISTER_UPLINE = 'async_register_upline'
export const ASYNC_REGISTER_USER = 'async_register_user'