/**
 * Defined the server site url including base api, site url for images, and payment
 */

//Local testing environment server
let localBaseUrl = 'http://10.10.0.129/api/'
let localSiteUrl = 'http://10.10.0.129/'

//Development environment server
let devBaseUrl = 'https://dev.erbenetwork.com/api/'
let devSiteUrl = 'https://dev.erbenetwork.com/'

//Production environment server
let prodBaseUrl = 'https://www.erbenetwork.com/api/'
let prodSiteUrl = 'https://www.erbenetwork.com/'

//Payment Url
let devPaymentUrl = 'https://erbedev.nextg.id/'
let prodPaymentUrl = 'https://erbe.nextg.id/'

//Supplier Url
const devSupplierUrl = 'https://devsupplier.erbenetwork.com/api/'
const prodSupplierUrl = 'https://supplier.erbenetwork.com/api/'

export const BASE_URL = __DEV__ ? devBaseUrl : prodBaseUrl
export const SITE_URL = __DEV__ ? devSiteUrl : prodSiteUrl
export const PAYMENT_URL = __DEV__ ? devPaymentUrl : prodPaymentUrl
export const SUPPLIER_URL = __DEV__ ? devSupplierUrl : prodSupplierUrl

// Define APP ID Erbe or Bumss
/**
 * Registered APP ID on server
 * 00 = Erbenetwork
 * 01 = Erbe Bumss Warranty
 */

export const APP_ID = '00'