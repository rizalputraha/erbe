/**
 * Constant Msig 
 * Use to action set reducers, side effect trough api submit data and uploading files
 * version 19.02.05
 */

/**
 * @param {String} RESET_CACHE_STATE_MSIG relied on INIT_STATE
 * reset state reducer msig to default based on INIT_STATE
 */
export const RESET_CACHE_STATE_MSIG = 'reset_cache_state_msig'

/**
 * @param {String} SET_STATE_FROM_CACHE_MSIG relied on cache Msig AsynStorage
 */
export const SET_STATE_FROM_CACHE_MSIG = 'set_state_from_cache_msig'

/**
 * @param {String} SET_STATE_ROOT_MSIG arguments [rootState, data]
 * used to set root reducer state like isPartner, isTertanggung, isCalonPembayar, product, periodeBayar
 */
export const SET_STATE_ROOT_MSIG = 'set_state_root_msig';

/**
 * @param {String} SET_STATE_PARTNER_MSIG arguments [isPartner, partner]
 * used to set reducer state isPartner and partner
 */
export const SET_STATE_PARTNER_MSIG = 'set_state_partner_msig';

/**
 * @param {String} SET_STATE_PROFILE_FROM_AUTHUSER arguments [authUser]
 * used to set namaPp, emailPp, phonePp, bankPp, etc relied on props authUser object
 */
export const SET_STATE_PROFILE_FROM_AUTHUSER_MSIG = 'set_state_profile_from_authuser'

/**
 * @param {String} SET_STATE_PROPOSAL_MSIG arguments [keyState, paramState, data]
 * used to set reducer state proposal pemegang polis
 */
export const SET_STATE_PROPOSAL_MSIG = 'set_state_proposal_msig';

/**
 * @param {String} SET_STATE_PROPOSAL_IS_TERTANGGUNG_MSIG relied on argument [pemegangPolis]
 * used to set state proposal tertanggung from state proposal pemegangPolis with condition if isTertanggung is equal true
 */
export const SET_STATE_PROPOSAL_IS_TERTANGGUNG_MSIG = 'set_state_proposal_is_tertanggung_msig'

/**
 * @param {String} SET_STATE_SPAJ_SIGNATURE_KEY_MSIG arguments [spajKey, imei, signatureKey ]
 * used to set reducer state SPAJ Signature and espay with device imei
 */
export const SET_STATE_SPAJ_SIGNATURE_KEY_MSIG = 'set_state_spaj_signature_key_msig';

/**
 * @param {String} SET_STATE_SPAJ_MSIG arguments [keyState, paramState, data]
 * used to set reducer state SPAJ Espaj 
 * pemegangPolis, tertanggung, calonPembayar, usulanAsuransi, detilInvestasi, detilAgen, profileResiko, kuisioner
 */
export const SET_STATE_SPAJ_MSIG = 'set_state_spaj_msig';

/**
 * @param {String} SET_STATE_SPAJ_IS_TERTANGGUNG_MSIG arguments [pemegangPolis] relied on spaj.espaj.pemegangPolis
 */
export const SET_STATE_SPAJ_IS_TERTANGGUNG_MSIG = 'set_state_spaj_is_tertanggung_msig'

/**
 * @param {String} SET_STATE_SPAJ_TERTANGGUNG_MSIG arguments [pemegangPolis] relied on spaj.espaj.pemegangPolis
 */
export const SET_STATE_SPAJ_TERTANGGUNG_MSIG = 'set_state_spaj_tertanggung_msig'

/**
 * @param {String} SET_STATE_SPAJ_CALON_PEMBAYAR_MSIG arguments [pemegangPolis] relied on spaj.espaj.pemegangPolis
 * Used to set reducer state SPAJ ESpaj calonPembayar default is pemegangPolis
 */
export const SET_STATE_SPAJ_CALON_PEMBAYAR_MSIG = 'set_state_spaj_calon_pembayar_msig'

/**
 * @param {String} SET_STATE_SPAJ_USULAN_ASURANSI_MSIG arguments [product, periodeBayar, proposal] relied on product, periodeBayar, proposal state
 * When users pick the product, the state product change already
 * Usulan asuransi reference to product, this is used to set state ESPAJ ESPAJ usulanAsuransi relied on product state
 */
export const SET_STATE_SPAJ_USULAN_ASURANSI_MSIG = 'set_state_spaj_usulan_asuransi_msig'


/**
 * @param {String} SET_STATE_SPAJ_INVESTASI_MSIG agruments [product, pemegangPolis] relied on product and pemegangPolis state
 * Use to set state of spaj.espaj.investasi
 */
export const SET_STATE_SPAJ_INVESTASI_MSIG = 'set_state_spaj_investasi_msig'

/**
 * @param {String} SET_STATE_SPAJ_AHLI_WARIS_MSIG arguments [paramState, data] relied on penerima_manfaat_di state inside of investasi
 * Use to set State of spaj.espaj.investasi.penerima_manfaat_di
 */
export const SET_STATE_SPAJ_AHLI_WARIS_MSIG = 'set_state_spaj_ahli_waris_msig'

/**
 * @param {String} SET_STATE_SPAJ_QUESTIONER_ANSWER_MSIG arguments [...paramState] relied on setStateSpajQuestionerAnswerMsig Actions
 * Set the answer of list questions based on user answer
 */
export const SET_STATE_SPAJ_QUESTIONER_ANSWER_MSIG = 'set_state_spaj_questioner_answer_msig'

/**
 * @param {String} SET_STATE_SPAJ_PROFILE_RESIKO_MSIG arguments [indexOfProfileResiko answerPoint]
 * Set the answer of list profile resiko msig base on users choice
 */
export const SET_STATE_SPAJ_PROFILE_RESIKO_MSIG = 'set_state_spaj_profile_resiko_msig'

/**
 * @param {String} SET_STATE_SPAJ_CACHE_STORAGE_MSIG arguments [questioner, profileResiko] relied on STORAGE_MSIG_MASTER_QUESTIONER AND STORAGE_MSIG_MASTER_PROFILE_RESIKO
 * Set state questioner list of question and profileResiko based on cache data
 */
export const SET_STATE_SPAJ_CACHE_STORAGE_MSIG = 'set_state_spaj_cache_storage_msig'

/**
 * @param {String} SET_STATE_SCREENSHOOT_MSIG arguments [paramState, data]
 * used to set reducer state screenshoot
 * data result on base64
 */
export const SET_STATE_SCREENSHOOT_MSIG = 'set_state_screenshoot_msig'

/**
 * @param {String} SET_STATE_SIGNATURE_MSIG arguments [keyState, paramState, signatureBase64]
 * used to set reducer assign signature pemegangPolis, tertanggung, calonPembayar
 */
export const SET_STATE_SIGNATURE_MSIG = 'set_state_signature_msig'

/**
 * @param {String} SET_STATE_FILE_ATTACHMENT_MSIG arguments [keyState, paramState, fileBase64]
 * used to set reducer fileAttachment pemegangPolis, tertanggung, calonPembayar
 */
export const SET_STATE_FILE_ATTACHMENT_MSIG = 'set_state_file_attachment_msig'

/**
 * @param {String} SAGA_SUBMIT_FORM_MSIG arguments [payloadReducers]
 * used to submit data and processing by server
 * when success saga will execute clear existing cache
 * if failed submit data, saga will show error message, and jum navigate to routeName of SPAJ screen
 * Success: relied on NAVIGATE_ROUTE reducer Navigation and CLEAR_CACHE_SUBMIT_FORM_MSIG
 * Failed: relied on SHOW_ERROR_MESSAGE reducer ErrorHandling and NAVIGATE_ROUTE reducer Navigation
 */
export const SAGA_SUBMIT_FORM_MSIG = 'saga_submit_form_msig'

/**
 * @param {String} SET_STATE_URI_RESPONSE_MSIG
 * Set the state uriResponse based on upload document api Insurance response data url
 */
export const SET_STATE_URI_RESPONSE_MSIG = 'set_state_uri_response_msig'

 /**
 * @param {String} SAGA_SAVE_SUBMIT_FORM_MSIG arguments [payloadReducers]
 * used to save the state to AsynStorage
 * This function call in FormSubmitSPaj
 */
export const SAGA_SAVE_SUBMIT_FORM_MSIG = 'saga_save_submit_form_msig'

/**
 * @param {String} STORAGE_SPAJ_MSIG
 * Used as nameStorage when cache the state of Msig reducers to AsyncStorage
 */
export const STORAGE_SPAJ_MSIG = 'storage_spaj_msig'

/**
 * Handle move screen button
 */
export const SAGA_PROPOSAL_CATEGORY_MSIG = 'saga_proposal_category_msig'
export const SAGA_NEXT_SCREEN_MSIG = 'saga_next_screen_msig'

/**
 * Handle loading button
 */
export const SHOW_LOADING_BUTTON_NASABAH_MSIG = 'show_loading_button_nasabah_msig'
export const HIDE_LOADING_BUTTON_NASABAH_MSIG = 'hide_loading_button_nasabah_msig'
export const SHOW_LOADING_BUTTON_AGEN_MSIG = 'show_loading_button_agen_msig'
export const HIDE_LOADING_BUTTON_AGEN_MSIG = 'hide_loading_button_agen_msig'
export const SHOW_LOADING_UPLOAD_DOCUMENT_MSIG = 'show_loading_upload_document_msig'
export const HIDE_LOADING_UPLOAD_DOCUMENT_MSIG = 'hide_loading_upload_document_msig'
export const SHOW_LOADING_GENERATE_PROPOSAL_MSIG = 'show_loading_generate_proposal_msig'
export const HIDE_LOADING_GENERATE_PROPOSAL_MSIG = 'hide_loading_generate_proposal_msig'
export const SHOW_LOADING_SUBMIT_SPAJ_MSIG = 'show_loading_submit_spaj_msig'
export const HIDE_LOADING_SUBMIT_SPAJ_MSIG = 'hide_loading_submit_spaj_msig'

/**
 * @param {String} SET_STATE_WILAYAH_MSIG Wilayah MSIG
 */
export const SET_STATE_WILAYAH_MSIG = 'set_state_wilayah_msig'