//Dispatcher action type for Insurance props
export const ASYNC_SET_PRODUCT_INSURANCE = 'async_set_product_insurance'
export const SET_PERUSAHAAN_ASURANSI = 'set_perusahaan_asuransi'
export const SET_SELECTED_PERUSAHAAN = 'set_selected_perusahaan'


//Some side effect for insurance props
export const ASYNC_GET_PRODUCT_INSURANCE = 'async_get_product_insurance'
export const SAGA_PERUSAHAAN_ASURANSI = 'saga_perusahaan_asuransi'
export const SAGA_SELECTED_PERUSAHAAN = 'saga_selected_perusahaan'


