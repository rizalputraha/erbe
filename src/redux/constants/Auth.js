//Reducers Store Management
export const SET_AUTH_ACCOUNT = 'set_auth_account'
export const SIGN_IN_SUCCESS = 'sign_in_success'
export const SIGN_OUT_SUCCESS = 'sign_out_success'

//Need some action type for side effect
export const SAGA_AUTH_ACCOUNT = 'saga_account'
export const SAGA_SIGN_IN = 'saga_sign_in'
export const SAGA_SIGN_IN_VERIFY = 'saga_sign_in_verify'
export const SAGA_SIGN_OUT = 'saga_sign_out'
export const SAGA_FORGOT_PASSWORD = 'saga_forgot_password'
export const SAGA_VERIFY_FORGOT_PASSWORD = 'saga_verify_forgot_password'

//Save token to storage
export const SET_AUTH_USER = 'set_auth_user'
export const SET_USER_FORGOT_PASSWORD = 'set_user_forgot_password'
export const ASYNC_USER_FORGOT_PASSWORD = 'async_user_forgot_password'