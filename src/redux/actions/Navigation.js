import {
    NAVIGATE_PUSH,
    NAVIGATE_SHARE,
    NAVIGATE_ROUTE,
    NAVIGATE_POP_TO_ROOT,
    NAVIGATE_POP
} from '../constants'

export const pushToScreen = (screen,param=undefined) => {
    return {
        type: NAVIGATE_PUSH,
        payload: {screen,param}
    }
}

export const shareButton = (content,contentOptions) => {
    return {
        type: NAVIGATE_SHARE,
        payload: {content,contentOptions}
    }
}

export const navigateToScreen = (routeName, params=undefined) => {
    return {
        type: NAVIGATE_ROUTE,
        payload: { routeName, params }
    }
}

export const navigatePopToRoot = (params = undefined) => {
    return {
        type: NAVIGATE_POP_TO_ROOT,
        payload: { params }
    }
}

export const navigatePop = (n) => {
    return {
        type: NAVIGATE_POP,
        payload: { n }
    }
}