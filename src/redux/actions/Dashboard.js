import {
    SET_DATA_SLIDER,
    SAGA_GET_DATA_SLIDER,
    SET_SPAJ_REFERAL,
    SAGA_GET_SPAJ_REFERAL
} from '../constants'

export const setDataSlider = (data) => {
    return {
        type: SET_DATA_SLIDER,
        payload: data
    }
}

export const sagaGetDataSlider = (token) => {
    return {
        type: SAGA_GET_DATA_SLIDER,
        payload: {token}
    }
}

export const setSpajReferal = (data) => {
    return {
        type: SET_SPAJ_REFERAL,
        payload: data
    }
}

export const sagaGetSpajReferal = (token) => {
    return {
        type: SAGA_GET_SPAJ_REFERAL,
        payload: {token}
    }
}