/**
 * Action Msig
 * version 19.02.11
 * Used to change the reducer and saga side effect relied on constant Msig, reducer Msig, saga Msig
 */

import { 
    RESET_CACHE_STATE_MSIG,
    SET_STATE_FROM_CACHE_MSIG,
    SET_STATE_ROOT_MSIG,
    SET_STATE_PARTNER_MSIG,
    SET_STATE_PROFILE_FROM_AUTHUSER_MSIG,
    SET_STATE_PROPOSAL_MSIG,
    SET_STATE_PROPOSAL_IS_TERTANGGUNG_MSIG,
    SET_STATE_SPAJ_SIGNATURE_KEY_MSIG,
    SET_STATE_SPAJ_MSIG,
    SET_STATE_SPAJ_IS_TERTANGGUNG_MSIG,
    SET_STATE_SPAJ_TERTANGGUNG_MSIG,
    SET_STATE_SPAJ_CALON_PEMBAYAR_MSIG,
    SET_STATE_SPAJ_USULAN_ASURANSI_MSIG,
    SET_STATE_SPAJ_INVESTASI_MSIG,
    SET_STATE_SPAJ_AHLI_WARIS_MSIG,
    SET_STATE_SPAJ_QUESTIONER_ANSWER_MSIG,
    SET_STATE_SPAJ_PROFILE_RESIKO_MSIG,
    SET_STATE_SPAJ_CACHE_STORAGE_MSIG,
    SET_STATE_SCREENSHOOT_MSIG,
    SET_STATE_SIGNATURE_MSIG,
    SET_STATE_FILE_ATTACHMENT_MSIG,
    SET_STATE_URI_RESPONSE_MSIG,
    SAGA_PROPOSAL_CATEGORY_MSIG,
    SAGA_SUBMIT_FORM_MSIG,
    SAGA_SAVE_SUBMIT_FORM_MSIG,
    SHOW_LOADING_BUTTON_NASABAH_MSIG,
    HIDE_LOADING_BUTTON_NASABAH_MSIG,
    SHOW_LOADING_BUTTON_AGEN_MSIG,
    HIDE_LOADING_BUTTON_AGEN_MSIG,
    SHOW_LOADING_UPLOAD_DOCUMENT_MSIG,
    HIDE_LOADING_UPLOAD_DOCUMENT_MSIG,
    SHOW_LOADING_GENERATE_PROPOSAL_MSIG,
    HIDE_LOADING_GENERATE_PROPOSAL_MSIG,
    SHOW_LOADING_SUBMIT_SPAJ_MSIG,
    HIDE_LOADING_SUBMIT_SPAJ_MSIG,
    SAGA_NEXT_SCREEN_MSIG,
    SET_STATE_WILAYAH_MSIG
} from '../constants'

/**
 * Reset cache reducer Msig based on INIT STATE
 */
export const resetCacheStateMsig = () => {
    return {
        type: RESET_CACHE_STATE_MSIG
    }
}

/**
 * @param {Any} stateData As object name from cache AsynStorage
 */
export const setStateFromCacheMsig = (stateData) => {
    return {
        type: SET_STATE_FROM_CACHE_MSIG,
        payload: { stateData }
    }
}

/**
 * 
 * @param {String} keyState As object name of reducer like isTertanggung
 * @param {Any} data As value of reducer object
 */
export const setStateRootMsig = (keyState, data) => {
    return {
        type: SET_STATE_ROOT_MSIG,
        payload: { keyState, data }
    }
}

/**
 * 
 * @param {Any} downline Relied on cache STORAGE_DOWNLINE_MEMBER
 */
export const setStatePartnerMsig = (downline) => {
    return {
        type: SET_STATE_PARTNER_MSIG,
        payload: { downline }
    }
}

/**
 * 
 * @param {String} keyState as object key names like pemegangPolis
 * @param {String} paramState as param state key name like namaPp
 * @param {String} data as value of reducer state
 */
export const setStateProposalMsig = (keyState, paramState, data) => {
    return {
        type: SET_STATE_PROPOSAL_MSIG,
        payload: { keyState, paramState, data }
    }
}

/**
 * 
 * @param {Any} authUser as object authUser from props Auth
 */
export const setStateProfileFromAuthUserMsig = (authUser) => {
    return {
        type: SET_STATE_PROFILE_FROM_AUTHUSER_MSIG,
        payload: { authUser }
    }
}

/**
 * 
 * @param {Any} pp as object relied on state proposal pemegangPolis
 */
export const setStateProposalIsTertanggungMsig = (pp) => {
    return {
        type: SET_STATE_PROPOSAL_IS_TERTANGGUNG_MSIG,
        payload: { pp }
    }
}

/**
 * @param {String} imei relied on reducer api DeviceReader imei
 * @param {String} uniqueId relied on reducer api DeviceReader uniqueId
 */
export const setStateSpajSignatureKeyMsig = (imei, uniqueId) => {
    return {
        type: SET_STATE_SPAJ_SIGNATURE_KEY_MSIG,
        payload: { imei, uniqueId }
    }
}

/**
 * 
 * @param {String} keyState as object key names like pemegangPolis
 * @param {String} paramState as param state key name like namaPp
 * @param {String} data as value of reducer state
 */
export const setStateSpajMsig = (keyState, paramState, data) => {
    return {
        type: SET_STATE_SPAJ_MSIG,
        payload: { keyState, paramState, data }
    }
}

/**
 * @param {Any} pemegangPolis as object relied on state spaj.espaj.pemegangPolis
 * Set object spaj.espaj tertanggung from pemegangPolis when isTertanggung true
 */
export const setStateSpajIsTertanggungMsig = (pemegangPolis) => {
    return {
        type: SET_STATE_SPAJ_IS_TERTANGGUNG_MSIG,
        payload: { pemegangPolis }
    }
}

/**
 * @param {Any} pemegangPolis as object relied on state spaj.espaj.pemegangPolis
 * Only set render lembaga tempat bekerja like
 * nmPerusahaanTt, alamatKantorTt, klasifikasiPekerjaanTt, jobDescTt
 */
export const setStateSpajTertanggungMsig = (pemegangPolis) => {
    return {
        type: SET_STATE_SPAJ_TERTANGGUNG_MSIG,
        payload: { pemegangPolis }
    }
}

/**
 * 
 * @param {Any} pemegangPolis as object relied on state spaj.espaj.pemegangPolis
 * This func call on saga nextScreen phase MsigPemegangPolis
 * Used to set state spaj.espaj.calonPembayar relied on spaj.espaj.pemegangPolis
 */
export const setStateSpajCalonPembayarMsig = (pemegangPolis) => {
    return {
        type: SET_STATE_SPAJ_CALON_PEMBAYAR_MSIG,
        payload: { pemegangPolis }
    }
}

/**
 * @param {Any} product as object relied on state product
 * @param {Any} periodeBayar as object relied on state periodeBayar
 * @param {Any} proposal as object relied on state proposal
 * This func call on componentDidMount screen FormUsulanAsuransi
 * Used to set state spaj.espaj.usulanAsuransi based on product pick by users 
 */
export const setStateSpajUsulanAsuransi = (product, periodeBayar, proposal) => {
    return {
        type: SET_STATE_SPAJ_USULAN_ASURANSI_MSIG,
        payload: { product, periodeBayar, proposal}
    }
}

/**
 * 
 * @param {Any} product as object relied on state product
 * @param {Any} pemegangPolis as object relied on state spaj.espaj.pemegangPolis
 * This func call on componentDidMount screen FormPenerimaManfaat
 */
export const setStateSpajInvestasiMsig = (product, pemegangPolis) => {
    return {
        type: SET_STATE_SPAJ_INVESTASI_MSIG,
        payload: { product, pemegangPolis }
    }
}

/**
 * @param {String} paramState as object key inside of penerima_manfaat_di state spaj.espaj.investasi
 * @param {String} data as value relied on component input or etc
 */
export const setStateSpajAhliWarisMsig = (paramState, data) => {
    return {
        type: SET_STATE_SPAJ_AHLI_WARIS_MSIG,
        payload: { paramState, data }
    }
}

/**
 * 
 * @param {Number} index is index of questioner
 * @param {Boolean} isOption if is options true, set answerOptions AND if false set answerList
 * @param {String} optionParam is key object inside of answerOption
 * @param {Boolean} optionValue is value ya or tidak based on users press the button
 * @param {Number} indexAnswerList os index of answerList
 * @param {String} answerTextValue 
 */
export const setStateSpajQuestionerAnswerMsig = (index, isOption, optionParam = undefined, optionValue = undefined, indexAnswerList = undefined, answerTextValue = undefined) => {
    return {
        type: SET_STATE_SPAJ_QUESTIONER_ANSWER_MSIG,
        payload: { index, isOption, optionParam, optionValue, indexAnswerList, answerTextValue }
    }
}

/**
 * @param {Array} questioner is the list of questioner from AsyncStorage relied on STORAGE_MSIG_MASTER_QUESTIONER
 * @param {Array} profileResiko is the list of questioner from AsyncStorage relied on STORAGE_MSIG_MASTER_PROFILE_RESIKO
 */
export const setStateSpajCacheStorageMsig = (questioner, profileResiko) => {
    return {
        type: SET_STATE_SPAJ_CACHE_STORAGE_MSIG,
        payload: { questioner, profileResiko }
    }
}

/**
 * 
 * @param {Number} index is the key index from profileResiko state
 * @param {Number} point is the point inside of answer array base on user choice
 */
export const setStateSpajProfileResikoMsig = (index, point) => {
    return {
        type: SET_STATE_SPAJ_PROFILE_RESIKO_MSIG,
        payload: { index, point }
    }
}

/**
 * 
 * @param {String} paramState as object screen key names like screenPp
 * @param {String} data base64 result of screen capture
 */
export const setStateScreenshootMsig = (paramState, data) => {
    return {
        type: SET_STATE_SCREENSHOOT_MSIG,
        payload: { paramState, data }
    }    
}

/**
 * 
 * @param {String} paramState as object key pemegangPolis or tertanggung relied on state signature
 * @param {data} md5File as md5 file name of signature save on txt
 */
export const setStateSignatureMsig = (paramState, md5File) => {
    return {
        type: SET_STATE_SIGNATURE_MSIG,
        payload: { paramState, md5File }
    }
}

export const setFileAttachmentMsig = (paramState, md5File) => {
    return {
        type: SET_STATE_FILE_ATTACHMENT_MSIG,
        payload: { paramState, md5File }
    }
}
/**
 * 
 * @param {String} buttonType as button name nasabah or agen
 * Integrated with saga msig
 */
export const sagaProposalCategoryMsig = (buttonType) => {
    return {
        type: SAGA_PROPOSAL_CATEGORY_MSIG,
        payload: { buttonType }
    }
}

/**
 * 
 * @param {String} prevRouteName as previouse route navigation name
 * @param {String} nextRouteName as next route navigation name
 * @param {Any} state as object parent screen from reducer to run validation
 * @param {Any} authUser from props authUser to set default on starter create proposal
 * @param {Any} device from props device to set signature key and imei
 */
export const sagaNextScreenMsig = (prevRouteName, nextRouteName, state, authUser=undefined, device=undefined, capture=undefined) => {
    return {
        type: SAGA_NEXT_SCREEN_MSIG,
        payload: { prevRouteName, nextRouteName, state, authUser, device, capture}
    }
}

// Upload the file and submit the state to create proposal and spaj
export const sagaSubmitFormMsig = () => {
    return {
        type: SAGA_SUBMIT_FORM_MSIG
    }
}

/**
 * 
 * @param {String} paramState as paramState of imgSignature, imgFileAttachment, imgScreenShoot or pdfDocument
 * @param {String} keyState as key of state insite paramState
 * @param {Any} responseUri rellied on response data
 */
export const setStateUriResponseMsig = (paramState, keyState, responseUri) => {
    return {
        type: SET_STATE_URI_RESPONSE_MSIG,
        payload: { paramState, keyState, responseUri }
    }
}

// Save state reducer Msig to AsynStorage
export const sagaSaveSubmitFormMsig = () => {
    return {
        type: SAGA_SAVE_SUBMIT_FORM_MSIG
    }
}

/**
 * Handle loading button
 */
export const showLoadingBtnNasabahMsig = () => {
    return {
        type: SHOW_LOADING_BUTTON_NASABAH_MSIG
    }
}

export const hideLoadingBtnNasabahMsig = () => {
    return {
        type: HIDE_LOADING_BUTTON_NASABAH_MSIG
    }
}

export const showLoadingBtnAgenMsig = () => {
    return {
        type: SHOW_LOADING_BUTTON_AGEN_MSIG
    }
}

export const hideLoadingBtnAgenMsig = () => {
    return {
        type: HIDE_LOADING_BUTTON_AGEN_MSIG
    }
}

export const showLoadingUploadDocumentMsig = () => {
    return {
        type: SHOW_LOADING_UPLOAD_DOCUMENT_MSIG
    }
}

export const hideLoadingUploadDocumentMsig = () => {
    return {
        type: HIDE_LOADING_UPLOAD_DOCUMENT_MSIG
    }
}

export const showLoadingGenerateProposalMsig = () => {
    return {
        type: SHOW_LOADING_GENERATE_PROPOSAL_MSIG
    }
}

export const hideLoadingGenerateProposalMsig = () => {
    return {
        type: HIDE_LOADING_GENERATE_PROPOSAL_MSIG
    }
}

export const showLoadingSubmitSpajMsig = () => {
    return {
        type: SHOW_LOADING_SUBMIT_SPAJ_MSIG
    }
}

export const hideLoadingSubmitSpajMsig = () => {
    return {
        type: HIDE_LOADING_SUBMIT_SPAJ_MSIG
    }
}

export const setStateWilayahMsig = (keyState, paramState, data) => {
    return {
        type: SET_STATE_WILAYAH_MSIG,
        payload: { keyState, paramState, data }
    }
}