import {
    SIGN_IN_SUCCESS,
    SIGN_OUT_SUCCESS,
    SAGA_AUTH_ACCOUNT,
    SAGA_SIGN_IN,
    SAGA_SIGN_IN_VERIFY,
    SAGA_SIGN_OUT,
    SAGA_FORGOT_PASSWORD,
    SAGA_VERIFY_FORGOT_PASSWORD,
    SET_AUTH_USER,
    SET_AUTH_ACCOUNT,
    SET_USER_FORGOT_PASSWORD
} from '../constants'

export const setAuthAccount = (account) => {
    return {
        type: SET_AUTH_ACCOUNT,
        payload: account
    }
}

export const signInSuccess = (authUser) => {
    return {
        type: SIGN_IN_SUCCESS,
        payload: authUser
    }
}

export const signOutSuccess = () => {
    return {
        type: SIGN_OUT_SUCCESS
    }
}

export const sagaAccount = (account) => {
    return {
        type: SAGA_AUTH_ACCOUNT,
        payload: account
    }
}

export const sagaSignIn = (user) => {
    return {
        type: SAGA_SIGN_IN,
        payload: user
    }
}

export const sagaSigninVerify = (user) => {
    return {
        type: SAGA_SIGN_IN_VERIFY,
        payload: user
    }
}

export const sagaSignOut = (token) => {
    return {
        type: SAGA_SIGN_OUT,
        payload: token
    }
}

export const setAuthUser = (authData) => {
    return {
        type: SET_AUTH_USER,
        payload: authData
    }
}

export const sagaForgotPassword = (accountId) => {
    return {
        type: SAGA_FORGOT_PASSWORD,
        payload: {
            id: accountId
        }
    }
}

export const sagaVerifyForgotPassword = (account) => {
    return {
        type: SAGA_VERIFY_FORGOT_PASSWORD,
        payload: account
    }
}

export const setUserForgotPassword = (user) => {
    return {
        type: SET_USER_FORGOT_PASSWORD,
        payload: user
    }
}