import {
    SET_KEY_ROOT_SIMAS,
    SET_KEY_PROPOSAL_SIMAS,
    SET_STATE_PROPOSAL_PP_SIMAS,
    SET_STATE_PROPOSAL_TT_SIMAS,
    SET_STATE_PROPOSAL_PRODUCT_SIMAS,
    SET_STATE_SIGNATURE_SIMAS,
    SET_SIGNATURE_SIMAS,
    SET_STATE_SPAJ_PP_SIMAS,
    SET_STATE_SPAJ_TT_SIMAS,
    SET_STATE_SPAJ_PM_SIMAS,
    SET_PROPOSAL_PP_TO_TT_SIMAS,
    SET_SPAJ_PP_TO_TT_SIMAS,
    LOAD_STORAGE_QUESTIONER_SIMAS,
    SET_ANSWER_HEALTH_QUESTIONER_TEXT_SIMAS,
    SET_ANSWER_HEALTH_QUESTIONER_OPTION_SIMAS,
    SAGA_NEXT_SCREEN_SIMAS,
    SAGA_PROPOSAL_CATEGORY_SIMAS,
    SHOW_LOADING_BUTTON_AGEN_SIMAS,
    HIDE_LOADING_BUTTON_AGEN_SIMAS,
    SHOW_LOADING_BUTTON_NASABAH_SIMAS,
    HIDE_LOADING_BUTTON_NASABAH_SIMAS,
    RESET_CACHE_SIMAS,
    SET_STATE_PROFILE_FROM_AUTHUSER_SIMAS,
    SET_ANSWER_PROFILE_RESIKO_SIMAS,
    SET_STATE_FILE_ATTACHMENT_SIMAS,
    SET_STATE_DOWNLINE_SIMAS,
    SET_PROPOSAL_IS_PARTNER,
    SHOW_LOADING_GENERATE_PROPOSAL_SIMAS,
    HIDE_LOADING_GENERATE_PROPOSAL_SIMAS,
    SHOW_LOADING_SUBMIT_SPAJ_SIMAS,
    HIDE_LOADING_SUBMIT_SPAJ_SIMAS,
    SHOW_LOADING_UPLOAD_DOCUMENT_SIMAS,
    HIDE_LOADING_UPLOAD_DOCUMENT_SIMAS,
    SAGA_SUBMIT_FORM_SIMAS,
    SAGA_SAVE_SUBMIT_FORM_SIMAS,
    SET_STATE_FROM_CACHE_SIMAS,
    SET_STATE_URI_RESPONSE_SIMAS
} from '../constants'

export const setKeyRootSimas = (key, data) => {
    return {
        type: SET_KEY_ROOT_SIMAS,
        payload: { key, data }
    }
}

export const setStateFromCacheSimas = (stateData) => {
    return {
        type: SET_STATE_FROM_CACHE_SIMAS,
        payload: { stateData }
    }
}

export const setKeyProposalSimas = (key, data) => {
    return {
        type: SET_KEY_PROPOSAL_SIMAS,
        payload: { key, data }
    }
}

export const setStateProposalPPSimas = (paramState, data) => {
    return {
        type: SET_STATE_PROPOSAL_PP_SIMAS,
        payload: { paramState, data }
    }
}

export const setStateProposalTTSimas = (paramState, data) => {
    return {
        type: SET_STATE_PROPOSAL_TT_SIMAS,
        payload: { paramState, data }
    }
}

export const setStateProposalProductSimas = (paramState, data) => {
    return {
        type: SET_STATE_PROPOSAL_PRODUCT_SIMAS,
        payload: { paramState, data }
    }
}

export const setStateSpajSignatureSimas = (paramState, data) => {
    return {
        type: SET_STATE_SIGNATURE_SIMAS,
        payload: { paramState, data }
    }
}

export const setSignatureSimas = (data) => {
    return {
        type: SET_SIGNATURE_SIMAS,
        payload: { data }
    }
}

export const setStateSpajPPSimas = (paramState, data) => {
    return {
        type: SET_STATE_SPAJ_PP_SIMAS,
        payload: { paramState, data }
    }
}


export const setStateSpajTTSimas = (paramState, data) => {
    return {
        type: SET_STATE_SPAJ_TT_SIMAS,
        payload: { paramState, data }
    }
}

export const setStateSpajPMSimas = (paramState, data) => {
    return {
        type: SET_STATE_SPAJ_PM_SIMAS,
        payload: { paramState, data }
    }
}

export const setProposalPPtoTTSimas = (data) => {
    return {
        type: SET_PROPOSAL_PP_TO_TT_SIMAS,
        payload: data,
    }
}

export const setSpajPPtoTTSimas = (data) => {
    return {
        type: SET_SPAJ_PP_TO_TT_SIMAS,
        payload: data,
    }
}

export const loadStorageQuestionerSimas = (data, productId) => {
    return {
        type: LOAD_STORAGE_QUESTIONER_SIMAS,
        payload: { data, productId }
    }
}

export const setStateSpajCacheStorageSimas = (questioner, profileResiko) => {
    return {
        type: SET_STATE_SPAJ_CACHE_STORAGE_SIMAS,
        payload: { questioner, profileResiko }
    }
}

export const setAnswerHealthQuestionerTextSimas = (paramState, data, index) => {
    return {
        type: SET_ANSWER_HEALTH_QUESTIONER_TEXT_SIMAS,
        payload: { index, paramState, data }
    }
}

export const setAnswerHealthQuestionerOptionSimas = (paramState, data, index) => {
    return {
        type: SET_ANSWER_HEALTH_QUESTIONER_OPTION_SIMAS,
        payload: { index, paramState, data }
    }
}

export const sagaProposalCategorySimas = (buttonType) => {
    return {
        type: SAGA_PROPOSAL_CATEGORY_SIMAS,
        payload: { buttonType }
    }
}

export const showLoadingBtnNasabahSimas = () => {
    return {
        type: SHOW_LOADING_BUTTON_NASABAH_SIMAS
    }
}

export const showLoadingBtnAgenSimas = () => {
    return {
        type: SHOW_LOADING_BUTTON_AGEN_SIMAS
    }
}

export const hideLoadingBtnNasabahSimas = () => {
    return {
        type: HIDE_LOADING_BUTTON_NASABAH_SIMAS
    }
}

export const hideLoadingBtnAgenSimas = () => {
    return {
        type: HIDE_LOADING_BUTTON_AGEN_SIMAS
    }
}

export const showLoadingUploadDocumentSimas = () => {
    return {
        type: SHOW_LOADING_UPLOAD_DOCUMENT_SIMAS
    }
}

export const hideLoadingUploadDocumentSimas = () => {
    return {
        type: HIDE_LOADING_UPLOAD_DOCUMENT_SIMAS
    }
}

export const showLoadingGenerateProposalSimas = () => {
    return {
        type: SHOW_LOADING_GENERATE_PROPOSAL_SIMAS
    }
}

export const hideLoadingGenerateProposalSimas = () => {
    return {
        type: HIDE_LOADING_GENERATE_PROPOSAL_SIMAS
    }
}

export const showLoadingSubmitSpajSimas = () => {
    return {
        type: SHOW_LOADING_SUBMIT_SPAJ_SIMAS
    }
}

export const hideLoadingSubmitSpajSimas = () => {
    return {
        type: HIDE_LOADING_SUBMIT_SPAJ_SIMAS
    }
}

export const sagaNextScreenSimas = (prevRouteName, nextRouteName, state, authUser = undefined, device = undefined) => {
    return {
        type: SAGA_NEXT_SCREEN_SIMAS,
        payload: { prevRouteName, nextRouteName, state, authUser, device }
    }
}

export const setStateProfileFromAuthUserSimas = (authUser) => {
    return {
        type: SET_STATE_PROFILE_FROM_AUTHUSER_SIMAS,
        payload: { authUser }
    }
}

export const setAnswerProfileResikoSimas = (answer) => {
    return {
        type: SET_ANSWER_PROFILE_RESIKO_SIMAS,
        payload: answer
    }
}

export const setStateFileAttachmentSimas = (paramState, data) => {
    return {
        type: SET_STATE_FILE_ATTACHMENT_SIMAS,
        payload: { paramState, data }
    }
}

export const setStateDownlineSimas = (data) => {
    return {
        type: SET_STATE_DOWNLINE_SIMAS,
        payload: { data }
    }
}

export const setProposalIsPartner = (data) => {
    return {
        type: SET_PROPOSAL_IS_PARTNER,
        payload: { data }
    }
}

export const sagaSubmitFormSimas = () => {
    return {
        type: SAGA_SUBMIT_FORM_SIMAS
    }
}

export const setStateUriResponseSimas = (paramState, keyState, responseUri) => {
    return {
        type: SET_STATE_URI_RESPONSE_SIMAS,
        payload: { paramState, keyState, responseUri }
    }
}

export const sagaSaveSubmitFormSimas = () => {
    return {
        type: SAGA_SAVE_SUBMIT_FORM_SIMAS
    }
}

export const resetCacheStateSimas = () => {
    return {
        type: RESET_CACHE_SIMAS,
    }
}


