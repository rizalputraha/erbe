import { 
    SHOW_GLOBAL_LOADING,
    HIDE_GLOBAL_LOADING,
    SHOW_BUTTON_LOADING,
    HIDE_BUTTON_LOADING,
    SHOW_ERROR_MESSAGE,
    HIDE_ERROR_MESSAGE,
    SHOW_FIRST_LOADING,
    HIDE_FIRST_LOADING
} from '../constants'
import { showMessage, hideMessage } from 'react-native-flash-message'

export const showGlobalLoading = () => {
    return {
        type: SHOW_GLOBAL_LOADING
    }
}

export const hideGlobalLoading = () => {
    return {
        type: HIDE_GLOBAL_LOADING
    }
}

export const showButtonLoading = () => {
    return {
        type: SHOW_BUTTON_LOADING
    }
}

export const hideButtonLoading = () => {
    return {
        type: HIDE_BUTTON_LOADING
    }
}

export const showFirstLoading = () => {
    return {
        type: SHOW_FIRST_LOADING
    }
}

export const hideFirstLoading = () => {
    return {
        type: HIDE_FIRST_LOADING
    }
}
/**
 * 
 * @param { String } message -> Message Title flash message
 * @param { String } description -> Error Description on body
 * @param { String } type -> Type or error can be danger, default, warning, etc
 * @param { String } backgroundColor -> The background color for body
 * @param { String } color -> Color text message and description 
 */
export const showErrorMessage = (message, description='', type='default', backgroundColor='black', color='white') => {
    const flashErrorMessage = {
        message,
        description,
        type,
        backgroundColor,
        color
    }
    showMessage(flashErrorMessage)
    return {
        type: SHOW_ERROR_MESSAGE,
        payload: message
    }
}

export const hideErrorMessage = () => {
    hideMessage()
    return {
        type: HIDE_ERROR_MESSAGE
    }
}