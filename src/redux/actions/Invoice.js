import {  
   GET_TAGIHAN,
   GET_TAGIHAN_SUCCESS
} from "../constants";

export const getTagihan = (token) => {
   return {
      type: GET_TAGIHAN,
      payload: {token}
   }
}

export const getTagihanSuccess = (data) => {
   return {
      type: GET_TAGIHAN_SUCCESS,
      payload: {data}
   }
}