import {
    CACHE_MASTER_DATA_MSIG,
    CACHE_MASTER_DATA_SIMASJIWA
} from '../constants'


export const cacheMasterDataMsig = () => {
    return {
        type: CACHE_MASTER_DATA_MSIG
    }
}

export const cacheMasterDataSimasJiwa = () => {
    return {
        type: CACHE_MASTER_DATA_SIMASJIWA
    }
}