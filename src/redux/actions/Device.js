import { SET_DEVICES_PARAMS } from '../constants'

export const setDeviceParams = (deviceInfo) => {
    return {
        type: SET_DEVICES_PARAMS,
        payload: deviceInfo
    }
}