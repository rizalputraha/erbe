import {
   GET_DOWNLINE,
   GET_KOMISI,
   GET_KOMISI_SUCCESS,
   CACHE_DOWNLINE,
   SET_DOWNLINE_SUCCESS,
   SHOW_REFRESH_CONTROL,
   HIDE_REFRESH_CONTROL,
   SET_DOWNLINE_SEARCH_KEY,
   SET_DOWNLINE_FILTER,
   SET_DOWNLINE_ACTIVE
} from "../constants";

export const getDownline = (token, data) => {
   return {
      type: GET_DOWNLINE,
      payload: { token, data }
   }
}

export const setDownlineSuccess = (data) => {
   return {
      type: SET_DOWNLINE_SUCCESS,
      payload: data
   }
}

export const setDownlineSearchKey = (searchKey) => {
   return {
      type: SET_DOWNLINE_SEARCH_KEY,
      payload: searchKey
   }
}

export const setDownlineFilter = (searchKey) => {
   return {
      type: SET_DOWNLINE_FILTER,
      payload: searchKey
   }
}

export const setDownlineActive = (selectedDownline) => {
   return {
      type: SET_DOWNLINE_ACTIVE,
      payload: selectedDownline
   }
}

export const getKomisi = (token) => {
   return {
      type: GET_KOMISI,
      payload: { token }
   }
}
export const getKomisiSuccess = (data) => {
   return {
      type: GET_KOMISI_SUCCESS,
      payload: data
   }
}

export const cacheDownlineMember = (token, data) => {
   return {
      type: CACHE_DOWNLINE,
      payload: { token, data }
   }
}

export const hideRefreshControl = () => {
   return {
      type: HIDE_REFRESH_CONTROL
   }
}

export const showRefreshControl = () => {
   return {
      type: SHOW_REFRESH_CONTROL
   }
}