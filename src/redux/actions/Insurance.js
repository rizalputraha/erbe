import {
    ASYNC_SET_PRODUCT_INSURANCE,
    ASYNC_GET_PRODUCT_INSURANCE,
    SET_PERUSAHAAN_ASURANSI,
    SAGA_PERUSAHAAN_ASURANSI,
    SET_SELECTED_PERUSAHAAN,
    SAGA_SELECTED_PERUSAHAAN
} from '../constants'

export const setCacheInsurance = (data) => {
    return {
        type: ASYNC_SET_PRODUCT_INSURANCE,
        payload: {data}
    }
}

export const getCacheInsurance = () => {
    return {
        type: ASYNC_GET_PRODUCT_INSURANCE,
    }
}

export const setPerusahaanAsuransi = (data) => {
    return {
        type: SET_PERUSAHAAN_ASURANSI,
        payload: {data}
    }
}

export const sagaPerusahaanAsuransi = (data) => {
    return {
        type: SAGA_PERUSAHAAN_ASURANSI,
        payload: data,
    }
}

export const setSelectedPerusahaan = (data) => {
    return {
        type: SET_SELECTED_PERUSAHAAN,
        payload: {data}
    }
}

export const sagaSelectedPerusahaan = (data) => {
    return {
        type: SAGA_SELECTED_PERUSAHAAN,
        payload: data
    }
}