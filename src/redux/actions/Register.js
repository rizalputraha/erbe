import {
    SET_REGISTER_REFERAL,
    SET_REGISTER_USER,
    SET_REGISTER_VERIFY,
    SAGA_REGISTER_USER,
    SAGA_REGISTER_VERIFY,
    SAGA_REGISTER_GET_REFERAL
}
from '../constants'

export const setRegisterReferal = (upline) => {
    return {
        type: SET_REGISTER_REFERAL,
        payload: upline
    }
}

export const setRegisterUser = (user) => {
    return {
        type: SET_REGISTER_USER,
        payload: user
    }
}

export const setRegisterVerify = (verify) => {
    return {
        type: SET_REGISTER_VERIFY,
        payload: verify
    }
}

export const sagaRegisterGetReferal = (referal) => {
    return {
        type: SAGA_REGISTER_GET_REFERAL,
        payload: referal
    }
}

export const sagaRegisterUser = (user) => {
    return {
        type: SAGA_REGISTER_USER,
        payload: user
    }
}

export const sagaRegisterVerify = (verify) => {
    return {
        type: SAGA_REGISTER_VERIFY,
        payload: verify
    }
}