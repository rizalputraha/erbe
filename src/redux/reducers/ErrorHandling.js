import { 
    SHOW_GLOBAL_LOADING,
    HIDE_GLOBAL_LOADING,
    SHOW_BUTTON_LOADING,
    HIDE_BUTTON_LOADING,
    SHOW_FIRST_LOADING,
    HIDE_FIRST_LOADING,
    SHOW_ERROR_MESSAGE,
    HIDE_ERROR_MESSAGE,
    
} from '../constants'

const INIT_STATE = {
    errorMessage: '',
    showErrorMessage: false,
    showButtonLoading: false,
    showGlobalLoading: false,
    showAppLoading: false
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case SHOW_ERROR_MESSAGE: {
            return {
                ...state,
                errorMessage: action.payload,
                showErrorMessage: true
            }
        }

        case HIDE_ERROR_MESSAGE: {
            return {
                ...state,
                errorMessage: '',
                showErrorMessage: false
            }
        }

        case SHOW_BUTTON_LOADING: {
            return {
                ...state,
                showButtonLoading: true
            }
        }

        case HIDE_BUTTON_LOADING: {
            return {
                ...state,
                showButtonLoading: false
            }
        }
        
        case SHOW_GLOBAL_LOADING: {
            return {
                ...state,
                showGlobalLoading: true
            }
        }

        case HIDE_GLOBAL_LOADING: {
            return {
                ...state,
                showGlobalLoading: false
            }
        }

        case SHOW_FIRST_LOADING: {
            return {
                ...state,
                showAppLoading: true
            }
        }

        case HIDE_FIRST_LOADING: {
            return {
                ...state,
                showAppLoading: false
            }
        }
        
        default:
            return state
    }
}