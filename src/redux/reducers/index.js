import { persistReducer } from 'redux-persist'
import { combineReducers } from 'redux'
import Auth from './Auth'
import Device from './Device'
import ErrorHandling from './ErrorHandling'
import Report from './Report'
import Register from './Register'
import Invoice from './Invoice'
import Dashboard from './Dashboard'
import Insurance from './Insurance'
import SimasJiwaReducer from './SimasJiwa'
import MsigReducer from './Msig'
import { SimasJiwaPersistedConfig, MsigPersistedConfig } from '../persist'

export default () => combineReducers({
    Auth,
    ErrorHandling,
    Device,
    Report,
    Register,
    Invoice,
    Dashboard,
    Insurance,
    Msig: persistReducer(MsigPersistedConfig, MsigReducer),
    SimasJiwa: persistReducer(SimasJiwaPersistedConfig, SimasJiwaReducer)
})