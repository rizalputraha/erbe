/**
 * State reducer msig
 * version 19.01.30
 * @param {String} titlePp Default value '0' is equal as TUAN
 * @param {String} titleTt Default value '0' is equal as TUAN
 * @param {String} negaraPp Default value '1' is equal with Indonesia
 * @param {String} negaraTt Default value '1' is equal with Indonesia
 * @param {String} greenCardPp Default value '0' is equal passport indonesia
 * @param {String} greenCardTt Default value '0' is equal passport indonesia
 * @param {String} agamaPp relied on cache masterReligion as religionId
 * @param {String} agamaTt relied on cache masterReligion as religionId
 * @param {String} statusPp relied on cache masterMarriageStatus as marriageStatusId
 * @param {String} statusTt relied on cache masterMarriageStatus as marriageStatusId
 * @param {String} genderPp relied on cache masterGender as genderId
 * @param {String} genderTt relied on cache masterGender as genderId
 * @param {Number} buktiIdentitasPp relied on cache masterIdentity as identityId
 * @param {Number} buktiIdentitasTt relied on cache masterIdentity as identityId
 * @param {String} hubunganCpPp With condition 
 * if Calon pembayar == pemegang polis set default 40.
 * if Calon pembayar == pihak ketiga, relied on cache masterRelationPembayar
 * @param {String} alamatRumahPp condition if tempat tinggal sesuai KTP atau tidak { 1: Sesuai Ktp, 0: Tidak sesuai ktp}
 * @param {String} alamatRumahTt condition if tempat tinggal sesuai KTP atau tidak { 1: Sesuai Ktp, 0: Tidak sesuai ktp}
 * Relied on cache masterRelationPembayar
 * @param {String} klasifikasiPekerjaanPp relied on cache masterPekerjaan as pekerjaanName
 * @param {String} klasifikasiPekerjaanTt relied on cache masterPekerjaan as pekerjaanName
 * @param {Array} profileResiko relied on cache masterProfileResko
 * @param {Array} questioner relied on cache masterQuestioner
 * @param {String} noProposal relied on server generate no proposal
 * @param {String} nm_refferal_da relied on state proposal namaPp
 * @param {String} screenPp relied on cache screenshoot jpg base64 FormPemegangPolis
 * @param {String} screenTt relied on cache screenshoot jpg base64 FormTertanggung
 * @param {String} screenCp relied on cache screenshoot jpg base64 FormCalonPembayar
 * @param {String} screenUa relied on cache screenshoot jpg base64 FormUsulanAsuransi
 * @param {String} screenDi relied on cache screenshoot jpg base64 FormInvestasi
 * @param {String} screenDa relied on cache screenshoot jpg base64 FormDetilAgen
 * @param {String} screenQu relied on cache screenshoot jpg base64 FormKuisioner
 * @param {String} screenPr relied on cache screenshoot jpg base64 FormProfileResiko
 * @param {String} screenPn relied on cache screenshoot jpg base64 FormPersetujuanNasabah
 * @param {String} screenSp relied on cache screenshoot jpg base64 FormSignaturePemegangPolis
 * @param {String} screenSt relied on cache screenshoot jpg base64 FormSignatureTertanggung
 * @param {String} screenFd relied on cache screenshoot jpg base64 FormFileDocument
 */

import AsyncStorage from '@react-native-community/async-storage'
import moment from 'moment'
import md5 from 'md5'
import { Platform } from 'react-native'
import { 
    RESET_CACHE_STATE_MSIG,
    SET_STATE_FROM_CACHE_MSIG,
    SET_STATE_ROOT_MSIG,
    SET_STATE_PARTNER_MSIG,
    SET_STATE_PROFILE_FROM_AUTHUSER_MSIG,
    SET_STATE_PROPOSAL_MSIG,
    SET_STATE_PROPOSAL_IS_TERTANGGUNG_MSIG,
    SET_STATE_SPAJ_SIGNATURE_KEY_MSIG,
    SET_STATE_SPAJ_MSIG,
    SET_STATE_SPAJ_IS_TERTANGGUNG_MSIG,
    SET_STATE_SPAJ_TERTANGGUNG_MSIG,
    SET_STATE_SPAJ_CALON_PEMBAYAR_MSIG,
    SET_STATE_SPAJ_USULAN_ASURANSI_MSIG,
    SET_STATE_SPAJ_INVESTASI_MSIG,
    SET_STATE_SPAJ_AHLI_WARIS_MSIG,
    SET_STATE_SPAJ_QUESTIONER_ANSWER_MSIG,
    SET_STATE_SPAJ_PROFILE_RESIKO_MSIG,
    SET_STATE_SPAJ_CACHE_STORAGE_MSIG,
    SET_STATE_SCREENSHOOT_MSIG,
    SET_STATE_SIGNATURE_MSIG,
    SET_STATE_FILE_ATTACHMENT_MSIG,
    SET_STATE_URI_RESPONSE_MSIG,
    SHOW_LOADING_BUTTON_NASABAH_MSIG,
    HIDE_LOADING_BUTTON_NASABAH_MSIG,
    SHOW_LOADING_BUTTON_AGEN_MSIG,
    HIDE_LOADING_BUTTON_AGEN_MSIG,
    SHOW_LOADING_UPLOAD_DOCUMENT_MSIG,
    HIDE_LOADING_UPLOAD_DOCUMENT_MSIG,
    SHOW_LOADING_GENERATE_PROPOSAL_MSIG,
    HIDE_LOADING_GENERATE_PROPOSAL_MSIG,
    SHOW_LOADING_SUBMIT_SPAJ_MSIG,
    HIDE_LOADING_SUBMIT_SPAJ_MSIG,
    SET_STATE_WILAYAH_MSIG
} from '../constants'
import { remapMobilePhone, remapKodePhone } from 'erbevalidation/Helper'
const INIT_STATE = {
    keyStorage: '',
    dateCreate: '',
    isPartner: false,
    partner: {
        downlineUserId: undefined,
        downlineUsername: undefined,
        downlineEmail: undefined,
        downlinePhone: undefined
    },
    product: {},
    periodeBayar: {},
    proposal: {
        pemegangPolis: {
            namaPp: '',
            dobPp: false,
            usiaPp: false,
            genderPp: false,
        },
        tertanggung: {
            namaTt: '',
            dobTt: false,
            usiaTt: false,
            genderTt: false,
        }
    },
    spaj: {
        signature: '',
        espaj: {
            noVa: '',
            gadgetSpajKey: '',
            imei: '',
            jenisSpaj: 4,
            flagApp: 2,
            noProposal: '',
            clientKey: 'gj1WX1',
            pemegangPolis: {
                //Profile pemegang polis
                titlePp: '0',
                gelarPp: '&nbsp;',
                aliasPp: '',
                namaPp: '',
                namaIbuPp: '',
                tptLahirPp: '',
                dobPp: false,
                usiaPp: false,
                agamaPp: false,
                agamaLainPp: '',
                statusPp: false,
                genderPp: false,
                buktiIdentitasPp: false,
                identitasPp: '',
                tglBerlakuIdentitasPp: false,
                hubunganPp: '',

                //Alamat dan kewarganegaraan pemegang polis
                negaraPp: parseInt(1),
                greencardPp: parseInt(0),
                alamatRumahPp: '1',
                addressPp: '',
                propinsiIdPp: '',
                kotaIdPp: '',
                kecamatanIdPp: '',
                kelurahanIdPp: '',
                kdPosPp: '',
                kotaPp: '',
                alamatKantorPp: '',
                alamatKantorPropinsiIdPp: '',
                alamatKantorKotaIdPp: '',
                alamatKantorKecamatanIdPp: '',
                alamatKantorKelurahanIdPp: '',
                alamatKantorKdPosPp: '',
                kdPosKantorPp: '',
                kotaKantorPp: '',
                alamatTinggalPp: '',
                alamatTinggalPropinsiIdPp: '',
                alamatTinggalKotaIdPp: '',
                alamatTinggalKecamatanIdPp: '',
                alamatTinggalKelurahanIdPp: '',
                alamatTinggalKdPos: '',
                kdPosTinggalPp: '',
                kotaTinggalPp: '',
                
                //Identitas pemegang polis
                noHpPp: '',
                noTelpPp: '',
                noTelpKantorPp: '',
                noTelpRumahPp: '',
                kdTelpPp: '',
                kdTelpKantorPp: '',
                kdTelpRumahPp: '',
                emailPp: '',

                //Info pekerjaan pemegang polis
                nmPerusahaanPp: '',
                klasifikasiPekerjaanPp: '',
                jobDescPp: '',

                //Info bank pemegang polis
                npwpPp: '',
                bankIdPp: '',
                branchIdPp: '',
                namaRekPp: '',
                noRekPp: '',

                //Defautl set
                sumberDanaPp: [ { dana: 'GAJI' } ],
                tujuanPp: [ { tujuan: 'PROTEKSI' } ],
                penghasilanPp: '> RP. 10 JUTA  RP. 50 JUTA',
                tagihanRutinPp: 'Email',
                tagihanPp: '0',
                pendidikanPp: '0',
                hubunganCpPp: 40, //Relation of calonPembayar is pemegangPolis
                
                //Not mandatory payload
                jabatan_klasifikasi_pp: '',
                telp2_pp: '',
                telp2_kantor_pp: '',
                telp2_tinggal_pp: '',
                kdtelp2_pp: '',
                kdtelp2_kantor_pp: '',
                kdtelp2_tinggal_pp: '',
                ttlsuami_rt_pp: '',
                kota_lahir_ibu_pp: '',
                ttl_ibu_pp: '',
                hp2_pp: '',
                kdfax_tinggal_pp: '',
                fax_kantor_pp: '',
                bidusaha_ibu_pp: '',
                krm_polis_pp: '',
                perusahaansuami_rt_pp: '',
                jabatan_ibu_pp: '',
                pekerjaan_ibu_pp: '',
                bidusaha_suamirt_pp: '',
                penghasilan_suamithn_pp: '',
                fax_tinggal_pp: '',
                nm_ibu_pp: '',
                jabatan_ayah_pp: '',
                perusahaan_ayah_pp: '',
                jabatansuami_rt_pp: '',
                bukti_identitas_lain_pp: '',
                npwp_ibu_pp: '',
                suamirt_pp: '',
                penghasilan_ayah_pp: '',
                usia_ibu_pp: '',
                pekerjaan_ayah_pp: '',
                no_ciff_pp: '',
                perusahaan_ibu_pp: '',
                npwp_suamirt_pp: '',
                ttl_ayah_pp: '',
                pekerjaansuami_rt_pp: '',
                penghasilan_ibu_pp: '',
                kdfax_kantor_pp: '',
                bidusaha_ayah_pp: '',
                ayah_pp: '',
                npwp_ayah_pp: '',
                usiasuami_rt_pp: '',
                usia_ayah_pp: ''
            },
            tertanggung: {
                //Profile tertanggung
                titleTt: '0',
                gelarTt: '&nbsp;',
                aliasTt: '',
                namaTt: '',
                namaIbuTt: '',
                tptLahirTt: '',
                dobTt: false,
                usiaTt: false,
                agamaTt: false,
                agamaLainTt: '',
                statusTt: false,
                genderTt: false,
                buktiIdentitasTt: '',
                identitasTt: '',
                tglBerlakuIdentitasTt: '',

                //Alamat dan kewarganegaraan tertanggung
                negaraTt: parseInt(1),
                greencardTt: parseInt(0),
                alamatRumahTt: '1',
                addressTt: '',
                kotaTt: '',
                propinsiIdTt: '',
                kotaIdTt: '',
                kecamatanIdTt: '',
                kelurahanIdTt: '',
                kdPosTt: '',
                kotaKantorTt: '',
                alamatKantorPropinsiIdTt: '',
                alamatKantorKotaIdTt: '',
                alamatKantorKecamatanIdTt: '',
                alamatKantorKelurahanIdTt: '',
                alamatKantorKdPosTt: '',
                kdPosKantorTt: '',
                alamatTinggalTt: '',
                alamatTinggalPropinsiIdTt: '',
                alamatTinggalKotaIdTt: '',
                alamatTinggalKecamatanIdTt: '',
                alamatTinggalKelurahanIdTt: '',
                kotaTinggalTt: '',
                kdPosTinggalTt: '',

                //Identitas tertanggung
                noHpTt: '',
                noTelpTt: '',
                noTelpKantorTt: '',
                noTelpRumahTt: '',
                kdTelpTt: '',
                kdTelpKantorTt: '',
                kdTelpRumahTt: '',
                emailTt: '',

                //Info pekerjaan tertanggung
                nmPerusahaanTt: '',
                alamatKantorTt: '',
                klasifikasiPekerjaanTt: '',
                jobDescTt: '',

                //Info bank tertanggung
                npwpTt: '',
                namaRekTt: '',

                //Set Default
                penghasilanThnTt: '> RP. 10 JUTA  RP. 50 JUTA',
                pendidikanTt: '0',
                sumberDanaTt: [ { dana: 'GAJI' } ],
                tujuanTt: [ { tujuan: 'PROTEKSI' } ],
                tagihanRutinTt: 'Email',
                
                kdfax_tinggal_tt: '',
                jabatan_klasifikasi_tt: '',
                hp2_tt: '',
                fax_kantor_tt: '',
                kdtelp2_kantor_tt: '',
                fax_tinggal_tt: '',
                telp2_tinggal_tt: '',
                kdtelp2_tt: '',
                kdtelp2_tinggal_tt: '',
                telp2_tt: '',
                telp2_kantor_tt: ''
            },
            calonPembayar: {
                sumberpendapatan_blnrutin_cp: [ { sumber: 'GAJI' } ],
                editpihak3_cp: '',
                int_spinbisnis_cp: 0,
                int_spinpihak3_cp: 0,
                gelar_cp_phk3: '&nbsp;',
                alias_cp_phk3: '',
                int_spin_hub_dgppphk3_cp: 40, //Relation of calonPembayar is pemegangPolis
                int_spin_pekerjaan_cp: '',
                nofax_perush_cp: '',
                int_ket_cp: 40, //Relation of calonPembayar is pemegangPolis
                tujuan_cp: [ { tujuan: 'PROTEKSI' } ],
                instansi_phk3_cp: '',
                bid_usaha_cp: '',
                npwp_phk3_cp: '',
                lain_jns_pkrjaan_cp: '',
                kdpos_perush_cp: '',
                int_propinsi_cp: '',
                edit_nmpihak3_cp: '',
                notlp_phk3_cp: '',
                nama_perush_cp: '',
                tmpt_kedudukan_cp: '',
                int_spin_kewrgn_cp: parseInt(1),
                tgl_pendirian_cp: '',
                tmpt_lhrphk3_cp: '',
                telp_perush_cp: '',
                bidusaha_phk3_cp: '',
                editbisnis_cp: '',
                sumber_dana_phk3_cp: '',
                str_spin_jns_pkrjaan_cp: '',
                notlp_kntrphk3_cp: '',
                tgl_lhrphk3_cp: '',
                usia_cp_phk3: '',
                jekel_cp_phk3: '',
                status_cp_phk3: '',
                edit_emailphk3_cp: '',
                str_spintotal_thn_cp: '> RP. 10 JUTA  RP. 50 JUTA',
                str_spintotal_bln_cp: '> RP. 10 JUTA  RP. 50 JUTA',
                alasan_phk3_cp: '',
                sumberpendapatan_thn_nonrutin_cp: [ { sumber_tahun: 'GAJI' } ],
                tujuan_phk3_cp: '',
                almt_phk3_cp: '',
                almt_perush_cp: '',
                kota_perush_cp: '',
                str_spin_jabatanphk3_cp: '',
                lspr_id_cp: '',
                lska_id_cp: '',
                lskc_id_cp: '',
                lskl_id_cp: '',
                kdpos_cp: '',
                ibu_cp_phk3: '',
                agama_cp_phk3: '',
                greencard_cp_phk3: parseInt(0),
                bukti_identitas_cp_phk3: '',
                no_identitas_cp_phk3: '',
                tgl_berlaku_cp_phk3: '',
                nm_lembaga_cp_phk3: '',
                alamat_lembaga_cp_phk3: '',
                no_hp_cpbu_phk3: '',
                email_cpbu_phk3: '',
                nama_pic_cpbu_phk3: '',
                siup_no_cpbu_phk3: '',
                anggaran_dasar_no_cpbu_phk3: '',
            },
            usulanAsuransi: {
                kd_produk_ua: '',
                up_ua: '',
                carabayar_ua: '',
                askes_ua: [],
                awalpertanggungan_ua: '',
                kurs_up_ua: '',
                ekasehat_plan_ua: parseInt(1),
                masa_pembayaran_ua: '',
                cuti_premi_ua: '',
                bentukbayar_ua: parseInt(2),
                jenis_produk_ua: parseInt(25),
                sub_produk_ua: 8,
                unitlink_kombinasi_ua: 'F',
                unitlink_opsipremi_ua: parseInt(0),
                unitlink_premistandard_ua: '',
                klas_ua: parseInt(1),
                masa_pertanggungan_ua: '',
                rider_ua: [],
                akhirpertanggungan_ua: '',
                unitlink_total_ua: '',
                unitlink_kurs_total_ua: '',
                paket_ua: '',
                unitlink_kurs_ua: ''
            },
            investasi: {
                invest_bayar_premi_di: 2,
                invest_jnstab_di: 1,
                jumlah_di: '',
                premitopup_berkala_di: 1,
                premi_pokok_di: '',
                jns_dana_invest_di: [
                    {
                        persen_alokasi: parseInt(100),
                        jumlah_alokasi: parseInt(0),
                        jenis_dana: '03'
                    }
                ],
                invest_keterangan_di: '',
                autodbt_kurs_di: '',
                autodbt_tglvalid_di: '',
                premi_berkala_di: '',
                invest_berikuasa_di: parseInt(0),
                penerima_manfaat_di: [
                    {
                        nama: '',
                        jekel: false,
                        kota_lahir: '',
                        warganegara: '1',
                        ttl: '',
                        usia: '',
                        manfaat: 100,
                        hub_dgcalon_tt: ''
                    }
                ],
                premi_tunggal_di: parseInt(0),
                premitopup_tunggal_di: parseInt(0),
                autodbt_norek_di: '',
                autodbt_no_simascard_di: '',
                autodbt_premipertama_di: parseInt(0),
                invest_cabang_di: '',
                autodbt_jnstab_di: parseInt(2),
                autodbt_nama_di: '',
                invest_nama_di: '',
                premi_tambahan_di: parseInt(0),
                invest_bank_di: '',
                invest_norek_di: '',
                autodbt_tgldebet_di: '',
                invest_kurs_di: '',
                invest_tglkuasa_di: '',
                autodbt_bank_di: '',
                autodbt_aktif_di: parseInt(0),
                invest_jnsnasabah_di: parseInt(2),
                invest_kota_di: '',
                danainvest_alokasi_di: parseInt(0)
            },
            detilAgen: {
                nama_broker_da: 'erbenetwork',
                kd_pnagihan_da: '',
                bank_da: 0,
                tgl_spaj_da: moment(new Date()).format('DD/MM/YYYY'),
                nm_refferal_da: '',
                pnytaan_setuju_da: 0,
                email_da: 'erbenetwork@gmail.com',
                id_sponsor_da: '',
                check_kd_ao_da: 0,
                lsrg_id: '00',
                kd_ao_da: '527884',
                broker_da: '',
                kd_regional_da: '730100',
                id_refferal_da: '527884',
                check_broker_da: 0,
                nmlead_pnutup_da: '',
                id_penempatan_da: '',
                lca_id: '73',
                nm_regional_da: 'BANK SINARMAS',
                norek_da: '',
                lwk_id: '01',
                kd_penutup_da: '527884',
                jn_bank_da: __DEV__?'2':'59',
                nm_pnutup_da: 'Erbenetwork',
                check_kd_pribadi_da: 0,
                kd_leader_da: '527884'
            },
            profileResiko: [],
            questioner: []
        }
    },
    signature: {
        isAssignedPp: false,
        pemegangPolis: '',
        isAssignedTt: false,
        tertanggung: ''
    },
    fileAttachment: {
        pemegangPolis: '',
        tertanggung: '',
        calonPembayar: ''
    },
    screenShoot: {
        screenPp: '',
        screenTt: '',
        screenCp: '',
        screenUa: '',
        screenDi: '',
        screenDa: '',
        screenQu: '',
        screenPr: '',
        screenPn: '',
        screenSp: '',
        screenSt: '',
        screenFd: ''
    },
    isTertanggung: false,
    isCalonPembayar: false,
    isAgreeProduk: false,
    isAgreePernyataan: false,
    loadingButtonNasabah: false,
    loadingButtonAgen: false,
    loadingUploadDocument: false,
    loadingGenerateProposal: false,
    loadingSubmitSpaj: false,
    npwpOptionPp: {
        optionYa: false,
        optionTidak: false
    },
    npwpOptionTt: {
        optionYa: false,
        optionTidak: false
    },
    successFileUpload: false,
    successGenerateProposal: false,
    successSubmitSpaj: false,
    uriResponse: {
        imgSignature: {
            pemegangPolis: '',
            tertanggung: ''
        },
        imgFileAttachment: {
            pemegangPolis: '',
            tertanggung: '',
            calonPembayar: ''
        },
        imgScreenShoot: {
            screenPp: '',
            screenTt: '',
            screenCp: '',
            screenUa: '',
            screenDi: '',
            screenDa: '',
            screenQu: '',
            screenPr: '',
            screenPn: '',
            screenSp: '',
            screenSt: '',
            screenFd: ''
        },
        pdfDocument: {
            proposal: '',
            spaj: ''
        }
    }
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {

        case SET_STATE_FROM_CACHE_MSIG: {
            const { stateData } = action.payload
            return {
                ...state,
                ...stateData
            }
        }

        case RESET_CACHE_STATE_MSIG: {
            AsyncStorage.removeItem('persist:msig')
            let newState = { ...INIT_STATE}
            return {
                ...state,
                ...newState
            }
        }

        case SET_STATE_ROOT_MSIG: {
            const { keyState, data } = action.payload;
            let newState = { ...state }
            newState[keyState] = data;
            return {
                ...state,
                ...newState
            }
        }

        case SET_STATE_PARTNER_MSIG: {
            const { downline } = action.payload

            let newPartner = state.partner
            newPartner.downlineEmail = downline.email
            newPartner.downlinePhone = downline.no_telp
            newPartner.downlineUsername = downline.username
            newPartner.downlineUserId = downline.id

            let newProposal = state.proposal
            newProposal.pemegangPolis.namaPp = downline.nama.substr(0, 30)

            let newSpaj = state.spaj
            newSpaj.espaj.pemegangPolis.namaPp = downline.nama.substr(0, 30)
            newSpaj.espaj.pemegangPolis.namaRekPp = downline.nama.substr(0, 30)
            newSpaj.espaj.pemegangPolis.aliasPp = downline.nama.substr(0, 30)
            newSpaj.espaj.pemegangPolis.emailPp = downline.email
            newSpaj.espaj.pemegangPolis.noHpPp = remapMobilePhone(downline.no_telp)
            newSpaj.espaj.pemegangPolis.noTelpPp = remapMobilePhone(downline.no_telp)
            newSpaj.espaj.pemegangPolis.noTelpKantorPp = remapMobilePhone(downline.no_telp)
            newSpaj.espaj.pemegangPolis.noTelpRumahPp = remapMobilePhone(downline.no_telp)
            newSpaj.espaj.pemegangPolis.kdTelpPp = remapKodePhone(downline.no_telp)
            newSpaj.espaj.pemegangPolis.kdTelpKantorPp = remapKodePhone(downline.no_telp)
            newSpaj.espaj.pemegangPolis.kdTelpRumahPp = remapKodePhone(downline.no_telp)

            newSpaj.espaj.tertanggung.emailTt = downline.email
            newSpaj.espaj.tertanggung.noHpTt = remapMobilePhone(downline.no_telp)
            newSpaj.espaj.tertanggung.noTelpTt = remapMobilePhone(downline.no_telp)
            newSpaj.espaj.tertanggung.noTelpKantorTt = remapMobilePhone(downline.no_telp)
            newSpaj.espaj.tertanggung.noTelpRumahTt = remapMobilePhone(downline.no_telp)
            newSpaj.espaj.tertanggung.kdTelpTt = remapKodePhone(downline.no_telp)
            newSpaj.espaj.tertanggung.kdTelpKantorTt = remapKodePhone(downline.no_telp)
            newSpaj.espaj.tertanggung.kdTelpRumahTt = remapKodePhone(downline.no_telp)

            newSpaj.espaj.detilAgen.nm_refferal_da = downline.nama.substr(0, 30)

            return {
                ...state,
                isPartner: true,
                partner: { ...newPartner },
                proposal: { ...newProposal },
                spaj: { ...newSpaj }
            }
        }

        case SET_STATE_PROPOSAL_MSIG: {
            const { keyState, paramState, data } = action.payload;
            let newProposal = { ... state.proposal }
            newProposal[keyState][paramState] = data;

            let spaj = { ...state.spaj }
            let newEspaj = { ...state.spaj.espaj }

            newEspaj[keyState][paramState] = data

            if(keyState == 'tertanggung' && paramState == 'namaTt') {
                newEspaj.tertanggung.namaRekTt = data
            }

            return {
                ...state,
                proposal: { ...newProposal },
                spaj: {...spaj, ...newEspaj }
            }
        }

        /**
         * Set default nama maximum 30 character
         */
        case SET_STATE_PROFILE_FROM_AUTHUSER_MSIG: {
            const { authUser } = action.payload
            let newProposal = { ...state.proposal }
            let userData = authUser.user_data
            let nama = userData.nama
            let mobilePhone = authUser.user_data.no_telp

            newProposal.pemegangPolis.namaPp = nama.substr(0, 30)

            let newSpaj = { ...state.spaj }
            newSpaj.espaj.pemegangPolis.namaPp = nama.substr(0, 30)
            newSpaj.espaj.pemegangPolis.namaRekPp = nama.substr(0, 30)
            newSpaj.espaj.pemegangPolis.aliasPp = nama.substr(0, 30)
            newSpaj.espaj.pemegangPolis.emailPp = authUser.user_data.email
            newSpaj.espaj.pemegangPolis.noHpPp = remapMobilePhone(mobilePhone)
            newSpaj.espaj.pemegangPolis.noTelpPp = remapMobilePhone(mobilePhone)
            newSpaj.espaj.pemegangPolis.noTelpKantorPp = remapMobilePhone(mobilePhone)
            newSpaj.espaj.pemegangPolis.noTelpRumahPp = remapMobilePhone(mobilePhone)
            newSpaj.espaj.pemegangPolis.kdTelpPp = remapKodePhone(mobilePhone)
            newSpaj.espaj.pemegangPolis.kdTelpKantorPp = remapKodePhone(mobilePhone)
            newSpaj.espaj.pemegangPolis.kdTelpRumahPp = remapKodePhone(mobilePhone)

            newSpaj.espaj.tertanggung.emailTt = authUser.user_data.email
            newSpaj.espaj.tertanggung.noHpTt = remapMobilePhone(mobilePhone)
            newSpaj.espaj.tertanggung.noTelpTt = remapMobilePhone(mobilePhone)
            newSpaj.espaj.tertanggung.noTelpKantorTt = remapMobilePhone(mobilePhone)
            newSpaj.espaj.tertanggung.noTelpRumahTt = remapMobilePhone(mobilePhone)
            newSpaj.espaj.tertanggung.kdTelpTt = remapKodePhone(mobilePhone)
            newSpaj.espaj.tertanggung.kdTelpKantorTt = remapKodePhone(mobilePhone)
            newSpaj.espaj.tertanggung.kdTelpRumahTt = remapKodePhone(mobilePhone)

            newSpaj.espaj.detilAgen.nm_refferal_da = nama.substr(0, 30)

            return {
                ...state,
                proposal: {...newProposal},
                spaj: { ...newSpaj }
            }
        }

        case SET_STATE_PROPOSAL_IS_TERTANGGUNG_MSIG: {
            const { pp } = action.payload

            let newProposal = { ...state.proposal }
            newProposal.tertanggung.namaTt = pp.namaPp
            newProposal.tertanggung.dobTt = pp.dobPp
            newProposal.tertanggung.usiaTt = pp.usiaPp
            newProposal.tertanggung.genderTt = pp.genderPp

            let spaj = { ...state.spaj }
            let newEspaj = { ...state.spaj.espaj }

            newEspaj.tertanggung.namaTt = pp.namaPp
            newEspaj.tertanggung.namaRekTt = pp.namaPp
            newEspaj.tertanggung.aliasTt = pp.namaPp
            newEspaj.tertanggung.dobTt = pp.dobPp
            newEspaj.tertanggung.usiaTt = pp.usiaPp
            newEspaj.tertanggung.genderTt = pp.genderPp

            return {
                ...state,
                proposal: { ...newProposal },
                spaj: {...spaj, ...newEspaj }
            }
        }

        case SET_STATE_SPAJ_SIGNATURE_KEY_MSIG: {
            const { imei, uniqueId } = action.payload
            const spajKey = 'ERBEbroker'+moment(new Date()).format('YYYY')+'.'+moment(new Date()).format('MM')+'.'+moment(new Date()).format('DD')+'.'+moment(new Date()).format('hh')+'.'+moment(new Date()).format('mm')+'.'+moment(new Date()).format('ss')+'.'+moment(new Date()).format('SSS');
            const deviceImei = Platform.OS == 'ios' ? uniqueId : imei

            let newSignature = md5(`${spajKey}${deviceImei}`)

            let newEspaj = { ...state.spaj.espaj }
            newEspaj.gadgetSpajKey = spajKey
            newEspaj.imei = deviceImei
            return {
                ...state,
                spaj: {
                    signature: newSignature,
                    espaj: {...newEspaj}
                }
            }
        }

        case SET_STATE_SPAJ_MSIG: {
            const { keyState, paramState, data } = action.payload

            let newSpaj = {...state.spaj}
            let newSignature = newSpaj.signature
            let newEspaj = { ...state.spaj.espaj }
            newEspaj[keyState][paramState] = data
            if(keyState == 'tertanggung' && paramState == 'noHpTt') {
                newEspaj.tertanggung.noHpTt = remapMobilePhone(data)
                newEspaj.tertanggung.noTelpTt = remapMobilePhone(data)
                newEspaj.tertanggung.noTelpKantorTt = remapMobilePhone(data)
                newEspaj.tertanggung.noTelpRumahTt = remapMobilePhone(data)
                newEspaj.tertanggung.kdTelpTt = remapKodePhone(data)
                newEspaj.tertanggung.kdTelpKantorTt = remapKodePhone(data)
                newEspaj.tertanggung.kdTelpRumahTt = remapKodePhone(data)
            }
            return {
                ...state,
                spaj: {
                    signature: newSignature,
                    espaj: {...newEspaj}
                }
            }
        }

        case SET_STATE_SPAJ_IS_TERTANGGUNG_MSIG: {
            const { pemegangPolis } = action.payload
            let newSpaj = {...state.spaj}
            let newSignature = newSpaj.signature
            let newEspaj = { ...state.spaj.espaj }
            
            //Profile Tertanggung
            newEspaj.tertanggung.aliasTt = pemegangPolis.aliasPp
            newEspaj.tertanggung.namaTt = pemegangPolis.namaPp
            newEspaj.tertanggung.namaIbuTt = pemegangPolis.namaIbuPp
            newEspaj.tertanggung.tptLahirTt = pemegangPolis.tptLahirPp
            newEspaj.tertanggung.dobTt = pemegangPolis.dobPp
            newEspaj.tertanggung.usiaTt = pemegangPolis.usiaPp
            newEspaj.tertanggung.agamaTt = pemegangPolis.agamaPp
            newEspaj.tertanggung.agamaLainTt = pemegangPolis.agamaLainPp
            newEspaj.tertanggung.statusTt = pemegangPolis.statusPp
            newEspaj.tertanggung.genderTt = pemegangPolis.genderPp
            newEspaj.tertanggung.buktiIdentitasTt = pemegangPolis.buktiIdentitasPp
            newEspaj.tertanggung.identitasTt = pemegangPolis.identitasPp
            newEspaj.tertanggung.tglBerlakuIdentitasTt = pemegangPolis.tglBerlakuIdentitasPp

            //Alamat dan kewarganegaraan tertanggung
            newEspaj.tertanggung.alamatRumahTt = pemegangPolis.alamatRumahPp
            newEspaj.tertanggung.addressTt = pemegangPolis.addressPp
            newEspaj.tertanggung.kotaTt = pemegangPolis.kotaPp
            newEspaj.tertanggung.propinsiIdTt = pemegangPolis.propinsiIdPp
            newEspaj.tertanggung.kotaIdTt = pemegangPolis.kotaIdPp
            newEspaj.tertanggung.kecamatanIdTt = pemegangPolis.kecamatanIdPp
            newEspaj.tertanggung.kelurahanIdTt = pemegangPolis.kelurahanIdPp
            newEspaj.tertanggung.kdPosTt = pemegangPolis.kdPosPp
            newEspaj.tertanggung.kotaKantorTt = pemegangPolis.kotaKantorPp
            newEspaj.tertanggung.alamatKantorPropinsiIdTt = pemegangPolis.alamatKantorPropinsiIdPp
            newEspaj.tertanggung.alamatKantorKotaIdTt = pemegangPolis.alamatKantorKotaIdPp
            newEspaj.tertanggung.alamatKantorKecamatanIdTt = pemegangPolis.alamatKantorKecamatanIdPp
            newEspaj.tertanggung.alamatKantorKelurahanIdTt = pemegangPolis.alamatKantorKelurahanIdPp
            newEspaj.tertanggung.alamatKantorKdPosTt = pemegangPolis.alamatKantorKdPosPp
            newEspaj.tertanggung.kdPosKantorTt = pemegangPolis.kdPosKantorPp
            newEspaj.tertanggung.alamatTinggalTt = pemegangPolis.alamatTinggalPp
            newEspaj.tertanggung.alamatTinggalPropinsiIdTt = pemegangPolis.alamatTinggalPropinsiIdPp
            newEspaj.tertanggung.alamatTinggalKotaIdTt = pemegangPolis.alamatTinggalKotaIdPp
            newEspaj.tertanggung.alamatTinggalKecamatanIdTt = pemegangPolis.alamatTinggalKecamatanIdPp
            newEspaj.tertanggung.alamatTinggalKelurahanIdTt = pemegangPolis.alamatTinggalKelurahanIdPp
            newEspaj.tertanggung.kotaTinggalTt = pemegangPolis.kotaTinggalPp
            newEspaj.tertanggung.kdPosTinggalTt = pemegangPolis.kdPosTinggalPp

            //Identitas tertanggung
            newEspaj.tertanggung.noHpTt = pemegangPolis.noHpPp
            newEspaj.tertanggung.noTelpTt = pemegangPolis.noTelpPp
            newEspaj.tertanggung.noTelpKantorTt = pemegangPolis.noTelpKantorPp
            newEspaj.tertanggung.noTelpRumahTt = pemegangPolis.noTelpRumahPp
            newEspaj.tertanggung.kdTelpTt = pemegangPolis.kdTelpPp
            newEspaj.tertanggung.kdTelpKantorTt = pemegangPolis.kdTelpKantorPp
            newEspaj.tertanggung.kdTelpRumahTt = pemegangPolis.kdTelpRumahPp
            newEspaj.tertanggung.emailTt = pemegangPolis.emailPp

            //Info pekerjaan tertanggung
            newEspaj.tertanggung.nmPerusahaanTt = pemegangPolis.nmPerusahaanPp
            newEspaj.tertanggung.alamatKantorTt = pemegangPolis.alamatKantorPp
            newEspaj.tertanggung.klasifikasiPekerjaanTt = pemegangPolis.klasifikasiPekerjaanPp
            newEspaj.tertanggung.jobDescTt = pemegangPolis.jobDescPp

            //Info bank tertanggung
            newEspaj.tertanggung.npwpTt = pemegangPolis.npwpPp
            newEspaj.tertanggung.namaRekTt = pemegangPolis.namaRekPp

            return {
                ...state,
                spaj: {
                    signature: newSignature,
                    espaj: {...newEspaj}
                }
            }
        }

        case SET_STATE_SPAJ_TERTANGGUNG_MSIG: {
            const { pemegangPolis } = action.payload
            let newSpaj = {...state.spaj}
            let newSignature = newSpaj.signature
            let newEspaj = { ...state.spaj.espaj }

            newEspaj.tertanggung.nmPerusahaanTt = pemegangPolis.nmPerusahaanPp
            newEspaj.tertanggung.alamatKantorTt = pemegangPolis.alamatKantorPp
            newEspaj.tertanggung.klasifikasiPekerjaanTt = pemegangPolis.klasifikasiPekerjaanPp
            newEspaj.tertanggung.jobDescTt = pemegangPolis.jobDescPp
            
            return {
                ...state,
                spaj: {
                    signature: newSignature,
                    espaj: {...newEspaj}
                }
            }
        }

        case SET_STATE_SPAJ_CALON_PEMBAYAR_MSIG: {
            const { pemegangPolis } = action.payload
            let newSpaj = {...state.spaj}
            let newSignature = newSpaj.signature
            let newEspaj = { ...state.spaj.espaj }

            newEspaj.calonPembayar.kdpos_perush_cp = pemegangPolis.kdPosKantorPp
            newEspaj.calonPembayar.int_propinsi_cp = pemegangPolis.propinsiIdPp
            newEspaj.calonPembayar.tmpt_lhrphk3_cp = pemegangPolis.tptLahirPp
            newEspaj.calonPembayar.notlp_kntrphk3_cp = pemegangPolis.noHpPp
            newEspaj.calonPembayar.tgl_lhrphk3_cp = pemegangPolis.dobPp
            newEspaj.calonPembayar.usia_cp_phk3 = pemegangPolis.usiaPp
            newEspaj.calonPembayar.jekel_cp_phk3 = pemegangPolis.genderPp
            newEspaj.calonPembayar.status_cp_phk3 = pemegangPolis.statusPp
            newEspaj.calonPembayar.edit_emailphk3_cp = pemegangPolis.emailPp
            newEspaj.calonPembayar.lspr_id_cp = pemegangPolis.propinsiIdPp
            newEspaj.calonPembayar.lska_id_cp = pemegangPolis.kotaIdPp
            newEspaj.calonPembayar.lskc_id_cp = pemegangPolis.kecamatanIdPp
            newEspaj.calonPembayar.lskl_id_cp = pemegangPolis.kelurahanIdPp
            newEspaj.calonPembayar.kdpos_cp = pemegangPolis.kdPosPp
            newEspaj.calonPembayar.agama_cp_phk3 = pemegangPolis.agamaPp

            return {
                ...state,
                spaj: {
                    signature: newSignature,
                    espaj: { ...newEspaj }
                }
            }
        }

        case SET_STATE_SPAJ_USULAN_ASURANSI_MSIG: {

            const getRiderByUsia = (extProduct, umurTt) => {
                let newRider = [];
                let data = [];
                if(umurTt < 17) {
                    extProduct.rider.map((item, index) => {
                        if(item.max_tt == 17) newRider.push(item)
                    })
                } else {
                    newRider = extProduct.rider
                }
                newRider.map((a, i) => {
                    const obj = {
                        masa_rider: parseInt(100 - umurTt),
                        tglmulai_rider: moment().format("DD/MM/YYYY"),
                        tglakhir_rider: moment().format("DD/MM/YYYY"),
                        akhirbayar_rider: moment().format("DD/MM/YYYY"),
                        up_rider: parseInt(extProduct.up),
                        premi_rider: 0,
                        klas_rider: 0,
                        rate_rider: 0,
                        kode_produk_rider: parseInt(a.rider_code),
                        kode_subproduk_rider: parseInt(a.rider_sub_code),
                        tertanggung_rider: 0,
                        unit_rider: 0
                    }
                    data.push(obj)
                })
                return data
            }

            const { product, periodeBayar, proposal} = action.payload
            const masaPertanggungan = 100 - proposal.tertanggung.usiaTt
            let newSpaj = { ...state.spaj }
            let newSignature = newSpaj.signature
            let newEspaj = { ...state.spaj.espaj }

            newEspaj.usulanAsuransi.kd_produk_ua = parseInt(product.product_param.lsbs_id)
            newEspaj.usulanAsuransi.up_ua = parseInt(product.up)
            newEspaj.usulanAsuransi.carabayar_ua = parseInt(periodeBayar.periode_code)
            newEspaj.usulanAsuransi.awalpertanggungan_ua = moment().format('DD/MM/YYYY')
            newEspaj.usulanAsuransi.kurs_up_ua = product.product_param.lku_id
            newEspaj.usulanAsuransi.masa_pembayaran_ua = parseInt(product.thn_masa_bayar)
            newEspaj.usulanAsuransi.cuti_premi_ua = parseInt(product.thn_cuti_premi)
            newEspaj.usulanAsuransi.sub_produk_ua = parseInt(product.product_param.lsdbs_number)
            newEspaj.usulanAsuransi.unitlink_premistandard_ua = parseInt(product.premi_pokok)
            newEspaj.usulanAsuransi.masa_pertanggungan_ua = parseInt(masaPertanggungan)
            newEspaj.usulanAsuransi.rider_ua = getRiderByUsia(product, proposal.tertanggung.usiaTt)
            newEspaj.usulanAsuransi.akhirpertanggungan_ua = moment().add(masaPertanggungan, "years").format('DD/MM/YYYY')
            newEspaj.usulanAsuransi.unitlink_total_ua = parseInt(product.premi_topup)
            newEspaj.usulanAsuransi.paket_ua = parseInt(product.product_param.paket_ua)
            newEspaj.usulanAsuransi.unitlink_kurs_ua = parseInt(product.product_param.lku_id)

            return {
                ...state,
                spaj: {
                    signature: newSignature,
                    espaj: { ...newEspaj }
                }
            }
        }

        case SET_STATE_SPAJ_INVESTASI_MSIG: {
            const { product, pemegangPolis } = action.payload
            const { spaj } = state

            spaj.espaj.investasi.jumlah_di = product.premi
            spaj.espaj.investasi.premi_pokok_di = parseInt(product.premi_pokok)
            spaj.espaj.investasi.premi_berkala_di = parseInt(product.premi_topup)
            
            spaj.espaj.investasi.invest_nama_di = pemegangPolis.namaPp
            spaj.espaj.investasi.invest_bank_di = pemegangPolis.bankIdPp
            spaj.espaj.investasi.invest_cabang_di = pemegangPolis.branchIdPp
            spaj.espaj.investasi.invest_norek_di = pemegangPolis.noRekPp
            spaj.espaj.investasi.invest_kota_di = pemegangPolis.kotaPp
            spaj.espaj.investasi.invest_kurs_di = product.product_param.lku_id

            spaj.espaj.investasi.autodbt_nama_di = pemegangPolis.namaPp
            spaj.espaj.investasi.autodbt_bank_di = pemegangPolis.bankIdPp
            spaj.espaj.investasi.autodbt_norek_di = pemegangPolis.noRekPp
            spaj.espaj.investasi.autodbt_kurs_di = product.product_param.lku_id

            return {
                ...state,
                spaj: { ...spaj }
            }
        }

        case SET_STATE_SPAJ_AHLI_WARIS_MSIG: {
            const { paramState, data } = action.payload
            const { spaj } = state
            spaj.espaj.investasi.penerima_manfaat_di[0][paramState] = data

            return {
                ...state,
                spaj: { ...spaj }
            }
        }

        case SET_STATE_SPAJ_QUESTIONER_ANSWER_MSIG: {
            const { index, isOption, optionParam, optionValue, indexAnswerList, answerTextValue } = action.payload
            let signature = state.spaj.signature
            let espaj = state.spaj.espaj
            let objQ = state.spaj.espaj.questioner

            if(isOption === true) {
                objQ[index].answerOption.optionYa = optionParam == 'optionYa' ? optionValue : !optionValue
                objQ[index].answerOption.optionTidak = optionParam == 'optionTidak' ? optionValue : !optionValue
            } else {
                objQ[index].answer.questionerPp.berat = indexAnswerList == 1 ? answerTextValue : ''
                objQ[index].answer.questionerPp.tinggi = indexAnswerList == 0 ? answerTextValue : ''
                objQ[index].answer.questionerTt.berat = indexAnswerList == 1 ? answerTextValue : ''
                objQ[index].answer.questionerTt.tinggi = indexAnswerList == 0 ? answerTextValue : ''
                objQ[index].answerList[indexAnswerList].answerText = answerTextValue
            }
            
            return {
                ...state,
                spaj: {
                    signature,
                    espaj: {
                        ...espaj,
                        questioner: [...objQ]
                    }
                }
            }
        }

        case SET_STATE_SPAJ_PROFILE_RESIKO_MSIG: {
            const { index, point } = action.payload
            const signature = state.spaj.signature
            let espaj = state.spaj.espaj
            let objPr = state.spaj.espaj.profileResiko
            objPr[index].answerDefault = point
            
            return {
                ...state,
                spaj: {
                    signature,
                    espaj: {
                        ...espaj,
                        profileResiko: [...objPr]
                    }
                }
            }
        }

        case SET_STATE_SPAJ_CACHE_STORAGE_MSIG: {
            const { questioner, profileResiko } = action.payload
            const { spaj } = state
            spaj.espaj.questioner = questioner
            spaj.espaj.profileResiko = profileResiko

            return {
                ...state,
                spaj: {
                    ...spaj
                }
            }
        }

        case SET_STATE_SIGNATURE_MSIG: {

            const { paramState, md5File } = action.payload
            const newSignature = state.signature
            newSignature[paramState] = md5File

            return {
                ...state,
                signature: {
                    ...newSignature
                }
            }
        }

        case SET_STATE_FILE_ATTACHMENT_MSIG: {
            const { paramState, md5File } = action.payload
            const newFileAttachment = state.fileAttachment
            newFileAttachment[paramState] = md5File

            return {
                ...state,
                fileAttachment: {
                    ...newFileAttachment
                }
            }
        }

        case SET_STATE_SCREENSHOOT_MSIG: {
            const { paramState, data } = action.payload
            const { screenShoot } = state
            screenShoot[paramState] = data
            return {
                ...state,
                screenShoot: {...screenShoot}
            }
        }

        case SET_STATE_URI_RESPONSE_MSIG: {
            const { paramState, keyState, responseUri } = action.payload
            const { uriResponse } = state
            uriResponse[paramState][keyState] = responseUri
            return {
                ...state,
                uriResponse: {...uriResponse}
            }
        }

        case SHOW_LOADING_BUTTON_NASABAH_MSIG: {
            return {
                ...state,
                loadingButtonNasabah: true
            }
        }
        
        case HIDE_LOADING_BUTTON_NASABAH_MSIG: {
            return {
                ...state,
                loadingButtonNasabah: false
            }
        }

        case SHOW_LOADING_BUTTON_AGEN_MSIG: {
            return {
                ...state,
                loadingButtonAgen: true
            }
        }

        case HIDE_LOADING_BUTTON_AGEN_MSIG: {
            return {
                ...state,
                loadingButtonAgen: false
            }
        }

        case SHOW_LOADING_UPLOAD_DOCUMENT_MSIG: {
            return {
                ...state,
                loadingUploadDocument: true
            }
        }

        case HIDE_LOADING_UPLOAD_DOCUMENT_MSIG: {
            return {
                ...state,
                loadingUploadDocument: false
            }
        }

        case SHOW_LOADING_GENERATE_PROPOSAL_MSIG: {
            return {
                ...state,
                loadingGenerateProposal: true
            }
        }

        case HIDE_LOADING_GENERATE_PROPOSAL_MSIG: {
            return {
                ...state,
                loadingGenerateProposal: false
            }
        }

        case SHOW_LOADING_SUBMIT_SPAJ_MSIG: {
            return {
                ...state,
                loadingSubmitSpaj: true
            }
        }

        case HIDE_LOADING_SUBMIT_SPAJ_MSIG: {
            return {
                ...state,
                loadingSubmitSpaj: false
            }
        }

        case SET_STATE_WILAYAH_MSIG: {
            const { keyState, paramState, data } = action.payload
            
            let newSpaj = { ...state.spaj }
            let newSignature = newSpaj.signature
            let newEspaj = { ...state.spaj.espaj }

            const reduceNewPp = (newEspaj, keyState, paramState, data) => {
                if(paramState == 'propinsi') {
                    newEspaj[keyState].propinsiIdPp = data
                    newEspaj[keyState].alamatKantorPropinsiIdPp = data
                    newEspaj[keyState].alamatTinggalPropinsiIdPp = data
                    newEspaj[keyState].kotaIdPp = ''
                    newEspaj[keyState].alamatKantorKotaIdPp = ''
                    newEspaj[keyState].alamatTinggalKotaIdPp = ''
                    newEspaj[keyState].kotaPp = '' 
                    newEspaj[keyState].kotaKantorPp = ''
                    newEspaj[keyState].kotaTinggalPp = ''
                    newEspaj[keyState].kecamatanIdPp = ''
                    newEspaj[keyState].alamatKantorKecamatanIdPp = ''
                    newEspaj[keyState].alamatTinggalKecamatanIdPp = ''
                    newEspaj[keyState].kelurahanIdPp = ''
                    newEspaj[keyState].alamatKantorKelurahanIdPp = ''
                    newEspaj[keyState].alamatTinggalKelurahanIdPp = ''
                    newEspaj[keyState].kdPosPp = ''
                    newEspaj[keyState].kdPosKantorPp = ''
                    newEspaj[keyState].alamatKantorKdPosPp = ''
                    newEspaj[keyState].kdPosTinggalPp = ''
                    newEspaj[keyState].alamatTinggalKdPos = ''
                }
    
                if(paramState == 'kota') {
                    newEspaj[keyState].kotaIdPp = data
                    newEspaj[keyState].alamatKantorKotaIdPp = data
                    newEspaj[keyState].alamatTinggalKotaIdPp = data
                    newEspaj[keyState].kotaKantorPp = data
                    newEspaj[keyState].kotaPp = ''
                    newEspaj[keyState].kotaTinggalPp = ''
                    newEspaj[keyState].kecamatanIdPp = ''
                    newEspaj[keyState].alamatKantorKecamatanIdPp = ''
                    newEspaj[keyState].alamatTinggalKecamatanIdPp = ''
                    newEspaj[keyState].kelurahanIdPp = ''
                    newEspaj[keyState].alamatKantorKelurahanIdPp = ''
                    newEspaj[keyState].alamatTinggalKelurahanIdPp = ''
                    newEspaj[keyState].kdPosPp = ''
                    newEspaj[keyState].kdPosKantorPp = ''
                    newEspaj[keyState].alamatKantorKdPosPp = ''
                    newEspaj[keyState].kdPosTinggalPp = ''
                    newEspaj[keyState].alamatTinggalKdPos = ''
                }
    
                if(paramState == 'kotaName') {
                    newEspaj[keyState].kotaPp = data
                    newEspaj[keyState].kotaTinggalPp = data
                    newEspaj[keyState].kecamatanIdPp = ''
                    newEspaj[keyState].alamatKantorKecamatanIdPp = ''
                    newEspaj[keyState].alamatTinggalKecamatanIdPp = ''
                    newEspaj[keyState].kelurahanIdPp = ''
                    newEspaj[keyState].alamatKantorKelurahanIdPp = ''
                    newEspaj[keyState].alamatTinggalKelurahanIdPp = ''
                    newEspaj[keyState].kdPosPp = ''
                    newEspaj[keyState].kdPosKantorPp = ''
                    newEspaj[keyState].alamatKantorKdPosPp = ''
                    newEspaj[keyState].kdPosTinggalPp = ''
                    newEspaj[keyState].alamatTinggalKdPos = ''
                }
    
                if(paramState == 'kecamatan') {
                    newEspaj[keyState].kecamatanIdPp = data
                    newEspaj[keyState].alamatKantorKecamatanIdPp = data
                    newEspaj[keyState].alamatTinggalKecamatanIdPp = data
                    newEspaj[keyState].kelurahanIdPp = ''
                    newEspaj[keyState].alamatKantorKelurahanIdPp = ''
                    newEspaj[keyState].alamatTinggalKelurahanIdPp = ''
                    newEspaj[keyState].kdPosPp = ''
                    newEspaj[keyState].kdPosKantorPp = ''
                    newEspaj[keyState].alamatKantorKdPosPp = ''
                    newEspaj[keyState].kdPosTinggalPp = ''
                    newEspaj[keyState].alamatTinggalKdPos = ''
                }
    
                if(paramState == 'kelurahan') {
                    newEspaj[keyState].kelurahanIdPp = data
                    newEspaj[keyState].alamatKantorKelurahanIdPp = data
                    newEspaj[keyState].alamatTinggalKelurahanIdPp = data
                    newEspaj[keyState].kdPosPp = ''
                    newEspaj[keyState].kdPosKantorPp = ''
                    newEspaj[keyState].alamatKantorKdPosPp = ''
                    newEspaj[keyState].kdPosTinggalPp = ''
                    newEspaj[keyState].alamatTinggalKdPos = ''
                }
    
                if(paramState == 'kodepos') {
                    let newData = ''
                    newData = data == '' ? '111' : data
                    newEspaj[keyState].kdPosPp = newData
                    newEspaj[keyState].kdPosKantorPp = newData
                    newEspaj[keyState].alamatKantorKdPosPp = newData
                    newEspaj[keyState].kdPosTinggalPp = newData
                    newEspaj[keyState].alamatTinggalKdPos = newData
                }

                return newEspaj
            }

            const reduceNewTt = (newEspaj, keyState, paramState, data) => {
                if(paramState == 'propinsi') {
                    newEspaj[keyState].propinsiIdTt = data
                    newEspaj[keyState].alamatKantorPropinsiIdTt = data
                    newEspaj[keyState].alamatTinggalPropinsiIdTt = data
                    newEspaj[keyState].kotaIdTt = ''
                    newEspaj[keyState].alamatKantorKotaIdTt = ''
                    newEspaj[keyState].alamatTinggalKotaIdTt = ''
                    newEspaj[keyState].kotaTt = '' 
                    newEspaj[keyState].kotaKantorTt = ''
                    newEspaj[keyState].kotaTinggalTt = ''
                    newEspaj[keyState].kecamatanIdTt = ''
                    newEspaj[keyState].alamatKantorKecamatanIdTt = ''
                    newEspaj[keyState].alamatTinggalKecamatanIdTt = ''
                    newEspaj[keyState].kelurahanIdTt = ''
                    newEspaj[keyState].alamatKantorKelurahanIdTt = ''
                    newEspaj[keyState].alamatTinggalKelurahanIdTt = ''
                    newEspaj[keyState].kdPosTt = ''
                    newEspaj[keyState].kdPosKantorTt = ''
                    newEspaj[keyState].alamatKantorKdPosTt = ''
                    newEspaj[keyState].kdPosTinggalTt = ''
                }

                if(paramState == 'kota') {
                    newEspaj[keyState].kotaIdTt = data
                    newEspaj[keyState].alamatKantorKotaIdTt = data
                    newEspaj[keyState].alamatTinggalKotaIdTt = data
                    newEspaj[keyState].kotaKantorTt = data
                    newEspaj[keyState].kotaTt = ''
                    newEspaj[keyState].kotaTinggalTt = ''
                    newEspaj[keyState].kecamatanIdTt = ''
                    newEspaj[keyState].alamatKantorKecamatanIdTt = ''
                    newEspaj[keyState].alamatTinggalKecamatanIdTt = ''
                    newEspaj[keyState].kelurahanIdTt = ''
                    newEspaj[keyState].alamatKantorKelurahanIdTt = ''
                    newEspaj[keyState].alamatTinggalKelurahanIdTt = ''
                    newEspaj[keyState].kdPosTt = ''
                    newEspaj[keyState].kdPosKantorTt = ''
                    newEspaj[keyState].alamatKantorKdPosTt = ''
                    newEspaj[keyState].kdPosTinggalTt = ''
                }

                if(paramState == 'kotaName') {
                    newEspaj[keyState].kotaTt = data
                    newEspaj[keyState].kotaTinggalTt = data
                    newEspaj[keyState].kecamatanIdTt = ''
                    newEspaj[keyState].alamatKantorKecamatanIdTt = ''
                    newEspaj[keyState].alamatTinggalKecamatanIdTt = ''
                    newEspaj[keyState].kelurahanIdTt = ''
                    newEspaj[keyState].alamatKantorKelurahanIdTt = ''
                    newEspaj[keyState].alamatTinggalKelurahanIdTt = ''
                    newEspaj[keyState].kdPosTt = ''
                    newEspaj[keyState].kdPosKantorTt = ''
                    newEspaj[keyState].alamatKantorKdPosTt = ''
                    newEspaj[keyState].kdPosTinggalTt = ''
                }
    
                if(paramState == 'kecamatan') {
                    newEspaj[keyState].kecamatanIdTt = data
                    newEspaj[keyState].alamatKantorKecamatanIdTt = data
                    newEspaj[keyState].alamatTinggalKecamatanIdTt = data
                    newEspaj[keyState].kelurahanIdTt = ''
                    newEspaj[keyState].alamatKantorKelurahanIdTt = ''
                    newEspaj[keyState].alamatTinggalKelurahanIdTt = ''
                    newEspaj[keyState].kdPosTt = ''
                    newEspaj[keyState].kdPosKantorTt = ''
                    newEspaj[keyState].alamatKantorKdPosTt = ''
                    newEspaj[keyState].kdPosTinggalTt = ''
                }
    
                if(paramState == 'kelurahan') {
                    newEspaj[keyState].kelurahanIdTt = data
                    newEspaj[keyState].alamatKantorKelurahanIdTt = data
                    newEspaj[keyState].alamatTinggalKelurahanIdTt = data
                    newEspaj[keyState].kdPosTt = ''
                    newEspaj[keyState].kdPosKantorTt = ''
                    newEspaj[keyState].alamatKantorKdPosTt = ''
                    newEspaj[keyState].kdPosTinggalTt = ''
                }
    
                if(paramState == 'kodepos') {
                    let newData = ''
                    newData = data == '' ? '111' : data
                    newEspaj[keyState].kdPosTt = newData
                    newEspaj[keyState].kdPosKantorTt = newData
                    newEspaj[keyState].alamatKantorKdPosTt = newData
                    newEspaj[keyState].kdPosTinggalTt = newData
                }

                return newEspaj
            }

            if(keyState == 'pemegangPolis') {
                newEspaj = reduceNewPp(newEspaj, keyState, paramState, data)
            }

            if(keyState == 'tertanggung') {
                newEspaj = reduceNewTt(newEspaj, keyState, paramState, data)
            }

            return {
                ...state,
                spaj: {
                    signature: newSignature,
                    espaj: { ...newEspaj }
                }
            }
        }

        default:
            return state
    }
}