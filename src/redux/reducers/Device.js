import { SET_DEVICES_PARAMS } from '../constants';

const initState = {
    applicationName:'',
    batteryLevel:100,
    brand:'',
    buildNumber:'',
    bundleId:'',
    carrier:'',
    deviceId:'',
    deviceName:'',
    macAddr:'',
    manufacturer:'',
    model:'',
    phoneNumber:'',
    buildId:'',
    uniqueId:'',
    userAgent:'',
    version:'',   
    isTablet:false,
    isLandscape:false,
    deviceType:'',
    systemName:'',
    systemVersion:'',
    readableVersion:'',
    isLocationEnable:false, 
    availLocationProvs:{},
    systemAvailFeature:[]
}

export default (state=initState, action)=>{
    switch (action.type) {
        case SET_DEVICES_PARAMS:
            return {
                ...state, 
                ...action.payload
            }
        default:
            return state;
    }
}