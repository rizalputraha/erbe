import {
    SET_DATA_SLIDER,
    SET_SPAJ_REFERAL
} from '../constants'

const INIT_STATE = {
    slider: [],
    isReferalShared: false
}

export default (state = INIT_STATE, action) => {
    
    switch (action.type) {
        case SET_DATA_SLIDER: {
            return {
                ...state,
                slider: [...state.slider, ...action.payload]
            }
        }

        case SET_SPAJ_REFERAL: {
            return {
                ...state,
                isReferalShared: true
            }
        }
        
        default:
            return state
    }
}