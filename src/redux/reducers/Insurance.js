import {
    ASYNC_SET_PRODUCT_INSURANCE,
    SET_PERUSAHAAN_ASURANSI,
    SET_SELECTED_PERUSAHAAN
} from '../constants'

const INIT_STATE = {
    asuransi: {},
    mainMenu: [],
    company: [],
    selectedCompany: {},
    productProposal: [],
}

const combineDataMenu = (data) => {
    const newData = [];
    var start = 0;
    var limit = 3;
    var offer = 0;
    for (let k = 0; k < (data.length / 3); k++) {
        if (k !== 0) {
            limit = limit * start;
            offer = limit + 3;
        }
        if (start == 0) {
            newData.push(data.slice(start, limit));
        } else {
            newData.push(data.slice(limit, offer));
        }
        start = k + 1;
    }

    return newData;
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {

        case ASYNC_SET_PRODUCT_INSURANCE: {
            return {
                ...state,
                asuransi: action.payload.data,
                mainMenu: combineDataMenu(action.payload.data),
            }
        }

        case SET_PERUSAHAAN_ASURANSI: {
            return {
                ...state,
                company: action.payload.data
            }
        }

        case SET_SELECTED_PERUSAHAAN: {
            return {
                ...state,
                selectedCompany: action.payload.data,
                productProposal: action.payload.data.product
            }
        }


        default:
            return state;
    }
}