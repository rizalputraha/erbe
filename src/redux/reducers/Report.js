import {
   GET_KOMISI_SUCCESS,
   SET_DOWNLINE_SUCCESS,
   SHOW_REFRESH_CONTROL,
   HIDE_REFRESH_CONTROL,
   SET_DOWNLINE_SEARCH_KEY,
   SET_DOWNLINE_FILTER,
   SET_DOWNLINE_ACTIVE
} from '../constants'

const INIT_STATE = {
   downline: [],
   downlineActive: {},
   downlineUserId: '',
   searchKey: '',
   isClick: false,
   refresh: false,
   komisi: 0, //Set default komisi to 0 (nol)
}

export default (state = INIT_STATE, action) => {
   switch (action.type) {
      case SET_DOWNLINE_SUCCESS: {
         return {
            ...state,
            downline: action.payload
         }
      }
      case SET_DOWNLINE_SEARCH_KEY: {
         return {
            ...state,
            searchKey: action.payload
         }
      }
      case SET_DOWNLINE_FILTER: {
         const searchKey = action.payload
         const obj = state.downline;
         const filterObj = obj.filter(item => {
            let username = item.username.toLowerCase();
            let s = searchKey.toLowerCase();
            return username.indexOf(s) > -1
         })
         return {
            ...state,
            searchKey,
            downline: [...filterObj]
         }
      }
      case SET_DOWNLINE_ACTIVE: {
         const selectedDownline = action.payload
         return {
            ...state,
            downlineActive: { ...selectedDownline },
            downlineUserId: selectedDownline.id,
            isClick: true
         }
      }
      case GET_KOMISI_SUCCESS: {
         return {
            ...state,
            komisi: action.payload
         }
      }
      case SHOW_REFRESH_CONTROL: {
         return {
            ...state,
            refresh: true,
         }
      }
      case HIDE_REFRESH_CONTROL: {
         return {
            ...state,
            refresh: false,
         }
      }
      default:
         return state
   }
}