/**
 * State reducer simasjiwa
 * version 19.01.23
 * 
 * Mandatory items pemegang polis:
 * namaPp, namaIbu, tptLahir, dobPp, usiaPp, relation, religion, genderPp, status, city, 
 * addressPp, kdPos, pendidikan, gelar, noHp, email, buktiIdentitas, identitas, jobDesc, 
 * penghasilan, npwp, namaRekening, cityRek, bankId, branchId, noRek
 * 
 * Mandatory items tertanggung:
 * namaTt namaIbu tptLahir dobTt usiaTt genderTt address jobDesc
 * 
 * @param {Number} negaraPp Set default 1 is equal indonesia
 * @param {Number} negaraTt Set default 1 is equal indonesia
 * @param {String} agamaLainPp condition set only if agamaPp value 6 is equal Lainnya relied on cache masterReligion
 * @param {String} agamaLainTt condition set only if agamaTt value 6 is equal Lainnya relied on cache masterReligion
 * @param {Number} noHp Set default relied on props reducer authUser as no_telp
 * @param {Number} tlpRmh Set default by 4 digit first relied on noHp
 * @param {String} kdTlpRmh Set default by 3 digit first relied on noHp 
 * @param {Array} profileResiko relied on cache masterQuestioner as question profileResikoPemegangPolis filter by productId and subProductId
 * @param {Array} questioner relied on cache masterQuestioner as question healthTertanggung filter by productId and subProductId
*/
import {
    RESET_CACHE_SIMAS,
    SET_SPAJ_PP_TO_TT_SIMAS,
    SET_PROPOSAL_PP_TO_TT_SIMAS,
    SET_STATE_SPAJ_PM_SIMAS,
    SET_STATE_SPAJ_TT_SIMAS,
    SET_STATE_SPAJ_PP_SIMAS,
    SET_SIGNATURE_SIMAS,
    SET_STATE_SIGNATURE_SIMAS,
    SET_KEY_PROPOSAL_SIMAS,
    SET_STATE_PROPOSAL_PRODUCT_SIMAS,
    SET_STATE_PROPOSAL_TT_SIMAS,
    SET_STATE_PROPOSAL_PP_SIMAS,
    SET_KEY_ROOT_SIMAS,
    LOAD_STORAGE_QUESTIONER_SIMAS,
    SET_ANSWER_HEALTH_QUESTIONER_TEXT_SIMAS,
    SET_ANSWER_HEALTH_QUESTIONER_OPTION_SIMAS,
    SHOW_LOADING_BUTTON_AGEN_SIMAS,
    HIDE_LOADING_BUTTON_AGEN_SIMAS,
    SHOW_LOADING_BUTTON_NASABAH_SIMAS,
    HIDE_LOADING_BUTTON_NASABAH_SIMAS,
    SET_STATE_PROFILE_FROM_AUTHUSER_SIMAS,
    SET_ANSWER_PROFILE_RESIKO_SIMAS,
    SET_STATE_FILE_ATTACHMENT_SIMAS,
    SET_STATE_DOWNLINE_SIMAS,
    SET_PROPOSAL_IS_PARTNER,
    SHOW_LOADING_GENERATE_PROPOSAL_SIMAS,
    HIDE_LOADING_GENERATE_PROPOSAL_SIMAS,
    SHOW_LOADING_SUBMIT_SPAJ_SIMAS,
    HIDE_LOADING_SUBMIT_SPAJ_SIMAS,
    SHOW_LOADING_UPLOAD_DOCUMENT_SIMAS,
    HIDE_LOADING_UPLOAD_DOCUMENT_SIMAS,
    SET_STATE_URI_RESPONSE_SIMAS,
    SET_STATE_FROM_CACHE_SIMAS,
    SET_STATE_SPAJ_CACHE_STORAGE_SIMAS
} from '../constants'

import { remapMobilePhone, remapKodePhone } from 'erbevalidation/Helper'

import AsyncStorage from '@react-native-community/async-storage';

import { Alert } from 'react-native'

const INIT_STATE = {
    keyStorage: '',
    dateCreate: '',
    isPartner: false,
    partner: {
        downlineUserId: '',
        downlineUsername: '',
        downlineEmail: '',
        downlinePhone: ''
    },
    product: false,
    periodeBayar: false,
    proposal: {
        pemegangPolis: {
            namaPp: '',
            dobPp: false,
            usiaPp: false,
            genderPp: false,
        },
        tertanggung: {
            namaTt: '',
            dobTt: false,
            usiaTt: false,
            genderTt: false,
            relationId: false,
        }
    },
    spaj: {
        pemegangPolis: {
            //Profile pemegang polis
            namaPp: '',
            namaIbuPp: '',
            tptLahirPp: '',
            dobPp: false,
            usiaPp: false,
            hubunganPp: false,
            agamaPp: false,
            agamaLainPp: '',
            genderPp: false,
            statusPp: false,

            //Alamat rumah pemegang polis
            negaraPp: 1,
            negaraLainPp: '',
            provinsiPp: false,
            kotaPp: false,
            addressPp: '',
            kdPosPp: '',

            //Pendidikan pemegang polis
            pendidikanPp: false,
            pendidikanLainPp: '',
            gelarPp: '',

            //Identitas pemegang polis
            noHpPp: '',
            emailPp: '',
            kdTlpRmhPp: '',
            tlpRmhPp: '',
            buktiIdentitasPp: false,
            identitasPp: '',
            identitasLainnyaPp: '',

            //Info pekerjaan pemegang polis
            jobDescPp: '',
            penghasilanPp: '',

            //Info bank pemegang polis
            npwpPp: '',
            namaRekeningPp: '',
            cityRekPp: '',
            bankIdPp: false,
            branchIdPp: false,
            noRekPp: ''
        },
        tertanggung: {
            //Profile tertanggung
            namaTt: '',
            namaIbuTt: '',
            tptLahirTt: '',
            dobTt: false,
            usiaTt: false,
            agamaTt: false,
            agamaLainTt: '',
            genderTt: false,
            statusTt: false,

            //Alamat rumah tertanggung
            negaraTt: 1,
            negaraLainTt: '',
            provinsiTt: false,
            kotaTt: false,
            addressTt: '',
            kdPosTt: '',

            //Pendidikan tertanggung
            pendidikanTt: false,
            pendidikanLainTt: '',
            gelarTt: '',

            //Identitas tertanggung
            noHpTt: '',
            emailTt: '',
            kdTlpRmhTt: '',
            tlpRmhTt: '',
            buktiIdentitasTt: false,
            identitasTt: '',
            identitasLainnyaTt: '',

            //Info pekerjaan tertanggung
            jobDescTt: '',
            penghasilanTt: ''
        },
        investasi: {
            ahliwaris: [
                {
                    tptLahir: '',
                    nama: '',
                    manfaat: 100,
                    hubunganAhliWaris: '',
                    genderAhliWaris: false,
                    dobAhliWaris: false
                }
            ]
        },

        profileResiko: [],
        questioner: []
    },
    signature: {
        isAssignedPp: false,
        pemegangPolis: '',
        isAssignedTt: false,
        tertanggung: '',
    },
    fileAttachment: {
        pemegangPolis: '',
        tertanggung: ''
    },
    isTertanggung: false,
    isCalonPembayar: false,
    isAgreeProduk: false,
    isAgreePernyataan: false,
    loadingButtonAgen: false,
    loadingButtonNasabah: false,
    loadingUploadDocument: false,
    loadingGenerateProposal: false,
    loadingSubmitSpaj: false,
    npwpOptionPp: {
        optionYa: false,
        optionTidak: false,
    },
    npwpOptionTt: {
        optionYa: false,
        optionTidak: false
    },
    successFileUpload: false,
    successGenerateProposal: false,
    successSubmitSpaj: false,
    uriResponse: {
        imgSignature: {
            pemegangPolis: '',
            tertanggung: ''
        },
        imgFileAttachment: {
            pemegangPolis: '',
            tertanggung: '',
            calonPembayar: ''
        },
        pdfDocument: {
            proposal: '',
            spaj: ''
        }
    }
}

const filterQuestioner = (data, productId) => {
    const questioner = data.filter(item => item.subProductId == productId);
    return questioner[0].question;
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case SET_STATE_FROM_CACHE_SIMAS: {
            const { stateData } = action.payload
            return {
                ...state,
                ...stateData
            }
        }

        case RESET_CACHE_SIMAS: {
            let newState = { ...INIT_STATE }
            AsyncStorage.removeItem('persist:root');
            AsyncStorage.removeItem('persist:msig');
            AsyncStorage.removeItem('persist:simasjiwa');
            return {
                ...state,
                ...newState
            }
        }

        case SET_KEY_ROOT_SIMAS: {
            const { key, data } = action.payload;
            let newState = { ...state }
            newState[key] = data;
            return {
                ...newState
            }
        }

        case SET_STATE_PROPOSAL_PP_SIMAS: {
            const { paramState, data } = action.payload;
            const { proposal } = state;
            proposal.pemegangPolis[paramState] = data;
            return {
                ...state,
                proposal: {
                    ...proposal
                }
            }
        }

        case SET_STATE_PROPOSAL_TT_SIMAS: {
            const { paramState, data } = action.payload;
            const { proposal } = state;
            proposal.tertanggung[paramState] = data;
            return {
                ...state,
                proposal: {
                    ...proposal
                }
            }
        }

        case SET_STATE_PROPOSAL_PRODUCT_SIMAS: {
            const { paramState, data } = action.payload;
            let newState = { ...state };
            newState.proposal.products[paramState] = data
            return {
                ...newState,
            }
        }

        case SET_KEY_PROPOSAL_SIMAS: {
            const { key, data } = action.payload;
            let newState = { ...state };
            newState.proposal[key] = data;
            return {
                ...newState,
            }
        }

        case SET_STATE_SIGNATURE_SIMAS: {
            const { paramState, data } = action.payload;
            let newState = { ...state };
            let newSignature = newState.signature
            newSignature[paramState] = data

            return {
                ...state,
                signature: {
                    ...newSignature
                }
            }
        }

        case SET_STATE_SPAJ_PP_SIMAS: {
            const { paramState, data } = action.payload;
            const { spaj } = state
            spaj.pemegangPolis[paramState] = data
            return {
                ...state,
                spaj: { ...spaj }
            }
        }

        case SET_STATE_SPAJ_TT_SIMAS: {
            const { paramState, data } = action.payload;
            const { spaj } = state
            spaj.tertanggung[paramState] = data
            return {
                ...state,
                spaj: { ...spaj }
            }
        }

        case SET_STATE_SPAJ_PM_SIMAS: {
            const { paramState, data } = action.payload;
            const { spaj } = state
            spaj.investasi.ahliwaris[0][paramState] = data
            return {
                ...state,
                spaj: { ...spaj }
            }
        }

        case SET_PROPOSAL_PP_TO_TT_SIMAS: {
            const { namaPp, dobPp, usiaPp, genderPp } = action.payload;
            return {
                ...state,
                proposal: {
                    ...state.proposal,
                    tertanggung: {
                        namaTt: namaPp,
                        dobTt: dobPp,
                        usiaTt: usiaPp,
                        genderTt: genderPp,
                        relationId: 1,
                    }
                }
            }
        }

        case SET_SPAJ_PP_TO_TT_SIMAS: {
            const {
                namaPp,
                namaIbuPp,
                tptLahirPp,
                dobPp,
                usiaPp,
                agamaPp,
                agamaLainPp,
                genderPp,
                statusPp,
                negaraPp,
                negaraLainPp,
                kotaPp,
                addressPp,
                kdPosPp,
                pendidikanPp,
                pendidikanLainPp,
                gelarPp,
                noHpPp,
                emailPp,
                kdTlpRmhPp,
                tlpRmhPp,
                buktiIdentitasPp,
                identitasPp,
                identitasLainnyaPp,
                jobDescPp,
                penghasilanPp
            } = action.payload;
            return {
                ...state,
                spaj: {
                    ...state.spaj,
                    tertanggung: {
                        namaTt: namaPp,
                        namaIbuTt: namaIbuPp,
                        tptLahir: tptLahirPp,
                        dobTt: dobPp,
                        usiaTt: usiaPp,
                        agamaTt: agamaPp,
                        agamaLainTt: agamaLainPp,
                        genderTt: genderPp,
                        status: statusPp,
                        negaraTt: negaraPp,
                        negaraLainTt: negaraLainPp,
                        kotaTt: kotaPp,
                        addressTt: addressPp,
                        kdPosTt: kdPosPp,
                        pendidikan: pendidikanPp,
                        pendidikanLain: pendidikanLainPp,
                        gelar: gelarPp,
                        noHp: noHpPp,
                        email: emailPp,
                        kdTlpRmh: kdTlpRmhPp,
                        tlpRmh: tlpRmhPp,
                        buktiIdentitas: buktiIdentitasPp,
                        identitas: identitasPp,
                        identitasLainnya: identitasLainnyaPp,
                        jobDesc: jobDescPp,
                        penghasilan: penghasilanPp
                    }
                }
            }
        }

        case LOAD_STORAGE_QUESTIONER_SIMAS: {
            const { data, productId } = action.payload
            const { spaj } = state;
            spaj.questioner = filterQuestioner(data, productId)
            return {
                ...state,
                spaj: { ...spaj }
            }
        }

        case SET_ANSWER_HEALTH_QUESTIONER_TEXT_SIMAS: {
            const { paramState, index, data } = action.payload
            const objQ = state.spaj.questioner.healthTertanggung;
            const objP = state.spaj.questioner.profileResikoPemegangPolis;
            const spaj = state.spaj
            objQ[index][paramState] = data;
            return {
                ...state,
                spaj: {
                    ...spaj,
                    questioner: {
                        healthTertanggung: [...objQ],
                        profileResikoPemegangPolis: [...objP]
                    }
                },
            }
        }

        case SET_ANSWER_HEALTH_QUESTIONER_OPTION_SIMAS: {
            const { paramState, index, data } = action.payload
            const spaj = state.spaj;
            const objQ = state.spaj.questioner.healthTertanggung;
            const objP = state.spaj.questioner.profileResikoPemegangPolis;
            if (paramState == 'optionYa') {
                Alert.alert('Maaf', 'Anda Tidak Memenuhi Syarat!')
            } else {
                objQ[index].answerOptions[paramState] = data;
                objQ[index].answerOptions.optionYa = !data;
            }
            return {
                ...state,
                spaj: {
                    ...spaj,
                    questioner: {
                        healthTertanggung: [...objQ],
                        profileResikoPemegangPolis: [...objP]
                    }
                },
            }
        }

        case SET_ANSWER_PROFILE_RESIKO_SIMAS: {
            return {
                ...state,
            }
        }

        case SHOW_LOADING_BUTTON_AGEN_SIMAS: {
            return {
                ...state,
                loadingButtonAgen: true,
            }
        }

        case HIDE_LOADING_BUTTON_AGEN_SIMAS: {
            return {
                ...state,
                loadingButtonAgen: false,
            }
        }

        case SHOW_LOADING_BUTTON_NASABAH_SIMAS: {
            return {
                ...state,
                loadingButtonNasabah: true,
            }
        }

        case HIDE_LOADING_BUTTON_NASABAH_SIMAS: {
            return {
                ...state,
                loadingButtonNasabah: false,
            }
        }

        case SHOW_LOADING_UPLOAD_DOCUMENT_SIMAS: {
            return {
                ...state,
                loadingUploadDocument: true
            }
        }

        case HIDE_LOADING_UPLOAD_DOCUMENT_SIMAS: {
            return {
                ...state,
                loadingUploadDocument: false
            }
        }

        case SHOW_LOADING_GENERATE_PROPOSAL_SIMAS: {
            return {
                ...state,
                loadingGenerateProposal: true
            }
        }

        case HIDE_LOADING_GENERATE_PROPOSAL_SIMAS: {
            return {
                ...state,
                loadingGenerateProposal: false
            }
        }

        case SHOW_LOADING_SUBMIT_SPAJ_SIMAS: {
            return {
                ...state,
                loadingSubmitSpaj: true
            }
        }

        case HIDE_LOADING_SUBMIT_SPAJ_SIMAS: {
            return {
                ...state,
                loadingSubmitSpaj: false
            }
        }

        case SET_STATE_PROFILE_FROM_AUTHUSER_SIMAS: {
            const { authUser } = action.payload
            let newProposal = { ...state.proposal }
            let userData = authUser.user_data
            let nama = userData.nama
            let mobilePhone = authUser.user_data.no_telp
            newProposal.pemegangPolis.namaPp = nama.substr(0, 30)

            let newSpaj = { ...state.spaj }
            __DEV__ && console.log('newSpaj', newSpaj);

            newSpaj.pemegangPolis.namaPp = nama.substr(0, 30)
            newSpaj.pemegangPolis.emailPp = authUser.user_data.email
            newSpaj.pemegangPolis.noHpPp = mobilePhone
            newSpaj.pemegangPolis.kdTlpRmhPp = remapKodePhone(mobilePhone)
            newSpaj.pemegangPolis.tlpRmhPp = remapMobilePhone(mobilePhone)

            newSpaj.tertanggung.noHpTt = mobilePhone
            newSpaj.tertanggung.kdTlpRmhTt = remapKodePhone(mobilePhone)
            newSpaj.tertanggung.tlpRmhTt = remapMobilePhone(mobilePhone)
            return {
                ...state,
                proposal: { ...newProposal },
                spaj: { ...newSpaj }
            }
        }

        case SET_STATE_FILE_ATTACHMENT_SIMAS: {
            const { paramState, data } = action.payload;
            let { fileAttachment } = state
            fileAttachment[paramState] = data
            return {
                ...state,
                fileAttachment: { ...fileAttachment }
            }
        }

        case SET_STATE_DOWNLINE_SIMAS: {
            const { data } = action.payload;
            let { partner } = state;
            partner.downlineUserId = data.id;
            partner.downlineUsername = data.username;
            partner.downlineEmail = data.email;
            partner.downlinePhone = data.no_telp;
            return {
                ...state,
                isPartner: true,
                partner: { ...partner }
            }
        }

        case SET_PROPOSAL_IS_PARTNER: {
            __DEV__ && console.log('action payload', action.payload);
            const { data } = action.payload;
            let { proposal, spaj } = state;
            proposal.pemegangPolis.namaPp = data.downlineUsername;

            spaj.pemegangPolis.emailPp = data.downlineEmail
            spaj.pemegangPolis.noHpPp = remapMobilePhone(data.downlinePhone)
            spaj.pemegangPolis.kdTlpRmhPp = remapKodePhone(data.downlinePhone)
            spaj.pemegangPolis.tlpRmhPp = remapMobilePhone(data.downlinePhone)
            return {
                ...state,
                proposal: { ...proposal },
                spaj: { ...spaj }
            }
        }

        case SET_STATE_URI_RESPONSE_SIMAS: {
            const { paramState, keyState, responseUri } = action.payload
            const { uriResponse } = state
            uriResponse[paramState][keyState] = responseUri
            return {
                ...state,
                uriResponse: { ...uriResponse }
            }
        }

        case SET_STATE_SPAJ_CACHE_STORAGE_SIMAS: {
            const { questioner, profileResiko } = action.payload
            const { spaj } = state
            spaj.espaj.questioner = questioner
            spaj.espaj.profileResiko = profileResiko

            return {
                ...state,
                spaj: {
                    ...spaj
                }
            }
        }

        default:
            return state
    }
}