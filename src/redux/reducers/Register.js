import { 
    SET_REGISTER_REFERAL,
    SET_REGISTER_USER,
    SET_REGISTER_VERIFY
} 
from '../constants'

const INIT_STATE = {
    upline: {},
    user: {
        hp: undefined,
        email: undefined,
        password: undefined
    },
    verifikasi: {
        kode: undefined
    }
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case SET_REGISTER_REFERAL: {
            return {
                ...state,
                upline: {...action.payload}
            }
        }

        case SET_REGISTER_USER: {
            return {
                ...state,
                user: {...action.payload}
            }
        }

        case SET_REGISTER_VERIFY: {
            return {
                ...state
            }
        }

        default:
            return state
    }
}