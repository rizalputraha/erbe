import {
    SET_AUTH_ACCOUNT,
    SIGN_IN_SUCCESS,
    SIGN_OUT_SUCCESS,
    SET_AUTH_USER,
    SET_USER_FORGOT_PASSWORD
} from '../constants'

const INIT_STATE = {
    account: {},
    isAccountAvail: false,
    authUser: false,
    initURL:'',
    rootNav:null,
    isForgotVerify: false,
    forgotData: {}
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case SET_AUTH_ACCOUNT: {
            return {
                ...state,
                isAccountAvail: true,
                account: {...action.payload}
            }
        }

        case SIGN_IN_SUCCESS: {
            return {
                ...state,
                authUser: action.payload
            }
        }
    
        case SIGN_OUT_SUCCESS: {
            return {
                ...state,
                authUser: null,
                initURL: '/login'
            }
        }

        case SET_AUTH_USER: {
            return {
                ...state,
                authUser: action.payload
            }
        }

        case SET_USER_FORGOT_PASSWORD: {
            return {
                ...state,
                isForgotVerify: true,
                forgotData: action.payload
            }
        }

        default: 
            return state
    }
}