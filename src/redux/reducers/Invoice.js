import {
   GET_TAGIHAN_SUCCESS
} from '../constants'

const INIT_STATE = {
   tagihan: [],
}

export default(state = INIT_STATE,action) => {
   switch (action.type) {
      case GET_TAGIHAN_SUCCESS: {
         return {
            ...state,
            tagihan: action.payload
         }
      }
      default:
         return state
   }
}