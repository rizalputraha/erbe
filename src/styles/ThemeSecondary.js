import { colors } from 'react-native-elements'
import Color from './Color'
import ButtonElement from './rnelement/ButtonElement'

const themeSecondary = {
    colors: {
        ...Color,
        primary: Color.primary,
        secondary: Color.secondary,
        grey0: Color.greyLight
    },
    Button: {
        ...ButtonElement.secondary
    }
}

export default themeSecondary;