/**
 * Difine default color
 * @param white string color clean
 */
const black = "#000000";
const white = "#FFFFFF";
const dark = "#1a2022";
const darkLight = "#333c40";
const grey = "#8d8d8e";
const greyLight = "#d3d3d3";
const greyLightTeen = "#f2f2f2";
const greyDark = "#707070";

/**
 * Define colors use for background
 * @param primary string color is red
 * @param secondary string color is green
 */
const primary = "#e84242";
const primaryLight = "#f77171";
const primaryDark = "#b82319";

const secondary = "#2abd64";
const secondaryLight = "#4ee68a";
const secondaryDark = "#94c168";

/**
 * Define colors use for background optional
 * This colors can use for information of title
 * @param navy string color is blue navy
 */
const lambo = "#0b1838";
const lamboLight = "#182850";
const lamboDark = "#061335";

/**
 * Color for bottom tab navigator
 */
const activeTintColor = '#FAFAFA';
const inactiveTintColor = '#ffc9c9';

/**
 * Color for status
 */
const greenSuccess = "#02B28C";
const blueInfo = "#2EB7EC";
const redDanger = "#E53036";
const maintenance = "#ffc926";

const Color = {
    black,
    white,
    dark,
    darkLight,
    grey,
    greyLight,
    greyLightTeen,
    greyDark,
    primary,
    primaryLight,
    primaryDark,
    secondary,
    secondaryLight,
    secondaryDark,
    lambo,
    lamboLight,
    lamboDark,
    activeTintColor,
    inactiveTintColor,
    greenSuccess,
    blueInfo,
    redDanger,
    maintenance
}

export default Color;