import { colors } from 'react-native-elements'
import Color from './Color'
import InputText from './rnelement/InputText'
import ButtonElement from './rnelement/ButtonElement'

const theme = {
    colors: {
        ...Color,
        primary: Color.primary,
        secondary: Color.secondary,
        grey0: Color.greyLight
    },
    Button: {
        ...ButtonElement.primary
    },
    statusBar: {
        backgroundColor: Color.primary,
        barStyle: 'light-content'
    },
    Card: {
        containerStyle: {
            elevation:4,
            shadowOffset: { width: 4, height: 4 },
            shadowColor: Color.greyLight,
            shadowOpacity: 0.5,
            shadowRadius: 10
        }
    }
}

export default theme;