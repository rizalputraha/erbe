import MainStyle from 'style/MainStyle'
import Color from 'style/Color'
import { StyleSheet } from 'react-native'

const style = StyleSheet.create({
    container: {
        flex: MainStyle.flexDefault
    },
    containerSearch: {
        backgroundColor: Color.primary,
        borderTopWidth: 0,
        borderBottomWidth: 0,
        padding: MainStyle.padding
    },
    containerItem: {
        padding: 5,
        margin: MainStyle.margin,
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: 'center',
        shadowColor: Color.greyLightTeen,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 2,
    },
    content: {
        backgroundColor: Color.white
    }
})

export default style;