import MainStyle from 'style/MainStyle'
import Color from 'style/Color'
import { StyleSheet } from 'react-native'

const style = StyleSheet.create({
    container: {
        flex: 1
    },
    parentContainer: {
		flex: 1,
        marginTop: Platform.OS === 'android' ? 0 : 0,
        backgroundColor: Color.white
	},
    logo: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: MainStyle.margin
    },
    logoImages: {
        width: 75,
        height: 75
    },
    loginBgImages: {
        width: 200,
        height: 200
    },
    loginBackground: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15,
        backgroundColor: 'transparent'
    }
})

export default style;