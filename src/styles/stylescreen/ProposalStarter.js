import MainStyle from 'style/MainStyle'
import Color from 'style/Color'
import { StyleSheet } from 'react-native'

const style = StyleSheet.create({
    container: {
        flex: 1
    },
    containerTitle: {
        backgroundColor: Color.lambo,
        flexDirection: 'column',
        padding: 15,
	},
    txtTitle: {
        color: Color.white,
        fontSize: 18,
    },
    containerForm: {
        margin: MainStyle.marginExtraSmall,
        backgroundColor: Color.bgSecondary
    }
})

export default style;