import MainStyle from 'style/MainStyle'
import Color from 'style/Color'
import { StyleSheet } from 'react-native'

const style = StyleSheet.create({
    container: {
        flex: MainStyle.flexDefault,
        backgroundColor: Color.white
    },
    containerItem: {
        padding: MainStyle.padding,
        margin: MainStyle.margin,
        borderRadius: 5,
        flexDirection: 'column',
        alignItems: 'center',
        shadowColor: Color.greyLightTeen,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.1,
        shadowRadius: 2.5,
        elevation: 2,
    },
    title: {
        fontFamily: 'DroidSans',
        fontSize: MainStyle.p,
        marginBottom: MainStyle.margin,
        fontWeight: "bold",
        color: Color.lamboLight
    },
    name: {
        fontFamily: 'DroidSans',
        fontSize: MainStyle.p,
        marginBottom: MainStyle.margin
    },
    info: {
        margin: MainStyle.margin,
        backgroundColor: Color.greyLightTeen,
        padding: MainStyle.margin,
        borderRadius: 5
    },
    infoText: {
        color: Color.dark
    },
    dateTitle: {
        fontFamily: 'DroidSans',
        fontSize: MainStyle.p,
        marginBottom: MainStyle.margin
    },
    dateText: {
        fontFamily: 'DroidSans',
        fontSize: MainStyle.p,
        marginBottom: MainStyle.margin,
        color: Color.primaryLight
    },
    buttonArea: {
        elevation: 3,
        shadowColor: Color.greyLightTeen,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        borderTopWidth: 1,
        borderTopColor: Color.greyLightTeen,
        backgroundColor: Color.white
    }
})

export default style;