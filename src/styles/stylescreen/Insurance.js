import MainStyle from 'style/MainStyle'
import {StyleSheet} from 'react-native'
import Color from '../Color'

const style = StyleSheet.create({
    container: {
        flex: MainStyle.flexDefault,
        backgroundColor: Color.white
    },
    
})

export default style