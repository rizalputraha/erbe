import MainStyle from 'style/MainStyle'
import {StyleSheet} from 'react-native'
import Color from '../Color'

const style = StyleSheet.create({
   headContent: {
      padding: MainStyle.marginBig,
      marginLeft: MainStyle.marginSmall,
      marginTop:  MainStyle.marginSmall,
      marginRight:  MainStyle.marginSmall,
      backgroundColor: Color.lambo,
      borderRadius:  MainStyle.marginSmall,
      shadowColor: "#000",
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.1,
      shadowRadius:  MainStyle.marginSmall,
      elevation: 6,
    },
    headContentTitle: {
      fontSize: 18,
      color: Color.white
    },
    headContentPrice: {
      fontSize: 30,
      color: Color.white
    },
    listContainer: {
      backgroundColor: Color.white,
      borderRadius: 5,
      padding: MainStyle.marginBig,
      margin: MainStyle.marginSmall,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 3,
      },
      shadowOpacity: 0.27,
      shadowRadius: 4.65,
      elevation: 2,
    },
    spanStatusReceived: {
      marginLeft: MainStyle.marginExtraSmall,
      paddingHorizontal: MainStyle.marginSmall,
      borderRadius: 8,
      backgroundColor: Color.greenSuccess,
      color: Color.white
    },
    spanStatusPending: {
      marginLeft: MainStyle.marginExtraSmall,
      paddingHorizontal: MainStyle.marginSmall,
      borderRadius: 8,
      backgroundColor: Color.blueInfo,
      color: Color.white
    }
})

export default style;