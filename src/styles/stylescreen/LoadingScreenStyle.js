import MainStyle from 'style/MainStyle'
import { StyleSheet } from 'react-native'

const style = StyleSheet.create({
    container: {
        flex: MainStyle.flexDefault,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageLoading: {
        width: 200,
        height: 100
    }
})

export default style;