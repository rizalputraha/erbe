import MainStyle from 'style/MainStyle'
import Color from 'style/Color'
import { StyleSheet } from 'react-native'

const style = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    containerImage: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        width: 100,
        height: 100
    },
    containerItem: {
        paddingVertical: 15,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#b4b4b4'
    }
})

export default style;