import MainStyle from 'style/MainStyle'
import Color from 'style/Color'
import { StyleSheet } from 'react-native'

const style = StyleSheet.create({
    container: {
        flex: MainStyle.flexDefault,
        backgroundColor: Color.white
    }
})

export default style;