import MainStyle from 'style/MainStyle'
import {StyleSheet} from 'react-native'
import Color from '../Color'

const style = StyleSheet.create({
   container: {
      flex:1,
      backgroundColor: Color.greyLight
   },
   topInfo: {
      backgroundColor: Color.primary,
      paddingTop: MainStyle.marginSmall,
      paddingBottom: MainStyle.marginSmall,
      height: 50
   },
   row: {
      flex: 1,
      flexDirection: 'row'
   },
   imageContainer: {
      marginLeft: MainStyle.marginSmall, 
      marginRight: MainStyle.marginSmall, 
      marginTop: MainStyle.marginBig, 
      marginBottom: MainStyle.marginBig
   },
   content: {
      padding: MainStyle.marginSmall
   },
   txtJumlah: {
      paddingTop: MainStyle.marginSmall
   },
   txtNominal: {
      paddingTop: MainStyle.marginSmall, 
      paddingBottom: MainStyle.marginSmall, 
      fontSize: MainStyle.h5
   },
   txtPaid: {
      color: Color.greenSuccess,
      marginLeft: MainStyle.marginSmall
   },
   txtUnpaid: {
      color: Color.redDanger,
      marginLeft: MainStyle.marginSmall
   },
   containerPaid: {
      padding: 10,
      backgroundColor: Color.greenSuccess,
      justifyContent: 'center',
      alignItems: 'center',
   },
   containerUnpaid: {
      padding: 10,
      backgroundColor: Color.redDanger,
      justifyContent: 'center',
      alignItems: 'center',
   }
})

export default style;