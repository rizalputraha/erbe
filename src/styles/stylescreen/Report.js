import MainStyle from 'style/MainStyle'
import {StyleSheet} from 'react-native'
import Color from '../Color'

const style = StyleSheet.create({
   listContainer: {
      flex: 1,
      flexDirection: 'row',
      padding: MainStyle.margin,
      borderBottomColor: Color.greyLight,
      borderBottomWidth: 1
   },
   titleContainer: {
      paddingLeft: MainStyle.margin
   },
   txtTitle: {
      fontSize: MainStyle.h5,
   },
   txtDesc: {
      color: Color.grey
   }
})

export default style;