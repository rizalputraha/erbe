import MainStyle from 'style/MainStyle'
import {StyleSheet} from 'react-native'
import Color from '../Color'

const style = StyleSheet.create({
   containerNoData: {
      alignItems: 'center',
      padding: MainStyle.marginSmall,
      margin: MainStyle.marginBig,
   },
   txtNoData: {
      fontSize: 16,
      color: Color.greyDark,
      fontWeight: 'bold'
   }
})

export default style;