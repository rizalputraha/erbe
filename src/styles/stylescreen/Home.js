import MainStyle from 'style/MainStyle'
import { StyleSheet } from 'react-native'

const style = StyleSheet.create({
    container: {
        flex: 1
    },
    parentContainer: {
        ...MainStyle.container,
        marginTop: 20
    },
    contentContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 15
    },
    childContainer: {
        flex: 1,
    },
    heading: {
        padding: 10,
        fontSize: 24,
        justifyContent: 'center'
    },
    subHeading: {
        padding: 10,
        fontSize: 20,
        justifyContent: 'center'
    },
    contentText: {
        textAlign: 'center',
        justifyContent: 'center',
        lineHeight: 25,
        fontSize: 16
    },

})

export default style;