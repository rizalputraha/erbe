import MainStyle from 'style/MainStyle'
import { StyleSheet } from 'react-native'
import Color from '../Color'

const style = StyleSheet.create({
    content: {
        backgroundColor: Color.white
    },
    title: {
        backgroundColor: Color.lamboLight,
        flexDirection: 'column',
        padding: MainStyle.margin,
    },
    titleText: {
        color: Color.white,
        fontSize: MainStyle.h4,
    },
    infoPolis: {
        margin: MainStyle.margin
    }
})

export default style;