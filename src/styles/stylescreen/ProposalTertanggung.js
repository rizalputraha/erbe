import MainStyle from 'style/MainStyle'
import {StyleSheet} from 'react-native'
import Color from '../Color'

const style = StyleSheet.create({
  containerTitle:{
    backgroundColor: Color.lamboLight,
    padding:15
  },  
  txtTitle: {
    color: Color.white,
    fontSize: 18,
  }
})

export default style;