import { StyleSheet } from "react-native";
import MainStyle from '../MainStyle'
import Color from '../Color'
import Layout from '../Layout'
const styles = StyleSheet.create({
    signatureContainer: {
        borderWidth: 1,
        borderColor: Color.greyLight,
        marginHorizontal: MainStyle.margin,
        marginTop: MainStyle.margin
    },
    signature: {
        height: Layout.window.height / 5,
        width: Layout.window.width - (2 * 1) - (2 * MainStyle.margin)
    },
    signatureTitleArea: {
        borderBottomWidth: 1,
        borderBottomColor: Color.greyLight,
        padding: MainStyle.margin,
        backgroundColor: '#f3f3f3'
    },
    signatureTitleText: {
        textAlign: 'center',
        fontFamily: 'DroidSans',
        fontSize: MainStyle.paragraph,
        lineHeight: MainStyle.lineHeightP,
        fontWeight: 'bold'
    },
    signatureButtonArea: {
        borderTopWidth: 1,
        borderTopColor: Color.greyLight
    },
    signatureSave: {
        borderLeftWidth: 1,
        borderLeftColor: Color.greyLight
    },
    assignAvail: {
        borderTopWidth: 1,
        borderTopColor: Color.greyLight,
        backgroundColor: '#f3f3f3'
    },
    imageAssign: {
        height: Layout.window.height / 5,
        width: Layout.window.width - (2 * 1) - (2 * MainStyle.margin)
    },
    signatureName: {
        textAlign: 'center',
        fontFamily: 'DroidSans',
        fontSize: MainStyle.paragraph,
        lineHeight: MainStyle.lineHeightP,
        marginTop: MainStyle.margin,
        marginBottom: MainStyle.margin
    }
})

export default styles;