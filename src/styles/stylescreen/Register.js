import MainStyle from 'style/MainStyle'
import Color from 'style/Color'
import { StyleSheet } from 'react-native'

const style = StyleSheet.create({
    container: {
        flex: 1,
    },
    parentContainer: {
		flex: 1,
        marginTop: Platform.OS === 'android' ? 0 : 0,
        backgroundColor: Color.white
    }
})

export default style;