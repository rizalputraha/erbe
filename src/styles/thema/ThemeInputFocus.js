import InputText from '../rnelement/InputText'

const theme = {
    Input: {
        ...InputText.focus
    }
}

export default theme;