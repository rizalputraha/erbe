import InputText from '../rnelement/InputText'

const theme = {
    Input: {
        ...InputText.blur
    }
}

export default theme;