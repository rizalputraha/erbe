import MainStyle from 'style/MainStyle'
import { StyleSheet } from 'react-native'
import Color from 'style/Color'

const style = StyleSheet.create({
    content: {
        backgroundColor: Color.white,
    },
    containerLabel: {
        paddingTop: MainStyle.marginSmall,
        paddingLeft: MainStyle.margin
    },
    iconInValid: {
        color: Color.redDanger,
    },
    iconInValid: {
        color: Color.greenSuccess,
    }
})

export default style;