import MainStyle from 'style/MainStyle'
import Color from 'style/Color'
import { StyleSheet } from 'react-native'

const style = StyleSheet.create({
    container: {
        margin: MainStyle.margin
    },
    p: {
        fontFamily: 'DroidSans',
        fontSize: MainStyle.paragraph,
        lineHeight: MainStyle.lineHeightP
    },
    h1: {
        fontFamily: 'DroidSans',
        fontSize: MainStyle.h1,
        lineHeight: MainStyle.lineHeightH1
    },
    h2: {
        fontFamily: 'DroidSans',
        fontSize: MainStyle.h2,
        lineHeight: MainStyle.lineHeightH2
    },
    h3: {
        fontFamily: 'DroidSans',
        fontSize: MainStyle.h3,
        lineHeight: MainStyle.lineHeightH3
    },
    h4: {
        fontFamily: 'DroidSans',
        fontSize: MainStyle.h4,
        lineHeight: MainStyle.lineHeightH4
    },
    h5: {
        fontFamily: 'DroidSans',
        fontSize: MainStyle.h5,
        lineHeight: MainStyle.lineHeightH5
    },
    left: {
        textAlign: 'left'
    },
    center: {
        textAlign: 'center'
    },
    right: {
        textAlign: 'right'
    },
    white: {
        color: Color.white
    },
    dark: {
        color: Color.dark
    },
    primary: {
        color: Color.primary
    }
})

export default style;