import MainStyle from 'style/MainStyle'
import { StyleSheet } from 'react-native'
import Color from 'style/Color'

const style = StyleSheet.create({
   container: {
      flexDirection: "row",
      justifyContent: 'space-between',
      margin: MainStyle.marginSmall,
      padding: MainStyle.marginSmall,
      alignItems: 'center',
      borderBottomColor: Color.grey,
      borderBottomWidth: 1
   },
   textUsername: {
      fontSize: MainStyle.h4,
   },
   textKota: {
      color: Color.grey
   },
   alphabeth: {
      width: 60,
      height: 60,
      borderRadius: 60/2,
      backgroundColor: '#ee97aa',
      alignItems: 'center',
   },
   textAlphabeth: {
      fontSize: MainStyle.h4,
      color: '#FFF',
      marginTop: MainStyle.margin,
   }
})

export default style;