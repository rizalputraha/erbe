import MainStyle from 'style/MainStyle'
import { StyleSheet } from 'react-native'
import Color from 'style/Color'
import Layout from 'style/Layout'

const styles = StyleSheet.create({
	container: {
		flex: 1,
        backgroundColor: '#ECEFF1',
        flex:1,
        alignItems:'center', 
        justifyContent:'center'
    },
    title: {
        marginTop: 15,
        fontSize: 20,
        fontFamily: 'DroidSans',
        color: Color.darkLight
    },
    subTitle: {
        marginTop: 15,
        fontSize: 14,
        fontFamily: 'DroidSans',
        color: Color.darkLight
    },
});

export default styles