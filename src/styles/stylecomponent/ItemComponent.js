import MainStyle from 'style/MainStyle'
import { StyleSheet } from 'react-native'
import Color from 'style/Color'


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,
        position: 'relative',
        marginHorizontal: MainStyle.margin,
        marginVertical: MainStyle.margin
    },
    item: {
        height: 40, 
        borderColor: Color.grey, 
        borderBottomWidth: 2,
        padding: 0
    },
    label: {
        fontSize: MainStyle.paragraph,
        color: Color.darkLight
    },
    itemData: {
        fontSize: 14,
        color: Color.dark,
        position: 'absolute',
        left: 0,
        top: 10
    },
    iconArea: {
        width: 20,
        height: 20,
        backgroundColor: 'transparent',
        position: 'absolute',
        right: 0,
        top: 10
    },
    active: {
        borderColor: Color.grey,
        color: Color.dark
    }
})