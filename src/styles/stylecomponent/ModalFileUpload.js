import MainStyle from 'style/MainStyle'
import { StyleSheet } from 'react-native'
import Color from 'style/Color'

const style = StyleSheet.create({
    buttonModal: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        backgroundColor: Color.white,
        marginHorizontal: MainStyle.margin,
        marginTop: 15,
        padding: 10,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: Color.darkLight
    },
    textButtonModal: {
        color: Color.darkLight,
        fontWeight: 'bold'
    },
    container: {
        flex: 1,
        backgroundColor: "#FFF"
    },
    contentModal: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: "#FFF",
        marginHorizontal: 10,
    },
})

export default style