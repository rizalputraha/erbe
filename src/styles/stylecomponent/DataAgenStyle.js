import MainStyle from 'style/MainStyle'
import { StyleSheet } from 'react-native'
import Color from 'style/Color'
import Layout from 'style/Layout'

const styles = StyleSheet.create({
	container: {
		flex: 1,
		borderBottomWidth: 1,
        borderColor: Color.greyLight,
		paddingTop: 10,
        paddingBottom: 10
	},
	borderLeft: {
		borderLeftWidth: 1,
		borderColor: Color.greyLight
	},
	title: {
		color: Color.textDark,
		textAlign: 'center'
	},
	content: {
		color: Color.primary,
		textAlign: 'center',
		fontSize: 16
	},
	contentLevel: {
		color: Color.primary,
		textAlign: 'center',
		fontSize: 16
	}
});

export default styles