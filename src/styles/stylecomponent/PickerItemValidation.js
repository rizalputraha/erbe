import MainStyle from 'style/MainStyle'
import { StyleSheet } from 'react-native'
import Color from 'style/Color'
import Layout from 'style/Layout'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 10
    },
    containerScroll: {
        flex: 1,
        backgroundColor: '#ECEFF1',
        alignItems: 'center',
        justifyContent: 'center'
    },
    containerLabel: {
        paddingLeft: MainStyle.margin
    },
    txtTitle: {
        fontSize: 14,
        color: Color.darkLight,
        marginTop: 10,
    },
    subTitle: {
        marginTop: 15,
        fontSize: 14,
        fontFamily: 'DroidSans',
        color: Color.darkLight
    },
    iconArea: {
        width: 20,
        height: 20,
        backgroundColor: 'transparent',
        position: 'absolute',
        right: 0,
        bottom: 10
    },
});

export default styles