import MainStyle from 'style/MainStyle'
import { StyleSheet } from 'react-native'
import Color from 'style/Color'

const style = StyleSheet.create({
    container: {
        flex: 1
    },
    col: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 15,
        marginTop: 15
    },
    maintenance: {
        width: 10,
        height: 10,
        borderRadius: 10,
        backgroundColor: Color.maintenance,
        borderWidth: 2,
        borderColor: Color.grey,
        position: 'absolute',
        left: -5,
        top: 0
    },
    wrapCol: {
        flex: 1,
        alignItems: 'center'
    },
    buttonArea: {
        backgroundColor: Color.bgWhite
    },
    icon: {
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconText: {
        textAlign: 'center'
    }
})

export default style