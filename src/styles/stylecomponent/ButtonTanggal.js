import MainStyle from 'style/MainStyle'
import { StyleSheet } from 'react-native'
import Color from 'style/Color'

const style = StyleSheet.create({
    content: {
        backgroundColor: Color.white,
    },
    containerLabel: {
        paddingLeft: MainStyle.margin,
        paddingBottom: MainStyle.margin
    },
    iconInValid: {
        color: Color.redDanger,
    },
    iconInValid: {
        color: Color.greenSuccess,
    },
    btnStyle: {
        backgroundColor: '#FAFAFA',
        elevation: 3,
        marginHorizontal: MainStyle.marginSmall
    },
    titleStyle: {
        color: '#000',
        marginRight: MainStyle.marginSmall
    }
})

export default style;