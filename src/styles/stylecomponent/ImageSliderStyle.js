import MainStyle from 'style/MainStyle'
import { StyleSheet } from 'react-native'
import Color from 'style/Color'
import Layout from 'style/Layout'

const style = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row'
    },
    slider: {
        flex: 1,
        height: 0.211 * Layout.window.height,
        padding: 0,
        margin: 0,
        position: 'relative',
    },
    sliderEmpty: {
        flex: 1,
        height: 135,
        padding: 0,
        margin: 0,
        backgroundColor: Color.greyLight
    },
    warpslider: {
        width: Layout.window.width,
        height: null,
        backgroundColor: Color.white
    },
    banner: {
        width: null,
        height: null,
        flex: 1
    },
    sliderWrapper: {
        position: 'absolute',
        flexDirection: 'row',
        padding: 10,
        right: 10,
        bottom: 0,
        top: 100,
        height: 30,

    },
    sliderIcon: {
        width: 15,
        height: 15,
        backgroundColor: Color.white,
        marginRight: 10,
        borderRadius: 15,
        opacity: 0.5
    },
    sliderIconActive: {
        width: 15,
        height: 15,
        backgroundColor: 'transparent',
        marginRight: 10,
        borderRadius: 15,
        borderColor: Color.white,
        borderWidth: 2
    }
})

export default style