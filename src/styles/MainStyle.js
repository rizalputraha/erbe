import Color from './Color'
/**
 * Set size of margin, text, border, radius by default
 */
const margin = 15;
const marginSmall = 10;
const marginExtraSmall = 5;
const marginBig = 20;

const padding = 15;

const paragraph = 14;
const h1 = 30;
const h2 = 24;
const h3 = 20;
const h4 = 18;
const h5 = 16;

const lineHeightP = 20;
const lineHeightH1 = 40;
const lineHeightH2 = 35;
const lineHeightH3 = 30;
const lineHeightH4 = 25;
const lineHeightH5 = 20;

const radius = 5;
const radiusCurve = 3;

const flexDefault = 1;

const border = 1;
const borderBold = 2;
const header = { backgroundColor: Color.primary }

const icon = 16;
const bigIcon = 32;

const MainStyle = {
    margin,
    marginSmall,
    marginExtraSmall,
    marginBig,
    padding,
    paragraph,
    h1,
    h2,
    h3,
    h4,
    h5,
    lineHeightP,
    lineHeightH1,
    lineHeightH2,
    lineHeightH3,
    lineHeightH4,
    lineHeightH5,
    radius,
    radiusCurve,
    flexDefault,
    border,
    borderBold,
    header,
    icon,
    bigIcon
}

export default MainStyle