import Color from 'style/Color'
import MainStyle from 'style/MainStyle'

const button = {
    primary: {
        buttonStyle: {
            margin: MainStyle.margin,
            backgroundColor: Color.primary
        },
        containerStyle: {
            margin: 0
        }
    },
    secondary: {
        buttonStyle: {
            margin: MainStyle.margin,
            backgroundColor: Color.secondary
        },
        containerStyle: {
            margin: 0
        }
    }
}
export default button;