import Color from 'style/Color'
import MainStyle from 'style/MainStyle'

const input = {
    inputContainerStyle: {
        blur: {
            borderBottomWidth : 1,
            borderBottomColor: Color.grey,
            margin: MainStyle.marginExtraSmall
        },
        focus: {
            borderBottomWidth : 2,
            borderBottomColor: Color.primary,
            margin: MainStyle.marginExtraSmall
        }
    },
    inputStyle: {
        margin: 0
    },
    labelStyle: {
        color: Color.grey,
        marginLeft: MainStyle.marginExtraSmall,
        fontWeight: 'normal'
    }
}
export default input;