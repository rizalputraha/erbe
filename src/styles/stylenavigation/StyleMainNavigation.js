import { Platform } from 'react-native'
const Style = {
    iconBottomNavigation: {
        fontSize: 20,
        marginTop: Platform.OS=='ios'?5:0
    }
}

export default Style