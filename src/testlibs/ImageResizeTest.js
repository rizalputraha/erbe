import React, { Component } from 'react'
import { Text, View, Image,TouchableOpacity } from 'react-native'
import ImagePicker from 'react-native-image-picker'
import ImageResizer from 'react-native-image-resizer'
import * as ImageManipulator from 'react-native-image-manipulator'

const options = {
    title: 'Select Avatar',
    customButtons: [{ name: 'fb', title: 'Choose Photo From Facebook' }],
    storageOptions: {
       skipBackup: true,
       path: 'images',
    }
 }

export default class ImageResizeTest extends Component {
    constructor(props) {
        super(props);
        this.state = {
           avatarSource: '',
           resizedImageUri: '',
           image: '',
           heightResize: null,
           widthResize: null,
        }
     }
  
     openCamera() {
        ImagePicker.launchCamera(options, (response) => {
           console.log("Response : ", response);
  
           if (response.didCancel) {
              console.log("user cancelled image picker");
           } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
           } else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
           } else {
              // const source = { uri: response.uri };
  
              // You can also display the image using data:
              const source = { uri: 'data:image/jpeg;base64,' + response.data };
  
              this.setState({
                 avatarSource: source,
                 heightResize: response.height,
                 heightResize: response.width,
              });
  
           }
        });
     }
  
     openLibrary() {
        ImagePicker.launchImageLibrary(options, (response) => {
           console.log("Response : ", response);
  
           if (response.didCancel) {
              console.log("user cancelled image picker");
           } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
           } else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
           } else {
              // const source = { uri: response.uri };
  
              // You can also display the image using data:
              const source = { uri: 'data:image/jpeg;base64,' + response.data };
  
              this.setState({
                 avatarSource: source,
              });
  
           }
        });
     }

     async imgBase64(response){
        let res = await ImageManipulator.manipulateAsync(
            response.uri,[],{base64:true}
        )
        console.log(res);
        
        this.setState({image: res.base64})
     }

     async resize(){
        // ImageResizer.createResizedImage(this.state.avatarSource.uri, 80, 60, 'JPEG', 100)
        // .then((response) => {
        //     this.imgBase64(response)
        // })
        // .catch(err => {
        //   console.log(err);
        //   return Alert.alert(
        //     'Unable to resize the photo',
        //     'Check the console for full the error message',
        //   );
        // });
        let res = await ImageManipulator.manipulateAsync(
            this.state.avatarSource.uri,[{resize:{width:this.state.widthResize/2,height:this.state.heightResize/2}}],{compress:1,base64:true}
        )
        console.log({user_id: 1,file: res.base64});
        
        this.setState({image: res.base64})
     }
  
     render() {
        return (
           <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Image source={{...this.state.avatarSource}} style={{ height: 200, width: 200 }}></Image>
              <TouchableOpacity style={{ backgroundColor: '#2F3061', padding: 10, borderRadius: 5 }} onPress={() => this.openCamera()}>
                 <Text style={{ color: '#fff', fontSize: 18, fontWeight: 'bold' }}>Open Camera</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ backgroundColor: '#2F3061', padding: 10, borderRadius: 5 }} onPress={() => this.openLibrary()}>
                 <Text style={{ color: '#fff', fontSize: 18, fontWeight: 'bold' }}>Open Album</Text>
              </TouchableOpacity>
              <Image source={{uri: `data:image/jpg;base64,${this.state.image}`}} style={{ height: 200, width: 200 }}></Image>
              <TouchableOpacity style={{ backgroundColor: '#2F3061', padding: 10, borderRadius: 5 }} onPress={() => this.resize()}>
                 <Text style={{ color: '#fff', fontSize: 18, fontWeight: 'bold' }}>Resize Image</Text>
              </TouchableOpacity>
           </View>
        )
     }
}
