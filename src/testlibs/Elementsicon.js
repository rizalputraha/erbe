import React from 'react'
import { ScrollView, View, Text, StyleSheet, TextInput } from 'react-native'
import { Button, FormInput as Input} from "react-native-elements"
import Color from 'style/Color'
import { NextgIcon } from 'component/v3'

export default class App extends React.Component {
    constructor(props){
        super(props)
        this.state={
            styleInputUsername: { borderColor: 'grey', borderWidth: 1, marginHorizontal: 15, borderRadius: 50, paddingLeft: 15, color: 'black', marginTop: 15 },
            styleInputPassword: { borderColor: 'grey', borderWidth: 1, marginHorizontal: 15, borderRadius: 50, paddingLeft: 15, color: 'black', marginTop: 15 }
        }
    }

    async setInputBlur(param) {
        if(param==='username') {
          await this.setState({
            styleInputUsername: { borderColor: 'grey', borderWidth: 1, marginHorizontal: 15, borderRadius: 50, paddingLeft: 15, color: 'black', marginTop: 15 }
          });
        } else {
          await this.setState({
            styleInputPassword: { borderColor: 'grey', borderWidth: 1, marginHorizontal: 15, borderRadius: 50, paddingLeft: 15, color: 'black', marginTop: 15 }
          });
        }
      }
    
    async setInputFocus(param) {
        if(param==='username') {
            await this.setState({
            styleInputUsername: { borderColor: Colors.borderRed, borderWidth: 1, marginHorizontal: 15, borderRadius: 50, paddingLeft: 15, color: 'black', marginTop: 15 }
            });
        } else {
            await this.setState({
            styleInputPassword: { borderColor: Colors.borderRed, borderWidth: 1, marginHorizontal: 15, borderRadius: 50, paddingLeft: 15, color: 'black', marginTop: 15 }
            });
        }
    }

    render() {
        return(
            <ScrollView style={styles.container}>
                <View>
                <TextInput 
                    placeholder='Username' 
                    style={this.state.styleInputUsername} 
                    onBlur={ ()=> this.setInputBlur('username') } 
                    onFocus={ ()=> this.setInputFocus('username') }
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholderTextColor= 'black'
                    onChangeText={ (username) => this.setState({username}) }
                />

                <Text style={{fontFamily: 'DroidSans', fontSize: 20}}>L</Text>

                <TextInput
                    placeholder='Password' 
                    style={this.state.styleInputPassword} 
                    onBlur={ ()=> this.setInputBlur('password') } 
                    onFocus={ ()=> this.setInputFocus('password') }
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholderTextColor='black'
                    onChangeText={ (password) => this.setState({password}) } 
                    secureTextEntry={true}
                />

                <NextgIcon
                    name='invoice-2'
                    color='#f50'
                />

                <Button
                    large
                    icon={{name: 'squirrel', type: 'octicon', buttonStyle: styles.someButtonStyle }}
                    title='OCTICON'
                />
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    left: {
        height: 300,
        backgroundColor: 'red'
    },
    right: {
        height: 300,
        backgroundColor: 'green'
    }
})