import React from 'react'
import { ScrollView, View, Text, StyleSheet } from 'react-native'
import { Col, Row, Grid } from "react-native-easy-grid";

export default class App extends React.Component {
    constructor(props){
        super(props)
        this.state={}
    }

    render() {
        return(
            <ScrollView style={styles.container}>
                <View>
                    <Grid>
                        <Col style={styles.left}></Col>
                        <Col style={styles.right}></Col>
                    </Grid>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    left: {
        height: 300,
        backgroundColor: 'red'
    },
    right: {
        height: 300,
        backgroundColor: 'green'
    }
})