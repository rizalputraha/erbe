import React from 'react'
import { View, Text, Button } from 'react-native'
import { createAppContainer, createBottomTabNavigator, createStackNavigator, createSwitchNavigator } from 'react-navigation'


class LoginScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state={...this.props.navigation}
    }
    componentDidMount() {
        console.log('LoginScreen', this.state)
    }
    login(){
        this.props.navigation.navigate('Main');
    }
    render() {
        return (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
            <Text>Login Screen</Text>
            <Button title="Dashboard" onPress={() => this.login()}></Button>
        </View>
        );
    }
}

class ForgotScreen extends React.Component {
    goBack(){
        this.props.navigation.navigate('Login');
    }
    render() {
        return (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
            <Text>Back Login Screen</Text>
            <Button title="Login" onPress={() => this.goBack()}></Button>
        </View>
        );
    }
}

class HomeScreen extends React.Component {
    render() {
        return (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
            <Text>Home Screen</Text>
        </View>
        );
    }
}

class DetailScreen extends React.Component {
    render() {
        return (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
            <Text>Detail Screen</Text>
        </View>
        );
    }
}

class OptionsScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state={...this.props.navigation}
    }
    componentDidMount() {
        console.log('OptionsScreen', this.state)
    }
    logout() {
        this.props.navigation.navigate('Auth');
    }

    render() {
        return (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
            <Text>Option Screen</Text>
            <Button title="Logout" onPress={() => this.logout()}></Button>
        </View>
        );
    }
}

const MainNavigator = createBottomTabNavigator({
    Home: HomeScreen,
    Detail: DetailScreen,
    Option: OptionsScreen,
});

const AuthNavigator = createStackNavigator({
    Login: LoginScreen,
    Forgot: ForgotScreen,
});

const AppNavigator = createSwitchNavigator({
    Auth: AuthNavigator,
    Main: MainNavigator,
});

export default createAppContainer(AppNavigator);