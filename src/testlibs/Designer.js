import React from 'react'
import { ScrollView, View, Text, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';



export default class Designer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>Test Icon</Text>
                <Icon name="user" size={30} color="#000" />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})