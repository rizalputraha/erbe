import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Image } from 'react-native'
import ImagePicker from 'react-native-image-picker'
import { writeFile, loadFile, deleteFile, readDir, deleteDir } from '../cache/model/FileModel';
import { SIMASJIWA_SIGNATURE_PEMEGANGPOLIS } from '../cache/constant/MasterFile';

export class FileSystemTest extends Component {

    constructor(props) {
        super(props);
        this.state = {
            avatarSource: '',
            resFs: '',
        }
    }

    async createDoc() {
        await writeFile('123', this.state.avatarSource, 'abc');
    }

    async loadDoc() {
        let res = await loadFile('681b39dbefbc2d6bcdf26312b527b0d9', 'msa');
        await this.setState({
            resFs: res
        })
        console.log('state', this.state.resFs);
    }

    async deleteDoc() {
        await deleteFile("d71b8d176d7db0618bb06d35b3f6427d", 'xyz')
    }

    async readDir() {
        await readDir('simasjiwa')
    }

    async deleteDir() {
        await deleteDir('simasjiwa')
    }

    openLibrary() {
        const options = {
            title: 'Select Avatar',
            customButtons: [{ name: 'fb', title: 'Choose Photo From Facebook' }],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            }
        }
        ImagePicker.launchImageLibrary(options, (response) => {
            console.log("Response : ", response);

            if (response.didCancel) {
                console.log("user cancelled image picker");
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                // const source = { uri: response.uri };

                // You can also display the image using data:
                const source = response.data;

                this.setState({
                    avatarSource: JSON.stringify(source),
                });

            }
        });
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text> TEST FILESYSTEM </Text>

                <TouchableOpacity style={{ borderRadius: 5, elevation: 2, marginVertical: 5, padding: 10, backgroundColor: '#2962ff' }} onPress={() => this.openLibrary()}>
                    <Text style={{ color: 'white' }}>PICK IMAGE</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{ borderRadius: 5, elevation: 2, marginVertical: 5, padding: 10, backgroundColor: '#2962ff' }} onPress={() => this.createDoc()}>
                    <Text style={{ color: 'white' }}>SAVE</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{ borderRadius: 5, elevation: 2, marginBottom: 4, padding: 10, backgroundColor: '#2962ff' }} onPress={() => this.loadDoc()}>
                    <Text style={{ color: 'white' }}>LOAD</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{ borderRadius: 5, elevation: 2, marginBottom: 4, padding: 10, backgroundColor: '#2962ff' }} onPress={() => this.deleteDoc()}>
                    <Text style={{ color: 'white' }}>DELETE</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{ borderRadius: 5, elevation: 2, marginBottom: 4, padding: 10, backgroundColor: '#2962ff' }} onPress={() => this.readDir()}>
                    <Text style={{ color: 'white' }}>READ DIR</Text>
                </TouchableOpacity>

                <TouchableOpacity style={{ borderRadius: 5, elevation: 2, marginBottom: 4, padding: 10, backgroundColor: '#2962ff' }} onPress={() => this.deleteDir()}>
                    <Text style={{ color: 'white' }}>DELETE DIR</Text>
                </TouchableOpacity>

                <Text> PLEASE CEK LOG IF YOU WANT TEST </Text>
                <Image style={{ width: 100, height: 100 }} source={{ uri: 'data:image/jpeg;base64,' + this.state.resFs }}></Image>
            </View>
        )
    }
}

export default FileSystemTest
