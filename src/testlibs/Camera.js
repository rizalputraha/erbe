import React from 'react'
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native'
import { RNCamera } from 'react-native-camera'
import Permission from '../constant/Permission'
import Styles from '../styles/component/CameraStyle'


export default class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            type: 'back',
            flash: 'off',
            autoFocus: 'on',
            autoFocusPoint: {
                normalized: { x: 0.5, y: 0.5 }, // normalized values required for autoFocusPointOfInterest
                drawRectPosition: {
                    x: Dimensions.get('window').width * 0.5 - 32,
                    y: Dimensions.get('window').height * 0.5 - 32,
                },
            },
            whiteBalance: 'auto',
            ratio: '16:9',
            depth: 0
        }
    }

    takePicture = async function () {
        if (this.camera) {
            const data = await this.camera.takePictureAsync();
            console.warn('takePicture ', data);
        }
    };

    render() {
        return (
            <RNCamera
                ref={ref => {
                    this.camera = ref;
                }}
                style={
                    Styles.cameraContainer
                }
                type={this.state.type}
                flashMode={this.state.flash}
                autoFocus={this.state.autoFocus}
                autoFocusPointOfInterest={this.state.autoFocusPoint.normalized}
                androidCameraPermissionOptions={Permission.AndroidCamera}
                androidRecordAudioPermissionOptions={Permission.AndroidRecordAudio}
                whiteBalance={this.state.whiteBalance}
                ratio={this.state.ratio}
                focusDepth={this.state.depth}
            >
                <TouchableOpacity
                    style={Styles.cameraSnapButton}
                    onPress={this.takePicture.bind(this)}
                >
                    <Text style={Styles.cameraSnapTitle}> SNAP </Text>
                </TouchableOpacity>
            </RNCamera>
        )
    }
}