import React from 'react'
import { View, Image } from 'react-native'
import { loadFile, readRootDir } from 'erbecache/model/FileModel'

class CacheScreenshoot extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            screenShoot: ''
        }
    }

    async componentDidMount() {
        const file = await readRootDir()
        console.log('File', file)
        const pp = await loadFile('6fd66a83d027135cba0bb4db6cd9f0ef.txt')
        const tt = await loadFile('f7bda3de86b2bb2557bdd2f98859cb96.txt')
        const cp = await loadFile('13aa45fe13d8d9369f775004b8a1b4b9.txt')
        const ua = await loadFile('798eed591e17a2ee29b09128023b83ae.txt')
        const di = await loadFile('6759680ba2bb31d350556c17b326c74a.txt')
        const da = await loadFile('bf72df44446e29f4050d801daf505fc1.txt')
        const qu = await loadFile('4953da10898670d2aafc52ad23048e61.txt')
        const pr = await loadFile('bdb3be1ae4637ee22607008ee50160d9.txt')
        const pn = await loadFile('f710bfe763e4bd402bc2a871ec2900b3.txt')
        const sp = await loadFile('546b0148bddf9b36316347a0a9935dcc.txt')
        const st = await loadFile('a14529379a6f87c2420120d11328c45d.txt')
        const fd = await loadFile('fdf6375aab0e7c47d7d92e1d684c36be.txt')
        await this.setState({screenShoot: fd})
        __DEV__ && console.log(this.state)
    }

    render() {
        return(
            this.state.screenShoot !== '' &&
            <View style={{margin: 15, padding: 10, borderWidth: 1, borderColor: 'red', backgroundColor: 'white'}}>
                <Image source={{uri: this.state.screenShoot}} style={{width: null, height: 300, resizeMode: 'contain'}} />
            </View>
        )
    }
}

export default CacheScreenshoot