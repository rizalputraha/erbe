import React, {Component} from 'react'
import { View, Text } from 'react-native'
import BtnBirthDayTertanggung from '../contents/components/v3/BtnBirthDayTertanggung'

export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dob: '',
            usia: ''
        }
        console.log('state apps', this.state)
    }

    render() {
        return (
            <View>
                <BtnBirthDayTertanggung
                    setDob={(value) => {
                        this.setState({dob: value})
                        console.log('setDob state datepicker', this.state)
                    }}
                    getDob={this.state.dob}
                    setUsia={(value) => {
                        this.setState({usia: value})
                        console.log('setDob state datepicker', this.state)
                    }}
                    getUsia={this.state.usia}
                    backNav={() => this.props.navigation.goBack(null)}
                    minYear={0}
                    minDay={1}
                    minWeek={2}
                    maxYear={70}
                    titleButton={'Tanggal Lahir'}
                />
            </View>
        )
    }
}