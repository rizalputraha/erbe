import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Image } from 'react-native'
import ImagePicker from 'react-native-image-picker'

const options = {
   title: 'Select Avatar',
   customButtons: [{ name: 'fb', title: 'Choose Photo From Facebook' }],
   storageOptions: {
      skipBackup: true,
      path: 'images',
   }
}

export default class ImagePickerTest extends Component {

   constructor(props) {
      super(props);
      this.state = {
         avatarSource: ''
      }
   }

   openCamera() {
      ImagePicker.launchCamera(options, (response) => {
         console.log("Response : ", response);

         if (response.didCancel) {
            console.log("user cancelled image picker");
         } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
         } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
         } else {
            // const source = { uri: response.uri };

            // You can also display the image using data:
            const source = { uri: 'data:image/jpeg;base64,' + response.data };

            this.setState({
               avatarSource: source,
            });

         }
      });
   }

   openLibrary() {
      ImagePicker.launchImageLibrary(options, (response) => {
         console.log("Response : ", response);

         if (response.didCancel) {
            console.log("user cancelled image picker");
         } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
         } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
         } else {
            // const source = { uri: response.uri };

            // You can also display the image using data:
            const source = { uri: 'data:image/jpeg;base64,' + response.data };

            this.setState({
               avatarSource: source,
            });

         }
      });
   }

   render() {
      return (
         <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Image source={{...this.state.avatarSource}} style={{ height: 200, width: 200 }}></Image>
            <TouchableOpacity style={{ backgroundColor: '#2F3061', padding: 10, borderRadius: 5 }} onPress={() => this.openCamera()}>
               <Text style={{ color: '#fff', fontSize: 18, fontWeight: 'bold' }}>Open Camera</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{ backgroundColor: '#2F3061', padding: 10, borderRadius: 5 }} onPress={() => this.openLibrary()}>
               <Text style={{ color: '#fff', fontSize: 18, fontWeight: 'bold' }}>Open Album</Text>
            </TouchableOpacity>
         </View>
      )
   }
}
