import React from 'react'
import { ScrollView, View, Text } from 'react-native'
import DeviceInfo from 'react-native-device-info'

export default class App extends React.Component {
    constructor(props) {
        super(props)
        this.state={}
    }

    renderAppName(){
        const appName = DeviceInfo.getApplicationName();
        return(
            <Text>{appName}</Text>
        );
    }

    renderUniqueId() {
        const uniqueId = DeviceInfo.getUniqueID();
        return(
            <Text>{uniqueId}</Text>
        );
    }

    render(){
        return(
            <ScrollView>
                <View>
                    {this.renderAppName()}
                </View>
                <View>
                    {this.renderUniqueId()}
                </View>
            </ScrollView>
        );
    }
}