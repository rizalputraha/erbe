import React from 'react'
import { View, Text } from 'react-native'
import { Grid, Col, Row } from 'react-native-easy-grid'
import Style from 'stylecomponent/DataAgenStyle'
import { getKomisi } from 'erberedux/actions'
import { connect } from 'react-redux'
import { getLevelAgen, formatCurrency } from 'erbevalidation/Helper'

class DataAgen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    async componentDidMount(){
        await this.props.getKomisi(this.props.authUser.token) 
    }

    checkLevel(){
        return getLevelAgen(this.props.authUser.user_data.level)
    }

    render() {
        if(this.props.authUser.user_data) {
            return(
                <Grid style={Style.container}>
                    <Col>
                        <Text style={Style.title} note>Bonus</Text>
                        <Text style={Style.content}>{formatCurrency(this.props.komisi.total)}</Text>
                    </Col>
                    <Col style={Style.borderLeft}>
                        <Text style={Style.title} note>Your Level</Text>
                        <Text style={Style.contentLevel}>{ this.checkLevel() }</Text>
                    </Col>
                </Grid>
            )
        } else {
            return(
                <Grid style={Style.container}>
                    <Col>
                        <Text style={Style.title} note>Bonus</Text>
                        <Text style={Style.content}></Text>
                    </Col>
                    <Col style={Style.borderLeft}>
                        <Text style={Style.title} note>Your Level</Text>
                        <Text style={Style.contentLevel}></Text>
                    </Col>
                </Grid>
            )
        }
    }
}

const mtp = ({Report, Auth}) => {
    const { komisi } = Report
    const { authUser } = Auth
    return { komisi, authUser }
}

export default connect(mtp, {getKomisi} )(DataAgen)