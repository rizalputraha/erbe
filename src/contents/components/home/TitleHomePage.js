import React from 'react'
import { View, Text } from 'react-native'

const TitleHomePage = (props) => {
   return (
      <View style={{ ...props.style, flex: 1, alignItems: 'center', justifyContent: 'center' }}>
         <Text style={{ ...props.textStyle, fontSize: 28 }}>{props.title}</Text>
      </View>
   )
}

export default TitleHomePage