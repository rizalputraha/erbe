import React from 'react'
import { View, Text, Platform, TouchableOpacity, Image } from 'react-native'
import Style from 'stylecomponent/MainMenuStyle'
import { Grid, Row, Col } from 'react-native-easy-grid'
import {
    sagaPerusahaanAsuransi,
    getCacheInsurance,
    pushToScreen,
} from 'erberedux/actions'
import { getIconSource } from 'erbevalidation/Helper'
import { connect } from 'react-redux'

class MainMenu extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    async componentDidMount() {
        this.props.getCacheInsurance()
    }

    gotoScreen(item) {
        this.props.sagaPerusahaanAsuransi(item);
    }

    renderChildIconDisabled(item) {
        return(
            <View style={Style.wrapCol}>
                <Image source={getIconSource(item.icon)} style={Style.icon} resizeMode="contain" />
                <Text style={Style.iconText}>{item.category}</Text>
                <View style={Style.maintenance}></View>
            </View>
        )
    }

    renderChildIconEnable(item) {
        return(
            <TouchableOpacity
                onPress={() => this.gotoScreen(item)}
                style={Style.buttonArea}
            >
                <View style={Style.wrapCol}>
                    <Image source={getIconSource(item.icon)} style={Style.icon} resizeMode="contain" />
                    <Text style={Style.iconText}>{item.category}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    renderChildIcon(item) {
        const { showGlobalLoading } = this.props
        if (item.asuransi === false) {
            if (Platform.OS === 'ios') {
                return (
                    <View style={Style.wrapCol}>
                    </View>
                );
            } else {
                return this.renderChildIconDisabled(item)
            }
        }
        return showGlobalLoading === false ? this.renderChildIconEnable(item) : this.renderChildIconDisabled(item)
    }

    renderChildItem(data) {
        return data.map((item, index) => {
            return (
                <Col style={Style.col} key={index}>{this.renderChildIcon(item)}</Col>
            );
        })
    }

    renderItem() {
        if (this.props.mainMenu.length > 0) {
            return this.props.mainMenu.map((item, index) => {
                return (
                    <Grid style={Style.container} key={index}>
                        {this.renderChildItem(item)}
                    </Grid>
                )
            })
        }
    }

    render() {
        const { showFirstLoading } = this.props
        return (
            showFirstLoading && <View></View> ||
            <View>
                {this.renderItem()}
            </View>
        )
    }
}

const mtp = ({ Insurance, ErrorHandling }) => {
    const { asuransi, mainMenu } = Insurance;
    const { showFirstLoading, showGlobalLoading } = ErrorHandling
    return { asuransi, mainMenu, showFirstLoading, showGlobalLoading }
}

export default connect(mtp, { sagaPerusahaanAsuransi, getCacheInsurance, pushToScreen })(MainMenu)