import TitleHomePage from './TitleHomePage'
import DataAgen from './DataAgen'
import MainMenu from './MainMenu'
import ShareReferal from './ShareReferal'

module.exports={
    TitleHomePage,
    DataAgen,
    MainMenu,
    ShareReferal
}