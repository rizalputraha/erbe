import React from 'react'
import { View, Text, Alert, Share, TouchableOpacity, Image, Platform } from 'react-native'
import { connect } from 'react-redux'
import Style from 'stylecomponent/ShareReferalStyle'
import { shareButton } from 'erberedux/actions';

class ShareReferal extends React.Component {

    constructor(props) {
        super(props)
        this.state={}
    }

    getContent() {
        let referalID = this.props.authUser.user_data.reff_id;
        const data =
		{
			message: '*Raih impian dgn penghasilan tanpa batas,* _Bisnis Digital bersama Erbe Corp. Daftar segera..._ https://erbenetwork.com/promote?referal='+referalID,
			url: 'https://www.erbenetwork.com/api/v2/auth/referal?id='+referalID,
			title: 'Broadcast Your Referal Code'
		}

		return data;
    }

    getContentOptions() {
        const activityIOS = {
			subject: 'Share your referal code',
		}
		const activityAndroid =
		{
			dialogTitle: 'Share your referal code'
		}
		if(Platform.OS=='ios') return activityIOS;
		return activityAndroid;
    }

    shareButton(){
		if (this.props.isReferalShared === false) {
			Alert.alert('Share Referal', 'Buat asuransi jiwa dan lakukan pembayaran untuk aktivasi fitur ini')
		} else {
			this.props.shareButton(this.getContent(),this.getContentOptions());
		}
    }
      
    render() {
        if(this.props.isReferalShared === true) {
            return(
                <View style={Style.container}>
                    <TouchableOpacity style={Style.container} onPress={()=>this.shareButton()}>
                      <Text style={Style.title}>Share Your Referal</Text>
                        <Image
                            style={{width: 40,height: 40,margin:20}}
                            source ={require('../../../assets/images/Referral.png')}
                        />
                    </TouchableOpacity>
                </View>
            )
        }
        return(
            <View style={Style.container}>
            </View>
        )
    }
}

const mtp = ({ Dashboard, Auth }) => {
    const { isReferalShared } = Dashboard
    const { authUser } = Auth
    return { isReferalShared, authUser }
}

export default connect(mtp, {shareButton})(ShareReferal)