import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { masterWilayah } from 'erbecache/model/SimasModel'
import ModalListSearchFilter from '../v3/ModalListSearchFilter'
import { ListItem } from 'react-native-elements';

export class WilayahOptions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataProvinsi: null,
            dataKota: null,
            selectedProvinsi: false,
            selectedKota: null,
            isCompletedLoadData: false,
        }
    }

    async componentDidMount() {
        const wilayah = await masterWilayah();
        this.setState({
            dataProvinsi: wilayah,
            isCompletedLoadData: true
        })
        if (this.props.selectedProvinsi) {
            this.setState({ dataKota: this.filterKota(this.props.selectedProvinsi) })
        }
    }

    renderProvinsi() {
        return (
            <View>
                {
                    this.state.isCompletedLoadData && <ModalListSearchFilter
                        title={'Pilih Provinsi'}
                        titleEmptyData={'Pilih Provinsi'}
                        titleForm={true}
                        paramSelected={'provinceId'}
                        paramView={'provinceName'}
                        data={this.state.dataProvinsi}
                        selected={this.props.selectedProvinsi}
                        onChangeItem={async (value) => {
                            this.props.onChangeProvinsi(value),
                                await this.setState({ dataKota: this.filterKota(value) })
                            console.log(this.state);
                        }
                        }
                    /> || <Text>Loading Data...</Text>
                }
            </View>
        )
    }

    filterKota(provinceId) {
        const kota = this.state.dataProvinsi.filter(item => item.provinceId == provinceId);
        return kota[0].kota;
    }

    renderKota() {
        return (
            <View>
                {
                    this.state.dataKota && <ModalListSearchFilter
                        title={'Pilih Kota'}
                        titleEmptyData={'Pilih Kota'}
                        titleForm={true}
                        paramSelected={'cityId'}
                        paramView={'cityName'}
                        data={this.state.dataKota}
                        selected={this.props.selectedKota}
                        onChangeItem={async (value) => await this.props.onChangeKota(value)} />

                }
            </View>
        )
    }

    render() {
        return (
            <View>
                {this.renderProvinsi()}
                {this.renderKota()}
            </View>
        )
    }
}

export default WilayahOptions
