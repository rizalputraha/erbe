import React from 'react'
import { View, Text } from 'react-native'
import { connect } from 'react-redux'
import { CheckBox } from 'react-native-elements'
import { setKeyRootSimas, setStateSpajPPSimas, setStateSpajTTSimas } from 'erberedux/actions'
import { InputComponent } from '../v3'
import { Grid, Col, Row } from 'react-native-easy-grid'
import Color from 'style/Color'

class NpwpOptionPemegangPolis extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        console.log('NpwpOptionPp', this.props)
    }

    renderFormNpwp() {
        const { npwpOptionPp, spaj, setStateSpajPPSimas } = this.props
        const { pemegangPolis } = spaj
        return (
            npwpOptionPp.optionYa &&
            <InputComponent
                label={'NPWP'}
                placeholder={''}
                value={pemegangPolis.npwpPp}
                onChangeText={(value) => {
                    setStateSpajPPSimas('npwpPp', value)
                }}
                validation={['required', 'length']}
                minCharacter={3}
                maxCharacter={20}
            />
        )
    }

    render() {
        const { npwpOptionPp, setStateSpajPPSimas } = this.props
        return (
            npwpOptionPp &&
            <View style={{
                marginHorizontal: 5
            }}>
                <Grid>
                    <Col>
                        <View sytle={{ flex: 1, marginHorizontal: 15 }}>
                            <Text style={{
                                fontSize: 14,
                                color: Color.darkLight,
                                marginLeft: 10
                            }}>
                                Apakah anda mempunyai npwp ?</Text>
                        </View>
                    </Col>
                </Grid>

                <Grid>
                    <Col>
                        <CheckBox
                            title="Ya"
                            size={16}
                            textStyle={{ fontSize: 12 }}
                            checkedIcon="dot-circle-o"
                            uncheckedIcon="circle-o"
                            checked={npwpOptionPp.optionYa}
                            onPress={() => {
                                this.props.setKeyRootSimas('npwpOptionPp', {
                                    optionYa: true,
                                    optionTidak: false
                                }),
                                    setStateSpajPPSimas('npwpPp', '')
                            }}
                            checkedColor="green"
                            containerStyle={{
                                backgroundColor: "white",
                                borderWidth: 0,
                                borderBottomWidth: 2,
                                borderBottomColor: Color.greyLight
                            }}
                        />
                    </Col>
                    <Col>
                        <CheckBox
                            title="Tidak"
                            size={16}
                            textStyle={{ fontSize: 12 }}
                            checkedIcon="dot-circle-o"
                            uncheckedIcon="circle-o"
                            checked={npwpOptionPp.optionTidak}
                            onPress={() => {
                                this.props.setKeyRootSimas('npwpOptionPp', {
                                    optionYa: false,
                                    optionTidak: true
                                }),
                                    this.props.setKeyRootSimas('npwpOptionTt', {
                                        optionYa: false,
                                        optionTidak: true
                                    }),
                                    setStateSpajPPSimas('npwpPp', '123456789'),
                                    setStateSpajTTSimas('npwpTt', '123456789')
                            }}
                            checkedColor="green"
                            containerStyle={{
                                backgroundColor: "white",
                                borderWidth: 0,
                                borderBottomWidth: 2,
                                borderBottomColor: Color.greyLight
                            }}
                        />
                    </Col>
                </Grid>
                <Grid>
                    <Col>
                        {this.renderFormNpwp()}
                    </Col>
                </Grid>
            </View>
        )
    }
}



const mtp = ({ SimasJiwa }) => {
    const { npwpOptionPp, spaj } = SimasJiwa
    return { npwpOptionPp, spaj }
}

export default connect(mtp, { setKeyRootSimas, setStateSpajPPSimas })(NpwpOptionPemegangPolis)