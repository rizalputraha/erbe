import React from 'react'
import { View, Text } from 'react-native'
import { connect } from 'react-redux'
import { CheckBox } from 'react-native-elements'
import { setKeyRootSimas, setStateSpajSimas } from 'erberedux/actions'
import { InputComponent } from '../v3'
import { Grid, Col, Row } from 'react-native-easy-grid'
import Color from 'style/Color'

class NpwpOptionTertanggung extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isCompleteLoadData: false
        }
    }

    async componentDidMount() {
        const { proposal, setKeyRootSimas, setStateSpajSimas } = this.props
        if (proposal.tertanggung.usiaTt < 17) {
            setKeyRootSimas('npwpOptionTt', {
                optionYa: false,
                optionTidak: true
            })
            setStateSpajSimas('tertanggung', 'npwpTt', '123456789')
            await this.setState({ isCompleteLoadData: true })
        } else {
            await this.setState({ isCompleteLoadData: true })
        }
        console.log('NpwpOptionTt', this.props)
    }

    renderFormNpwp() {
        const { npwpOptionTt, spaj, setStateSpajSimas } = this.props
        const { espaj } = spaj
        const { tertanggung } = espaj
        return (
            npwpOptionTt.optionYa &&
            <InputComponent
                label={'NPWP'}
                placeholder={''}
                value={tertanggung.npwpTt}
                onChangeText={(value) => {
                    setStateSpajSimas('tertanggung', 'npwpTt', value)
                }}
                validation={['required', 'length']}
                minCharacter={3}
                maxCharacter={20}
            />
        )
    }

    render() {
        const { npwpOptionTt, setStateSpajSimas, proposal } = this.props
        return (
            this.state.isCompleteLoadData &&
            <View style={{
                marginHorizontal: 5
            }}>
                <Grid>
                    <Col>
                        <View sytle={{ flex: 1, marginHorizontal: 15 }}>
                            <Text style={{
                                fontSize: 14,
                                color: Color.darkLight,
                                marginLeft: 10
                            }}>
                                Apakah tertanggung mempunyai npwp ?</Text>
                        </View>
                    </Col>
                </Grid>

                <Grid>
                    <Col>
                        <CheckBox
                            title="Ya"
                            size={16}
                            textStyle={{ fontSize: 12 }}
                            checkedIcon="dot-circle-o"
                            uncheckedIcon="circle-o"
                            checked={npwpOptionTt.optionYa}
                            onPress={() => {
                                this.props.setKeyRootSimas('npwpOptionTt', {
                                    optionYa: true,
                                    optionTidak: false
                                }),
                                    setStateSpajSimas('tertanggung', 'npwpTt', '')
                            }}
                            checkedColor="green"
                            containerStyle={{
                                backgroundColor: "white",
                                borderWidth: 0,
                                borderBottomWidth: 2,
                                borderBottomColor: Color.greyLight
                            }}
                        />
                    </Col>
                    <Col>
                        <CheckBox
                            title="Tidak"
                            size={16}
                            textStyle={{ fontSize: 12 }}
                            checkedIcon="dot-circle-o"
                            uncheckedIcon="circle-o"
                            checked={npwpOptionTt.optionTidak}
                            onPress={() => {
                                this.props.setKeyRootSimas('npwpOptionTt', {
                                    optionYa: false,
                                    optionTidak: true
                                }),
                                    setStateSpajSimas('tertanggung', 'npwpTt', '123456789')
                            }}
                            checkedColor="green"
                            containerStyle={{
                                backgroundColor: "white",
                                borderWidth: 0,
                                borderBottomWidth: 2,
                                borderBottomColor: Color.greyLight
                            }}
                        />
                    </Col>
                </Grid>
                <Grid>
                    <Col>
                        {this.renderFormNpwp()}
                    </Col>
                </Grid>
            </View>
        )
    }
}



const mtp = ({ Msig }) => {
    const { npwpOptionTt, spaj, proposal } = Msig
    return { npwpOptionTt, spaj, proposal }
}

export default connect(mtp, { setKeyRootSimas, setStateSpajSimas })(NpwpOptionTertanggung)