import React, { Component } from 'react'
import { Text } from 'react-native'
import { PickerItemValidation } from 'component/v3'
import { masterIdentity } from 'erbecache/model/SimasModel'

class IdentityOptions extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            isCompleteLoadData: false
        }
    }

    async componentDidMount() {
        const identity = await masterIdentity()
        await this.setState({
            isCompleteLoadData: true,
            data: identity
        })
    }

    render() {
        return (
            this.state.isCompleteLoadData && <PickerItemValidation
                title={'Identitas'}
                titleEmptyData={'Pilih Identitas'}
                titleForm={true}
                paramSelected={'identityId'}
                paramView={'identityName'}
                data={this.state.data}
                selected={this.props.selected}
                onChangeItem={(value) => this.props.onChangeItem(value)}
            />
            ||
            <Text>Loading data</Text>
        )
    }
}

export default IdentityOptions