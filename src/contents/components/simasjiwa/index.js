import GenderOptions from './GenderOptions'
import RelationOptions from './RelationOptions'
import ReligionOptions from './ReligionOptions'
import StatusOptions from './StatusOptions'
import GraduatedOptions from './GraduatedOptions'
import IdentityOptions from './IdentityOptions'
import WilayahOptions from './WilayahOptions'
import KusionerOptions from './KusionerOptions';
import OptionYaProposalStarter from './OptionYaProposalStarter';
import OptionTidakProposalStarter from './OptionTidakProposalStarter';
import BankOptions from './BankOptions';
import ButtonAgen from './ButtonAgen';
import ButtonNasabah from './ButtonNasabah';
import InfoSpajSubmit from './InfoSpajSubmit';
import NpwpOptionPemegangPolis from './NpwpOptionPemegangPolis'
import NpwpOptionTertanggung from './NpwpOptionTertanggung'
import BirthdayProposal from './BirthdayProposal'
import RelationManfaatOptions from './RelationManfaatOptions'

module.exports = {
    GenderOptions,
    RelationOptions,
    ReligionOptions,
    StatusOptions,
    GraduatedOptions,
    IdentityOptions,
    WilayahOptions,
    KusionerOptions,
    OptionYaProposalStarter,
    OptionTidakProposalStarter,
    BankOptions,
    ButtonAgen,
    ButtonNasabah,
    InfoSpajSubmit,
    NpwpOptionPemegangPolis,
    NpwpOptionTertanggung,
    BirthdayProposal,
    RelationManfaatOptions
}