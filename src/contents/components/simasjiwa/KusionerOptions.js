import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { CheckBox } from "react-native-elements";
import ErbeListMenu from "../v3/ErbeListMenu";
import InputComponent from "../v3/InputComponent";
import PickerItemValidation from "../v3/PickerItemValidation";
import { setAnswerHealthQuestionerTextSimas, setAnswerHealthQuestionerOptionSimas } from "erberedux/actions";
import { connect } from "react-redux";

export class KusionerOptions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            input: '',
        }
    }

    handleFormInput(value, index) {
        const newInput = this.state.data.map((data, idx) => {
            if (index !== idx) return data;
            return { ...data, answerText: value }
        })

        this.setState({ data: newInput })
    }

    _getLabel(index, item) {
        return (
            <Text>{++index}. {item.label}</Text>
        )
    }

    renderItems(item, index) {
        const { spaj, setAnswerHealthQuestionerTextSimas, setAnswerHealthQuestionerOptionSimas } = this.props;
        // console.log('answer option', item.answerOptions.optionYa);

        return (
            <View>
                {
                    item.isOption == false ?
                        <View>
                            {this._getLabel(index, item)}
                            <InputComponent
                                label={'Jawaban'}
                                placeholder={'Jawaban'}
                                value={item.answerText}
                                onChangeText={(value) => setAnswerHealthQuestionerTextSimas('answerText', value, index)}
                                validation={['required', 'length']}
                            />
                        </View>
                        :
                        <View>
                            {this._getLabel(index, item)}
                            <CheckBox
                                title="Ya"
                                checkedIcon="dot-circle-o"
                                uncheckedIcon="circle-o"
                                checked={item.answerOptions.optionYa}
                                onPress={() => setAnswerHealthQuestionerOptionSimas('optionYa', !item.answerOptions.optionYa, index)}
                                checkedColor="green"
                            />
                            <CheckBox
                                title="Tidak"
                                checkedIcon="dot-circle-o"
                                uncheckedIcon="circle-o"
                                checked={item.answerOptions.optionTidak}
                                onPress={() => setAnswerHealthQuestionerOptionSimas('optionTidak', !item.answerOptions.optionTidak, index)}
                                checkedColor="green"
                            />
                        </View>
                }
            </View>
        )
    }

    renderHealth() {
        console.log('props data', this.props.data);

        return (
            <ErbeListMenu
                tipe="list"
                data={this.props.data.healthTertanggung}
                renderListItem={(item, index) => this.renderItems(item, index)}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }

    render() {
        return (
            <View style={{ margin: 10 }}>
                {this.renderHealth()}
            </View>
        )
    }
}

const mtp = ({ SimasJiwa }) => {
    const { spaj } = SimasJiwa
    return { spaj }
}

export default connect(mtp, { setAnswerHealthQuestionerTextSimas, setAnswerHealthQuestionerOptionSimas })(KusionerOptions)
