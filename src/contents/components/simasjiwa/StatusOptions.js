import React, { Component } from 'react'
import { Text } from 'react-native'
import { PickerItemValidation } from 'component/v3'
import { masterMarriageStatus } from 'erbecache/model/SimasModel'

class StatusOptions extends Component {
    constructor(props) {
        super(props)
        this.state={
            data: [],
            isCompleteLoadData: false
        }
    }

    async componentDidMount() {
        const item = await masterMarriageStatus()
        await this.setState({
            isCompleteLoadData: true,
            data: item
        })
    }

    render() {
        return(
            this.state.isCompleteLoadData && <PickerItemValidation
                title={'Status Pernikahan'}
                titleEmptyData={'Pilih Status Pernikahan'}
                titleForm={true}
                paramSelected={'marriageId'}
                paramView={'marriageName'}
                data={this.state.data}
                selected={this.props.selected}
                onChangeItem={(value) => this.props.onChangeItem(value)}
            />
            ||
            <Text>Loading data</Text>
        )
    }
}

export default StatusOptions