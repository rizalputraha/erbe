import React, { Component } from 'react'
import { View, Text, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
import ErbeText from '../v3/ErbeText'
import Color from "style/Color"
import { formatCurrency } from "erbevalidation/Helper"
import { Icon } from "react-native-elements"
import { Grid, Col } from 'react-native-easy-grid'

export class InfoSpajSubmit extends Component {
    render() {
        const {
            proposal,
            product,
            loadingUploadDocument,
            loadingGenerateProposal,
            loadingSubmitSpaj,
            successFileUpload,
            successGenerateProposal,
            successSubmitSpaj
        } = this.props;

        const styleGrid = { backgroundColor: Color.white, borderBottomWidth: 1, borderBottomColor: Color.greyLight }
        const styleContainer = { marginHorizontal: 15, marginTop: 15, backgroundColor: Color.white, elevation: 5 }
        const styleInfoSpaj = { backgroundColor: Color.primary, borderBottomWidth: 1, borderBottomColor: Color.greyLight }
        const styleLoading = { marginLeft: 15, marginRight: 15, marginTop: 15 }
        const styleIcon = { marginLeft: 15, marginTop: 13 }
        return (
            (proposal && product) &&
            <View style={styleContainer}>
                <Grid style={styleInfoSpaj}>
                    <Col>
                        <ErbeText
                            value={`Pemegang Polis: ${proposal.pemegangPolis.namaPp}`}
                            style={'p'}
                            color={'white'}
                        />
                        <ErbeText
                            value={`Tertanggung: ${proposal.tertanggung.namaTt}`}
                            style={'p'}
                            color={'white'}
                        />
                    </Col>
                </Grid>
                <Grid style={styleGrid}>
                    <Col>
                        <ErbeText
                            value={`Product ${product.product_sub}`}
                            style={'h5'}
                            position={'left'}
                        />
                    </Col>
                </Grid>
                <Grid style={styleGrid}>
                    <Col>
                        <ErbeText
                            value={`Premi ${formatCurrency(product.premi)}`}
                            style={'p'}
                            position={'left'}
                        />
                    </Col>
                </Grid>
                {
                    this.props.isConnected &&
                    <View>
                        <Grid style={styleGrid}>
                            <Col size={65}>
                                <ErbeText
                                    value={`Upload Document`}
                                    style={'h5'}
                                    position={'left'}
                                    color={Color.dark}
                                />
                            </Col>
                            <Col size={15}>
                                {
                                    loadingUploadDocument &&
                                    <ActivityIndicator
                                        size="small"
                                        color={Color.secondary}
                                        style={styleLoading}
                                    />
                                }
                            </Col>
                            <Col size={20}>
                                <Icon
                                    containerStyle={styleIcon}
                                    size={30}
                                    color={successFileUpload ? Color.greenSuccess : Color.redDanger}
                                    name={successFileUpload ? 'check' : 'close-o'}
                                    type="evilicon"
                                />
                            </Col>
                        </Grid>
                        <Grid style={styleGrid}>
                            <Col size={65}>
                                <ErbeText
                                    value={`Generate Proposal`}
                                    style={'h5'}
                                    position={'left'}
                                    color={Color.dark}
                                />
                            </Col>
                            <Col size={15}>
                                {
                                    loadingGenerateProposal &&
                                    <ActivityIndicator
                                        size="small"
                                        color={Color.secondary}
                                        style={styleLoading}
                                    />
                                }
                            </Col>
                            <Col size={20}>
                                <Icon
                                    containerStyle={styleIcon}
                                    size={30}
                                    color={successGenerateProposal ? Color.greenSuccess : Color.redDanger}
                                    name={successGenerateProposal ? 'check' : 'close-o'}
                                    type="evilicon"
                                />
                            </Col>
                        </Grid>
                        <Grid style={styleGrid}>
                            <Col size={65}>
                                <ErbeText
                                    value={`Submit SPAJ`}
                                    style={'h5'}
                                    position={'left'}
                                    color={Color.dark}
                                />
                            </Col>
                            <Col size={15}>
                                {
                                    loadingSubmitSpaj &&
                                    <ActivityIndicator
                                        size="small"
                                        color={Color.secondary}
                                        style={styleLoading}
                                    />
                                }
                            </Col>
                            <Col size={20}>
                                <Icon
                                    containerStyle={styleIcon}
                                    size={30}
                                    color={successSubmitSpaj ? Color.greenSuccess : Color.redDanger}
                                    name={successSubmitSpaj ? 'check' : 'close-o'}
                                    type="evilicon"
                                />
                            </Col>
                        </Grid>
                    </View>
                }
            </View>
        )
    }
}

const mtp = ({ SimasJiwa }) => {
    const {
        proposal,
        product,
        loadingUploadDocument,
        loadingGenerateProposal,
        loadingSubmitSpaj,
        successFileUpload,
        successGenerateProposal,
        successSubmitSpaj
    } = SimasJiwa
    return {
        proposal,
        product,
        loadingUploadDocument,
        loadingGenerateProposal,
        loadingSubmitSpaj,
        successFileUpload,
        successGenerateProposal,
        successSubmitSpaj
    }
}

export default connect(mtp, {})(InfoSpajSubmit)
