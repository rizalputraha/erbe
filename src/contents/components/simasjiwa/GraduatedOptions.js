import React, { Component } from 'react'
import { Text } from 'react-native'
import { PickerItemValidation } from 'component/v3'
import { masterGraduate } from 'erbecache/model/SimasModel'

class GraduatedOptions extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            isCompleteLoadData: false
        }
    }

    async componentDidMount() {
        const grad = await masterGraduate()
        await this.setState({
            isCompleteLoadData: true,
            data: grad
        })
    }

    render() {
        return (
            this.state.isCompleteLoadData && <PickerItemValidation
                title={'Pendidikan'}
                titleEmptyData={'Pilih Pendidikan Terakhir'}
                titleForm={true}
                paramSelected={'graduateId'}
                paramView={'graduateName'}
                data={this.state.data}
                selected={this.props.selected}
                onChangeItem={(value) => this.props.onChangeItem(value)}
            />
            ||
            <Text>Loading data</Text>
        )
    }
}

export default GraduatedOptions