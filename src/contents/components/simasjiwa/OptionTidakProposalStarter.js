import React from 'react'
import { connect } from 'react-redux'
import { CheckBox } from 'react-native-elements'
import { setKeyRootSimas } from 'erberedux/actions'

class OptionTidakProposalStarter extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        console.log('OptionTidakProposalStarter', this.props)
    }

    render() {
        return (
            <CheckBox
                title="Tidak"
                checkedIcon="dot-circle-o"
                uncheckedIcon="circle-o"
                checked={!this.props.isTertanggung}
                onPress={() => {
                    this.props.setKeyRootSimas('isTertanggung', false)
                }}
                checkedColor="green"
            />
        )
    }
}



const mtp = ({ SimasJiwa }) => {
    const { isTertanggung } = SimasJiwa
    return { isTertanggung }
}

export default connect(mtp, { setKeyRootSimas })(OptionTidakProposalStarter)