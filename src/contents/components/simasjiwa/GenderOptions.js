import React, { Component } from 'react'
import { Text } from 'react-native'
import { PickerItemValidation } from 'component/v3'
import { masterGender } from 'erbecache/model/SimasModel'

class GenderOptions extends Component {
    constructor(props) {
        super(props)
        this.state={
            data: [],
            isCompleteLoadData: false
        }
    }

    async componentDidMount() {
        const gender = await masterGender()
        await this.setState({
            isCompleteLoadData: true,
            data: gender
        })
    }

    render() {
        return(
            this.state.isCompleteLoadData && <PickerItemValidation
                title={'Jenis Kelamin'}
                titleEmptyData={'Pilih Jenis Kelamin'}
                titleForm={true}
                paramSelected={'genderId'}
                paramView={'genderName'}
                data={this.state.data}
                selected={this.props.selected}
                onChangeItem={(value) => this.props.onChangeItem(value)}
            />
            ||
            <Text>Loading data</Text>
        )
    }
}

export default GenderOptions