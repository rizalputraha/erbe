import React, { Component } from 'react'
import { Text } from 'react-native'
import { PickerItemValidationOther } from 'component/v3'
import { masterReligion } from 'erbecache/model/SimasModel'

class ReligionOptions extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            isCompleteLoadData: false
        }
    }

    async componentDidMount() {
        const item = await masterReligion()
        await this.setState({
            isCompleteLoadData: true,
            data: item
        })
    }

    render() {
        return (
            this.state.isCompleteLoadData && <PickerItemValidationOther
                otherLabel={'Agama Lainnya'}
                otherPlaceHolder={'Agama Lainnya'}
                title={'Agama'}
                titleEmptyData={'Pilih Agama'}
                titleForm={true}
                paramSelected={'religionId'}
                paramOther={6}
                paramView={'religionName'}
                placeholder={'Ketika agama anda'}
                placeText={'Pilih Agama'}
                data={this.state.data}
                selected={this.props.selected}
                onChangeItem={(value) => {
                    console.log('Religion options onChaneItem', { res: value, prop: this.props }),
                        this.props.onChangeItem(value)
                }}
                onChangeText={(value) => {
                    console.log('Religion options onChangeText', { res: value, prop: this.props }),
                        this.props.onChangeText(value)
                }}
            />
            ||
            <Text>Loading data</Text>
        )
    }
}

export default ReligionOptions