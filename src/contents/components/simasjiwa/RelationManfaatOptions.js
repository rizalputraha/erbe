import React, { Component } from 'react'
import { Text } from 'react-native'
import { PickerItemValidation, ItemComponent } from 'component/v3'
import { masterRelation } from 'erbecache/model/SimasModel'
import { connect } from 'react-redux'
import { setStateSpajPPSimas } from 'erberedux/actions'

class RelationOptions extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            isCompleteLoadData: false
        }
    }

    async componentDidMount() {
        const { product } = this.props
        const item = await masterRelation()

        console.log('item realtion', item);
        item.map(async (obj, i) => {
            if (product.product_code == obj.productId && product.product_sub_code == obj.subProductId) {
                await this.setState({
                    isCompleteLoadData: true,
                    data: obj.relation
                })
            }
        })
    }

    render() {
        const { spaj } = this.props;
        console.log('state spaj hubungan', { data: this.state.data, hubPp: spaj.pemegangPolis.hubunganPp });
        return (
            (this.state.isCompleteLoadData && this.state.data.length > 0) && <PickerItemValidation
                title={'Hubungan dengan pemegang polis'}
                titleEmptyData={'Pilih hubungan'}
                titleForm={true}
                paramSelected={'relationId'}
                paramView={'relationName'}
                data={this.state.data}
                selected={this.props.selected}
                onChangeItem={(value) => {
                    console.log('Relation options onChangeItem', { res: value, prop: this.props }),
                        this.props.onChangeItem(value)
                }}
            />
            ||
            <Text>Loading data</Text>
        )
    }
}

const mtp = ({ SimasJiwa }) => {
    const { product, spaj } = SimasJiwa
    return { product, spaj }
}
export default connect(mtp, { setStateSpajPPSimas })(RelationOptions)