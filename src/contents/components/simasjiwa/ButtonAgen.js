import React, { Component } from 'react'
import { Button, ThemeProvider } from 'react-native-elements'
import Theme from 'style/Theme'
import ThemeSecondary from 'style/ThemeSecondary'
import { connect } from 'react-redux'

export class ButtonAgen extends Component {

    renderButton() {
        const { loadingButtonAgen } = this.props

        if (loadingButtonAgen === true) {
            return (
                <Button
                    title={'Proses...'}
                    loading={true}
                    type={'solid'}
                    disabled={true}>
                </Button>
            )
        }

        return (
            <Button
                title={'Buat SPAJ Downline'}
                type={'solid'}
                onPress={() => this.props.onPress()}
            >
            </Button>
        )
    }

    render() {
        return (
            <ThemeProvider theme={Theme}>
                {this.renderButton()}
            </ThemeProvider>
        )
    }
}

const mtp = ({ SimasJiwa }) => {
    const { loadingButtonAgen } = SimasJiwa;
    return { loadingButtonAgen };
}

export default connect(mtp, {})(ButtonAgen)
