import React, { Component } from 'react'
import { Text } from 'react-native'
import { PickerItemValidation, ItemComponent } from 'component/v3'
import { masterRelation } from 'erbecache/model/SimasModel'
import { connect } from 'react-redux'
import { setStateSpajPPSimas } from 'erberedux/actions'

class RelationOptions extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            isCompleteLoadData: false
        }
    }

    getRelationId(relationName, res) {
        const data = res.filter(item => item.relationName == relationName)
        return data[0].relationId
    }

    getRelationName(relationId) {
        const data = this.state.data.filter(item => item.relationId == relationId)
        console.log(data, 'relation name');
        return data[0].relationName
    }

    async componentDidMount() {
        const { isTertanggung, product, setStateSpajPPSimas } = this.props
        const item = await masterRelation()

        console.log('item realtion', item);
        item.map(async (obj, i) => {
            if (product.product_code == obj.productId && product.product_sub_code == obj.subProductId) {
                if (isTertanggung === true) {
                    const hubunganPp = this.getRelationId('Diri Sendiri', obj.relation)
                    setStateSpajPPSimas('hubunganPp', hubunganPp) //Set default diri sendiri
                    await this.setState({
                        isCompleteLoadData: true,
                        data: obj.relation,
                    })
                } else {
                    const relation = obj.relation.filter(item => item.relationName !== 'Diri Sendiri')
                    await this.setState({
                        isCompleteLoadData: true,
                        data: relation
                    })
                }
            }
        })
    }

    render() {

        const { isTertanggung, spaj } = this.props;
        console.log('state spaj hubungan', { data: this.state.data, hubPp: spaj.pemegangPolis.hubunganPp });
        if (isTertanggung == true) {
            return (
                this.state.isCompleteLoadData && <ItemComponent
                    label={'Hubungan Dengan Tertanggung'}
                    value={this.getRelationName(spaj.pemegangPolis.hubunganPp)}
                />
            )
        } else {
            return (
                (this.state.isCompleteLoadData && this.state.data.length > 0) && <PickerItemValidation
                    title={'Hubungan dengan pemegang polis'}
                    titleEmptyData={'Pilih hubungan'}
                    titleForm={true}
                    paramSelected={'relationId'}
                    paramView={'relationName'}
                    data={this.state.data}
                    selected={this.props.selected}
                    onChangeItem={(value) => {
                        console.log('Relation options onChangeItem', { res: value, prop: this.props }),
                            this.props.onChangeItem(value)
                    }}
                />
                ||
                <Text>Loading data</Text>
            )
        }
    }
}

const mtp = ({ SimasJiwa }) => {
    const { isTertanggung, product, spaj } = SimasJiwa
    return { product, isTertanggung, spaj }
}
export default connect(mtp, { setStateSpajPPSimas })(RelationOptions)