import React from 'react'
import { connect } from 'react-redux'
import { CheckBox } from 'react-native-elements'
import { setKeyRootSimas } from 'erberedux/actions'

class OptionYaProposalStarter extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        console.log('OptionYaProposalStarter', this.props)
    }

    render() {
        return (
            <CheckBox
                title="Ya"
                checkedIcon="dot-circle-o"
                uncheckedIcon="circle-o"
                checked={this.props.isTertanggung}
                onPress={() => {
                    this.props.setKeyRootSimas('isTertanggung', true)
                }}
                checkedColor="green"
            />
        )
    }
}



const mtp = ({ SimasJiwa }) => {
    const { isTertanggung } = SimasJiwa
    return { isTertanggung }
}

export default connect(mtp, { setKeyRootSimas })(OptionYaProposalStarter)