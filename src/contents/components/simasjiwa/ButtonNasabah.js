import React, { Component } from 'react'
import { Button, ThemeProvider } from 'react-native-elements'
import Theme from 'style/Theme'
import ThemeSecondary from 'style/ThemeSecondary'
import { connect } from 'react-redux'


export class ButtonAgen extends Component {

    renderButton() {
        const { loadingButtonNasabah } = this.props

        if (loadingButtonNasabah === true) {
            return (
                <Button
                    title={'Proses...'}
                    loading={true}
                    type={'solid'}
                    disabled={true}>
                </Button>
            )
        }

        return (
            <Button
                title={'Buat SPAJ Nasabah'}
                type={'solid'}
                onPress={() => this.props.onPress()}
            >
            </Button>
        )
    }

    render() {
        return (
            <ThemeProvider theme={ThemeSecondary}>
                {this.renderButton()}
            </ThemeProvider>
        )
    }
}

const mtp = ({ SimasJiwa }) => {
    const { loadingButtonNasabah } = SimasJiwa;
    return { loadingButtonNasabah };
}

export default connect(mtp, {})(ButtonAgen)
