import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { masterBank } from 'erbecache/model/SimasModel'
import ModalListSearchFilter from '../v3/ModalListSearchFilter'

export class BankOptions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataBank: null,
            dataBranch: null,
            selectedBank: null,
            selectedBranch: null,
            isCompletedLoad: false,
        }
    }

    async componentDidMount() {
        const dataBank = await masterBank();
        await this.setState({ dataBank, isCompletedLoad: true })
        if (this.props.selectedBank) {
            this.setState({ dataBranch: this.filterBranch(this.props.selectedBank) })
        }
    }

    filterBranch(bankId) {
        const branch = this.state.dataBank.filter(item => item.bankId == bankId)
        return branch[0].branch;
    }

    renderBank() {
        return (
            <View>
                {
                    this.state.isCompletedLoad && <ModalListSearchFilter
                        title={'Pilih Bank'}
                        titleEmptyData={'Pilih Bank'}
                        titleForm={true}
                        paramSelected={'bankId'}
                        paramView={'bankName'}
                        data={this.state.dataBank}
                        selected={this.props.selectedBank}
                        onChangeItem={async (value) => {
                            this.props.onChangeBank(value),
                                await this.setState({ dataBranch: this.filterBranch(value) }),
                                console.log(this.state);
                        }} />
                    || <Text>Loading</Text>
                }
            </View>
        )
    }

    renderBranch() {
        return (
            <View>
                {
                    this.state.dataBranch && <ModalListSearchFilter
                        title={'Pilih Cabang Bank'}
                        titleEmptyData={'Pilih Cabang Bank'}
                        titleForm={true}
                        paramSelected={'branchId'}
                        paramView={'branchName'}
                        filterData={this.state.selectedBank}
                        data={this.state.dataBranch}
                        selected={this.props.selectedBranch}
                        onChangeItem={async (value) => {
                            this.props.onChangeBranch(value),
                                console.log(this.state);
                        }} />
                }
            </View>
        )
    }

    render() {
        return (
            <View>
                {this.renderBank()}
                {this.renderBranch()}
            </View>
        )
    }
}

export default BankOptions
