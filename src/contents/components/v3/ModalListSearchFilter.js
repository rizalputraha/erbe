import React, { Component } from 'react'
import { Text, View, Modal, ActivityIndicator, TouchableOpacity, ScrollView } from 'react-native'
import { ListItem, Icon, Input, } from 'react-native-elements'
import Styles from "stylecomponent/ModalListSearch";
import Color from '../../../styles/Color';
import HeaderComponent from "./HeaderComponent";
import ErbeListMenu from './ErbeListMenu';

export class ModalListSearchFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      selected: this.props.selected,
      paramSelectedProps: this.props.paramSelected,
      paramViewProps: this.props.paramView,
      paramPlaceholder: this.props.placeholder,
      searchKey: '',
      data: this.props.data
    }
  }

  async componentDidMount() {
    this.setState({
      searchKey: '',
    })
  }

  getValueParam(value) {
    const obj = this.props.data;
    const objSelectedParam = this.state.paramSelectedProps;
    const objViewParam = this.state.paramViewProps;
    let resp;
    obj.map((item, index) => {
      if (item[objSelectedParam] == value) resp = item[objViewParam];
    })
    return resp;
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
    this.setState({ data: this.props.data })
  }

  SearchFilterFunction(text) {
    const s = this.state.paramViewProps;
    const newData = this.state.data.filter(function (item) {
      const itemData = item[s].toUpperCase()
      const textData = text.toUpperCase()
      return itemData.indexOf(textData) > -1
    })
    this.setState({ data: newData })
  }

  _searchData(value) {
    this.setState({ searchKey: value })
    if (this.state.searchKey.length > 1) {
      this.SearchFilterFunction(value);
    } else {
      this.setState({ data: this.props.data })
    }
  }

  async setModalValue(item) {
    await this.setState({ modalVisible: !this.state.modalVisible, selected: item});
    this.props.onChangeItem(item[this.state.paramSelectedProps]);
  }

  renderItems(item) {
    return (
      <TouchableOpacity
        onPress={ async () => this.setModalValue(item) }
        style={{ 
          flex: 1, 
          padding: 20, 
          margin: 5, 
          borderBottomColor: '#b4b4b4',
          borderBottomWidth: 1 
        }}
      >
        <Text>{item[this.state.paramViewProps]}</Text>
      </TouchableOpacity>
    )
  }

  renderLoading() {
    return(
      <ActivityIndicator style={{ margin: 10 }} size="small" color={Color.primary}/>
    )
  }
  renderList() {
    if (this.state.data == null)
      return this.renderLoading()
    else {
      return (
        <ErbeListMenu
          tipe="list"
          data={this.state.data}
          renderListItem={(item) => this.renderItems(item)}
          keyExtractor={(item, index) => index.toString()}
        />
      );
    }
  }

  renderFormInput() {
    return (
    <View style={{ flex: 1, margin: 10 }}>
        <Input
          onChangeText={(value) => this._searchData(value)}
          value={this.state.searchKey}
          placeholder={this.props.placeholder}
          rightIcon={
            <Icon
              type="material"
              name="search"
              size={24}
            />
          }
        />
        { this.renderList() }
      </View>
    )
  }

  renderModal() {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.modalVisible}
        onRequestClose={() => { this.setModalVisible(!this.state.modalVisible); }}>
        <HeaderComponent
          title={this.props.titleEmptyData}
          pushBackButtonHeader={() => this.setModalVisible(!this.state.modalVisible)}
        />
        {this.renderFormInput()}
      </Modal>
    )
  }

  _getValidationRequire() {
    if (!this.props.selected) return <Icon style={{top: 30}} size={20} type="evilicon" name='close-o' color={Color.redDanger} />;
    return <Icon style={{top: 30}} size={20} type="evilicon" name='check' color={Color.greenSuccess} />;
  }

  render() {
    return (
      <View style={Styles.content}>
        <View style={Styles.containerLabel}>
          <Text>{this.props.title}</Text>
        </View>
        <ListItem
          containerStyle={{ padding: 0, paddingVertical: 10 }}
          style={{ borderBottomColor: Color.greyLight, borderBottomWidth: 2, marginHorizontal: 15 }}
          title={this.props.selected ? <Text>{this.getValueParam(this.props.selected)}</Text> : <Text>{this.props.titleEmptyData}</Text>}
          onPress={() => { this.setModalVisible(!this.state.modalVisible) }}
          rightIcon={this._getValidationRequire()}
          underlayColor={Color.greyLight}
        />
        {this.renderModal()}
      </View>
    )
  }
}

export default ModalListSearchFilter
