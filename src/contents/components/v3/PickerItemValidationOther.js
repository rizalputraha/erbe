import React, { Component } from 'react'
import { Text, View, Modal, StyleSheet, ScrollView, ActivityIndicator } from 'react-native'
import { ListItem, Icon } from "react-native-elements";
import HeaderComponent from './HeaderComponent'
import InputComponent from './InputComponent'
import Color from "style/Color";
import Styles from "stylecomponent/PickerItemValidation";

class PickerItemValidationOther extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paramViewProps: this.props.paramView,
            paramSelectedProps: this.props.paramSelected,
            modalVisible: false,
            other: false,
            otherValue: '',
        }
    }

    _changeState(value, parentState) {
        this.setState({ [parentState]: value });
        this.props.onChangeText(value)
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    getValueParam(value) {
        const obj = this.props.data;
        const objSelectedParam = this.state.paramSelectedProps;
        const objViewParam = this.state.paramViewProps;
        let resp;
        obj.forEach(function (item, index) {
            if (item[objSelectedParam] == value) resp = item[objViewParam];
        })

        return resp;
    }

    _getValidationRequire() {
        if (this.props.selected == false) return <Icon type="evilicon" name='close-o' color={Color.redDanger} size={20} />;
        return <Icon type="evilicon" name='check' color={Color.greenSuccess} size={20} />;
    }

    _getIconSelected(item) {
        if (parseInt(this.props.selected) == parseInt(item[this.props.paramSelected])) {
            return <Icon type="evilicon" name='check' color={Color.greenSuccess} size={20} />
        }
    }

    setValueModal(item) {
        this.props.onChangeItem(item[this.state.paramSelectedProps]);
        this.setState({ modalVisible: !this.state.modalVisible, selected: item[this.state.paramSelectedProps] });
        if (parseInt(item[this.props.paramSelected]) == this.props.paramOther) {
            this.setState({ other: true })
        } else {
            this.setState({ other: false })
        }

    }

    renderHeaderModal() {
        return (
            <HeaderComponent
                title={this.props.titleEmptyData}
                pushBackButtonHeader={() => this.setModalVisible(!this.state.modalVisible)}
            />
        )
    }

    renderItem() {
        if (this.props.data == null) return (<ActivityIndicator size="small" color="#F44336" />);
        return this.props.data.map((item, index) => {
            return (
                <ListItem
                    underlayColor={Color.greyLight}
                    rightIcon={this._getIconSelected(item)}
                    bottomDivider
                    key={index}
                    title={item[this.props.paramView]}
                    onPress={() => this.setValueModal(item)}
                />
            );
        })
    }

    renderModal() {
        return (
            <View>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }}>
                    <ScrollView>
                        {this.renderHeaderModal()}
                        <View>
                            {this.renderItem()}
                        </View>
                    </ScrollView>
                </Modal>
            </View>
        )
    }

    renderLabel() {
        return (
            <View style={[Styles.content, { marginTop: 10 }]}>
                <View style={Styles.containerLabel}>
                    <Text style={styles.content}>{this.props.title}</Text>
                </View>
                <ListItem
                    underlayColor={Color.greyLight}
                    containerStyle={{ padding: 0, paddingVertical: 10 }}
                    style={{ borderBottomColor: Color.greyLight, borderBottomWidth: 2, marginHorizontal: 15 }}
                    title={this.props.selected == false ? <Text>{this.props.titleEmptyData}</Text> : <Text>{this.getValueParam(this.props.selected)}</Text>}
                    rightIcon={this._getValidationRequire()}
                    onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }}
                />
            </View>
        )
    }

    renderInputOther() {
        return (
            this.state.other == true ?
                <View style={styles.content}>
                    <InputComponent
                        label={this.props.otherLabel}
                        placeholder={this.props.otherPlaceHolder}
                        value={this.state.otherValue}
                        onChangeText={(value) => this._changeState(value, 'otherValue')}
                        validation={['length']}
                        minCharacter={3}
                        maxCharacter={20}
                    />
                </View>
                : <View></View>
        )
    }

    render() {
        return (
            <View>
                {this.renderLabel()}
                {this.renderModal()}
                {this.renderInputOther()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white
    },
    content: {
        backgroundColor: Color.white
    }
})

export default PickerItemValidationOther

