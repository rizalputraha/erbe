import React, { Component } from 'react'
import { Text, View, Modal, ActivityIndicator, TouchableOpacity } from 'react-native'
import { ListItem, Icon, Input, } from 'react-native-elements'
import Styles from "stylecomponent/ModalListSearch";
import Color from '../../../styles/Color';
import HeaderComponent from "./HeaderComponent";
import ErbeListMenu from './ErbeListMenu';

export class ModalListSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      selected: this.props.selected,
      paramSelectedProps: this.props.paramSelected,
      paramViewProps: this.props.paramView,
      paramPlaceholder: this.props.placeholder,
      searchKey: '',
      data: this.props.data
    }
  }

  getValueParam(value) {
    const obj = this.props.data;
    const objSelectedParam = this.state.paramSelectedProps;
    const objViewParam = this.state.paramViewProps;
    let resp;
    obj.forEach(function (item, index) {
      if (item[objSelectedParam] == value) resp = item[objViewParam];
    })

    return resp;
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  SearchFilterFunction(text) {
    const newData = this.state.data.filter(function (item) {
      const itemData = item.name.toUpperCase()
      const textData = text.toUpperCase()
      return itemData.indexOf(textData) > -1
    })
    this.setState({ data: newData })
  }

  _searchData(value) {
    this.setState({ searchKey: value })
    if (this.state.searchKey.length > 1) {
      this.SearchFilterFunction(value);
    } else {
      this.setState({ data: this.props.data })
    }
  }

  setModalValue(item) {
    this.setState({ modalVisible: !this.state.modalVisible, selected: item });
    this.props.onChangeItem(item);
  }

  renderItems(item) {
    return (
      <View style={{ flex: 1, padding: 20, margin: 5, backgroundColor: '#fff', elevation: 2 }}>
        <TouchableOpacity onPress={() => this.setModalValue(item[this.state.paramSelectedProps])}>
          <Text>{item.name}</Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderList() {
    if (this.state.data == null)
      return (<ActivityIndicator style={{ margin: 10 }} size="small" color="#F44336" />)
    else {
      return (
        <ErbeListMenu
          tipe="list"
          data={this.state.data}
          renderListItem={(item) => this.renderItems(item)}
          keyExtractor={(item, index) => index.toString()}
        />
      );
    }
  }

  renderModal() {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.modalVisible}
        onRequestClose={() => { this.setModalVisible(!this.state.modalVisible); }}>
        <HeaderComponent
          title={this.props.titleEmptyData}
        />
        <View style={{ margin: 10 }}>
          <Input
            onChangeText={(value) => this._searchData(value)}
            value={this.state.searchKey}
            placeholder={this.props.placeholder}
            rightIcon={
              <Icon
                type="material"
                name="search"
                size={24}
              />
            }
          />
          {this.renderList()}
        </View>
      </Modal>
    )
  }

  _getValidationRequire() {
    if (this.state.selected == '') return <Icon type="evilicon" name='close-o' color={Color.redDanger} />;
    return <Icon type="evilicon" name='check' color={Color.greenSuccess} />;
  }

  render() {
    return (
      <View style={Styles.content}>
        <View style={Styles.containerLabel}>
          <Text>{this.props.title}</Text>
        </View>
        <ListItem
          containerStyle={{ padding: 0, paddingVertical: 10 }}
          style={{ borderBottomColor: Color.greyLight, borderBottomWidth: 2, marginHorizontal: 15 }}
          title={this.state.selected == '' ? <Text>{this.props.titleEmptyData}</Text> : <Text>{this.getValueParam(this.state.selected)}</Text>}
          onPress={() => { this.setModalVisible(!this.state.modalVisible) }}
          rightIcon={this._getValidationRequire()}
        />
        {this.renderModal()}
      </View>
    )
  }
}

export default ModalListSearch
