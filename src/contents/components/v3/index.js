import NextgIcon from './NextgIcon'
import ErbeText from './ErbeText'
import ErbeListMenu from './ErbeListMenu'
import ListReportDownline from './ListReportDownline'
import InputComponent from './InputComponent'
import ImageAsuransi from './ImageAsuransi'
import ImageSlider from './ImageSlider'
import PickerItemValidation from "./PickerItemValidation"
import PickerItemValidationOther from "./PickerItemValidationOther"
import HeaderComponent from "./HeaderComponent"
import ModalListItemArray from "./ModalListItemArray"
import ItemComponent from "./ItemComponent"
import ModalListSearch from "./ModalListSearch"
import ModalListSearchFilter from "./ModalListSearchFilter"
import ListItemValidation from "./ListItemValidation"
import TitleComponent from './TitleComponent'
import ListItem from './ListItem';
import ModalFileUpload from './ModalFileUpload'
import BirthdayPemegangPolis from './BirthdayPemegangPolis'
import BirthdayTertanggung from './BirthdayTertanggung'
import BirthdayPenerimaManfaat from './BirthdayPenerimaManfaat'
import ProductAsuransiJiwa from './ProductAsuransiJiwa'
import DateTime from './DateTime'
import BadgeLevel from './BadgeLevel'
import ListItemDownline from './ListItemDownline'
import Signature from './Signature'
import DisconnectView from './DisconnectView'

module.exports = {
    NextgIcon,
    ErbeText,
    ErbeListMenu,
    ListReportDownline,
    InputComponent,
    ImageAsuransi,
    ImageSlider,
    PickerItemValidation,
    PickerItemValidationOther,
    HeaderComponent,
    ModalListItemArray,
    ItemComponent,
    ModalListSearch,
    ModalListSearchFilter,
    ListItemValidation,
    TitleComponent,
    ListItem,
    ModalFileUpload,
    BirthdayPemegangPolis,
    BirthdayTertanggung,
    BirthdayPenerimaManfaat,
    ProductAsuransiJiwa,
    DateTime,
    BadgeLevel,
    ListItemDownline,
    Signature,
    DisconnectView,
}