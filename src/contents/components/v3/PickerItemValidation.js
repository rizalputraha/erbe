import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Modal, ScrollView } from 'react-native'
import Color from "style/Color";
import Style from "stylecomponent/PickerItemValidation"
import ErbeListMenu from './ErbeListMenu';
import HeaderComponent from "./HeaderComponent";
import { Icon, ListItem } from 'react-native-elements';

export default class PickerItemValidation2 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isActive: false,
            selected: this.props.selected,
            title: this.props.title,
            titleEmptyData: this.props.titleEmptyData,
            paramSelected: this.props.paramSelected,
            paramView: this.props.paramView,
            data: this.props.data,
        }
    }

    componentDidMount() {

    }

    getValueParam(value) {
        const { selected, titleEmptyData, data, paramSelected, paramView } = this.state;
        let resp;
        if (selected === undefined || selected === '' || selected === false || selected === null) {
            return titleEmptyData;
        }
        data.forEach((item, index) => {
            if (item[paramSelected] == value) resp = item[paramView]
        });
        return resp.substr(0, 48);
    }

    async setIsActive(visible) {
        await this.setState({ isActive: visible });
    }

    async setSelected(value) {
        await this.setState({ selected: value });
        this.props.onChangeItem(value);
    }

    renderRowDataIcon(value) {
        const { selected } = this.state;
        if (selected === undefined || selected === '' || selected === false || selected === null) {
            return;
        }
        if (value == selected) {
            return (
                <Icon
                    name="check"
                    type="evilicon"
                    color={Color.greenSuccess}
                    size={20}
                />
            );
        }
    }

    _getIconSelected(item) {
        if (parseInt(this.props.selected) == parseInt(item[this.props.paramSelected])) {
            return <Icon type="evilicon" name='check' color={Color.greenSuccess} size={20} />
        }
    }

    renderRowData(item) {
        return (
            <View>
                <ListItem
                    underlayColor={Color.greyLight}
                    rightIcon={this._getIconSelected(item)}
                    title={item[this.state.paramView]}
                    bottomDivider
                    onPress={() => {
                        this.setSelected(item[this.state.paramSelected]),
                            this.setIsActive(!this.state.isActive)
                    }}
                />
            </View>
        )
    }

    renderContentModal() {
        return (
            <ErbeListMenu
                tipe="list"
                data={this.state.data}
                renderListItem={(item) => this.renderRowData(item)}
                keyExtractor={(item, index) => item[this.state.paramSelected].toString()}
            />
        )
    }

    renderHeaderModal() {
        return (
            <HeaderComponent
                title={this.state.title}
                pushBackButtonHeader={() => this.setIsActive(!this.state.isActive)}
            />
        )
    }

    renderModal() {
        return (
            <View>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.isActive}
                    onRequestClose={() => {
                        this.setIsActive(!this.state.isActive);
                    }}>
                    <View>
                        {this.renderHeaderModal()}
                        {this.renderContentModal()}
                    </View>
                </Modal>
            </View>
        )
    }

    renderItemIcon() {
        if (this.state.selected === undefined || this.state.selected === '' || this.state.selected === false || this.state.selected === null) {
            return (
                <View style={Style.iconArea}>
                    <Icon
                        name='close-o'
                        type='evilicon'
                        color={Color.redDanger}
                        size={20}
                        style={{ padding: 0, marginRight: 0 }}
                    />
                </View>
            );
        }
        return (
            <View style={Style.iconArea}>
                <Icon
                    name='check'
                    type='evilicon'
                    color={Color.greenSuccess}
                    size={20}
                />
            </View>
        );
    }


    renderShowTitleForm() {
        return (
            this.props.titleForm === true ? <Text style={Style.txtTitle}>{this.state.title}</Text> : ''
        )
    }

    renderItemSelected() {
        return (
            this.props.disabled &&
            <View style={{
                paddingVertical: 15, borderBottomWidth: 2,
                borderBottomColor: Color.greyLight
            }}>
                <Text>{this.getValueParam(this.state.selected)}</Text>
                {this.renderItemIcon()}
            </View>
            ||
            <View>
                <TouchableOpacity
                    onPress={() => this.setIsActive(true)}
                    style={{
                        paddingVertical: 15, borderBottomWidth: 2,
                        borderBottomColor: Color.greyLight
                    }}
                >
                    <Text>{this.getValueParam(this.state.selected)}</Text>
                </TouchableOpacity>
                {this.renderItemIcon()}
            </View>
        )
    }

    render() {
        if (this.state.isActive == true) {
            return this.renderModal();
        }
        return (
            <View style={{ marginHorizontal: 15 }}>
                <Text style={Style.txtTitle}>{this.state.title}</Text>
                {this.renderItemSelected()}
            </View>
        )
    }
}
