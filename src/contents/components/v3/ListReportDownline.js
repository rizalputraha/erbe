import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import Style from 'stylecomponent/StyleListReport'
import BadgeLevel from './BadgeLevel'
import ErbeListMenu from './ErbeListMenu'

export default class ListReportDownline extends Component {
   constructor(props) {
      super(props);
      this.state = {}
   }

   componentDidMount() {
      console.log("PROPS COMPONENTS REPORT DOWNLINE", this.props);
   }

   _getLevelName(value) {
      if (value == 0) return "Agen";
      else if (value == 1) return "Manager";
      else if (value == 2) return "Director";
   }

   expandList(id) {
      this.props.changeUid(id);
   }

   render() {
      const { nama, username, email, no_telp, level, id } = this.props;
      return (
         <TouchableOpacity onPress={() => this.expandList(id)} >
            <View style={{ padding: 5, margin: 5, borderRadius: 5, elevation: 2, flexDirection: 'row', alignItems: 'center' }}>
               <View style={{ flex: 1, margin: 10 }}>
                  <Text style={{ margin: 5, fontWeight: 'bold', fontSize: 16 }}>{username}</Text>
                  <Text style={{ margin: 5 }}>Nama: {nama}</Text>
                  <Text style={{ margin: 5 }}>Email: {email}</Text>
                  <Text style={{ margin: 5 }}>No Hp: {no_telp}</Text>
               </View>
               <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: 20 }}>
                  <BadgeLevel level={level} />
                  <Text style={{ fontWeight: 'bold' }}>{this._getLevelName(level)}</Text>
               </View>
            </View>
         </TouchableOpacity>
      )
   }
}
