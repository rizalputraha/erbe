import React, { Component } from 'react'
import { Text, View, RefreshControl, TouchableOpacity } from 'react-native'
import { getDownline, getDownlineByUname, getDownlineById, setDownlineSearchKey, setDownlineActive, showErrorMessage } from 'erberedux/actions';
import BadgeLevel from './BadgeLevel'
import ErbeListMenu from './ErbeListMenu'
import ListReportDownline from './ListReportDownline'
import { connect } from 'react-redux'

class ListItemDownline extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        console.log("PROPS COMPONENTS REPORT DOWNLINE", this.props);
    }

    _getLevelName(value) {
        if (value == 0) return "Agen";
        else if (value == 1) return "Manager";
        else if (value == 2) return "Director";
    }

    onRefresh(authUser, downlineUserId) {
        this.props.getDownline(authUser.token, downlineUserId);
    }

    renderItemDownline(item) {
        const { authUser, setDownlineActive, getDownline } = this.props;
        return (
            <View>
                <TouchableOpacity onPress={() => {
                    setDownlineActive(item),
                        getDownline(authUser.token, item.id)
                }} >
                    <View style={{ padding: 5, margin: 5, borderRadius: 5, elevation: 2, flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ flex: 1, margin: 10 }}>
                            <Text style={{ margin: 5, fontWeight: 'bold', fontSize: 16 }}>{item.username}</Text>
                        </View>
                        <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: 20 }}>
                            <BadgeLevel level={item.level} />
                            <Text style={{ fontWeight: 'bold' }}>{this._getLevelName(item.level)}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    renderListChild() {
        const { authUser, downlineUserId } = this.props
        return (
            <ErbeListMenu
                data={this.props.downline}
                tipe="list"
                renderListItem={(item) => this.renderItemDownline(item)}
                refreshControl={
                    <RefreshControl refreshing={this.props.refresh} onRefresh={() => this.onRefresh(authUser, downlineUserId)} />
                }
            />
        )
    }

    render() {
        const { nama, username, email, no_telp, level, id } = this.props;
        return (
            <View>
                <View style={{ padding: 5, margin: 5, borderRadius: 5, elevation: 2, flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ flex: 1, margin: 10 }}>
                        <Text style={{ margin: 5, fontWeight: 'bold', fontSize: 16 }}>{username}</Text>
                        <Text style={{ margin: 5 }}>Nama: {nama}</Text>
                        <Text style={{ margin: 5 }}>Email: {email}</Text>
                        <Text style={{ margin: 5 }}>No Hp: {no_telp}</Text>
                    </View>
                    <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: 20 }}>
                        <BadgeLevel level={level} />
                        <Text style={{ fontWeight: 'bold' }}>{this._getLevelName(level)}</Text>
                    </View>
                </View>
                {this.renderListChild()}
            </View>
        )
    }
}


const mtp = ({ Report, Auth, ErrorHandling }) => {
    const { authUser } = Auth
    const { showGlobalLoading, errorMessage } = ErrorHandling
    const { downline, refresh, searchKey, downlineActive, isClick } = Report
    return { authUser, errorMessage, downline, refresh, searchKey, downlineActive, isClick }
}

export default connect(mtp, { getDownlineByUname, getDownlineById, getDownline, setDownlineSearchKey, showErrorMessage, setDownlineActive })(ListItemDownline)