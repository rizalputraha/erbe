import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Color from 'style/Color'

export default class TitleComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {...this.props}
    }
    render() {
        return(
            <View style={styles.title}>
                <Text style={styles.titleText}>{this.state.value}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        backgroundColor: Color.lambo,
        flexDirection: 'column',
        padding: 15,
    },
        titleText: {
        color: Color.white,
        fontSize: 18,
    },
})