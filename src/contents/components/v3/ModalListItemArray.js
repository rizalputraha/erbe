import React from 'react';
import { ScrollView, View, Text, StyleSheet, Modal, StatusBar, ActivityIndicator } from 'react-native';
import { ListItem, Icon } from 'react-native-elements';
import HeaderComponent from './HeaderComponent'
import Color from "style/Color";

export default class ModalListItemArray extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      modalVisible: false,
      data: this.props.data,
      selected: this.props.defaultSelected
    }
  }

  getLabel(item) {
    return item[this.props.labelItem];
  }

  renderItem() {
    console.log(this.props.data);

    if (this.props.data == null) return (<ActivityIndicator size="small" color="#F44336" />);
    return this.props.data.map((item, index) => {
      return (
        <ListItem
          key={index}
          title={this.getLabel(item)}
          onPress={() => this.setValueModal(item)} />
      );
    })
  }

  setValueModal(item) {
    this.props.onChangeItem(item);
    this.setState({ modalVisible: !this.state.modalVisible, selected: item[this.props.labelItem] });
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  renderHeaderModal() {
    return (
      <HeaderComponent
        title={this.props.title}
        pushBackButtonHeader={() => this.setModalVisible(!this.state.modalVisible)}
      />
    );
  }

  renderModal() {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setModalVisible(!this.state.modalVisible);
        }}>
        <StatusBar
          backgroundColor="#F44336"
          barStyle="light-content"
        />
        <ScrollView>
          {this.renderHeaderModal()}
          <View>
            {this.renderItem()}
          </View>
        </ScrollView>
      </Modal>
    );
  }

  _getValidationRequire() {
    const { selected } = this.state
    if (selected === false || selected === undefined) return <Icon name='arrow-forward' style={styles.iconInValid} />;
    return <Icon name='checkmark-circle' style={styles.iconValid} />;
  }

  render() {
    const { selected } = this.state
    return (
      <View>
        <Text style={styles.content}>{this.props.title}</Text>
        <ListItem underlayColor={Color.greyLight}
          title={selected === false || selected === undefined ? <Text>{this.props.titleOptions}</Text> : <Text>{this.state.selected}</Text>}
          onPress={() => {
            this.setModalVisible(!this.state.modalVisible);
          }}
        />
        {this.renderModal()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  label: {
    fontSize: 16,
    padding: 10,
    color: '#616161',
    fontFamily: 'DroidSans',
  },
  content: {
    flexDirection: 'column',
    margin: 10,
    fontSize: 16,
    color: Color.greyDark
  },
  header: {
    backgroundColor: '#F44336',
  },
  iconValid: {
    color: 'green',
  },
  iconInValid: {
    color: 'red',
  }
});
