import React from 'react'
import { View, Image, ScrollView } from 'react-native'
import Style from 'stylecomponent/ImageSliderStyle'
import Layout from 'style/Layout'
import { loadCacheSlider } from "erbecache/model/HomeModel";
import { SLIDER_DASHBOARD } from "erbecache/constant/MasterAsuransi";

class ImageSlider extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            width: Layout.window.width,
            position: 0,
            images: [],
        }
    }

    async componentDidMount() {        
        await loadCacheSlider(SLIDER_DASHBOARD)
        .then(res => {
            this.setState({images: res})
        })
        .catch(err => {
            console.log('Error images slider cache')
        })
    }

    renderItem() {
        return this.state.images.map((item,index) => {
            return(
                    <View key={index} style={Style.warpslider}>
                        <Image source={{uri: item.slider_images}} style={Style.banner} resizeMode="contain" />
                    </View>
            );
        })
    }

    renderIconSlider()
    {
        return this.state.images.map((item, index) => {
            if(index === this.state.position){
            return(
                <View key={index} style={Style.sliderIconActive}></View>
            );
            }
            return(
                <View key={index} style={Style.sliderIcon}></View>
            );
        })
    }
  
    watchScroll = (event) => {
        const width = event.nativeEvent.layoutMeasurement.width;
        const maxWidth = event.nativeEvent.contentSize.width;
        const currentWidth = event.nativeEvent.contentOffset.x;
        const totalData = this.state.images.length;
        
  
        this.state.images.map((item,index) => {
          const newPosition = (currentWidth / width ) - (maxWidth / width) + totalData;
          this.setState({position: newPosition});
        })
    }

    watchResponder = (event) => {
        console.log('watchResponder: ', event);
    }
    
    watchTouchStart = (event) => {
        console.log('watchTouchStart: ', event);
    }
    
    watchTouchEnd = (event) => {
        console.log('watchTouchEnd: ', event);
    }

    render() {
        if (this.state.images && this.state.images.length > 0) {
            return(
                <View style={Style.container}>
                    <ScrollView
                        ref={(snapScroll) => { this.snapScroll = snapScroll; }}
                        style={Style.slider}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        pagingEnabled={true}
                        onScroll={ (event) => this.watchScroll(event) }
                    >
                        {this.renderItem()}
                    </ScrollView>
                    <View style={Style.sliderWrapper}>
                        {this.renderIconSlider()}
                    </View>
                </View>
            )
        } else {
            return(
                <View style={Style.container}>
                    <View style={Style.sliderEmpty}>
                    </View>
                </View>
            )
        }
    }
}


export default ImageSlider