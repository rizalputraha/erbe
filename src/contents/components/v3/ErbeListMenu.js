import React, { Component } from 'react'
import { Text, View, FlatList, TouchableOpacity } from 'react-native'
import NextgIcon from './NextgIcon'
import { navigate } from 'erbeservice/AppNavigation'
import Style from 'style/stylescreen/Report';
import PropTypes from 'prop-types';

class ErbeListMenu extends Component {
   renderMenu(item) {
      console.log(item);
      return (
         <TouchableOpacity onPress={() => navigate(item.screen, { title: `${item.title}` })} style={Style.listContainer}>
            <NextgIcon name={item.icon} size={32} />
            <View style={Style.titleContainer}>
               <Text style={Style.txtTitle} >{item.title}</Text>
               <Text style={Style.txtDesc} >{item.desc}</Text>
            </View>
         </TouchableOpacity>
      )
   }

   renderCond = (item, index) => {
      if (this.props.tipe == "menu") {
         return this.renderMenu(item)
      } else {
         return this.props.renderListItem(item, index);
      }
   }

   render() {
      const { data, renderListItem, type } = this.props;
      return (
         data != null ?
            <FlatList
               data={data}
               renderItem={({ item, index }) => this.renderCond(item, index)}
               {...this.props}
            />
            : <View></View>
      )
   }
}

ErbeListMenu.propTypes = {
   data: PropTypes.any,
   tipe: PropTypes.string,
   renderListItem: PropTypes.func,
   renderListMenu: PropTypes.bool,
}

ErbeListMenu.defaultProps = {
   data: null,
   tipe: "menu",
   renderListItem: () => { },
   renderListMenu: false,
}

export default ErbeListMenu;