import React from 'react';
import { Text, View } from 'react-native';
import Style from 'stylecomponent/Text'
export default class ErbeText extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}    
    }

    render() {
        return(
            <View style={Style.container}>
                <Text style={[Style[this.props.style], Style[this.props.position], Style[this.props.color]]}>{this.props.value}</Text>
            </View>
        );
    }
}