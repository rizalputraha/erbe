import React from 'react'
import { View, Image } from 'react-native'
import Layout from 'style/Layout'

export default class DisconnectView extends React.Component {
    constructor(props) {
        super(props)
        this.state={}
    }

    render() {
        return(
            <View>
                <Image 
                    resizeMode={'contain'} source={require('../../../assets/images/disconnect.png')} 
                    style={{width: Layout.window.width, height: Layout.window.height / 3, marginTop: 30}} />
            </View>
        )
    }
}