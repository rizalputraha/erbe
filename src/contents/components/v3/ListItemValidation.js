import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet, ScrollView, Modal } from 'react-native'
import { Icon, CheckBox } from "react-native-elements";
import HeaderComponent from "./HeaderComponent";
import Color from 'style/Color';
export default class ListItemValidation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isActive: false,
      selected: this.props.selected,
      title: this.props.title,
      titleEmptyData: this.props.titleEmptyData,
      paramSelectedProps: this.props.paramSelectedProps,
      paramViewProps: this.props.paramView,
      data: this.props.data,
    }
  }

  async setIsActive(visible) {
    await this.setState({ isActive: visible });
  }

  async setSelected(value) {
    await this.setState({ selected: value });
    await this.props.onChangeItem(value);
  }

  getValueParam(value) {
    const { paramSelectedProps, paramViewProps, selected, titleEmptyData } = this.state
    if (selected === undefined || selected === '' || selected === false || selected === null) {
      return titleEmptyData;
    }
    const obj = this.props.data;
    const objSelectedParam = paramSelectedProps;
    const objViewParam = paramViewProps;
    let resp;
    obj.forEach(function (item, index) {
      if (item.id == value) resp = item[objViewParam];
    })

    return resp;
  }

  renderHeaderModal() {
    return (
      <HeaderComponent
        title={this.state.title}
        pushBackButtonHeader={() => this.setIsActive(!this.state.isActive)}
      />
    )
  }

  renderContentModal() {
    return this.state.data.map((item, index) => {
      return (
        <CheckBox
          key={index}
          center
          title={item[this.state.paramViewProps]}
          uncheckedIcon='circle-o'
          checkedIcon='dot-circle-o'
          checked={this.getStatusCheckItem(item.id)}
          onPress={() => {
            console.log('CheckBox Pressed');
            this.setStatusCheckItem(item.id)
            console.log(this.state);
          }}
        />
      );
    });
  }

  getStatusCheckItem(itemId) {
    return this.state[itemId];
  }

  async setStatusCheckItem(itemId) {
    this.state.data.map((item, index) => {
      this.setState({ [item.id]: false });
    });
    this.setState({ [itemId]: !this.state[itemId] });
    await this.setSelected(itemId);
    await this.setIsActive(!this.state.isActive);
  }

  renderModal() {
    return (
      <View style={{ marginTop: 22 }}>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.isActive}
          onRequestClose={() => {
            this.setIsActive(!this.state.isActive)
          }} >
          <ScrollView>
            {this.renderHeaderModal()}
            {this.renderContentModal()}
          </ScrollView>
        </Modal>
      </View>
    )
  }

  renderShowTitleForm() {
    return (
      this.props.titleForm && <Text style={styles.title}>{this.state.title}</Text>
    )
  }

  renderItemSelected() {
    return (
      <View style={styles.item}>
        <Text style={styles.itemData}>{this.getValueParam(this.state.selected)}</Text>
      </View>
    )
  }

  renderItemIcon() {
    const { selected } = this.state;
    if (selected === undefined || selected === '' || selected === false || selected === null) {
      return (
        <View style={styles.iconArea}>
          <Icon
            name='close-o'
            type='evilicon'
            color={'red'}
            size={20}
            style={{ padding: 0, marginRight: 0 }}
          />
        </View>
      )
    }
    return (
      <View style={styles.iconArea}>
        <Icon
          name='check'
          type='evilicon'
          color={'green'}
          size={20}
        />
      </View>
    );
  }

  render() {
    if (this.state.isActive === true) {
      return this.renderModal();
    }
    return (
      <TouchableOpacity onPress={() => this.setIsActive(true)} >
        <View style={styles.container}>
          {this.renderShowTitleForm()}
          {this.renderItemSelected()}
          {this.renderItemIcon()}
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.white,
    position: 'relative',
    marginHorizontal: 15,
    marginVertical: 15
  },
  title: {
    fontSize: 14,
    color: Color.darkLight,
    marginRight: 15
  },
  contentModal: {
    backgroundColor: Color.white
  },
  iconArea: {
    width: 20,
    height: 20,
    backgroundColor: 'transparent',
    position: 'absolute',
    right: 0,
    top: 10
  },
  item: {
    height: 40,
    borderColor: Color.grey,
    borderBottomWidth: 2,
    padding: 0
  },
  itemData: {
    fontSize: 14,
    color: Color.dark,
    position: 'absolute',
    left: 0,
    top: 10
  },
})
