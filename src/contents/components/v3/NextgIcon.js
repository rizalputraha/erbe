import React from 'react';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../../../nextgfont/config.json';
const Icon = createIconSetFromFontello(fontelloConfig);

export default class NextgIcon extends React.PureComponent {
    constructor(props) {
        super(props)
        this.state = {}    
    }

    render() {
        return(
            <Icon name={this.props.name} size={this.props.size} color={this.props.color} style={this.props.style} />
        );
    }
}