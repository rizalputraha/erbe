import React, { Component } from 'react'
import { Text, View, Modal, TouchableOpacity, Image, ScrollView, Dimensions } from 'react-native'
import { Icon } from "react-native-elements";
import HeaderComponent from './HeaderComponent';
import ButtonElement from "../rnelement/ButtonElement";
import styles from "stylecomponent/ModalFileUpload";
import ImagePicker from "react-native-image-picker";
import Color from 'style/Color'
import { Grid, Col } from 'react-native-easy-grid'

const { height, width } = Dimensions.get('window');

export class ModalFileUpload extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalVisible: false,
            showImage: null
        }
    }

    setImage() {
        if (this.state.showImage === null) Alert.alert('Please select file');
        this.props.onSelectFiles(this.state.showImage);
        console.log('image', this.state.showImage);

        this.setModalVisible(!this.state.isModalVisible);
    }

    openGallery() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            title: 'Select Avatar',
            customButtons: [{ name: 'fb', title: 'Choose Photo From Facebook' }],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            }
        }

        ImagePicker.launchImageLibrary(options, (response) => {
            console.log("Response : ", response);

            if (response.didCancel) {
                console.log("user cancelled image picker");
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = 'data:image/jpeg;base64,' + response.data;
                // const source = { uri: response.uri };

                this.setState({
                    showImage: source,
                });

            }
        });
    }


    openCamera() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            title: 'Select Avatar',
            customButtons: [{ name: 'fb', title: 'Choose Photo From Facebook' }],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            }
        }
        ImagePicker.launchCamera(options, (response) => {
            console.log("Response : ", response);

            if (response.didCancel) {
                console.log("user cancelled image picker");
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                // const source = { uri: response.uri };

                // You can also display the image using data:
                const source = 'data:image/jpg;base64,' + response.data;

                this.setState({
                    showImage: source,
                });

            }
        });
    }

    setModalVisible(state) {
        this.setState({
            isModalVisible: state
        })
    }

    renderButton() {
        return (
            <View>
                <TouchableOpacity onPress={() => this.setModalVisible(!this.state.isModalVisible)} style={styles.buttonModal} >
                    <Icon containerStyle={{ marginRight: 10 }} color={Color.darkLight} name="backup"></Icon>
                    <Text style={styles.textButtonModal}>{this.props.title}</Text>
                    {
                        this.props.fileSelected == '' ?
                            <Icon containerStyle={{ marginLeft: 15 }} color={Color.primary} name="close" type="evilicons"></Icon>
                            :
                            <Icon containerStyle={{ marginLeft: 15 }} color={Color.secondary} name="check" type="evilicons"></Icon>
                    }

                </TouchableOpacity>
                {
                    this.props.fileSelected !== '' &&
                    <Image
                        resizeMode={'contain'}
                        source={{ uri: this.props.fileSelected }}
                        style={{ width: width, height: 0.2 * height, marginTop: 15 }}
                    />
                }
            </View>
        )
    }

    renderContentModal() {
        return (
            <Grid>
                <Col style={{ borderBottomWidth: 1, borderBottomColor: Color.greyLight }}>
                    <TouchableOpacity onPress={() => this.openGallery()} style={{ backgroundColor: Color.white, paddingBottom: 15 }}>
                        <Text style={{ paddingHorizontal: 15, paddingTop: 10, fontSize: 14, textAlign: 'center', color: Color.lamboDark, marginBottom: 15 }}>Gallery</Text>
                        <Icon
                            style={{ marginBottom: 15 }}
                            name="image"
                            size={36}
                            color={Color.lamboDark}
                            type="evilicon"
                        />
                    </TouchableOpacity>
                </Col>
                <Col style={{ borderLeftWidth: 1, borderLeftColor: Color.greyLight, borderBottomWidth: 1, borderBottomColor: Color.greyLight }}>
                    <TouchableOpacity onPress={() => this.openCamera()} style={{ backgroundColor: Color.white, paddingBottom: 15 }}>
                        <Text style={{ paddingHorizontal: 15, paddingTop: 10, fontSize: 14, textAlign: 'center', color: Color.lamboDark, marginBottom: 15 }}>Camera</Text>
                        <Icon
                            style={{ marginBottom: 15 }}
                            name="camera"
                            size={36}
                            color={Color.lamboDark}
                            type="evilicon"
                        />
                    </TouchableOpacity>
                </Col>
            </Grid>
        )
    }

    renderResultImage() {
        return (
            this.state.showImage != null ?
                <View style={styles.contentModal}>
                    <View style={styles.container}>
                        <Image
                            resizeMode={'contain'}
                            source={{ uri: this.state.showImage }}
                            style={{ width: width, height: 0.4 * height, marginTop: 15 }}
                        />
                    </View>
                </View>
                : <View></View>
        )
    }

    renderModal() {
        return (
            <Modal
                animationType='slide'
                transparent={false}
                visible={this.state.isModalVisible}
                onRequestClose={() => this.setModalVisible(!this.state.isModalVisible)}
            >
                <ScrollView>
                    <HeaderComponent title={this.props.title} pushBackButtonHeader={() => this.setModalVisible(!this.state.isModalVisible)} />
                    {this.renderContentModal()}
                    {this.renderResultImage()}
                    {
                        this.state.showImage === null ? <View></View> :
                            <ButtonElement onPress={() => this.setImage()} title='Save' />
                    }

                </ScrollView>
            </Modal>
        )
    }

    render() {
        return (
            <View>
                {this.renderButton()}
                {this.renderModal()}
            </View>
        )
    }
}

export default ModalFileUpload
