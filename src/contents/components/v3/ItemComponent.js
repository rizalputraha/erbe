import React from 'react'
import { View, Text, TextInput, StyleSheet, Platform } from 'react-native'
import Color from 'style/Color'
import { Icon } from 'react-native-elements'

export default class ItemComponent extends React.Component {
    
    constructor(props){
        super(props)
        this.state={...this.props}
    }

    componentDidMount() {
        // console.log('Didmount ItemComponent', this.state);
    }

    getValidation() {
        if(this.state.value === undefined) return false;
        return true;
    }

    getColorIcon() {
        var check = this.getValidation();
        if(check) return Color.greenSuccess;
        return Color.redDanger;
    }

    getNameIcon() {
        var check = this.getValidation();
        if(check) return 'check';
        return 'close-o';
    }

    renderIcon() {
        return(
            <View style={styles.iconArea}>
                <Icon
                    type='evilicon'
                    name={this.getNameIcon()}
                    size={20}
                    color={this.getColorIcon()}
                />
            </View>
        )
    }

    renderAndroid() {
        return(
            <View style={styles.item}>
                <Text style={styles.itemData}>{this.state.value}</Text>
            </View>
        );
    }

    renderIos() {
        return(
            <View style={styles.item}>
                <Text style={styles.itemData}>{this.state.value}</Text>
            </View>
        );
    }

    renderItem() {
        if(Platform.OS=='android') return this.renderAndroid();
        return this.renderIos();
    }

    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.label}>{this.props.label}</Text>
                {this.renderItem()}
                {this.renderIcon()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,
        position: 'relative',
        marginHorizontal: 15,
        marginVertical: 15
    },
    item: {
        height: 40, 
        borderColor: Color.greyLight, 
        borderBottomWidth: 2,
        padding: 0
    },
    label: {
        fontSize: 14,
        color: Color.darkLight
    },
    itemData: {
        fontSize: 14,
        color: Color.dark,
        position: 'absolute',
        left: 0,
        top: 10
    },
    iconArea: {
        width: 20,
        height: 20,
        backgroundColor: 'transparent',
        position: 'absolute',
        right: 0,
        top: 30
    },
    active: {
        borderColor: Color.grey,
        color: Color.dark
    }
})