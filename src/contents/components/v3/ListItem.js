import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
export default class ListItem extends React.Component {
    constructor(props) {
        super(props)
        this.state = { ...this.props }
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.label}>{this.state.label}</Text>
                <Text style={styles.data}>{this.state.data}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 15,
        marginVertical: 15
    },
    label: {
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'DroidSans',
        marginBottom: 15,
    },
    data: {
        fontSize: 14,
        fontFamily: 'DroidSans'
    }
})