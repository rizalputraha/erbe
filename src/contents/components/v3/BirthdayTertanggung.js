import {
  Alert
} from 'react-native';
import moment from 'moment';
import Birthday from './Birthday'

class BirthdayTertanggung extends Birthday {
    constructor(props) {
        super(props)
        this.state = { ...this.state }
    }

    _onPickedLahir = (event, date) => {
        var dateBirthDay = moment(date).format('DD/MM/YYYY');
        var minYear = this.props.minYear;
        var minMonth = 6;
        var maxYear = this.props.maxYear;
    
        const dobParse = dateBirthDay.split('/');
        const dobResult = this._compute(dobParse);
        if (dobResult && (dobResult[0].value >= minYear && dobResult[0].value <= maxYear)) {
            if (dobResult[1].value < minMonth) {
                if (dobResult[0].value <= this.props.minYear) {
                    if (this.props.minDay !== undefined && this.props.minWeek !== undefined) {
                        if (dobResult[2].value < this.props.minWeek) {
                            Alert.alert('Maaf', 'Umur hari anda kurang 15 hari');
                        } else {
                            if (dobResult[2].value <= this.props.minWeek && dobResult[3].value == 0) {
                                Alert.alert('Maaf', 'Umur hari anda kurang 15 hari');
                            } else {
                                this._setValidateBirthDate(date, dobResult[0].value + 1);
                            }
                        }
                    } else {
                        Alert.alert('Maaf', 'Umur tahun anda tidak mencukupi');
                    }
                } else {
                    this._setValidateBirthDate(date, dobResult[0].value);
                }
            } else if (dobResult[1].value >= minMonth) {
                this._setValidateBirthDate(date, dobResult[0].value + 1);
            } else {
                Alert.alert('Maaf', 'Umur anda tidak mencukupi');
            }
        } else {
            Alert.alert('Maaf', 'Umur anda tidak mencukupi');
        }
    }
}

export default BirthdayTertanggung