import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'
import { Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/EvilIcons'
import { Grid, Col } from 'react-native-easy-grid'
import Style from 'stylescreen/FormSignature'
import Color from 'style/Color'
import SignatureCapture from 'react-native-signature-capture'

class Signature extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    _saveSignature(result) {
        this.props.onSaveSignature(`data:image/png;base64,${result.encoded}`)
    }

    _dragSignature(event) {
        this.props.onDragSignature(event)
    }

    saveSign() {
        this.refs["sign"].saveImage();
    }

    resetSign() {
        this.refs["sign"].resetImage()
        this.props.onResetSignature()
    }

    renderAvailSignature() {
        if (this.props.assign == '') {
            return (
                <View style={Style.assignAvail}>
                    <Text style={Style.signatureName}>Silahkan tanda tangan di area yang tersedia</Text>
                </View>
            )
        }
        return (
            <View style={Style.assignAvail}>
                <Image source={{ uri: this.props.assign }} style={Style.imageAssign} />
                <Text style={Style.signatureName}>{this.props.name}</Text>
            </View>
        )
    }

    renderAssignArea() {
        return (
            <View>
                <View style={Style.signatureTitleArea}>
                    <Text style={Style.signatureTitleText}>Area Tanda Tangan</Text>
                </View>
                <SignatureCapture
                    style={Style.signature}
                    ref={"sign"}
                    onSaveEvent={(result) => this._saveSignature(result)}
                    onDragEvent={(e) => this._dragSignature(e)}
                    saveImageFileInExtStorage={false}
                    showNativeButtons={false}
                    showTitleLabel={true}
                    viewMode={"portrait"}
                />
            </View>
        )
    }

    renderButtonSave() {
        return (
            <Col style={Style.signatureSave}>
                <Button
                    buttonStyle={{ borderRadius: 0 }}
                    type="clear"
                    icon={
                        <Icon
                            style={{ marginRight: 15 }}
                            name="check"
                            size={24}
                            color={Color.primary}
                        />
                    }
                    title="Save"
                    titleStyle={{ color: Color.primary }}
                    onPress={() => this.saveSign()}
                />
            </Col>
        )
    }

    render() {
        return (
            <View style={Style.signatureContainer}>
                {this.renderAssignArea()}
                <Grid style={Style.signatureButtonArea}>
                    <Col>
                        <Button
                            buttonStyle={{ borderRadius: 0 }}
                            type="clear"
                            icon={
                                <Icon
                                    style={{ marginRight: 15 }}
                                    name="undo"
                                    size={24}
                                    color={Color.greyDark}
                                />
                            }
                            title="Reset"
                            titleStyle={{ color: Color.greyDark }}
                            onPress={() => this.resetSign()}
                        />
                    </Col>
                    {
                        this.props.assign == '' ? this.renderButtonSave() : <View></View>
                    }
                </Grid>
                {this.renderAvailSignature()}
            </View>
        )
    }
}

export default Signature