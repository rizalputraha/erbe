import React from 'react'
import { View, Text, TextInput, StyleSheet, Platform } from 'react-native'
import Color from 'style/Color'
import { Icon } from 'react-native-elements'
import { validateRequired, validateEmail, validateCountCharacter, formatPhoneNumber } from 'erbevalidation/Helper'

export default class InputComponent extends React.Component {

    constructor(props) {
        super(props)
        this.state = { ...this.props }
    }

    getValue() {
        if (this.props.value !== this.state.value) return this.props.value;
        return this.state.value;
    }

    getValidation() {
        var res = false;
        if (this.state.validation === undefined) return res;
        const validationParam = this.state.validation;

        validationParam.forEach(item => {
            if (item === 'required') {
                res = validateRequired(this.props.value)
            }

            if (item === 'email') {
                res = validateEmail(this.props.value)
            }

            if (item === 'length') {
                res = validateCountCharacter(this.props.value, this.props.minCharacter, this.props.maxCharacter)
            }

            if (item === 'phone') {
                res = formatPhoneNumber(this.props.value)
            }
        });
        return res;

    }

    getColorIcon() {
        var check = this.getValidation();
        if (check) return Color.greenSuccess;
        return Color.redDanger;
    }

    getNameIcon() {
        var check = this.getValidation();
        if (check) return 'check';
        return 'close-o';
    }

    renderIcon() {
        return (
            <View style={styles.iconArea}>
                <Icon
                    type='evilicon'
                    name={this.getNameIcon()}
                    size={20}
                    color={this.getColorIcon()}
                />
            </View>
        )
    }

    async setValueText(text) {
        await this.setState({ value: text })
        this.props.onChangeText(text)
        if (this.props.callback !== undefined) {
            var check = this.getValidation();
            this.props.callback(check)
        }
    }

    setInputRef = ref => {
        const { getRef } = this.props;
        if (getRef) {
            getRef(ref);
        }
    };

    renderAndroid() {
        return (
            <TextInput
                style={[styles.input, this.state.isActive && styles.active]}
                onChangeText={async (text) => await this.setValueText(text)}
                onBlur={() => {
                    this.setState({ isActive: false })
                }}
                onFocus={() => {
                    this.setState({ isActive: true })
                }}
                value={this.getValue()}
                keyboardType={this.props.keyboardType !== undefined ? this.props.keyboardType : 'default'}
                blurOnSubmit={this.props.blurOnSubmit !== undefined ? this.props.blurOnSubmit : false}
                returnKeyType={this.props.returnKeyType !== undefined ? this.props.returnKeyType : 'done'}
                autoCapitalize={this.props.autoCapitalize !== undefined ? this.props.autoCapitalize : 'sentences'}
                secureTextEntry={this.props.secureTextEntry !== undefined ? this.props.secureTextEntry : false}
                onSubmitEditing={() => {
                    this.props.onSubmitEditing ?
                        this.props.onSubmitEditing()
                        :
                        this.props.nextInput && this.props.nextInput.focus()
                }
                }
                ref={this.setInputRef}
                editable={this.props.editable}
            />
        );
    }

    renderIos() {
        return (
            <TextInput
                style={[styles.input, this.state.isActive && styles.active]}
                onChangeText={async (text) => await this.setValueText(text)}
                onBlur={() => {
                    this.setState({ isActive: false })
                }}
                onFocus={() => {
                    this.setState({ isActive: true })
                }}
                value={this.state.value}
                keyboardType={this.props.keyboardType !== undefined ? this.props.keyboardType : 'default'}
                blurOnSubmit={this.props.blurOnSubmit !== undefined ? this.props.blurOnSubmit : false}
                returnKeyType={this.props.returnKeyType !== undefined ? this.props.returnKeyType : 'done'}
                autoCapitalize={this.props.autoCapitalize !== undefined ? this.props.autoCapitalize : 'sentences'}
                secureTextEntry={this.props.secureTextEntry !== undefined ? this.props.secureTextEntry : false}
                onSubmitEditing={() => {
                    this.props.onSubmitEditing ?
                        this.props.onSubmitEditing()
                        :
                        this.props.nextInput && this.props.nextInput.focus()
                }
                }
                ref={this.setInputRef}
            />
        );
    }

    renderInput() {
        if (Platform.OS == 'android') return this.renderAndroid();
        return this.renderIos();
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.label}>{this.props.label}</Text>
                {this.renderInput()}
                {this.renderIcon()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white,
        position: 'relative',
        marginHorizontal: 15,
        marginVertical: 15
    },
    input: {
        height: 40,
        borderColor: Color.greyLight,
        borderBottomWidth: 2,
        color: Color.textDark,
        padding: 0,
        fontSize: 14
    },
    label: {
        fontSize: 14,
        color: Color.black
    },
    iconArea: {
        width: 20,
        height: 20,
        backgroundColor: 'transparent',
        position: 'absolute',
        right: 0,
        top: 30
    },
    active: {
        borderColor: Color.primary,
        color: Color.black
    }
})