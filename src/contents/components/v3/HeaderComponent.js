import React from 'react'
import { View, Text, StyleSheet, StatusBar, Platform } from 'react-native'
import { Col, Row, Grid } from 'react-native-easy-grid'
import { Icon } from 'react-native-elements'
import Color from 'style/Color'

export class HeaderComponent extends React.Component {
  constructor(props) {
    super(props)
    this.state = { ...this.props }
  }

  renderBackButton() {
    return (
      <Icon
        name={Platform.OS == 'ios' ? 'ios-arrow-back' : 'md-arrow-back'}
        type='ionicon'
        color={Color.white}
        onPress={() => this.props.pushBackButtonHeader()}
        underlayColor='transparent'
      />
    );
  }

  render() {
    console.log(this.state.title);

    return (
      <View style={{ height: 60 }}>
        <View style={styles.container}>
          <Grid style={styles.header}>
            <Col size={15} style={styles.left}>{this.renderBackButton()}</Col>
            <Col size={85} style={styles.right}><Text style={styles.title}>{this.state.title}</Text></Col>
          </Grid>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 35
  },
  header: {
    backgroundColor: Color.primary,
    paddingVertical: 15,
    height: 35
  },
  title: {
    textAlign: 'left',
    fontSize: 18,
    color: Color.white
  },
  left: {
    height: 35,
  },
  right: {
    height: 35,
  }
})

export default HeaderComponent;