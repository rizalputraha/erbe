import React, { Component } from 'react';
import { View, Text, StyleSheet, Modal, TouchableOpacity, ActivityIndicator } from 'react-native';
import { ListItem, Icon } from 'react-native-elements';
import HeaderComponent from './HeaderComponent'
import ErbeListMenu from './ErbeListMenu'
import ErbeText from './ErbeText'
import Color from "style/Color";
import { formatCurrency } from "erbevalidation/Helper";
import { Grid, Col } from 'react-native-easy-grid'

class ProductAsuransiJiwa extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modalVisible: false,
      data: this.props.data,
      selected: this.props.defaultSelected,
      product: null,
    }
  }

  getLabel(item) {
    return item[this.props.labelItem];
  }

  async setProduct(item) {
    await this.setState({ product: item });
  }

    renderItems(item) {
        return (
        <TouchableOpacity style={{ margin: 10, backgroundColor: '#fff', elevation: 5 }} onPress={() => this.setValueModal(item)}>
            <Grid>
            <Col>
            <ErbeText
                value={item.product_sub}
                style={'h4'}
                position={'center'}
            />
            </Col>
            </Grid>
            <Grid>
            <Col>
            <ErbeText
                value={'Premi Pokok'}
                style={'p'}
                position={'left'}
            />
            </Col>
            <Col>
            <ErbeText
                value={'Premi Topup'}
                style={'p'}
                position={'right'}
            />
            </Col>
            </Grid>
            <Grid>
            <Col>
            <ErbeText
                value={formatCurrency(item.premi_pokok)}
                style={'p'}
                position={'left'}
            />
            </Col>
            <Col>
            <ErbeText
                value={formatCurrency(item.premi_topup)}
                style={'p'}
                position={'right'}
            />
            </Col>
            </Grid>
            <Grid style={{backgroundColor: Color.lamboDark}}>
            <Col>
                <ErbeText
                value={`Total premi ${formatCurrency(item.premi)}`}
                style={'h5'}
                position={'center'}
                color={'white'}
                />
            </Col>
            </Grid>
        </TouchableOpacity>
        )
    }

    renderList() {
        if (this.props.data == null) return (<ActivityIndicator size="small" color="#F44336" />);
        else return (
        <ErbeListMenu
            tipe="list"
            data={this.props.data}
            renderListItem={(item) => this.renderItems(item)}
            keyExtractor={(item, index) => index.toString()}
        />
        )
    }

  setValueModal(item) {
    this.props.onChangeItem(item);
    this.setState({ modalVisible: !this.state.modalVisible, selected: item[this.props.labelItem] });
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  renderHeaderModal() {
    return (
      <HeaderComponent
        title={this.props.title}
        pushBackButtonHeader={() => this.setModalVisible(!this.state.modalVisible)}
      />
    );
  }

  renderModal() {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.setModalVisible(!this.state.modalVisible);
        }}>
          {this.renderHeaderModal()}
          <View>
            {this.renderList()}
          </View>
      </Modal>
    );
  }

  _getValidationRequire() {
    if (this.state.selected === false) return <Icon name='arrow-forward' style={styles.iconInValid} />;
    return <Icon name='checkmark-circle' style={styles.iconValid} />;
  }

  renderTitle() {
    if (this.props.value === undefined) {
        return <Text>{this.props.titleOptions}</Text>
    } else {
        return <Text>{this.props.value}</Text>
    }
  }

  render() {
    return (
      <View>
        <Text style={styles.content}>{this.props.title}</Text>
        <ListItem underlayColor={Color.greyLight}
          title={this.renderTitle()}
          onPress={() => {
            this.setModalVisible(!this.state.modalVisible);
          }}
        />
        {this.renderModal()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  label: {
    fontSize: 16,
    padding: 10,
    color: '#616161',
    fontFamily: 'DroidSans',
  },
  content: {
    flexDirection: 'column',
    margin: 10,
    fontSize: 16,
    color: Color.greyDark
  },
  header: {
    backgroundColor: '#F44336',
  },
  iconValid: {
    color: 'green',
  },
  iconInValid: {
    color: 'red',
  },
  containerProduct: {
    margin: 10,
    elevation: 5
  }
});

export default ProductAsuransiJiwa
