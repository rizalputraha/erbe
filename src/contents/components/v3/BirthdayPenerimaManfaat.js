import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    View,
    Text,
} from 'react-native';
import { Button, Icon } from 'react-native-elements';
import RNDateTimePicker from '@react-native-community/datetimepicker';
import Color from 'style/Color';
import moment from 'moment';

export default class BirthdayPenerimaManfaat extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dobDate: new Date(),
            dobText: null,
            age: null,
            show: false,
            maximumDate: new Date()
        }
    }

    componentDidMount() {
        this.setState({
            dobDate: new Date(),
            dobText: this.props.getDob == '' ? null : this.props.getDob,
        })
    }

    _onDobPress = () => {
        this.setState({
            show: true
        })
    }

    setDobDate(date) {
        var newFormatDate = moment(date).format('DD/MM/YYYY');
        this.setState({
            dobDate: date,
            dobText: newFormatDate
        });
        this.props.setDob(newFormatDate);
        return newFormatDate;
    }

    _getBirthday(Inputyear, Inputmonth, Inputday) {
        var inputYear = parseInt(Inputyear),
            inputMonth = parseInt(Inputmonth),
            inputDay = parseInt(Inputday),
            birthday;

        if (isNaN(inputDay)) {
            // Alert.alert('Invalid day.');
        }
        else if (isNaN(inputMonth)) {
            // Alert.alert('Invalid month.');
        }
        else if (inputMonth < 1 || inputMonth > 12) {
            // Alert.alert("Month is out of range");
        }
        else if (isNaN(inputYear)) {
            // Alert.alert('Invalid year.');
        }
        else if (inputYear < 1900) {
            // Alert.alert("You're that old!?");
        }
        else {
            var tempBirthday = moment([inputYear, inputMonth - 1, inputDay]);
            if (tempBirthday.isValid()) {
                if (tempBirthday.diff(new Date(), 'days') < 0) {
                    birthday = tempBirthday;
                }
                else {
                    // Alert.alert("Tanggal lahir minimal 1 hari sebelumnya");
                }
            }
            else if (tempBirthday.invalidAt(2)) {  //month overflow
                // Alert.alert('Day is out of range');
            }
        }

        return birthday;
    }

    getFormattedDateDiff(birthday, today) {
        var b = moment(birthday),
            a = moment(today),
            interval = ['years', 'months', 'weeks', 'days'],
            out = [];

        interval.forEach(function (item, index) {
            var res = a.diff(b, item);
            b.add(res, item);
            out.push({ param: item, value: res })
        })
        return out;
    };

    /**
     * 
     * @param {Array} dobResult //0: years, 1: months, 2: weeks, 3: days
     */
    _compute(dobResult) {
        var today = moment(new Date()),
            birthday = this._getBirthday(dobResult[2], dobResult[1], dobResult[0]);
        if (!birthday) return;
        let res = this.getFormattedDateDiff(birthday, today);
        __DEV__ && console.log('Res _compute', res)
        return res;
    }

    _onPickedLahir = (event, date) => {
        var dateBirthDay = moment(date).format('DD/MM/YYYY');
        var minYear = this.props.minYear;
        var minMonth = 6;
        var maxYear = this.props.maxYear;

        const dobParse = dateBirthDay.split('/');
        const dobResult = this._compute(dobParse);
        if (dobResult && (dobResult[0].value >= minYear && dobResult[0].value <= maxYear)) {
            if (dobResult[1].value < minMonth) {
                if (dobResult[0].value <= this.props.minYear) {
                    if (this.props.minDay !== undefined && this.props.minWeek !== undefined) {
                        if (dobResult[2].value < this.props.minWeek) {
                            Alert.alert('Maaf', 'Umur hari anda kurang 15 hari');
                        } else {
                            if (dobResult[2].value <= this.props.minWeek && dobResult[3].value == 0) {
                                Alert.alert('Maaf', 'Umur hari anda kurang 15 hari');
                            } else {
                                this._setValidateBirthDate(date, dobResult[0].value + 1);
                            }
                        }
                    } else {
                        Alert.alert('Maaf', 'Umur tahun anda tidak mencukupi');
                    }
                } else {
                    this._setValidateBirthDate(date, dobResult[0].value);
                }
            } else if (dobResult[1].value >= minMonth) {
                this._setValidateBirthDate(date, dobResult[0].value + 1);
            } else {
                Alert.alert('Maaf', 'Umur anda tidak mencukupi');
            }
        } else {
            Alert.alert('Maaf', 'Umur anda tidak mencukupi');
        }
    }

    _setValidateBirthDate(date, age) {
        this.setState({
            show: Platform.OS === 'ios' ? true : false
        })
        this.setDobDate(date);
    }

    async _denyApply() {
        await this.props.backNav();
    }

    _getValidationRequire() {
        const { dobText, age } = this.state
        if (dobText == null || age == null) {
            return <Icon name='date-range' containerStyle={{ marginLeft: 10 }} type='material' color={Color.dark} />
        }
        return <Icon name='done' containerStyle={{ marginLeft: 10 }} type='material' color='green' />;
    }

    renderButtonTglLahir() {
        const { show, dobDate, dobText } = this.state

        return (
            <View style={{ paddingHorizontal: 15 }}>
                <Text style={styles.label}>Tanggal Lahir</Text>
                <Button
                    type={'outline'}
                    icon={this._getValidationRequire()}
                    iconRight
                    title={dobText !== null ? dobText : 'Pilih Tanggal'}
                    onPress={() => this._onDobPress()}
                    buttonStyle={{ backgroundColor: Color.white, borderColor: Color.grey }}
                    titleStyle={{ color: Color.darkLight }}
                />
                {
                    show &&
                    <RNDateTimePicker
                        display="default"
                        onChange={(event, date) => this._onPickedLahir(event, date)}
                        value={dobDate}
                        mode={'date'}
                        maximumDate={this.state.maximumDate}
                    />
                }
            </View>
        )
    }

    render() {
        return (
            <View>
                {this.renderButtonTglLahir()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    label: {
        fontSize: 14,
        paddingVertical: 10,
        color: Color.darkLight,
        fontFamily: 'DroidSans',
    },
    datePickerBox: {
        paddingTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FAFAFA'
    },
    datePickerText: {
        fontSize: 14,
        color: 'black',
    },
});
