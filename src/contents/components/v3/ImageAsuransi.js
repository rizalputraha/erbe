import React, { Component } from 'react'
import { Text, View, Image } from 'react-native'

export default class ImageAsuransi extends Component {

   getLogoAsuransi(value){
      const obj = {
         'msig.png': require('asset/imgproduct/msig.png'),
         'car.png': require('asset/imgproduct/car.png'),
         'heksa.png': require('asset/imgproduct/heksa.png'),
         'adira.png': require('asset/imgproduct/adira.png'),
         'aca.png': require('asset/imgproduct/aca.png'),
         'allianz.png': require('asset/imgproduct/allianz.png'),
         'askrindo.png': require('asset/imgproduct/askrindo.png'),
         'astra.png': require('asset/imgproduct/astra.png'),
         'aswata.png': require('asset/imgproduct/aswata.png'),
         'jasindo.png': require('asset/imgproduct/jasindo.png'),
         'sinarmas.png': require('asset/imgproduct/sinarmas.png'),
         'tokio_marine.png': require('asset/imgproduct/tokio_marine.png'),
         'wanaartha.png': require('asset/imgproduct/wanaartha.png'),
         'zurich.png': require('asset/imgproduct/zurich.png'),
         'artarindo.png': require('asset/imgproduct/artarindo.png'),
         'panpacific.png': require('asset/imgproduct/panpacific.png'),
         'wahanatata.png': require('asset/imgproduct/wahanatata.png'),
         'mnc.png': require('asset/imgproduct/mnc.png'),
         'sinarmas.png': require('asset/imgproduct/sinarmas.png'),
       }
   
       return obj[value];
   }

   render() {
      return(
         <Image 
            source={this.getLogoAsuransi(this.props.logo)}
            style={{height: 30,width: 'auto'}}
            {...this.props}
         />
      )
   }
}
