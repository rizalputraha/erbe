import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import NextgIcon from './NextgIcon'
import Color from 'style/Color'

export class BadgeLevel extends Component {

    _getLevelName(value) {
        if (value == 0) {
            return (
                <View style={styles.agen}>
                    <NextgIcon name="star-0" size={32} color={Color.white} />
                </View>
            )
        } else if (value == 1) {
            return (
                <View style={styles.manajer}>
                    <NextgIcon name="star-1" size={32} color={Color.white} />
                </View>
            )
        } else if (value == 2) {
            return (
                <View style={styles.direktur}>
                    <NextgIcon name="star-2" size={32} color={Color.white} />
                </View>
            )
        }
    }

    render() {
        return (
            this._getLevelName(this.props.level)
        )
    }
}

const styles = StyleSheet.create({
    agen: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
        width: 60,
        borderRadius: 100,
        backgroundColor: '#AB4E11'
    },
    manajer: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
        width: 60,
        borderRadius: 100,
        backgroundColor: '#989898'
    },
    direktur: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
        width: 60,
        borderRadius: 100,
        backgroundColor: '#5A3985'
    }
})

export default BadgeLevel
