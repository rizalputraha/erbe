import ButtonNasabah from './ButtonNasabah'
import ButtonAgen from './ButtonAgen'
import OptionIsTertanggungTrue from './OptionIsTertanggungTrue'
import OptionIsTertanggungFalse from './OptionIsTertanggungFalse'
import GenderOptions from './GenderOptions'
import ReligionOptions from './ReligionOptions'
import MarriageOptions from './MarriageOptions'
import IdentityOptions from './IdentityOptions'
import RelationOptions from './RelationOptions'
import PekerjaanOptions from './PekerjaanOptions'
import WilayahOptionsPp from './WilayahOptionsPp'
import WilayahOptionsTt from './WilayahOptionsTt'
import NpwpOptionPemegangPolis from './NpwpOptionPemegangPolis'
import NpwpOptionTertanggung from './NpwpOptionTertanggung'
import BankOptions from './BankOptions'
import PembayarOptions from './PembayarOptions'
import RelationAhliwarisOptions from './RelationAhliwarisOptions'
import InfoSpajSubmit from './InfoSpajSubmit'

module.exports = {
    ButtonNasabah,
    ButtonAgen,
    OptionIsTertanggungTrue,
    OptionIsTertanggungFalse,
    GenderOptions,
    ReligionOptions,
    MarriageOptions,
    IdentityOptions,
    RelationOptions,
    PekerjaanOptions,
    WilayahOptionsPp,
    WilayahOptionsTt,
    NpwpOptionPemegangPolis,
    NpwpOptionTertanggung,
    BankOptions,
    PembayarOptions,
    RelationAhliwarisOptions,
    InfoSpajSubmit
}