import React, { Component } from 'react'
import { PickerItemValidation } from 'component/v3'
import { masterRelationAhliWaris } from 'erbecache/model/MsigModel'

class RelationAhliwarisOptions extends Component {
    constructor(props) {
        super(props)
        this.state={
            data: [],
            isCompleteLoadData: false
        }
    }

    async componentDidMount() {
        const res = await masterRelationAhliWaris()
        await this.setState({
            data: res,
            isCompleteLoadData: true
        })
    }

    render() {
        return(
            this.state.isCompleteLoadData && <PickerItemValidation
                title={'Hubungan Dengan Tertanggung'}
                titleEmptyData={'Pilih Hubungan'}
                titleForm={true}
                paramSelected={'relationId'}
                paramView={'relationName'}
                data={this.state.data}
                selected={this.props.selected}
                onChangeItem={(value) => this.props.onChangeItem(value)}
            />
        )
    }
}

export default RelationAhliwarisOptions