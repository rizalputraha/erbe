import React from 'react'
import { connect } from 'react-redux'
import { CheckBox } from 'react-native-elements'
import { setStateRootMsig } from 'erberedux/actions'

class OptionIsTertanggungFalse extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        console.log('OptionIsTertanggungFalse', this.props)
    }

    render() {
        return (
            <CheckBox
                title="Tidak"
                checkedIcon="dot-circle-o"
                uncheckedIcon="circle-o"
                checked={!this.props.isTertanggung}
                onPress={() => {
                    this.props.setStateRootMsig('isTertanggung', false)
                }}
                checkedColor="green"
            />
        )
    }
}



const mtp = ({ Msig }) => {
    const { isTertanggung } = Msig
    return { isTertanggung }
}

export default connect(mtp, { setStateRootMsig })(OptionIsTertanggungFalse)