import React, { Component } from 'react'
import { PickerItemValidation } from 'component/v3'
import { masterReligion } from 'erbecache/model/MsigModel'

class ReligionOptions extends Component {
    constructor(props) {
        super(props)
        this.state={
            data: [],
            isCompleteLoadData: false
        }
    }

    async componentDidMount() {
        const res = await masterReligion()
        await this.setState({
            isCompleteLoadData: true,
            data: res
        })
    }

    render() {
        return(
            this.state.isCompleteLoadData && <PickerItemValidation
                title={'Agama'}
                titleEmptyData={'Pilih agama'}
                titleForm={true}
                paramSelected={'religionId'}
                paramView={'religionName'}
                data={this.state.data}
                selected={this.props.selected}
                onChangeItem={(value) => this.props.onChangeItem(value)}
            />
        )
    }
}

export default ReligionOptions