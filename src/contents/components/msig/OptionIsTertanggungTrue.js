import React from 'react'
import { connect } from 'react-redux'
import { CheckBox } from 'react-native-elements'
import { setStateRootMsig } from 'erberedux/actions'

class OptionIsTertanggungTrue extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        console.log('OptionIsTertanggungTrue', this.props)
    }

    render() {
        return (
            <CheckBox
                title="Ya"
                checkedIcon="dot-circle-o"
                uncheckedIcon="circle-o"
                checked={this.props.isTertanggung}
                onPress={() => {
                    this.props.setStateRootMsig('isTertanggung', true)
                }}
                checkedColor="green"
            />
        )
    }
}



const mtp = ({ Msig }) => {
    const { isTertanggung } = Msig
    return { isTertanggung }
}

export default connect(mtp, { setStateRootMsig })(OptionIsTertanggungTrue)