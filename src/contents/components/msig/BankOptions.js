import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { connect } from 'react-redux'
import { setStateSpajMsig } from 'erberedux/actions'
import { masterBank } from 'erbecache/model/MsigModel'
import { ModalListSearchFilter, InputComponent, TitleComponent } from '../v3'

class BankOptions extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataBank: null,
            dataBranch: null,
            selectedBank: this.props.spaj.espaj.pemegangPolis.bankIdPp,
            selectedBranch: this.props.spaj.espaj.pemegangPolis.branchIdPp,
            namaRekening: this.props.spaj.espaj.pemegangPolis.namaRekPp,
            noRekening: this.props.spaj.espaj.pemegangPolis.noRekPp,
            maxRekBank: 10,
            isCompletedLoad: false
        }
    }

    async componentDidMount() {
        const bank = await masterBank()
        await this.setState({dataBank: bank, isCompletedLoad: true})
        if(this.state.selectedBank !== '') {
            const bankRek = this.state.dataBank.filter(item => item.bankId == this.state.selectedBank);
            await this.setState({
                dataBranch: this.filterBranch(this.state.selectedBank),
                maxRekBank: bankRek[0].maxrek
            })
        }
    }

    filterBranch(bankId) {
        const bank = this.state.dataBank.filter(item=> item.bankId == bankId)
        return bank[0].cabang
    }

    filterMaxRek(bankId) {
        const bankRek = this.state.dataBank.filter(item => item.bankId == bankId);
        return bankRek[0].maxrek
    }

    renderBranch() {
        const { spaj, setStateSpajMsig } = this.props
        return this.state.dataBranch &&
            <ModalListSearchFilter
                title={'Cabang Bank'}
                titleEmptyData={'Pilih Cabang Bank'}
                titleForm={true}
                paramSelected={'branchName'}
                paramView={'branchName'}
                data={this.state.dataBranch}
                selected={this.state.selectedBranch}
                onChangeItem={(value) => {
                    this.setState({
                        selectedBranch: value
                    }),
                    setStateSpajMsig('pemegangPolis', 'branchIdPp', value)
            }} />
    }

    renderBank() {
        const { spaj, setStateSpajMsig } = this.props
        return this.state.isCompletedLoad &&
            <ModalListSearchFilter
                title={'Bank'}
                titleEmptyData={'Pilih Bank'}
                titleForm={true}
                paramSelected={'bankId'}
                paramView={'bankName'}
                data={this.state.dataBank}
                selected={spaj.espaj.pemegangPolis.bankIdPp}
                onChangeItem={(value) => {
                    this.setState({ 
                        dataBranch: this.filterBranch(value),
                        selectedBranch: '',
                        maxRekBank: this.filterMaxRek(value)
                    }),
                    setStateSpajMsig('pemegangPolis', 'bankIdPp', value),
                    setStateSpajMsig('pemegangPolis', 'branchIdPp', ''),
                    console.log('componentDidMount Bank Options', this.state)
            }} />
    }

    renderNama() {
        const { spaj, setStateSpajMsig } = this.props
        return (
        <InputComponent
            label={'Nama Rekening Bank Pemegang Polis'}
            placeholder={''}
            value={this.state.namaRekening}
            onChangeText={ (value) => {
                setStateSpajMsig('pemegangPolis', 'namaRekPp', value),
                this.setState({ namaRekening: value })
            }}
            validation={['required', 'length']}
            minCharacter={3}
            maxCharacter={30}
        />
        )
    }

    renderNoRek() {
        const { spaj, setStateSpajMsig } = this.props
        return (
            (this.state.selectedBranch !== '' && this.state.maxRekBank !== '' && spaj) &&
            <InputComponent
                label={'No Rekening'}
                placeholder={''}
                value={this.state.noRekening}
                onChangeText={ (value) => {
                    console.log('This state', this.state),
                    setStateSpajMsig('pemegangPolis', 'noRekPp', value),
                    this.setState({ noRekening: value })
                }}
                validation={['required', 'length']}
                minCharacter={3}
                maxCharacter={this.state.maxRekBank}
                keyboardType={'numeric'}
            />
        )
    }

    renderInfoPolis() {
        return(
            <View>
                <TitleComponent
                    value='Polis dikirim melalui'
                />
                <Text style={{marginHorizontal: 15}}>Softcopy Polis dikirim ke alamat email Ikhtisar Polis dikirim melalui Kurir/ POS</Text>
            </View>
        )
    }

    render() {
        return(
            <View>
                {this.renderBank()}
                {this.renderBranch()}
                {this.renderNama()}
                {this.renderNoRek()}
                {this.renderInfoPolis()}
            </View>
        )
    }
}

const mtp = ({Msig}) => {
    const { spaj } = Msig
    return { spaj }
}
export default connect(mtp, {setStateSpajMsig})(BankOptions)