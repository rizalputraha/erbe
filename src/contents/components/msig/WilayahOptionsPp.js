import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { ModalListSearchFilter, InputComponent } from '../v3'
import { masterWilayah } from 'erbecache/model/MsigModel'
import { connect } from 'react-redux'
import { setStateWilayahMsig } from 'erberedux/actions'

class WilayahOptionsPp extends Component {
    constructor(props) {
        super(props)
        this.state={
            dataPropinsi: null,
            dataKota: null,
            dataKecamatan: null,
            dataKelurahan: null,
            selectedPropinsi: this.props.spaj.espaj.pemegangPolis.propinsiIdPp,
            selectedKota: this.props.spaj.espaj.pemegangPolis.kotaIdPp,
            selectedKecamatan: this.props.spaj.espaj.pemegangPolis.kecamatanIdPp,
            selectedKelurahan: this.props.spaj.espaj.pemegangPolis.kelurahanIdPp,
            selectedKodePos: this.props.spaj.espaj.pemegangPolis.kdPosPp,
            isCompletedLoad: false
        }
    }

    async componentDidMount() {
        await this.filterPropinsi()
        if(this.state.selectedPropinsi !== '') {
            await this.setState({
                dataKota: this.filterKota(this.state.selectedPropinsi)
            })
        }
        if(this.state.selectedKota !== '') {
            await this.setState({
                dataKecamatan: this.filterKecamatan(this.state.selectedKota)
            })
        }
        if(this.state.selectedKecamatan !== '') {
            await this.setState({
                dataKelurahan: this.filterKelurahan(this.state.selectedKecamatan)
            })
        }
        if(this.state.selectedKelurahan !== '') {
            const kodepos = this.filterKodePos(this.state.selectedKelurahan)
            await this.setState({
                selectedKodePos: kodepos.toString()
            })
        }

        if(this.state.selectedKodePos == '') {
            this.props.setStateWilayahMsig('pemegangPolis', 'kodepos', '');
        }
    }

    async filterPropinsi() {
        const wilayah = await masterWilayah()
        const propinsi = wilayah;
        await this.setState({dataPropinsi: propinsi, isCompletedLoad: true})
    }

    filterKota(propinsiId) {
        const propinsi = this.state.dataPropinsi.filter(item => item.propinsiId == propinsiId)
        return propinsi[0].kota;
    }

    filterKotaName(kotaId) {
        const kota = this.state.dataKota.filter(item => item.kotaId == kotaId)
        return kota[0].kotaName
    }

    filterKecamatan(kotaId) {
        const kota = this.state.dataKota.filter(item => item.kotaId == kotaId)
        return kota[0].kecamatan
    }

    filterKelurahan(kecamatanId) {
        const kecamatan = this.state.dataKecamatan.filter(item => item.kecamatanId == kecamatanId)
        return kecamatan[0].kelurahan
    }

    filterKodePos(kelurahanId) {
        const kelurahan = this.state.dataKelurahan.filter(item => item.kelurahanId == kelurahanId)
        return kelurahan[0].kodepos
    }

    renderKodePos() {
        const { spaj, setStateWilayahMsig } = this.props
        const espaj = spaj.espaj
        const pemegangPolis = espaj.pemegangPolis
        const kdPosPp = pemegangPolis
        return(
            <View>
                {
                    (this.state.dataKelurahan && spaj.espaj.pemegangPolis.kdPosPp !== '') &&
                    <InputComponent
                        label={'Kode Pos'}
                        placeholder={''}
                        value={spaj.espaj.pemegangPolis.kdPosPp !== '' ? spaj.espaj.pemegangPolis.kdPosPp : ''}
                        onChangeText={ async (value) => {
                            setStateWilayahMsig('pemegangPolis', 'kodepos', value),
                            await this.setState({ selectedKodePos: value})
                        }}
                        validation={['required']}
                        minCharacter={3}
                        maxCharacter={30}
                    />
                }
            </View>
        )
    }

    renderKelurahan() {
        const { spaj, setStateWilayahMsig } = this.props
        return(
            <View>
                {
                    this.state.dataKelurahan &&
                    <ModalListSearchFilter
                        title={'Kelurahan'}
                        titleEmptyData={'Pilih Kelurahan'}
                        titleForm={true}
                        paramSelected={'kelurahanId'}
                        paramView={'kelurahanName'}
                        data={this.state.dataKelurahan}
                        selected={spaj.espaj.pemegangPolis.kelurahanIdPp}
                        onChangeItem={async (value) => {
                            setStateWilayahMsig('pemegangPolis', 'kelurahan', value),
                            setStateWilayahMsig('pemegangPolis', 'kodepos', this.filterKodePos(value).toString()),
                            await this.setState({ 
                                selectedKelurahan: value,
                                selectedKodePos: this.filterKodePos(value).toString()
                            })
                    }} />
                }
            </View>
        )
    }

    renderKecamatan() {
        const { spaj, setStateWilayahMsig } = this.props
        return(
            <View>
                {
                    this.state.dataKecamatan &&
                    <ModalListSearchFilter
                        title={'Kecamatan'}
                        titleEmptyData={'Pilih Kecamatan'}
                        titleForm={true}
                        paramSelected={'kecamatanId'}
                        paramView={'kecamatanName'}
                        data={this.state.dataKecamatan}
                        selected={spaj.espaj.pemegangPolis.kecamatanIdPp}
                        onChangeItem={async (value) => {
                            setStateWilayahMsig('pemegangPolis', 'kecamatan', value),
                            await this.setState({
                                dataKelurahan: this.filterKelurahan(value)
                            })
                    }} />
                }
            </View>
        )
    }

    renderKota() {
        const { spaj, setStateWilayahMsig } = this.props
        return(
            <View>
                {
                    this.state.dataKota &&
                    <ModalListSearchFilter
                        title={'Kota'}
                        titleEmptyData={'Pilih Kota'}
                        titleForm={true}
                        paramSelected={'kotaId'}
                        paramView={'kotaName'}
                        data={this.state.dataKota}
                        selected={spaj.espaj.pemegangPolis.kotaIdPp}
                        onChangeItem={async (value) => {
                            setStateWilayahMsig('pemegangPolis', 'kota', value),
                            setStateWilayahMsig('pemegangPolis', 'kotaName', this.filterKotaName(value)),
                            await this.setState({
                                dataKecamatan: this.filterKecamatan(value),
                                dataKelurahan: null
                            })
                    }} />
                }
            </View>
        )
    }

    renderPropinsi() {
        const { spaj, setStateWilayahMsig } = this.props
        return(
            <View>
                {
                    this.state.isCompletedLoad && <ModalListSearchFilter
                        title={'Propinsi'}
                        titleEmptyData={'Pilih Propinsi'}
                        titleForm={true}
                        paramSelected={'propinsiId'}
                        paramView={'propinsiName'}
                        data={this.state.dataPropinsi}
                        selected={spaj.espaj.pemegangPolis.propinsiIdPp}
                        onChangeItem={async (value) => {
                            setStateWilayahMsig('pemegangPolis', 'propinsi', value),
                            await this.setState({
                                dataKota: this.filterKota(value),
                                dataKecamatan: null,
                                dataKelurahan: null
                            })
                        }} />
                }
            </View>
        )
    }

    render() {
        return(
            <View>
                {this.renderPropinsi()}
                {this.renderKota()}
                {this.renderKecamatan()}
                {this.renderKelurahan()}
                {this.renderKodePos()}
            </View>
        )
    }
}

const mtp = ({ Msig }) => {
    const { spaj } = Msig
    return { spaj }
}
export default connect(mtp, { setStateWilayahMsig} )(WilayahOptionsPp)