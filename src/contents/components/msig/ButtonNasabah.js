import React from 'react'
import { Button, ThemeProvider } from 'react-native-elements'
import Theme from 'style/Theme'
import ThemeSecondary from 'style/ThemeSecondary'
import { connect } from 'react-redux'

class ButtonNasabah extends React.Component {
    constructor(props) {
        super(props)
    }

    renderButton() {
        const { loadingButtonNasabah } = this.props

        if(loadingButtonNasabah === true) {
            return(
                <Button
                    title={'Proses...'}
                    loading={true}
                    type={'solid'}
                    disabled={true}>
                </Button>
            )
        }

        return (
            <Button
                title={'Buat SPAJ Nasabah'}
                type={'solid'}
                onPress={() => this.props.onPress()}
            >
            </Button>
        )
    }

    render() {
        return (
            <ThemeProvider theme={Theme}>
                {this.renderButton()}
            </ThemeProvider>
        )
    }
}



const mtp = ({ Msig }) => {
    const { loadingButtonNasabah } = Msig
    return { loadingButtonNasabah }
}

export default connect(mtp, { })(ButtonNasabah)