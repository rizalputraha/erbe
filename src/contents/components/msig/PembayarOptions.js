import React, { Component } from 'react'
import { PickerItemValidation } from 'component/v3'
import { View } from 'react-native'
import { connect } from 'react-redux'
import { setStateSpajMsig, setStateRootMsig } from 'erberedux/actions'
class PembayarOptions extends Component {
    constructor(props) {
        super(props)
        this.state={
            data: [
                {pembayarId: 1, pembayarName: 'Pemegang Polis'},
                {pembayarId: 2, pembayarName: 'Pihak ke 3'}
            ],
            selected: undefined,
            isCompleteLoadData: false
        }
    }

    async componentDidMount() {
        await this.setState({
            selected: 1
        })
    }

    _changeProp(value) {
        const { setStateRootMsig } = this.props
        if(parseInt(value) == 1) {
            setStateRootMsig('isCalonPembayar', true)
        } else {
            setStateRootMsig('isCalonPembayar', false)
        }
    }

    _changeRelation(relationId) {
        const { setStateSpajMsig } = this.props
        setStateSpajMsig('calonPembayar', 'int_spin_hub_dgppphk3_cp', relationId)
        setStateSpajMsig('calonPembayar', 'int_ket_cp', relationId)
        setStateSpajMsig('pemegangPolis', 'hubunganCpPp', relationId)
    }

    render() {
        return(
            <View>
                {
                    this.state.selected &&
                    <PickerItemValidation
                        disabled={true}
                        title={'Calon Pembayar'}
                        titleEmptyData={''}
                        titleForm={true}
                        paramSelected={'pembayarId'}
                        paramView={'pembayarName'}
                        data={this.state.data}
                        selected={this.state.selected}
                        onChangeItem={ (value) => this._changeProp(value) }
                    />
                }
            </View>
        )
    }
}

const mtp = ({Msig}) => {
    const {isCalonPembayar, spaj} = Msig
    return { isCalonPembayar, spaj }
}

export default connect(mtp, {setStateSpajMsig, setStateRootMsig})(PembayarOptions)