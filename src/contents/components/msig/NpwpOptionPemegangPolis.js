import React from 'react'
import { View, Text } from 'react-native'
import { connect } from 'react-redux'
import { CheckBox } from 'react-native-elements'
import { setStateRootMsig, setStateSpajMsig } from 'erberedux/actions'
import { InputComponent } from '../v3'
import { Grid, Col, Row } from 'react-native-easy-grid'
import Color from 'style/Color'

class NpwpOptionPemegangPolis extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        console.log('NpwpOptionPp', this.props)
    }

    renderFormNpwp() {
        const { npwpOptionPp, spaj, setStateSpajMsig } = this.props
        const { espaj } = spaj
        const { pemegangPolis } = espaj
        return(
            npwpOptionPp.optionYa &&
            <InputComponent
                label={'NPWP'}
                placeholder={''}
                value={pemegangPolis.npwpPp}
                onChangeText={ (value) => {
                    setStateSpajMsig('pemegangPolis', 'npwpPp', value)
                }}
                validation={['required', 'length']}
                minCharacter={3}
                maxCharacter={20}
            />
        )
    }

    render() {
        const { npwpOptionPp, setStateSpajMsig } = this.props
        return (
            npwpOptionPp &&
            <View style={{
                marginHorizontal: 5
            }}>
                <Grid>
                <Col>
                    <View sytle={{flex: 1, marginHorizontal: 15}}>
                        <Text style={{
                            fontSize: 14,
                            color: Color.darkLight,
                            marginLeft: 10
                        }}>
                            Apakah anda mempunyai npwp ?</Text>
                    </View>
                </Col>
                </Grid>

                <Grid>
                    <Col>
                        <CheckBox
                            title="Ya"
                            size={16}
                            textStyle={{fontSize: 12}}
                            checkedIcon="dot-circle-o"
                            uncheckedIcon="circle-o"
                            checked={npwpOptionPp.optionYa}
                            onPress={() => {
                                this.props.setStateRootMsig('npwpOptionPp', {
                                    optionYa: true,
                                    optionTidak: false
                                }),
                                setStateSpajMsig('pemegangPolis', 'npwpPp', '')
                            }}
                            checkedColor="green"
                            containerStyle={{
                                backgroundColor: "white",
                                borderWidth: 0,
                                borderBottomWidth: 2,
                                borderBottomColor: Color.greyLight
                            }}
                        />
                    </Col>
                    <Col>
                        <CheckBox
                            title="Tidak"
                            size={16}
                            textStyle={{fontSize: 12}}
                            checkedIcon="dot-circle-o"
                            uncheckedIcon="circle-o"
                            checked={npwpOptionPp.optionTidak}
                            onPress={() => {
                                this.props.setStateRootMsig('npwpOptionPp', {
                                    optionYa: false,
                                    optionTidak: true
                                }),
                                this.props.setStateRootMsig('npwpOptionTt', {
                                    optionYa: false,
                                    optionTidak: true
                                }),
                                setStateSpajMsig('pemegangPolis', 'npwpPp', '123456789'),
                                setStateSpajMsig('tertanggung', 'npwpTt', '123456789')
                            }}
                            checkedColor="green"
                            containerStyle={{
                                backgroundColor: "white",
                                borderWidth: 0,
                                borderBottomWidth: 2,
                                borderBottomColor: Color.greyLight
                            }}
                        />
                    </Col>
                </Grid>
                <Grid>
                    <Col>
                    {this.renderFormNpwp()}
                    </Col>
                </Grid>
            </View>
        )
    }
}



const mtp = ({ Msig }) => {
    const { npwpOptionPp, spaj } = Msig
    return { npwpOptionPp, spaj }
}

export default connect(mtp, { setStateRootMsig, setStateSpajMsig })(NpwpOptionPemegangPolis)