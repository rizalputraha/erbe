import React, { Component } from 'react'
import { PickerItemValidation } from 'component/v3'
import { masterRelation } from 'erbecache/model/MsigModel'
import { connect } from 'react-redux'
import { ItemComponent } from 'component/v3'
import { setStateSpajMsig } from 'erberedux/actions'
class RelationOptions extends Component {
    constructor(props) {
        super(props)
        this.state={
            data: [],
            isCompleteLoadData: false
        }
    }

    async componentDidMount() {
        const { isTertanggung, setStateSpajMsig } = this.props
        const res = await masterRelation()
        
        if(isTertanggung === true) {
            const hubunganPp = this.getRelationId('Diri Sendiri', res)
            setStateSpajMsig('pemegangPolis', 'hubunganPp', hubunganPp) //Set default diri sendiri
        } else {
            const relation = res.filter(item => item.relationName !== 'Diri Sendiri')
            await this.setState({
                isCompleteLoadData: true,
                data: relation
            })
        }
        
    }

    getRelationId(relationName, res) {
        const data = res.filter(item => item.relationName == relationName)
        return data[0].relationId
    }

    getRelationName(relationId) {
        const data = this.state.data.filter(item => item.relationId == relationId)
        return data[0].relationName
    }

    render() {
        const { isTertanggung, spaj, setStateSpajMsig } = this.props
        const pemegangPolis = spaj.espaj.pemegangPolis

        if(isTertanggung === true) {
            return(
                this.state.isCompleteLoadData && <ItemComponent
                    label={'Hubungan Dengan Tertanggung'}
                    value={this.getRelationName(pemegangPolis.hubunganPp)}
                />
            )
        } else {
            return(
                this.state.isCompleteLoadData && <PickerItemValidation
                    title={'Hubungan Dengan Tertanggung'}
                    titleEmptyData={'Pilih Hubungan'}
                    titleForm={true}
                    paramSelected={'relationId'}
                    paramView={'relationName'}
                    data={this.state.data}
                    selected={pemegangPolis.hubunganPp}
                    onChangeItem={(value) => setStateSpajMsig('pemegangPolis', 'hubunganPp', value)}
                />
            )
        }
    }
}

const mtp = ({Msig}) => {
    const {isTertanggung, spaj} = Msig
    return { isTertanggung, spaj }
}

export default connect(mtp, {setStateSpajMsig})(RelationOptions)