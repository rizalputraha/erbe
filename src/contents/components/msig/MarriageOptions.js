import React, { Component } from 'react'
import { PickerItemValidation } from 'component/v3'
import { masterMarriageStatus } from 'erbecache/model/MsigModel'

class MarriageOptions extends Component {
    constructor(props) {
        super(props)
        this.state={
            data: [],
            isCompleteLoadData: false
        }
    }

    async componentDidMount() {
        const res = await masterMarriageStatus()
        await this.setState({
            isCompleteLoadData: true,
            data: res
        })
    }

    render() {
        return(
            this.state.isCompleteLoadData && <PickerItemValidation
                title={'Status'}
                titleEmptyData={'Pilih Status'}
                titleForm={true}
                paramSelected={'marriageId'}
                paramView={'marriageName'}
                data={this.state.data}
                selected={this.props.selected}
                onChangeItem={(value) => this.props.onChangeItem(value)}
            />
        )
    }
}

export default MarriageOptions