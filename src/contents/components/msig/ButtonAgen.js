import React from 'react'
import { Button, ThemeProvider } from 'react-native-elements'
import Theme from 'style/Theme'
import ThemeSecondary from 'style/ThemeSecondary'
import { connect } from 'react-redux'

class ButtonAgen extends React.Component {
    constructor(props) {
        super(props)
    }

    renderButton() {
        const { loadingButtonAgen } = this.props

        if(loadingButtonAgen === true) {
            return(
                <Button
                    title={'Proses...'}
                    loading={true}
                    type={'solid'}
                    disabled={true}>
                </Button>
            )
        }

        return (
            <Button
                title={'Buat SPAJ Downline'}
                type={'solid'}
                onPress={() => this.props.onPress()}
            >
            </Button>
        )
    }

    render() {
        return (
            <ThemeProvider theme={ThemeSecondary}>
                {this.renderButton()}
            </ThemeProvider>
        )
    }
}



const mtp = ({ Msig }) => {
    const { loadingButtonAgen } = Msig
    return { loadingButtonAgen }
}

export default connect(mtp, { })(ButtonAgen)