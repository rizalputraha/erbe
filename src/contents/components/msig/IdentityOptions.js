import React, { Component } from 'react'
import { PickerItemValidation } from 'component/v3'
import { masterIdentity } from 'erbecache/model/MsigModel'
import { connect } from 'react-redux'
import { setStateSpajMsig } from 'erberedux/actions'

class IdentityOptions extends Component {
    constructor(props) {
        super(props)
        this.state={
            data: [],
            selected: undefined,
            isCompleteLoadData: false
        }
    }

    async componentDidMount() {
        const res = await masterIdentity()
        const {spaj, setStateSpajMsig} = this.props
        const { espaj } = spaj
        const { pemegangPolis, tertanggung } = espaj
        const identityDefault = 'KTP/EKTP'
        
        if(this.props.keyState === 'pemegangPolis') {
            const identity = res.filter(item => item.identityName == identityDefault)
            setStateSpajMsig('pemegangPolis', 'buktiIdentitasPp', identity[0].identityId)
            await this.setState({ isCompleteLoadData: true, data: identity, selected: identity[0].identityId})
            
        } else {
            if(tertanggung.usiaTt < 17) {
                const identity = res.filter(item => item.identityName !== identityDefault)
                setStateSpajMsig('tertanggung', 'buktiIdentitasTt', identity[0].identityId)
                await this.setState({ isCompleteLoadData: true, data: identity, selected: identity[0].identityId})
            } else {
                const identity = res.filter(item => item.identityName == identityDefault)
                setStateSpajMsig('tertanggung', 'buktiIdentitasTt', identity[0].identityId)
                await this.setState({ isCompleteLoadData: true, data: identity, selected: identity[0].identityId})
            }
        }
    }

    render() {
        const { spaj, setStateSpajMsig } = this.props
        return(
            this.state.isCompleteLoadData && <PickerItemValidation
                disabled={true}
                title={'Bukti Identitas'}
                titleEmptyData={'Pilih Bukti Identitas'}
                titleForm={true}
                paramSelected={'identityId'}
                paramView={'identityName'}
                data={this.state.data}
                selected={this.state.selected}
                onChangeItem={(value) => setStateSpajMsig(value)}
            />
        )
    }
}

const mtp = ({ Msig }) => {
    const { spaj } = Msig
    return { spaj }
}

export default connect(mtp, {setStateSpajMsig})(IdentityOptions)