import React, { Component } from 'react'
import { PickerItemValidation } from 'component/v3'
import { masterPekerjaan } from 'erbecache/model/MsigModel'

class PekerjaanOptions extends Component {
    constructor(props) {
        super(props)
        this.state={
            data: [],
            isCompleteLoadData: false
        }
    }

    async componentDidMount() {
        const res = await masterPekerjaan()
        await this.setState({
            isCompleteLoadData: true,
            data: res
        })
    }

    render() {
        return(
            this.state.isCompleteLoadData && <PickerItemValidation
                title={'Pekerjaan'}
                titleEmptyData={'Pilih Pekerjaan'}
                titleForm={true}
                paramSelected={'pekerjaanName'}
                paramView={'pekerjaanName'}
                data={this.state.data}
                selected={this.props.selected}
                onChangeItem={(value) => this.props.onChangeItem(value)}
            />
        )
    }
}

export default PekerjaanOptions

