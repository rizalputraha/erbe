import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { SearchBar } from 'react-native-elements';
import Color from 'style/Color'

export default class SearchElement extends Component {
   render() {
      return (
         <View>
            <SearchBar 
                placeholder={this.props.placeholder}
                onChangeText={(e) => this.props.changeTextHandler(e)}
                value={this.props.value}
                containerStyle={{backgroundColor: Color.primary,borderTopWidth:0}}
                inputContainerStyle={{backgroundColor: Color.white}}
                {...this.props}
                />
         </View>
      )
   }
}
