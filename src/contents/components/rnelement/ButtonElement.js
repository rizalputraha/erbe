import React from 'react'
import { Button, ThemeProvider } from 'react-native-elements'
import Theme from 'style/Theme'
import ThemeSecondary from 'style/ThemeSecondary'
import { connect } from 'react-redux'
class ButtonElement extends React.Component {

    constructor(props) {
        super(props)
        this.state = {}
    }

    renderButton() {
        if (this.props.showButtonLoading === true) {
            return (
                <Button
                    title={'Proses...'}
                    loading={true}
                    type={this.props.type}
                    disabled={true}>
                </Button>
            )
        }

        return (
            <Button
                title={this.props.title}
                type={this.props.type}
                onPress={() => this.props.onPress()}
                disabled={this.props.disabled}
                {...this.props.icon}
                {...this.props.iconRight}
            >
            </Button>
        )
    }

    render() {
        return (
            <ThemeProvider theme={this.props.theme === 'primary' ? Theme : ThemeSecondary}>
                {this.renderButton()}
            </ThemeProvider>
        )
    }
}

const mtp = ({ ErrorHandling }) => {
    const { showButtonLoading } = ErrorHandling
    return { showButtonLoading }
}

export default connect(mtp, {})(ButtonElement)