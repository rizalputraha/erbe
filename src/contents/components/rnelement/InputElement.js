import React from 'react'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Input } from 'react-native-elements'
import Color from 'style/Color'
import MainStyle from 'style/MainStyle'

class InputElement extends React.Component {

    constructor(props) {
        super(props)
        this.state={
            isFocus: false,
            ...this.props
        }
    }

    renderIcon() {
        if(this.props.icon==='yes') {
            return (
                <Icon
                    name={this.props.iconName}
                    size={MainStyle.icon}
                    color={Color.primary}
                />
            )
        }
        return null;
    }

    handleBlur(event) {
        this.setState({ isFocus: false})
        if(this.props.onBlur) {
            this.props.onBlur(event)
        }
    };

    handleFocus(event) {
        this.setState({ isFocus: true})
        if(this.props.onFocus) {
            this.props.onFocus(event)
        }
    };

    setInputRef = ref => {
        const { getRef } = this.props;
        if (getRef) {
          getRef(ref);
        }
    };

    render() {
        return(
            <Input
                placeholder={ this.props.placeholder }
                leftIcon={ this.renderIcon() }
                rightIcon={
                    this.props.rightIcon ?
                        <Icon
                            name={this.props.rightIcon}
                            size={MainStyle.bigIcon}
                            color={this.props.rightIconState === true ? Color.grey : Color.darkLight }
                            onPress={ () => this.props.rightIconAction({...this.state}) }
                        />
                    :
                    null
                }
                label={ this.props.label }
                onBlur={ (event) => this.handleBlur(event) }
                onFocus={ (event) => this.handleFocus(event) }
                inputContainerStyle = {
                    [
                        {
                            borderBottomWidth : this.state.isFocus?2:1,
                            borderBottomColor: this.state.isFocus?Color.primary:Color.grey,
                            margin: MainStyle.marginExtraSmall
                        }
                    ]
                }
                labelStyle = {
                    [
                        {
                            color: Color.grey,
                            marginLeft: 0,
                            marginTop: 10,
                            fontWeight: 'normal',
                            fontSize: MainStyle.paragraph
                        }
                    ]
                }
                keyboardType={this.props.keyboardType!==undefined?this.props.keyboardType:'default'}
                blurOnSubmit={this.props.blurOnSubmit!==undefined?this.props.blurOnSubmit:false}
                returnKeyType={this.props.returnKeyType!==undefined?this.props.returnKeyType:'done'}
                autoCapitalize={this.props.autoCapitalize!==undefined?this.props.autoCapitalize:'sentences'}
                secureTextEntry={this.props.secureTextEntry!==undefined?this.props.secureTextEntry:false}
                onSubmitEditing={() => 
                    {
                        this.props.onSubmitEditing ? 
                        this.props.onSubmitEditing()
                        : 
                        this.props.nextInput && this.props.nextInput.focus()
                    }
                }
                ref={this.setInputRef}
                onChangeText={ (text) => {
                    this.props.onChangeText(text)
                    this.setState({ value: text})
                } }
                {...this.props}
            />
        )
    }
}

export default InputElement