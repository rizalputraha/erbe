import InvoiceCategory from './InvoiceCategory'
import InvoicePembayaran from './InvoicePembayaran'
import InvoiceStatusPembayaran from './InvoiceStatusPembayaran'
import InvoiceSpajSelect from './jiwa/InvoiceSpajSelect'
import InvoiceStatus from './jiwa/InvoiceStatus'
import InvoicePdf from './jiwa/InvoicePdf'
import InvoicePay from './jiwa/InvoicePay'

module.exports={
    InvoiceCategory,
    InvoicePembayaran,
    InvoiceStatusPembayaran,
    InvoiceSpajSelect,
    InvoiceStatus,
    InvoicePdf,
    InvoicePay
}