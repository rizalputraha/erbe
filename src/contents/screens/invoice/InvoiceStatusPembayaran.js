import React, { Component } from 'react'
import { Text, View } from 'react-native'

import {ErbeListMenu} from 'component/v3'

const menu = [
   {
       id: "1",
       title: "Asuransi Jiwa",
       desc: "Status pembayaran asuransi jiwa",
       icon: "report-daftar-asuransi",
       screen: "InvoiceStatus"
   },
]

export default class InvoiceStatusPembayaran extends Component {
   render() {
      return (
         <ErbeListMenu 
            tipe="menu"
            data={menu}
         />
      )
   }
}
