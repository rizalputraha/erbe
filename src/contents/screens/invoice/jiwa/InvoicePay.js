import React, { Component } from 'react'
import { Text, View, ActivityIndicator } from 'react-native'
import { Badge } from "react-native-elements"

import Color from "style/Color"
import Styles from 'stylescreen/InvoiceSpaj'
import { APP_ID,PAYMENT_URL } from "erberedux/constants";
import { WebView } from "react-native-webview";
import { 
   CustomTabs,
   ANIMATIONS_SLIDE,
   ANIMATIONS_FADE 
} from "react-native-custom-tabs";

export default class InvoicePay extends Component {
   constructor(props){
      super(props);
      this.state={
         data: false,
         loaded: false,
      }
   }

   async componentDidMount(){
      console.log("params state: ", this.props.navigation.state.params);
      await this.setState({data: this.props.navigation.state.params.data});
   }

   async _onNavigationStateChange(param, kode_tagihan){
      if(__DEV__){
        console.log('param nav', param);
        console.log('kode_tagihan', kode_tagihan);
      }
      try {
        if(param.url.indexOf('merchants/create_payment_plan/espay')) {
          this.openWebPage(param.url);
        }
        if(param.url.indexOf('merchants?tagihan='+kode_tagihan) == 24) {
          setTimeout(()=> {
            Alert.alert(
              'Informasi',
              'Silahkan cek status pembayaran',
              [
                {text: 'OK', onPress: () => this._goToInvoice()},
              ],
              { cancelable: false }
            )
          }, 1000);
        }
        console.log('on navigation state change webview', {param, kode_tagihan});
      } catch (e) {
  
      } finally {
  
      }
    }

    openWebPage(url) {
      const option = {
        toolbarColor: '#607D8B',
        enableUrlBarHiding: true,
        showPageTitle: false,
        enableDefaultShare: false,
        animations: {
          startEnter: 'slide_up',
          startExit: 'android:anim/fade_out',
          endEnter: 'android:anim/fade_in',
          endExit: 'slide_down',
        },
        forceCloseOnRedirection: true,
      };
      CustomTabs.openURL(url, option).then((launched: boolean) => {
        console.log(`Launched custom tabs: ${launched}`);
      }).catch(err => {
        console.error(err)
      });
    }

   renderStepArea(){
      return(
         <View style={Styles.topInfo}>
            <View style={{alignItems:'center'}}>
               <View style={{flexDirection: 'row'}}>
                  <Badge badgeStyle={{backgroundColor: Color.greenSuccess,padding: 10 }} containerStyle={{padding: 5}} value={<Text style={{color: '#FFF', fontWeight: 'bold'}}>1</Text>} />
                  <Text style={{color: '#FFF', fontSize: 16, marginLeft: 5, marginTop: 2, marginRight: 10}}>Select SPAJ</Text>
                  <Badge badgeStyle={{backgroundColor: Color.primaryDark,padding: 10}} containerStyle={{padding: 5}} value={<Text style={{color: '#FFF', fontWeight: 'bold'}}>2</Text>} />
                  <Text style={{color: '#FFF', fontSize: 16, marginLeft: 5, marginTop: 2, marginRight: 10}}>Pay SPAJ</Text>
               </View>
            </View>
         </View>
      )
   }
   
   render() {
      const kode_tagihan = APP_ID+this.state.data.invoice_code+this.state.data.kode_tagihan;
      const payUrl = PAYMENT_URL+'merchants/view/'+kode_tagihan;
      return(
      this.state.data != false ?
         (<View style={{flex: 1}}>
            {this.renderStepArea()}
            {this.state.loaded ? <View></View> : <ActivityIndicator size="small" color={Color.primary} />}
            <WebView
               style={{flex: 1
               }}
               source={{uri: payUrl}}
               onLoadEnd={() => this.setState({loaded: true})}
               automaticallyAdjustContentInsets={false}
               onMessage={(event)=> console.log(event.nativeEvent.data)}
               renderError={() => console.log('this webview render error!!!')}
               onNavigationStateChange={(value) => this._onNavigationStateChange(value, kode_tagihan)}
               mixedContentMode={'always'}
            />
         </View>)
         : <ActivityIndicator size="small" color={Color.primary} />
      )
   }
}
