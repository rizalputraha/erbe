import React, { Component } from 'react'
import { Text, View, Button, ActivityIndicator} from 'react-native'
import WebView from 'react-native-webview'
import Color from '../../../../styles/Color'
import {goBack} from 'erbeservice/AppNavigation'

export default class InvoicePdf extends Component {
   constructor(props){
      super(props);
      this.state = {
         loaded: false
      }
   }

   onClose(){
      goBack();
   }
   
   render() {
      const GoogleUrl = "http://docs.google.com/gview?embedded=true&url=" + this.props.navigation.state.params.data;
      return (
         <View style={{flex: 1}}>
            <WebView 
               style={{flex: 1, marginTop: 20}}
               source={{uri: GoogleUrl}}
               onLoadEnd={() => this.setState({loaded: true})}
            />
            {this.state.loaded ? <Button onPress={() => this.onClose()} title="Close"></Button> : <ActivityIndicator size="small" color={Color.primary} />}
         </View>
      )
   }
}
