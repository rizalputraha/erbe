import React, { Component } from 'react'
import { Text, View, Button, ActivityIndicator } from 'react-native'
import { Badge, Card, ListItem } from 'react-native-elements'
import {ErbeListMenu,ImageAsuransi} from 'component/v3'
import {ButtonElement} from 'component/rnelement'

import { navigate } from 'erbeservice/AppNavigation'
import { connect } from 'react-redux'
import { getTagihan } from 'erberedux/actions'

import Styles from 'stylescreen/InvoiceSpaj'
import Color from '../../../../styles/Color'

class InvoiceStatus extends Component {

   componentDidMount(){
      const {authUser} = this.props;
      this.props.getTagihan(authUser.token);
   }

   formatRupiah(value){
      let NumberFormat = value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
      return 'Rp. '+NumberFormat+',00';
   }

   renderItemSpaj(item){
      return(
         <Card containerStyle={{padding:0}}>
            <View style={item.status !=0 ? Styles.containerPaid : Styles.containerUnpaid }>
               <Text style={{color: Color.white, fontWeight: 'bold',fontSize: 16}}>{item.status !=0 ? 'PAID' : 'UNPAID' }</Text>
            </View>
            <View style={Styles.content}>
               <View style={Styles.imageContainer}>
                  <ImageAsuransi logo={item.logo}/>
               </View>
               <Text>No Spaj <Text style={{color: Color.blueInfo}}>{item.no_spaj}</Text></Text>
               <Text>Tertanggung <Text style={{color: Color.blueInfo}}>{item.tertanggung}</Text></Text>
               <ListItem 
                  title='SPAJ'
                  subtitle='Document'
                  containerStyle={{paddingVertical:10,paddingHorizontal: 0}}
                  rightIcon={{name: 'zoom-out-map', color: '#6a7276', type: 'material'}}
                  rightTitle='PDF'
                  bottomDivider
                  onPress={() => navigate('InvoicePdf',{data: item.spaj_pdf})}
               />
               <ListItem 
                  title='Proposal'
                  subtitle='Document'
                  containerStyle={{paddingVertical:10,paddingHorizontal: 0}}
                  rightIcon={{name: 'zoom-out-map', color: '#6a7276', type: 'material'}}
                  rightTitle='PDF'
                  bottomDivider
                  onPress={() => navigate('InvoicePdf',{data: item.proposal_pdf})}
               />
               <Text style={Styles.txtJumlah}>Jumlah yang harus dibayar</Text>
               <Text style={Styles.txtNominal}>{this.formatRupiah(item.jumlah)}</Text>
            </View>
         </Card>
      )
   }

   render() {      
      return (
         <View style={Styles.container}>
            {
               this.props.showGlobalLoading &&
               <ActivityIndicator size="large" color={Color.primary} /> ||
               (<ErbeListMenu 
                  tipe="list"
                  data={this.props.tagihan.data}
                  renderListItem={(item) => this.renderItemSpaj(item)}
                  keyExtractor={(item,index) => index.toString()}
               />)
            }
             
         </View>
      )
   }
}

const mtp = ({Auth,Invoice,ErrorHandling}) => {
   const {authUser} = Auth;
   const {errorMessage,showGlobalLoading} = ErrorHandling;
   const {tagihan} = Invoice;
   return {tagihan,authUser,errorMessage,showGlobalLoading}
}

export default connect(mtp, {getTagihan})(InvoiceStatus)