import React, { Component } from 'react'
import {ErbeListMenu} from 'component/v3'

const menu = [
   {
       id: "1",
       title: "Asuransi Jiwa",
       desc: "Bayar tagihan asuransi jiwa",
       icon: "asuransi-jiwa",
       screen: "InvoiceSpajSelect"
   },
]
export default class InvoicePembayaran extends Component {
   render() {
      return (
         <ErbeListMenu 
            tipe="menu"
            data={menu}
         />
      )
   }
}
