import React from 'react'
import { View,Text,ActivityIndicator } from 'react-native'

import {
    ErbeListMenu
} from 'component/v3';

import { getKomisi,showErrorMessage } from 'erberedux/actions';
import { connect } from 'react-redux'
import Color from 'style/Color'
import Styles from 'stylescreen/ReportKomisi';


class ReportKomisi extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount(){
        const {authUser,getKomisi} = this.props;
        getKomisi(authUser.token);
    }

    formatRupiah(value){
        let NumberFormat = value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
        return 'Rp. '+NumberFormat+',00';
    }

    renderHeadContent(){        
        const {komisi} = this.props;
        return(
            <>
            <Text style={{paddingLeft: 10,paddingTop: 10, fontSize: 20, fontWeight: 'bold'}}>Bonus Komisi</Text>
            <View style={Styles.headContent}>
                <Text style={Styles.headContentTitle}>Your Total Bonus</Text>
                <Text style={Styles.headContentPrice}>{this.formatRupiah(this.props.komisi.total)}</Text>
            </View>
            <Text style={{paddingLeft: 10,paddingTop: 10,}}>Recent Bonus Activity</Text>
            {this.renderList()} 
            </>
        )
    }

    renderHeadNotFound(){
        return (
            <View style={Styles.headContent}>
                <Text style={Styles.headContentTitle}>You Dont Have Bonus Yet</Text>
                <Text style={Styles.headContentPrice}>Rp. 0,-</Text>
            </View>
        )
    }

    renderItemKomisi(item){
        console.log("item Komisi List",item);
        return(
            <View style={Styles.listContainer}>
                <View style={{flexDirection: 'row'}}>
                    <Text>{item.nama_pembayar}</Text>
                    {
                        item.status != 0 ? (<Text style={Styles.spanStatusReceived}>RECEIVED</Text>) : (<Text style={Styles.spanStatusPending}>PENDING</Text>)
                    }
                </View>
                <Text>{item.keterangan}</Text>
                <Text style={{color: '#02B28C'}}>+{this.formatRupiah(item.jumlah)}</Text>
            </View>
        )
    }

    renderList(){
        return (
            <ErbeListMenu
                data={this.props.komisi.data}
                tipe="list"
                renderListItem={(item) => this.renderItemKomisi(item)}
                keyExtractor={(item,index) => index }
            />
        )
    }

    render() {
        const {komisi} = this.props;
        if (komisi != null) {
            if (komisi == 0 ) {
                return this.renderHeadNotFound()
            }
            return this.renderHeadContent()
        }else{
            return (
                <ActivityIndicator size="large" color="#0000ff"/>
            )
        }
    }
}

const mtp = ({Report,Auth,ErrorHandling}) => {
    const { authUser } = Auth
    const { errorMessage } = ErrorHandling
    const { komisi } = Report
    return { authUser,errorMessage,komisi }
}

export default connect(mtp, { getKomisi, showErrorMessage })(ReportKomisi)