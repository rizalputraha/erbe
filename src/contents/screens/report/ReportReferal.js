import React from 'react'
import { Text, View, Button, SafeAreaView, RefreshControl } from 'react-native'

import {
    InputText,
    ButtonElement,
    SearchElement
} from 'component/rnelement'

import {
    ErbeListMenu,
    ListReportDownline,
    ListItemDownline
} from 'component/v3'

import { getDownline, getDownlineByUname, getDownlineById, setDownlineSearchKey, setDownlineActive, showErrorMessage } from 'erberedux/actions';
import { connect } from 'react-redux'
import Color from 'style/Color'
import Styles from 'stylescreen/ReportReferal'

class ReportReferal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        let { authUser, downlineUserId } = this.props;
        this.props.getDownline(authUser.token, downlineUserId);
    }

    onRefresh(authUser, downlineUserId) {
        this.props.getDownline(authUser.token, downlineUserId);
    }

    async onSubmitUname(value) {
        const { authUser, getDownlineByUname } = this.props;
        let data = {
            "data": {
                "username": value
            }
        }
        await getDownlineByUname(authUser.token, data);
    }


    renderItemExpand(item) {
        return (
            <View style={{ padding: 5, margin: 5, borderRadius: 5, elevation: 2, flexDirection: 'row', alignItems: 'center' }}>
                <View style={{ flex: 1, margin: 10 }}>
                    <Text style={{ margin: 5, fontWeight: 'bold', fontSize: 16 }}>{item.username}</Text>
                </View>
                <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: 20 }}>
                    {/* <BadgeLevel level={level} /> */}
                    {/* <Text style={{ fontWeight: 'bold' }}>{this._getLevelName(level)}</Text> */}
                </View>
            </View>
        )
    }

    renderItemDownline(item) {
        const { authUser, setDownlineActive, getDownline } = this.props;
        return (
            <View>
                <ListReportDownline {...item} changeUid={(value) => {
                    setDownlineActive(item),
                        getDownline(authUser.token, item.id)
                }} />
            </View>
        )
    }

    renderSearch() {
        const { setDownlineSearchKey, searchKey } = this.props
        return (
            <SearchElement
                lightTheme
                placeholder="Username"
                showLoading={this.props.showGlobalLoading}
                changeTextHandler={(value) => setDownlineSearchKey(value)}
                value={searchKey}
                containerStyle={{ backgroundColor: Color.primary, borderTopWidth: 0 }}
                onSubmitEditing={event => this.onSubmitUname(event.nativeEvent.text)}
            />
        )
    }

    renderList() {
        const { authUser, downlineUserId, refresh } = this.props;

        return (
            this.props.downline &&
            <ErbeListMenu
                data={this.props.downline}
                tipe="list"
                renderListItem={(item) => this.renderItemDownline(item)}
                refreshControl={
                    <RefreshControl refreshing={this.props.refresh} onRefresh={() => this.onRefresh(authUser, downlineUserId)} />
                }
            />
        )
    }

    render() {
        const { isClick, downlineActive } = this.props;
        return (
            <SafeAreaView style={{ flex: 1 }}>
                {this.renderSearch()}
                {
                    isClick === true ? <ListItemDownline {...downlineActive} /> :
                        this.renderList()
                }
            </SafeAreaView>
        )
    }
}

const mtp = ({ Report, Auth, ErrorHandling }) => {
    const { authUser } = Auth
    const { showGlobalLoading, errorMessage } = ErrorHandling
    const { downline, refresh, searchKey, downlineActive, isClick } = Report
    return { authUser, errorMessage, downline, refresh, searchKey, downlineActive, isClick }
}

export default connect(mtp, { getDownlineByUname, getDownlineById, getDownline, setDownlineSearchKey, showErrorMessage, setDownlineActive })(ReportReferal)