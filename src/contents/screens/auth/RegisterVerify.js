import React from 'react'
import { ScrollView } from 'react-native'
import { BaseScreen } from 'screen/base'
import { connect } from 'react-redux'
import { showErrorMessage, sagaRegisterVerify } from 'erberedux/actions'
import Style from 'style/stylescreen/Register'
import { ErbeText } from 'component/v3'
import { validationRegister } from 'erbevalidation/Register'
import { ButtonElement } from 'component/rnelement'
import { InputComponent } from 'component/v3'

class RegisterVerify extends BaseScreen {
    constructor(props) {
        super(props)
        this.state={
            ...this.props.verifikasi
        }
    }

    doComponentDidMount() {
        __DEV__ && console.log('Props doComponentDidMount RegisterVerify', this.props)
    }

    doVerify() {
        let data = {}
        data.upline = this.props.upline
        data.user = this.props.user
        data.verifikasi = {
            kode: this.state.kode
        }
        this.props.sagaRegisterVerify(data)
        __DEV__ && console.log('Verifikasi State', this.state)
    }

    doRender() {
        return(
            <ScrollView style={Style.container}>
                <ErbeText
                    value={`Kode verifikasi telah dikirim`}
                    style={'h1'}
                />
                <ErbeText
                    value={`Cek kode verifikasi pada hp ${this.props.user.hp} dan email ${this.props.user.email}. Masukkan kode verifikasi`}
                    style={'p'}
                />
                <InputComponent
                    label={'Kode Verifikasi'}
                    value={this.state.kode}
                    onChangeText={ (value) =>  this.setState({kode: value}) }
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={20}
                    returnKeyType={'go'}
                    onSubmitEditing={ () => this.doVerify()}
                    secureTextEntry={true}
                />
                <ButtonElement
                    title='Lanjutkan'
                    type='solid'
                    onPress={ () => this.doVerify()}
                    theme='secondary'
                />
            </ScrollView>
        )
    }

}

const mtp = ({Register}) => {
    const {upline, user, verifikasi} = Register
    return {upline, user, verifikasi};
}

export default connect(mtp, {sagaRegisterVerify, showErrorMessage})(RegisterVerify)