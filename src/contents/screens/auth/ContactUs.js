import React, { Component } from 'react'
import { Text, View, ScrollView } from 'react-native'
import { Icon } from 'react-native-elements'
import Style from 'style/stylescreen/Home'
import {
   TitleHomePage
} from 'component/home'
import { BaseScreen } from 'screen/base'

export default class ContactUs extends BaseScreen {
    render() {
        return(
            <ScrollView style={Style.parentContainer}>
                <TitleHomePage title="Contact Us" />
                <View style={[Style.childContainer, { padding: 10 }]}>
                <Icon name="home" />
                <Text style={[Style.contentText, { marginBottom: 10 }]}>
                    Epicentrum, Epiwalk Lantai 7 Ruang A705, Jl. H.R. Rasuna Said, Jakarta 12940
                </Text>
                <Icon name="customerservice" type='antdesign'/>
                <Text style={Style.contentText}>
                    Tel. 021 8086 8499
                </Text>
                <Icon name="phone" type='antdesign'/>
                <Text style={Style.contentText}>
                    Fax. 021 8086 8498
                </Text>
                <Icon name="cloud" type='antdesign'/>
                <Text style={Style.contentText}>
                    www.erbecorp.com
                </Text>
                </View >
            </ScrollView >
        )
    }
}