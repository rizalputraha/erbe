import React from 'react'
import { View } from 'react-native'
import { ErbeText } from 'component/v3'
import {
    BaseScreen
} from 'screen/base'
import { 
    InputElement, ButtonElement
} from 'component/rnelement'
import Style from 'style/stylescreen/Register'
import { 
    sagaForgotPassword,
    sagaVerifyForgotPassword
} from 'erberedux/actions'
import { connect } from 'react-redux'
import AsyncStorage from '@react-native-community/async-storage' 
import { ASYNC_USER_FORGOT_PASSWORD } from 'erberedux/constants'
import { navigate } from 'erbeservice/AppNavigation'

class ForgotPassword extends BaseScreen {

    constructor(props) {
        super(props)
        this.state = {
            id: ''
        }
    }

    async doComponentDidMount() {
        const cacheForgot = await AsyncStorage.getItem(ASYNC_USER_FORGOT_PASSWORD)

        if(!(!cacheForgot)) {
            let data = JSON.parse(cacheForgot)
            navigate('ForgotPasswordVerify', {...data})
        }
    }

    doForgot() {
        this.props.sagaForgotPassword(this.state.id);
    }

    doRender() {
        return(
            <View style={Style.container}>
                <ErbeText
                    value={'Lupa Password'}
                    style={'h1'}
                />
                <ErbeText
                    value={'Masukkan akun login anda email, username atau no handphone'}
                    style={'p'}
                />
                <InputElement
                    leftIconName='user'
                    placeholder='Please input your login id'
                    onChangeText={ (value) => this.setState({id: value})}
                />
                <ButtonElement
                    title='Forgot Password'
                    type='solid'
                    onPress={ () => this.doForgot() }
                    theme='secondary'
                />
            </View>
        )
    }
}

const mtp = ({ErrorHandling}) => {
    const { errorMessage } = ErrorHandling
    return { errorMessage }
}

export default connect(mtp, {sagaForgotPassword, sagaVerifyForgotPassword})(ForgotPassword)