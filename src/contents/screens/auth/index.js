import Login from './Login'
import LoginVerify from './LoginVerify'
import ForgotPassword from './ForgotPassword'
import ForgotPasswordVerify from './ForgotPasswordVerify'
import WhoWeAre from './WhoWeAre'
import WhyUs from './WhyUs'
import OurJob from './OurJob'
import ContactUs from './ContactUs'
import Register from './Register'
import RegisterUser from './RegisterUser'
import RegisterVerify from './RegisterVerify'

module.exports={
    Login,
    LoginVerify,
    ForgotPassword,
    ForgotPasswordVerify,
    WhoWeAre,
    WhyUs,
    OurJob,
    ContactUs,
    Register,
    RegisterUser,
    RegisterVerify
}