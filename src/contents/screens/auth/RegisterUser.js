import React from 'react'
import { ScrollView } from 'react-native'
import { BaseScreen } from 'screen/base'
import { connect } from 'react-redux'
import { sagaRegisterUser, showErrorMessage, setRegisterUser } from 'erberedux/actions'
import Style from 'style/stylescreen/Register'
import { ErbeText } from 'component/v3'
import { validationRegister } from 'erbevalidation/Register'
import { ButtonElement } from 'component/rnelement'
import { InputComponent } from 'component/v3'
import { ASYNC_REGISTER_REFERAL, ASYNC_REGISTER_UPLINE, ASYNC_REGISTER_USER } from 'erberedux/constants'
import AsyncStorage from '@react-native-community/async-storage'
import { reset, navigate } from 'erbeservice/AppNavigation'

class RegisterUser extends BaseScreen {
    constructor(props) {
        super(props)
        this.state={
            ...this.props.user
        }
    }

    async _changeState(value, parentState) {
        await this.setState({[parentState]: value});
    }

    async doComponentDidMount() {
        const cacheRegister = await AsyncStorage.getItem(ASYNC_REGISTER_USER);

        if(!(!cacheRegister)) {
            __DEV__ && console.log('cache Register', JSON.parse(cacheRegister))
            this.props.setRegisterUser(JSON.parse(cacheRegister))
            navigate('RegisterVerify')
        }
    }

    doRegister() {
        __DEV__ && console.log('data doRegister', this.state)
        const cleanState = validationRegister({...this.state});
        if(cleanState.error !== 0) {
            this.props.showErrorMessage('Registrasi', cleanState.message)
            __DEV__ && console.log('Failed Validation data doRegister', this.state)
        } else {
            let user = {...cleanState.data};
            __DEV__ && console.log('Success Validation data doRegister', user)
            let data = {}
            data.upline = this.props.upline
            data.user = {hp: user.hp, email: user.email, password: user.password}
            this.props.sagaRegisterUser(data)
        }
    }

    async doCancel() {
        __DEV__ && console.log(this.state)
        // Remove cache upline and referal
        try {
            await AsyncStorage.removeItem(ASYNC_REGISTER_REFERAL)
            await AsyncStorage.removeItem(ASYNC_REGISTER_UPLINE)
            await AsyncStorage.removeItem(ASYNC_REGISTER_USER)
        } catch (error) {
            __DEV__ && console.log('Error do Cancel', error)
            reset(0, 'Register', {})
        }
    }

    /**
     * Form rendering payload attribute should be include
     * username, hp, email, nama, alamat, kota, password
     */
    doRender() {
        const { emailRef, passwordRef, confirmPasswordRef } = this.state;
        return(
            <ScrollView style={Style.container}>
                <ErbeText
                    value={`Upline Registrasi Anda ${this.props.upline.upline_nama}`}
                    style={'h1'}
                />
                <ErbeText
                    value={'Lengkapi form data pribadi anda untuk melanjutkan registrasi'}
                    style={'p'}
                />
                <InputComponent
                    label={'Handphone'}
                    value={this.state.hp}
                    onChangeText={ (value) =>  this._changeState(value, 'hp') }
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={16}
                    keyboardType={'numeric'}
                    returnKeyType={'next'}
                    getRef={ref => this.setState({ hpRef: ref })}
                    nextInput={emailRef}
                />
                <InputComponent
                    label={'Email'}
                    value={this.state.email}
                    onChangeText={ (value) =>  this._changeState(value, 'email') }
                    validation={['required', 'length', 'email']}
                    minCharacter={3}
                    maxCharacter={32}
                    autoCapitalize={'none'}
                    returnKeyType={'next'}
                    getRef={ref => this.setState({ emailRef: ref })}
                    nextInput={passwordRef}
                />
                <InputComponent
                    label={'Password'}
                    value={this.state.password}
                    onChangeText={ (value) =>  this._changeState(value, 'password') }
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={20}
                    returnKeyType={'next'}
                    getRef={ref => this.setState({ passwordRef: ref })}
                    nextInput={confirmPasswordRef}
                    secureTextEntry={true}
                />
                <InputComponent
                    label={'Confirm Password'}
                    value={this.state.confirm_password}
                    onChangeText={ (value) =>  this._changeState(value, 'confirm_password') }
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={20}
                    returnKeyType={'go'}
                    getRef={ref => this.setState({ confirmPasswordRef: ref })}
                    onSubmitEditing={ () => this.doRegister()}
                    secureTextEntry={true}
                />
                <ButtonElement
                    title='Lanjutkan'
                    type='solid'
                    onPress={ () => this.doRegister()}
                    theme='secondary'
                />
                <ButtonElement
                    title='Batalkan'
                    type='solid'
                    onPress={ () => this.doCancel()}
                    theme='primary'
                />
            </ScrollView>
        )
    }

}

const mtp = ({Register}) => {
    const {upline, user, verifikasi} = Register
    return {upline, user, verifikasi};
}

export default connect(mtp, {sagaRegisterUser, showErrorMessage, setRegisterUser})(RegisterUser)