import React from 'react'
import { View } from 'react-native'
import {
    BaseScreen
} from 'screen/base'
import { sagaRegisterGetReferal, navigatePage, setRegisterReferal, showErrorMessage} from 'erberedux/actions'
import {
    InputElement,
    ButtonElement
} from 'component/rnelement'
import { ErbeText } from 'component/v3'
import Style from 'style/stylescreen/Register'
import { connect } from 'react-redux'
import { ASYNC_REGISTER_REFERAL, ASYNC_REGISTER_UPLINE } from 'erberedux/constants'
import AsyncStorage from '@react-native-community/async-storage'
import { validationGetReferal } from 'erbevalidation/Register'
import { push } from 'erbeservice/AppNavigation'

class Register extends BaseScreen {

    constructor(props) {
        super(props)
        this.state = {
            ...this.props.navigation.state.params
            // referal: '92mnvaar'
        }
    }

    async doComponentDidMount() {
        // Check cache storage from sagaRegisterGetReferal for first time 
        const cacheReferal = await AsyncStorage.getItem(ASYNC_REGISTER_REFERAL)
        const cacheUpline = await AsyncStorage.getItem(ASYNC_REGISTER_UPLINE)
        __DEV__ && console.log('cacheUpline', cacheUpline)
        if(cacheReferal === null || cacheReferal === false) {
            if(this.state.referal !== undefined) {
                this.props.sagaRegisterGetReferal({...this.state})
            }
        } else {
            await this.setState({ referal: cacheReferal })
            this.props.setRegisterReferal(JSON.parse(cacheUpline))
            push('RegisterUser')
        }
    }

    renderNamaUpline() {
        __DEV__ && console.log('renderNamaUpline', this.props);
        if(this.props.upline.length > 0) {
            return this.props.upline.upline_nama
        }
    }

    doGetReferal() {
        const cleanData = validationGetReferal({...this.state})
        if(cleanData.error !== 0) {
            this.props.showErrorMessage('Registrasi', cleanData.message)
        } else {
            this.props.sagaRegisterGetReferal({...this.state})
            // this.props.navigatePage('RegisterUser', {referal: this.state.referal})
        }
    }

    doRegister() {
        this.props.navigation.navigate('RegisterUser')
        __DEV__ && console.log('Do Register Screen')
    }

    renderUndefinedFormReferal() {
        return(
            <View style={Style.container}>
                <ErbeText
                    value={'Registrasi Member Erbenetwork'}
                    style={'h1'}
                />
                <ErbeText
                    value={'Masukkan kode referal dari upline anda, kemudian tekan tombol Next'}
                    style={'p'}
                />
                <InputElement
                    icon='yes'
                    iconName='user'
                    placeholder='Please input referal code'
                    onChangeText={ (value) => this.setState({referal: value})}
                />
                <ButtonElement
                    title='Next'
                    type='solid'
                    onPress={ () => this.doGetReferal()}
                    theme='secondary'
                />
            </View>
        )
    }

    renderFormReferal() {
        return(
            <View style={Style.container}>
                <ErbeText
                    value={'Registrasi Member Erbenetwork'}
                    style={'h1'}
                />
                <ErbeText
                    value={`Referal anda adalah ${this.props.upline.upline_nama}, tekan next untuk registrasi`}
                    style={'p'}
                />
                <InputElement
                    icon='yes'
                    iconName='user'
                    placeholder={this.props.upline.upline_nama}
                    disabled={true}
                />
                <ButtonElement
                    title='Next'
                    type='solid'
                    onPress={ () => this.doRegister()}
                    theme='secondary'
                />
            </View>
        )
    }

    doRender() {
        let param = this.props.navigation.state.params
        if(param === undefined) {
            return this.renderUndefinedFormReferal()
        }
        return this.renderFormReferal()
    }
}

const mtp = ({ Register }) => {
    __DEV__ && console.log('mtp Register', Register)
    const { upline, user, verifikasi } = Register
    return { upline, user, verifikasi }
}
export default connect(mtp, {sagaRegisterGetReferal, navigatePage, setRegisterReferal, showErrorMessage})(Register)