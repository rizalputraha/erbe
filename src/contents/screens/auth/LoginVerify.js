import React from 'react'
import { View, Text, StatusBar, Linking, ImageBackground, Image, KeyboardAvoidingView, Alert } from 'react-native'
import { Grid, Row, Col } from 'react-native-easy-grid'
import AsyncStorage from '@react-native-community/async-storage'
import {
    BaseScreen
} from 'screen/base'
import Style from 'style/stylescreen/Login'
import { ErbeText } from 'component/v3'
import { 
    InputElement, ButtonElement
} from 'component/rnelement'

import { sagaSigninVerify } from 'erberedux/actions'
import { connect } from 'react-redux'

class LoginVerify extends BaseScreen {
    constructor(props) {
        super(props)
        this.state = {
            ...this.props.navigation.state.params,
            kode_verifikasi: ''
        }
    }

    doVerify() {
        this.props.sagaSigninVerify({...this.state});
        __DEV__ && console.log('Verifikasi', this.state)
    }

    doRender() {
        return(
            <View style={Style.container}>
                <ErbeText
                    value={'Verifikasi Login AKun Erbe'}
                    style={'h1'}
                />
                <ErbeText
                    value={ `Masukkan kode verifikasi untuk akun ${this.state.account_id}`}
                    style={'p'}
                />

                <InputElement
                    placeholder='Please input your verification code'
                    onChangeText={ (value) => this.setState({kode_verifikasi: value})}
                />
                
                <ButtonElement
                    title={'Verify'}
                    type='solid'
                    onPress={ () => this.doVerify() }
                    theme='secondary'
                />
            </View>
        )
    }
}

const mtp = ({Auth}) => {
    const { account, isAccountAvail, authUser } = Auth
    return { account, isAccountAvail, authUser }
}

export default connect(mtp, { sagaSigninVerify })(LoginVerify)