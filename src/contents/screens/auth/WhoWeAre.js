import React, { Component } from 'react'
import { Text, View, ScrollView } from 'react-native'
import Style from 'style/stylescreen/Home'
import {
   TitleHomePage
} from 'component/home'

export default class WhoWeAre extends Component {
   renderContent() {
      return (
         <View style={Style.childContainer}>
            <View style={Style.contentContainer}>
               <Text style={Style.subHeading}>Visi</Text>
               <Text style={Style.contentText}>Menjadi perusahaan pialang terbaik
               dan terpercaya dalam memberi solusi asuransi.
                  </Text>
            </View>
            <View style={Style.contentContainer}>
               <Text style={Style.subHeading}>Misi</Text>
               <Text style={Style.contentText}>Melayani semua lapisan masyarakat dengan semua jenis asuransi terbaik
               Memberikan solusi terbaik sesuai kebutuhan dengan harga terbaik
               Memudahkan proses pelayanan melalui sistem digital
                  </Text>
            </View>
            <View style={Style.contentContainer}>
               <Text style={Style.subHeading}>PT ERBE BROKER INDONESIA</Text>
               <Text style={Style.contentText}>ERBE Broker didirikan oleh sosok Patricia Rolla Bawata, memiliki pengalaman yang sangat luas dalam industri asuransi baik asuransi lokal di Indonesia maupun mancanegara.
               Beliau memulai karirnya di asuransi sebagai Field Underwriter Asuransi Jiwa Join Venture pada tahun 1992, dan meniti kariernya di bagian Sales dan
               Marketing Asuransi sebelum ditunjuk sebagai Direksi perusahaan asuransi asing dari mancanegara, tahun 2001,
               Patricia Rolla Bawata telah memimpin sebagai CEO dan Direktur Utama dari beberapa perusahaan asing, baik Perusahaan dari Amerika, Perancis, Australia dan menjadi pelopor pendiri perusahaan asing maupun lokal di Indonesia.
               </Text>
            </View>
            <View style={Style.contentContainer}>
               <Text style={Style.contentText}>ERBE Broker adalah wujud dari pengalamannya mendirikan perusahaan asuransi dan
               sekarang menjajaki dunia marketing menjadi spesialis dan consultant insurance. Pengalaman dan perjalanan karir beliau di beberapa perusahaan sebelumnya, dan memiliki gelar master/advance dalam bidang Insurance (LLIF) dari Limra Wharton School of the University of Pennsylvania,
               adalah gelar tertinggi dan saat ini hanya dipunyai tidak lebih dari 500 orang di seluruh dunia.
               </Text>
            </View>
            <View style={Style.contentContainer}>
               <Text style={Style.heading}>NILAI-NILAI UTAMA</Text>
            </View>
            <View style={Style.contentContainer}>
               <Text style={Style.subHeading}>TRUST</Text>
               <Text style={Style.contentText}>Setiap personel, semua proses pada ERBE Corporation harus bisa dipercaya dan saling mempercayai.</Text>
            </View>
            <View style={Style.contentContainer}>
               <Text style={Style.subHeading}>INTEGRITY</Text>
               <Text style={Style.contentText}>Setiap personel ERBE Corporation adalah personel yang berintegritas.
                  </Text>
            </View>
            <View style={Style.contentContainer}>
               <Text style={Style.subHeading}>EXCELLENT</Text>
               <Text style={Style.contentText}>Setiap proses dan outcome yang dihasilkan oleh ERBE Corporation bisa dipertanggungjawabkan kualitasnya
                  </Text>
            </View>
         </View>
      )
   }

   render() {
      return (
         <ScrollView style={Style.parentContainer}>
            <TitleHomePage title="Who We are" />
            {this.renderContent()}
         </ScrollView>
      )
   }
}
