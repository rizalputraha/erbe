import React from 'react'
import { View } from 'react-native'
import { ErbeText } from 'component/v3'
import {
    BaseScreen
} from 'screen/base'
import { 
    InputElement, ButtonElement
} from 'component/rnelement'
import Style from 'style/stylescreen/Register'

import { 
    sagaForgotPassword,
    sagaVerifyForgotPassword
} from 'erberedux/actions'

import { connect } from 'react-redux'

class ForgotPasswordVerify extends BaseScreen {

    constructor(props) {
        super(props)
        this.state = {
            id: this.props.navigation.state.params.id,
            kode_verifikasi: '',
            password: false,
            isHideTextEntry: true
        }
    }

    doVerify() {
        this.props.sagaVerifyForgotPassword({...this.state});
    }

    renderForm() {
        if(this.props.isForgotVerify === false) {
            return(
                <InputElement
                    placeholder='Please input your verification code'
                    onChangeText={ (value) => this.setState({kode_verifikasi: value})}
                />
            )
        } else {
            return(
                <InputElement
                    placeholder='Your new password'
                    onChangeText={ (value) => this.setState({password: value})}
                    secureTextEntry={this.state.isHideTextEntry}
                    rightIcon={'eye'}
                    rightIconAction={() => this.setState({ isHideTextEntry: !this.state.isHideTextEntry })}
                    rightIconState={this.state.isHideTextEntry}
                    autoCapitalize='none'
                />
            )
        }
    }

    doRender() {
        return(
            <View style={Style.container}>
                <ErbeText
                    value={ this.props.isForgotVerify === false ? 'Verifikasi Lupa Password' : 'Update Password'}
                    style={'h1'}
                />
                <ErbeText
                    value={ this.props.isForgotVerify === false ? 
                        `Lakukan verifikasi dengan kode yang telah dikirim ke ${this.state.id}`
                        :
                        `Update password akun ${this.state.id}`
                    }
                    style={'p'}
                />
                {this.renderForm()}
                <ButtonElement
                    title={ this.props.isForgotVerify === false ? 'Verify':'Update Password'}
                    type='solid'
                    onPress={ () => this.doVerify() }
                    theme='secondary'
                />
            </View>
        )
    }
}

const mtp = ({Auth}) => {
    console.log(Auth)
    const { isForgotVerify, forgotData } = Auth
    return { isForgotVerify, forgotData }
}

export default connect(mtp, {sagaForgotPassword, sagaVerifyForgotPassword})(ForgotPasswordVerify)