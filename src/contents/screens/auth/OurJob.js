import React, { Component } from 'react'
import { Text, View, ScrollView } from 'react-native'
import Style from 'style/stylescreen/Home'
import {
   TitleHomePage
} from 'component/home'

export default class OurJob extends Component {
    render() {
        return(
            <ScrollView style={Style.parentContainer}>
                <TitleHomePage title="Our Job" />
                <View style={Style.childContainer,{padding: 5}}>
                <Text style={Style.contentText}>
                    Insurance broker mewakili klien sebagai konsultan asuransi Anda, untuk semua yang berurusan dengan Asuransi. 
                    Kami bekerja untuk Anda, dan kami mewakili Anda sebagai Customer kami, bukan mewakil Perusahaan Asuransi. 
                    Tugas kami untuk memastikan keluarga Anda, Aset Anda, Bisnis Anda semuanya terproteksi selayaknya sesuai kebutuhan Anda. ... 
                    Jadi fokus kami adalah terhadap Kebutuhan Customer bukan dari kebutuhan Perusahaan Asuransi.</Text>
                </View>
            </ScrollView >
        )
    }
}