import React from 'react'
import { View, Text, StatusBar, Linking, ImageBackground, Image, KeyboardAvoidingView, Alert } from 'react-native'
import { Grid, Row, Col } from 'react-native-easy-grid'
import AsyncStorage from '@react-native-community/async-storage'
import {
    BaseScreen
} from 'screen/base'
import Style from 'style/stylescreen/Login'
import {
    InputElement, ButtonElement
} from 'component/rnelement'

import { sagaSignIn, sagaAccount, showErrorMessage, setAuthAccount } from 'erberedux/actions'
import { connect } from 'react-redux'

class Login extends BaseScreen {
    constructor(props) {
        super(props)
        this.state = {
            account_login: '',
            password: ''
        }
    }

    doComponentDidMount() {
        __DEV__ && console.log('props doComponentDidMount Login', this.props)
        Linking.getInitialURL().then((url) => {
            __DEV__ && console.log('Linking url', url)
            if (url) {
                this.navAppLink(url)
            }
        }).catch(err => __DEV__ && console.log('An error occurred', err))

        Linking.addEventListener('url', this._handleOpenURL)

        __DEV__ && console.log('componentDidMount class App ', this.props)
    }

    doComponentWillUnmount() {
        Linking.removeEventListener('url', this._handleOpenURL);
    }

    navAppLink(url) {
        const route = url.replace(/.*?:\/\//g, '')
        const id = route.match(/\/([^\/]+)\/?$/)[1]
        const routeName = route.split('/')[0]

        const appLink = { route, id, routeName }
        __DEV__ && console.log('navAppLink', appLink)
        if (routeName === 'join') {
            this.props.navigation.navigate('Register', { referal: id })
        };
    }

    _handleOpenURL(event) {
        __DEV__ && console.log('_handleOpenURL', event.url);
    }

    doAccountSync() {
        if (this.state.account_login == '') {
            this.props.showErrorMessage('Login Form Error', 'Mohon isi account login anda', 'danger', 'red')
        } else {
            this.props.sagaAccount({ account_login: this.state.account_login })
        }
    }

    doLogin() {
        if (this.state.password == '') {
            this.props.showErrorMessage('Login Form Error', 'Mohon password login anda', 'danger', 'red')
        } else {
            this.props.sagaSignIn({
                account_id: this.props.account.account_id,
                login_id: this.props.account.login_id,
                password: this.state.password
            })
        }
    }

    renderPassword() {
        if (this.props.isAccountAvail === true) {
            return (
                <InputElement
                    leftIconName='lock'
                    placeholder='Please input your password'
                    onChangeText={(value) => this.setState({ password: value })}
                    secureTextEntry={true}
                />
            )
        }
    }

    renderButton() {
        if (this.props.isAccountAvail === true) {
            return (
                <ButtonElement
                    title='Login'
                    type='solid'
                    onPress={() => this.doLogin()}
                    theme='secondary'
                />
            )
        }

        return (
            <ButtonElement
                title='Lanjutkan'
                type='solid'
                onPress={() => this.doAccountSync()}
                theme='secondary'
            />
        )
    }
    doRender() {
        return (
            <Grid>
                <Row>
                    <ImageBackground style={Style.container}>
                        <KeyboardAvoidingView behavior='padding' style={Style.parentContainer}>
                            <View style={Style.logo}>
                                <Image source={require('../../../assets/images/logo.png')} style={Style.logoImages} />
                            </View>
                            <InputElement
                                leftIconName='user'
                                placeholder='Please input your email, username, or hp'
                                onChangeText={(value) => {
                                    this.setState({ account_login: value })
                                }}
                            />
                            {this.renderPassword()}
                            {this.renderButton()}
                            <View style={Style.loginBackground}>
                                <Image source={require('../../../assets/images/login_background.png')} style={Style.loginBgImages} />
                            </View>

                        </KeyboardAvoidingView>
                    </ImageBackground>
                </Row>
            </Grid>
        )
    }
}

const mtp = ({ Auth, ErrorHandling }) => {
    const { account, isAccountAvail, authUser } = Auth
    const { errorMessage } = ErrorHandling
    return { account, isAccountAvail, authUser, errorMessage }
}

export default connect(mtp, { sagaAccount, sagaSignIn, showErrorMessage, setAuthAccount })(Login)