import React from 'react'
import { View, Text } from 'react-native'

export default class BaseScreen extends React.Component {
    constructor(props) {
        super(props)
    }

    doComponentWillUnmount() {

    }

    componentWillUnmount() {
        return this.doComponentWillUnmount()
    }

    doComponentDidMount() {
        
    }

    componentDidMount() {
        return this.doComponentDidMount()
    }

    doLoading() {
        return(
            <View>
                <Text>Loading Base Screen</Text>
            </View>
        )
    }

    doRender() {
        return(
            <View></View>
        )
    }

    render() {
        if(this.props.showGlobalLoading) return this.doLoading()
        return this.doRender()
    }
}