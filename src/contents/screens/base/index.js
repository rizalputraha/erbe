import BaseScreen from './BaseScreen'
import LoadingScreen from './LoadingScreen'

module.exports={
    BaseScreen,
    LoadingScreen
}