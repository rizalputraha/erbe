import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'
import Style from 'stylescreen/LoadingScreenStyle'

class LoadingScreen extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const imageLoading = require('../../../assets/images/loadingScreen.gif')
        return(
            <View style={Style.container}>
                <Image style={Style.imageLoading} source={imageLoading}/>
            </View>
        )
    }
}

export default LoadingScreen