import Home from './Home'
import Invoice from './Invoice'
import Profile from './Profile'
import Report from './Report'

module.exports={
    Home,
    Invoice,
    Profile,
    Report
}