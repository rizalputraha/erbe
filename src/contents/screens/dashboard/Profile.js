import React from 'react'
import { View, Text, Image, Alert } from 'react-native'
import { BaseScreen } from 'screen/base'
import { ButtonElement } from 'component/rnelement'
import { ErbeText } from 'component/v3'
import { connect } from 'react-redux'
import { sagaSignOut } from 'erberedux/actions'
import { goBack } from 'erbeservice/AppNavigation'
import { Button, Icon } from 'react-native-elements'
import NextgIcon from '../../components/v3/NextgIcon'
import styles from 'stylescreen/ProfileScreen'

class Profile extends BaseScreen {
    constructor(props) {
        super(props)
        this.state = {}
    }

    logout() {
        const { authUser, sagaSignOut } = this.props;
        Alert.alert(
            'Logout',
            'Anda yakin Ingin Logout ?',
            [
                {
                    text: 'Cancel',
                    style: 'cancel',
                },
                { text: 'OK', onPress: () => sagaSignOut(authUser.token) },
            ],
        );
    }

    doRender() {
        const { authUser } = this.props
        return (
            <View style={styles.container}>
                <View style={styles.containerImage}>
                    <Image source={require('asset/images/placeholder-profile.png')} style={styles.image} />
                    <ErbeText style='h5' value={authUser.user_data.username} />
                </View>
                <View style={styles.containerItem}>
                    <NextgIcon name="profile-3" size={24} style={{ marginHorizontal: 15 }} />
                    <View style={{ flexDirection: 'column' }}>
                        <Text style={{ fontSize: 16 }}>Nama</Text>
                        <Text>{authUser.user_data.nama}</Text>
                    </View>
                </View>
                <View style={styles.containerItem}>
                    <NextgIcon name="mail" size={24} style={{ marginHorizontal: 15 }} />
                    <View style={{ flexDirection: 'column' }}>
                        <Text style={{ fontSize: 16 }}>Email</Text>
                        <Text>{authUser.user_data.email}</Text>
                    </View>
                </View>
                <View style={styles.containerItem}>
                    <NextgIcon name="call-2" size={24} style={{ marginHorizontal: 15 }} />
                    <View style={{ flexDirection: 'column' }}>
                        <Text style={{ fontSize: 16 }}>No Telp</Text>
                        <Text>{authUser.user_data.no_telp}</Text>
                    </View>
                </View>
                <View style={{ marginTop: 20 }}>
                    <Button
                        title='Logout'
                        icon={<Icon type="material-community" name='logout' size={20} color='white' />}
                        iconRight
                        buttonStyle={{ backgroundColor: '#e84242' }}
                        titleStyle={{ marginRight: 10 }}
                        onPress={() => this.logout()}
                    />
                </View>
            </View>
        )
    }
}

const mtp = ({ Auth }) => {
    const { authUser } = Auth
    return { authUser }
}

export default connect(mtp, { sagaSignOut })(Profile)