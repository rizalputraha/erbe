import React from 'react'
import { View, Text, FlatList,TouchableOpacity} from 'react-native'
import Style from 'style/stylescreen/Report';
import { BaseScreen } from 'screen/base'
import { ButtonElement } from 'component/rnelement'
import { navigate } from 'erbeservice/AppNavigation'
import { NextgIcon,ErbeText,ErbeListMenu } from 'component/v3';

const menu = [
    {
        id: "1",
        title: "Report Referal",
        desc: "Data referal yang anda punya",
        icon: "referral",
        screen: "ReportReferal"
    },
    {
        id: "2",
        title: "Report Komisi Bonus",
        desc: "Total bonus pendapatan",
        icon: "bonus-referensi",
        screen: "ReportKomisiBonus"
    }
]

class Report extends BaseScreen {
    constructor(props) {
        super(props)
        this.state = {}
    }

    doRender() {
        return(
            <View>
                <ErbeListMenu 
                    data={menu}
                    tipe="menu"
                />
            </View>
        )
    }
}

export default Report