import React from 'react'
import { View, Text } from 'react-native'
import { BaseScreen } from 'screen/base'
import { ButtonElement } from 'component/rnelement'
import {ErbeListMenu,NextgIcon} from 'component/v3'
import { StackActions } from 'react-navigation'

const pushAction = StackActions.push({
    routeName: 'InvoiceCategory',
    params: {
      myUserId: 9,
    },
});

const menu = [
    {
        id: "1",
        title: "Bayar Tagihan",
        desc: "Bayar tagihan asuransi anda",
        icon: "tagihan-asuransi",
        screen: "InvoicePembayaran"
    },
    {
        id: "2",
        title: "Status Pembayaran",
        desc: "Cek status pembayaran",
        icon: "status-pembayaran",
        screen: "InvoiceStatusPembayaran"
    }
]

class Invoice extends BaseScreen {
    constructor(props) {
        super(props);
    }

    doRender() {
        return(
            <View>
                <ErbeListMenu
                    tipe="menu"
                    data={menu}
                />
            </View>
        )
    }
}

export default Invoice