import React from 'react'
import { ScrollView } from 'react-native'
import { Button } from 'react-native-elements'
import Style from 'stylescreen/Home'
import { ImageSlider } from 'component/v3'
import { DataAgen, MainMenu, ShareReferal } from 'component/home'
import { connect } from 'react-redux'
import { sagaGetSpajReferal, getCacheInsurance, resetCacheStateSimas, resetCacheStateMsig } from 'erberedux/actions'
import AsyncStorage from '@react-native-community/async-storage'
import { STORAGE_SPAJ_MSIG, STORAGE_SPAJ_SIMAS } from 'erberedux/constants'
import { loadCacheMsig } from 'erbecache/model/MsigModel'
import { loadCacheSimas } from 'erbecache/model/SimasModel'

class Home extends React.Component {

    constructor(props) {
        super(props)
        this.state = {}
    }

    async componentDidMount() {
        this.props.sagaGetSpajReferal(this.props.authUser.token)
    }

    async checkStorage() {
        await AsyncStorage.getAllKeys()
            .then(resp => {
                console.log('AsyncStorage', resp)
                resp.map(async (item) => {
                    await AsyncStorage.getItem(item).then((res) => {
                        console.log(`AsyncStorage Item ${item}`, { res: JSON.parse(res) })
                    }).catch(err => {
                        console.log('AsyncStorage Error', err)
                    })
                })
            }).catch(err => {
                console.log('AsyncStorage Error', err)
            })
    }

    async resetStorage() {
        await this.props.resetCacheStateSimas();
    }

    render() {
        return (
            <ScrollView style={Style.container}>
                <ImageSlider />
                <DataAgen />
                <MainMenu />
                <ShareReferal />
                <Button onPress={async () => await this.checkStorage()} title='Check Storage' />
                <Button containerStyle={{ marginTop: 10 }} onPress={async () => await this.resetStorage()} title='Reset Data' />
                <Button containerStyle={{ marginTop: 10 }} onPress={() => this.props.resetCacheStateMsig()} title='Reset Data Msig' />

                <Button
                    containerStyle={{ marginTop: 10 }}
                    onPress={async () => {
                        const data = await loadCacheMsig(STORAGE_SPAJ_MSIG)
                        console.log('Storage SPAJ MSIG', data)
                    }}
                    title='Storage SPAJ MSIG' />
                <Button
                    containerStyle={{ marginTop: 10 }}
                    onPress={async () => {
                        const data = await loadCacheSimas(STORAGE_SPAJ_SIMAS)
                        console.log('Storage SPAJ SIMAS', data)
                    }}
                    title='Storage SPAJ SIMAS' />
            </ScrollView>
        )
    }
}

const mtp = ({ Dashboard, Auth }) => {
    const { slider } = Dashboard
    const { authUser } = Auth
    return { slider, authUser }
}

export default connect(mtp, { sagaGetSpajReferal, getCacheInsurance, resetCacheStateSimas, resetCacheStateMsig })(Home)