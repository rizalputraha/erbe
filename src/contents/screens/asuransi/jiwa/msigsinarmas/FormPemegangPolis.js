import React, { Component } from 'react'
import { ScrollView, View , Image } from 'react-native'
import ViewShot from 'react-native-view-shot'
import FormSpajStyle from 'stylescreen/FormSpajStyle'
import { connect } from 'react-redux'
import { setStateSpajMsig, showErrorMessage, sagaNextScreenMsig, setStateSpajCacheStorageMsig } from 'erberedux/actions'
import { ButtonElement } from 'component/rnelement'
import { ItemComponent, ErbeText, InputComponent, TitleComponent, DateTime } from 'component/v3'
import { masterQuestioner, masterProfileResiko } from 'erbecache/model/MsigModel'
import {
    ReligionOptions,
    MarriageOptions,
    IdentityOptions,
    RelationOptions,
    PekerjaanOptions,
    WilayahOptionsPp,
    NpwpOptionPemegangPolis,
    BankOptions
} from 'component/msig'
import Color from 'style/Color'
class FormPemegangPolis extends Component {
    constructor(props) {
        super(props)
        this.state={
            isCapture: '',
            result: '',
            options: {
                format: "jpg",
                quality: 0.9,
                result: "base64"
            }
        }
    }

    async componentDidMount() {
        const { spaj, setStateSpajCacheStorageMsig} = this.props
        if(spaj.espaj.questioner.length == 0) {
            /**
             * Set questioner and profileResiko state reducer from AsynStorage data
             * Used to list of question and answer to users
             */
            const questionerCache = await masterQuestioner()
            const profileResikoCache = await masterProfileResiko()
            setStateSpajCacheStorageMsig(questionerCache, profileResikoCache)
        }
    }

    _changeState(paramState, data) {
        this.props.setStateSpajMsig('pemegangPolis', paramState, data)
    }

    renderImageCapture(){
        const imgSource = { uri: `data:image/jpg;base64, ${this.state.result}` }
        return(
            <View style={{marginHorizontal: 10, padding: 10, borderWidth: 1, borderColor: Color.primary, backgroundColor: Color.greyLight}}>
                <Image source={imgSource} style={{width: null, height: 400, resizeMode: 'contain'}} />
            </View>
        )
    }

    renderFailedCapture() {
        return(
            <View>
                <ErbeText
                    value={'Maaf anda tidak bisa melanjutkan halaman ini'}
                />
                {this.renderImageCapture()}
                <ButtonElement
                    title="Ulangi Lagi"
                    type="solid"
                    onPress={ () => this.setState({isCapture: '', result: ''})}
                    theme="primary"
                />
            </View>
        )
    }
    
    /**
     * @param {String} uri base64 result of capture screen
     */
    captureViewShoot() {
        const { isTertanggung, showErrorMessage, sagaNextScreenMsig, spaj } = this.props
        const pemegangPolis = spaj.espaj.pemegangPolis
        try {
            this.refs.full.capture()
            .then( async uri => {
                // Test screenshoot
                // await this.setState({result: uri})
                // await this.setState({isCapture: 'success' })

                sagaNextScreenMsig('MsigPemegangPolis', isTertanggung === true ? 'MsigCalonPembayar' : 'MsigTertanggung', pemegangPolis, '', '', uri)
            })
        } catch (error) {
            this.setState({isCapture: 'failed'})
            showErrorMessage(error.message)
        }
    }

    renderButtonSubmit() {
        return(
            <ButtonElement
                title="Next"
                type="solid"
                onPress={ async () => await this.captureViewShoot()}
                theme="secondary"
            />
        )
    }

    renderIdentitas() {
        const {spaj} = this.props
        const pemegangPolis = spaj.espaj.pemegangPolis
        return(
            pemegangPolis &&
            <View>
                <ItemComponent
                    label={'Nama Pemegang Polis'}
                    value={pemegangPolis.namaPp}
                />
                <InputComponent
                    label={'Nama Ibu Kandung'}
                    placeholder={''}
                    value={pemegangPolis.namaIbuPp}
                    onChangeText={ (value) => {
                        this._changeState('namaIbuPp', value)
                    }}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={30}
                />
                <InputComponent
                    label={'Kota Kelahiran'}
                    placeholder={''}
                    value={pemegangPolis.tptLahirPp}
                    onChangeText={ (value) => {
                        this._changeState('tptLahirPp', value)
                    }}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={50}
                />
                <ItemComponent
                    label={'Tanggal Lahir'}
                    value={pemegangPolis.dobPp}
                />
                <ItemComponent
                    label={'Usia'}
                    value={pemegangPolis.usiaPp}
                />
                <ReligionOptions
                    selected={pemegangPolis.agamaPp}
                    onChangeItem={(value) => this._changeState('agamaPp', value)}
                />
                <MarriageOptions
                    selected={pemegangPolis.statusPp}
                    onChangeItem={(value) => this._changeState('statusPp', value)}
                />
                <IdentityOptions keyState={'pemegangPolis'}/>
                <InputComponent
                    label={'No Bukti Identitas'}
                    placeholder={''}
                    value={pemegangPolis.identitasPp}
                    onChangeText={ (value) => {
                        this._changeState('identitasPp', value)
                    }}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={50}
                />
                <DateTime
                    setDate={(value) => this._changeState('tglBerlakuIdentitasPp', value)}
                    getDate={pemegangPolis.tglBerlakuIdentitasPp}
                    label={'Tanggal Berlaku'}
                />
                <RelationOptions />
            </View>
        )
    }

    renderTempatTinggal() {
        const { spaj } = this.props
        const pemegangPolis = spaj.espaj.pemegangPolis
        return(
            pemegangPolis &&
            <View>
                <InputComponent
                    label={'Alamat Rumah (sesuai KTP)'}
                    placeholder={''}
                    value={pemegangPolis.addressPp}
                    onChangeText={ (value) => {
                        this._changeState('addressPp', value),
                        this._changeState('alamatTinggalPp', value)
                    }}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={200}
                />
                <WilayahOptionsPp />
            </View>
        )
    }

    renderLembagaBekerja() {
        const {spaj} = this.props
        const pemegangPolis = spaj.espaj.pemegangPolis
        return(
            pemegangPolis &&
            <View>
                <InputComponent
                    label={'Nama Lembaga Tempat Bekerja'}
                    placeholder={''}
                    value={pemegangPolis.nmPerusahaanPp}
                    onChangeText={ (value) => {
                        this._changeState('nmPerusahaanPp', value),
                        this._changeState('alamatKantorPp', value)
                    }}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={50}
                />
                <PekerjaanOptions
                    selected={pemegangPolis.klasifikasiPekerjaanPp}
                    onChangeItem={(value) => this._changeState('klasifikasiPekerjaanPp', value)}
                />
                <InputComponent
                    label={'Uraian pekerjaan'}
                    placeholder={''}
                    value={pemegangPolis.jobDescPp}
                    onChangeText={ (value) => {
                        this._changeState('jobDescPp', value)
                    }}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={50}
                />
            </View>
        )
    }

    renderContact() {
        const {spaj} = this.props
        const pemegangPolis = spaj.espaj.pemegangPolis
        return(
            pemegangPolis &&
            <View>
                <ItemComponent
                    label={'No Handphone'}
                    value={pemegangPolis.noHpPp}
                />
                <ItemComponent
                    label={'Email Address'}
                    value={pemegangPolis.emailPp}
                />
            </View>
        )
    }

    renderInfoBank() {
        const {spaj} = this.props
        const pemegangPolis = spaj.espaj.pemegangPolis
        return(
            pemegangPolis &&
            <View>
                <NpwpOptionPemegangPolis/>
                <BankOptions/>
            </View>
        )
    }

    renderTitleForm() {
        return(
            <TitleComponent
                value='Data Calon Pemegang Polis'
            />
        )
    }

    render() {
        if(this.state.isCapture === 'failed' || this.state.result !== '') return this.renderFailedCapture()
        return(
            <ScrollView style={FormSpajStyle.container}>
                <ViewShot ref="full" options={this.state.options} style={FormSpajStyle.container}>
                    {this.renderTitleForm()}
                    {this.renderIdentitas()}
                    {this.renderTempatTinggal()}
                    {this.renderLembagaBekerja()}
                    {this.renderContact()}
                    {this.renderInfoBank()}
                    {this.renderButtonSubmit()}
                </ViewShot>
            </ScrollView>
        )
    }
}

const mtp = ({Msig}) => {
    const { isTertanggung, spaj } = Msig
    return { spaj }
}

export default connect(mtp, { setStateSpajMsig, showErrorMessage, sagaNextScreenMsig, setStateSpajCacheStorageMsig })(FormPemegangPolis)