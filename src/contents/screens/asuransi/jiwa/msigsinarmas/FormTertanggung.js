import React, { Component } from 'react'
import { ScrollView, View , Image } from 'react-native'
import ViewShot from 'react-native-view-shot'
import FormSpajStyle from 'stylescreen/FormSpajStyle'
import { connect } from 'react-redux'
import { setStateSpajMsig, showErrorMessage, sagaNextScreenMsig } from 'erberedux/actions'
import { ButtonElement } from 'component/rnelement'
import { ItemComponent, ErbeText, InputComponent, TitleComponent, DateTime } from 'component/v3'
import {
    ReligionOptions,
    MarriageOptions,
    IdentityOptions,
    PekerjaanOptions,
    WilayahOptionsTt,
    NpwpOptionTertanggung,
} from 'component/msig'
import Color from 'style/Color'

export class FormTertanggung extends Component {
    constructor(props) {
        super(props)
        this.state={
            isCapture: '',
            result: '',
            options: {
                format: "jpg",
                quality: 0.9,
                result: "base64"
            }
        }
    }

    async componentDidMount() {
        
    }

    _changeState(paramState, data) {
        this.props.setStateSpajMsig('tertanggung', paramState, data)
    }

    renderImageCapture(){
        const imgSource = { uri: `data:image/jpg;base64, ${this.state.result}` }
        return(
            <View style={{marginHorizontal: 10, padding: 10, borderWidth: 1, borderColor: Color.primary, backgroundColor: Color.greyLight}}>
                <Image source={imgSource} style={{width: null, height: 400, resizeMode: 'contain'}} />
            </View>
        )
    }

    renderFailedCapture() {
        return(
            <View>
                <ErbeText
                    value={'Maaf anda tidak bisa melanjutkan halaman ini'}
                />
                {this.renderImageCapture()}
                <ButtonElement
                    title="Ulangi Lagi"
                    type="solid"
                    onPress={ () => this.setState({isCapture: '', result: ''})}
                    theme="primary"
                />
            </View>
        )
    }
    
    /**
     * @param {String} uri base64 result of capture screen
     */
    captureViewShoot() {
        const { showErrorMessage, sagaNextScreenMsig, spaj } = this.props
        const tertanggung = spaj.espaj.tertanggung
        try {
            this.refs.full.capture()
            .then( async uri => {
                // Test screenshoot
                // await this.setState({result: uri})
                // await this.setState({isCapture: 'success' })

                sagaNextScreenMsig('MsigTertanggung', 'MsigCalonPembayar', tertanggung, '', '', uri)
            })
        } catch (error) {
            this.setState({isCapture: 'failed'})
            showErrorMessage(error.message)
        }
    }

    renderButtonSubmit() {
        return(
            <ButtonElement
                title="Next"
                type="solid"
                onPress={ async () => await this.captureViewShoot()}
                theme="secondary"
            />
        )
    }

    renderTitleForm() {
        return(
            <TitleComponent
                value='Data Calon Tertanggung'
            />
        )
    }

    renderIdentitas() {
        const {spaj} = this.props
        const tertanggung = spaj.espaj.tertanggung
        return(
            tertanggung &&
            <View>
                <ItemComponent
                    label={'Nama Tertanggung'}
                    value={tertanggung.namaTt}
                />
                <InputComponent
                    label={'Nama Ibu Kandung'}
                    placeholder={''}
                    value={tertanggung.namaIbuTt}
                    onChangeText={ (value) => {
                        this._changeState('namaIbuTt', value)
                    }}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={30}
                />
                <InputComponent
                    label={'Kota Kelahiran'}
                    placeholder={''}
                    value={tertanggung.tptLahirTt}
                    onChangeText={ (value) => {
                        this._changeState('tptLahirTt', value)
                    }}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={50}
                />
                <ItemComponent
                    label={'Tanggal Lahir'}
                    value={tertanggung.dobTt}
                />
                <ItemComponent
                    label={'Usia'}
                    value={tertanggung.usiaTt}
                />
                <ReligionOptions
                    selected={tertanggung.agamaTt}
                    onChangeItem={(value) => this._changeState('agamaTt', value)}
                />
                <MarriageOptions
                    selected={tertanggung.statusTt}
                    onChangeItem={(value) => this._changeState('statusTt', value)}
                />
                <IdentityOptions keyState={'tertanggung'}/>
                <InputComponent
                    label={'No Bukti Identitas'}
                    placeholder={''}
                    value={tertanggung.identitasTt}
                    onChangeText={ (value) => {
                        this._changeState('identitasTt', value)
                    }}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={50}
                />
                <DateTime
                    setDate={(value) => this._changeState('tglBerlakuIdentitasTt', value)}
                    getDate={tertanggung.tglBerlakuIdentitasTt}
                    label={'Tanggal Berlaku'}
                />
            </View>
        )
    }

    renderTempatTinggal() {
        const { spaj } = this.props
        const tertanggung = spaj.espaj.tertanggung
        return(
            tertanggung &&
            <View>
                <InputComponent
                    label={'Alamat Rumah (sesuai KTP)'}
                    placeholder={''}
                    value={tertanggung.addressTt}
                    onChangeText={ (value) => {
                        this._changeState('addressTt', value),
                        this._changeState('alamatTinggalTt', value)
                    }}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={200}
                />
                <WilayahOptionsTt />
            </View>
        )
    }

    renderLembagaBekerja() {
        const {spaj} = this.props
        const tertanggung = spaj.espaj.tertanggung
        return(
            (tertanggung && tertanggung.usiaTt > 17) &&
            <View>
                <InputComponent
                    label={'Nama Lembaga Tempat Bekerja'}
                    placeholder={''}
                    value={tertanggung.nmPerusahaanTt}
                    onChangeText={ (value) => {
                        this._changeState('nmPerusahaanTt', value),
                        this._changeState('alamatKantorTt', value)
                    }}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={50}
                />
                <PekerjaanOptions
                    selected={tertanggung.klasifikasiPekerjaanTt}
                    onChangeItem={(value) => this._changeState('klasifikasiPekerjaanTt', value)}
                />
                <InputComponent
                    label={'Uraian pekerjaan'}
                    placeholder={''}
                    value={tertanggung.jobDescTt}
                    onChangeText={ (value) => {
                        this._changeState('jobDescTt', value)
                    }}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={50}
                />
            </View>
        )
    }

    renderContact() {
        const {spaj} = this.props
        const tertanggung = spaj.espaj.tertanggung
        return(
            tertanggung &&
            <View>
                <InputComponent
                    label={'No Handphone'}
                    placeholder={''}
                    value={tertanggung.noHpTt}
                    onChangeText={ (value) => {
                        this._changeState('noHpTt', value)
                    }}
                    validation={['required', 'length']}
                    minCharacter={10}
                    maxCharacter={20}
                    keyboardType={'numeric'}
                />
                <InputComponent
                    label={'Email'}
                    placeholder={''}
                    value={tertanggung.emailTt}
                    onChangeText={ (value) => {
                        this._changeState('emailTt', value)
                    }}
                    validation={['required', 'length', 'email']}
                    minCharacter={3}
                    maxCharacter={15}
                    keyboardType={'email-address'}
                />
            </View>
        )
    }

    renderInfoBank() {
        const {spaj} = this.props
        const tertanggung = spaj.espaj.tertanggung
        return(
            tertanggung &&
            <View>
                <NpwpOptionTertanggung/>
            </View>
        )
    }

    render() {
        if(this.state.isCapture === 'failed' || this.state.result !== '') return this.renderFailedCapture()
        return(
            <ScrollView style={FormSpajStyle.container}>
                <ViewShot ref="full" options={this.state.options} style={FormSpajStyle.container}>
                    {this.renderTitleForm()}
                    {this.renderIdentitas()}
                    {this.renderTempatTinggal()}
                    {this.renderLembagaBekerja()}
                    {this.renderContact()}
                    {this.renderInfoBank()}
                    {this.renderButtonSubmit()}
                </ViewShot>
            </ScrollView>
        )
    }
}

const mtp = ({Msig}) => {
    const { spaj } = Msig
    return { spaj }
}

export default connect(mtp, { setStateSpajMsig, showErrorMessage, sagaNextScreenMsig })(FormTertanggung)
