import React, { Component } from 'react'
import { ScrollView, View , Image } from 'react-native'
import { ListItem } from 'react-native-elements'
import ViewShot from 'react-native-view-shot'
import FormSpajStyle from 'stylescreen/FormSpajStyle'
import { connect } from 'react-redux'
import { setStateSpajMsig, showErrorMessage, sagaNextScreenMsig, setStateSpajUsulanAsuransi } from 'erberedux/actions'
import { ButtonElement } from 'component/rnelement'
import { ItemComponent, ErbeText, TitleComponent } from 'component/v3'
import { formatCurrency } from 'erbevalidation/Helper'
import Color from 'style/Color'

export class FormUsulanAsuransi extends Component {
    constructor(props) {
        super(props)
        this.state={
            isCapture: '',
            result: '',
            options: {
                format: "jpg",
                quality: 0.9,
                result: "base64"
            }
        }
    }

    async componentDidMount() {
        const { product, periodeBayar, proposal, setStateSpajUsulanAsuransi } = this.props
        setStateSpajUsulanAsuransi(product, periodeBayar, proposal)
    }

    renderImageCapture(){
        const imgSource = { uri: `data:image/jpg;base64, ${this.state.result}` }
        return(
            <View style={{marginHorizontal: 10, padding: 10, borderWidth: 1, borderColor: Color.primary, backgroundColor: Color.greyLight}}>
                <Image source={imgSource} style={{width: null, height: 400, resizeMode: 'contain'}} />
            </View>
        )
    }

    renderFailedCapture() {
        return(
            <View>
                <ErbeText
                    value={'Maaf anda tidak bisa melanjutkan halaman ini'}
                />
                {this.renderImageCapture()}
                <ButtonElement
                    title="Ulangi Lagi"
                    type="solid"
                    onPress={ () => this.setState({isCapture: '', result: ''})}
                    theme="primary"
                />
            </View>
        )
    }
    
    /**
     * @param {String} uri base64 result of capture screen
     */
    captureViewShoot() {
        const { showErrorMessage, sagaNextScreenMsig } = this.props
        const state = {}
        try {
            this.refs.full.capture()
            .then( async uri => {
                // Test screenshoot
                // await this.setState({result: uri})
                // await this.setState({isCapture: 'success' })

                sagaNextScreenMsig('MsigUsulanAsuransi', 'MsigPenerimaManfaat', state, '', '', uri)
            })
        } catch (error) {
            this.setState({isCapture: 'failed'})
            showErrorMessage(error.message)
        }
    }

    renderButtonSubmit() {
        return(
            <ButtonElement
                title="Next"
                type="solid"
                onPress={ async () => await this.captureViewShoot()}
                theme="secondary"
            />
        )
    }

    renderInfoProduk() {
        const { product } = this.props
        return(
            <View>
                <ItemComponent
                    label={'Nama Produk'}
                    value={product.product_name}
                />
                <ItemComponent
                    label={'Nama Sub Produk'}
                    value={product.product_sub}
                />
                <ItemComponent
                    label={'Uang Pertanggungan'}
                    value={formatCurrency(product.up)}
                />
                <ItemComponent
                    label={'Premi'}
                    value={formatCurrency(product.premi)}
                />
                <ItemComponent
                    label={'Masa Bayar Premi'}
                    value={`${product.thn_masa_bayar} Tahun`}
                />
            </View>
        )
    }

    renderRider() {
        const { product, spaj, proposal } = this.props
        let data = []
        const rider = spaj.espaj.usulanAsuransi.rider_ua
        rider.map((a, i) => {
            product.rider.map((b, j) => {
                if(a.kode_produk_rider == b.rider_code) data.push(b)
            })
        })
        return (
            data.length > 0 &&
            <View>
                <ListItem
                    containerStyle={{ backgroundColor: Color.lambo }}
                    titleStyle={{ color: Color.white }}
                    title='Rider Investasi'
                    bottomDivider />
                {
                    data.map((item, index) => {
                        return (
                            <ListItem
                                key={index}
                                title={`${item.rider_name} usia ${item.min_tt} s.d ${item.max_tt} tahun`}
                                bottomDivider />
                        )
                    })
                }
            </View>
        )
    }

    renderFund() {
        const { product } = this.props;
        return (
            product.fund &&
            <View>
                <ListItem
                    containerStyle={{ backgroundColor: Color.lambo }}
                    titleStyle={{ color: Color.white }}
                    title='Investasi Asuransi'
                    bottomDivider />
                {
                    product.fund.map((item, index) => {
                        return (
                            <ListItem
                                key={index}
                                title={item.fund_name}
                                bottomDivider />
                        )
                    })
                }
            </View>
        )
    }

    renderTitleForm() {
        return(
            <TitleComponent
                value='Produk Asuransi'
            />
        )
    }

    render() {
        if(this.state.isCapture === 'failed' || this.state.result !== '') return this.renderFailedCapture()
        return(
            <ScrollView style={FormSpajStyle.container}>
                <ViewShot ref="full" options={this.state.options} style={FormSpajStyle.container}>
                    {this.renderTitleForm()}
                    {this.renderInfoProduk()}
                    {this.renderRider()}
                    {this.renderFund()}
                    {this.renderButtonSubmit()}
                </ViewShot>
            </ScrollView>
        )
    }
}

const mtp = ({Msig}) => {
    const { spaj, product, proposal, periodeBayar } = Msig
    return { spaj, product, proposal, periodeBayar }
}

export default connect(mtp, { setStateSpajMsig, showErrorMessage, sagaNextScreenMsig, setStateSpajUsulanAsuransi })(FormUsulanAsuransi)
