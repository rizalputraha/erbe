import React, { Component } from 'react'
import { ScrollView, View , Image } from 'react-native'
import ViewShot from 'react-native-view-shot'
import FormSpajStyle from 'stylescreen/FormSpajStyle'
import { connect } from 'react-redux'
import { setStateSpajProfileResikoMsig, showErrorMessage, sagaNextScreenMsig } from 'erberedux/actions'
import { ButtonElement } from 'component/rnelement'
import { ErbeText, TitleComponent, PickerItemValidation } from 'component/v3'
import Color from 'style/Color'

export class FormProfileResiko extends Component {
    constructor(props) {
        super(props)
        this.state={
            isCapture: '',
            result: '',
            options: {
                format: "jpg",
                quality: 0.9,
                result: "base64"
            }
        }
    }

    async componentDidMount() {
        
    }

    renderImageCapture(){
        const imgSource = { uri: `data:image/jpg;base64, ${this.state.result}` }
        return(
            <View style={{marginHorizontal: 10, padding: 10, borderWidth: 1, borderColor: Color.primary, backgroundColor: Color.greyLight}}>
                <Image source={imgSource} style={{width: null, height: 400, resizeMode: 'contain'}} />
            </View>
        )
    }

    renderFailedCapture() {
        return(
            <View>
                <ErbeText
                    value={'Maaf anda tidak bisa melanjutkan halaman ini'}
                />
                {this.renderImageCapture()}
                <ButtonElement
                    title="Ulangi Lagi"
                    type="solid"
                    onPress={ () => this.setState({isCapture: '', result: ''})}
                    theme="primary"
                />
            </View>
        )
    }
    
    /**
     * @param {String} uri base64 result of capture screen
     */
    captureViewShoot() {
        const { showErrorMessage, sagaNextScreenMsig } = this.props
        const state = {}
        try {
            this.refs.full.capture()
            .then( async uri => {
                // Test screenshoot
                // await this.setState({result: uri})
                // await this.setState({isCapture: 'success' })

                sagaNextScreenMsig('MsigProfileResiko', 'MsigPersetujuanNasabah', state, '', '', uri)
            })
        } catch (error) {
            this.setState({isCapture: 'failed'})
            showErrorMessage(error.message)
        }
    }

    renderButtonSubmit() {
        return(
            <ButtonElement
                title="Next"
                type="solid"
                onPress={ async () => await this.captureViewShoot()}
                theme="secondary"
            />
        )
    }

    renderTitleForm() {
        return(
            <TitleComponent
                value='Data Profil Resiko'
            />
        )
    }

    renderItems(item, index) {
        const { setStateSpajProfileResikoMsig } = this.props
        return(
            <View key={index.toString()}>
                <PickerItemValidation
                    title={item.question}
                    titleEmptyData={''}
                    titleForm={true}
                    paramSelected={'point'}
                    paramView={'label'}
                    data={item.answer}
                    selected={item.answerDefault}
                    onChangeItem={(value) => {
                        setStateSpajProfileResikoMsig(index, value)
                    }}
                />
            </View>
        )
    }

    renderProfileResiko() {
        const { spaj } = this.props
        return(
            spaj &&
            <View>
                {
                    spaj.espaj.profileResiko.map((item, index) => {
                        return this.renderItems(item, index)
                    })
                }
            </View>
        )
    }

    render() {
        if(this.state.isCapture === 'failed' || this.state.result !== '') return this.renderFailedCapture()
        return(
            <ScrollView style={FormSpajStyle.container}>
                <ViewShot ref="full" options={this.state.options} style={FormSpajStyle.container}>
                    {this.renderTitleForm()}
                    {this.renderProfileResiko()}
                    {this.renderButtonSubmit()}
                </ViewShot>
            </ScrollView>
        )
    }
}

const mtp = ({Msig}) => {
    const { spaj } = Msig
    return { spaj }
}

export default connect(mtp, { setStateSpajProfileResikoMsig, showErrorMessage, sagaNextScreenMsig })(FormProfileResiko)
