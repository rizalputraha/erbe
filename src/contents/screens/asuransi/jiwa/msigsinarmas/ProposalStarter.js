import React from 'react'
import { View, Text, ScrollView } from 'react-native'
import { ButtonElement } from "component/rnelement";
import Styles from "stylescreen/ProposalStarter";
import { connect } from 'react-redux'
import { OptionIsTertanggungTrue, OptionIsTertanggungFalse } from 'component/msig'
import { sagaNextScreenMsig } from 'erberedux/actions'

class ProposalStarter extends React.Component {
    constructor(props) {
        super(props)
    }
    
    submitForm() {
        this.props.sagaNextScreenMsig('MsigStarter', 'MsigProposalPp', '', this.props.authUser)
    }

    render() {
        return (
            <ScrollView style={Styles.container}>
                <View style={Styles.containerTitle}>
                    <Text style={Styles.txtTitle}>Apakah data tertanggung sama dengan pemegang polis ?</Text>
                </View>

                <View style={Styles.containerForm}>
                    <OptionIsTertanggungTrue />
                    <OptionIsTertanggungFalse />
                </View>

                <ButtonElement
                    title="Next"
                    type="solid"
                    onPress={() => this.submitForm()}
                    color="secondary"
                />

            </ScrollView>
        )
    }
}

const mtp = ({ Auth, Msig }) => {
    const { authUser } = Auth
    const { proposal } = Msig
    return { authUser, proposal }
}

export default connect(mtp, { sagaNextScreenMsig })(ProposalStarter)