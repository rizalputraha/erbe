import React, { Component } from 'react'
import { ScrollView, View, Alert } from 'react-native'
import { ListItem, CheckBox } from 'react-native-elements'
import { ModalListItemArray, ProductAsuransiJiwa } from 'component/v3'
import { ButtonElement } from 'component/rnelement'
import { formatCurrency } from 'erbevalidation/Helper'
import Color from 'style/Color'
import { setStateRootMsig, sagaNextScreenMsig } from 'erberedux/actions'
import { connect } from 'react-redux'

class ProposalProduk extends Component {

    constructor(props) {
        super(props)
        this.state={}
    }

    submitForm() {
        const { imei, uniqueId, product, periodeBayar, sagaNextScreenMsig, isAgreeProduk } = this.props
        if(Object.entries(product).length === 0) 
            Alert.alert('Error', 'Mohon pilih Produk');
        else if(Object.entries(periodeBayar).length === 0) 
            Alert.alert('Error', 'Mohon pilih Periode Bayar');
        else if (isAgreeProduk === false)
            Alert.alert('Error', 'Mohon check kolom "Saya setuju"');
        else 
        sagaNextScreenMsig('MsigProduk', 'MsigPemegangPolis', '', '', {imei, uniqueId});
    }

    renderButtonSubmit() {
        return (
            <ButtonElement onPress={() => this.submitForm()} title="Next" type="solid" theme="secondary" />
        );
    }

    renderRider() {
        const { product } = this.props
        return (
            product.rider &&
            <View>
                <ListItem
                    containerStyle={{ backgroundColor: Color.lambo }}
                    titleStyle={{ color: Color.white }}
                    title='Rider Investasi'
                    bottomDivider />
                {
                    product.rider.map((item, index) => {
                        return (
                            <ListItem
                                key={index}
                                title={`${item.rider_name} usia ${item.min_tt} tahun s.d ${item.max_tt} tahun`}
                                bottomDivider />
                        )
                    })
                }
            </View>
        )
    }

    renderFund() {
        const { product } = this.props;
        return (
            product.fund &&
            <View>
                <ListItem
                    containerStyle={{ backgroundColor: Color.lambo }}
                    titleStyle={{ color: Color.white }}
                    title='Investasi Asuransi'
                    bottomDivider />
                {
                    product.fund.map((item, index) => {
                        return (
                            <ListItem
                                key={index}
                                title={item.fund_name}
                                bottomDivider />
                        )
                    })
                }
            </View>
        )
    }

    rendeInfoPackage() {
        const { periodeBayar, product, proposal, isAgreeProduk, setStateRootMsig } = this.props;
        let masa_pertanggungan = 100 - proposal.pemegangPolis.usiaPp;
        return (
            (product.periode_bayar && periodeBayar.name) &&
            <View>
                <ListItem
                    containerStyle={{ backgroundColor: Color.lambo }}
                    titleStyle={{ color: Color.white }}
                    title='Package Product Utama'
                    bottomDivider />
                <ListItem
                    title={product.product_name}
                    bottomDivider />
                <ListItem
                    title={'Cara bayar ' + periodeBayar.name}
                    bottomDivider />
                <ListItem
                    title={'Masa Pertanggungan ' + masa_pertanggungan + ' Tahun'}
                    bottomDivider />
                <ListItem
                    title={'Total Premi ' + formatCurrency(product.premi)}
                    bottomDivider />
                <ListItem
                    title={'Premi Pokok ' + formatCurrency(product.premi_pokok)}
                    bottomDivider />
                <ListItem
                    title={'Premi Topup ' + formatCurrency(product.premi_topup)}
                    bottomDivider />
                <ListItem
                    title={'Cuti Premi setelah tahun ke ' + product.thn_cuti_premi + ' Tahun'}
                    bottomDivider />
                <ListItem
                    title={'Masa Pembayaran Premi ' + product.thn_masa_bayar + ' Tahun'}
                    bottomDivider />
                <ListItem
                    title={'Masa Asuransi ' + masa_pertanggungan + ' Tahun'}
                    bottomDivider />
                <ListItem
                    title={'Uang Pertanggungan ' + formatCurrency(product.up)}
                    bottomDivider />

                {this.renderRider()}

                {this.renderFund()}

                <View style={{marginHorizontal: 5, marginTop: 10}}>
                    <CheckBox
                        title='Saya setuju dengan product tersebut diatas.'
                        checked={isAgreeProduk}
                        onPress={() => setStateRootMsig('isAgreeProduk', !isAgreeProduk)}
                        checkedColor={Color.secondary}
                    />
                </View>
            </View>
            ||
            <View></View>
        )
    }

    renderPeriodeBayar() {
        const { product, periodeBayar, setStateRootMsig } = this.props;
        return (
            product.periode_bayar
            &&
            <View>
                <ListItem
                    containerStyle={{ backgroundColor: Color.lambo }}
                    titleStyle={{ color: Color.white }}
                    title='Tentukan Cara Bayar'
                    bottomDivider />
                <ModalListItemArray
                    data={product.periode_bayar}
                    labelItem={'name'}
                    title={'Cara Bayar'}
                    titleOptions={'Pilih Cara Bayar'}
                    onChangeItem={(value) => setStateRootMsig('periodeBayar', { ...value })}
                    defaultSelected={periodeBayar.name}
                />
            </View>
        )
    }

    renderProduct() {
        const { productProposal, product, setStateRootMsig, proposal } = this.props;
        return (
            productProposal &&
            <ProductAsuransiJiwa
                proposal={proposal}
                value={product.product_sub}
                data={productProposal}
                title={'Product'}
                labelItem={'product_sub'}
                titleOptions={'Pilih Product'}
                onChangeItem={(value) => {
                    setStateRootMsig('product', {...value})
                }}
                defaultSelected={false} />
        )
    }

    render() {
        return (
            <ScrollView>
                <ListItem
                    containerStyle={{ backgroundColor: Color.lambo }}
                    titleStyle={{ color: Color.white }}
                    title='Tentukan Pilihan Produk'
                    bottomDivider
                />
                {this.renderProduct()}
                {this.renderPeriodeBayar()}
                {this.rendeInfoPackage()}
                {this.renderButtonSubmit()}
            </ScrollView>
        )
    }
}
const mtp = ({ Insurance, Msig, Device }) => {
    const { productProposal } = Insurance
    const { proposal, product, periodeBayar, isAgreeProduk } = Msig
    const { imei, uniqueId } = Device
    return { productProposal, proposal, product, periodeBayar, imei, uniqueId, isAgreeProduk }
}

export default connect(mtp, {setStateRootMsig, sagaNextScreenMsig})(ProposalProduk)