import React, { Component } from 'react'
import { ScrollView, View , Image } from 'react-native'
import ViewShot from 'react-native-view-shot'
import FormSpajStyle from 'stylescreen/FormSpajStyle'
import { connect } from 'react-redux'
import { showErrorMessage, sagaNextScreenMsig, setFileAttachmentMsig } from 'erberedux/actions'
import { ButtonElement } from 'component/rnelement'
import { ErbeText, TitleComponent, ModalFileUpload } from 'component/v3'
import Color from 'style/Color'
import { writeFile, loadFile, readRootDir} from 'erbecache/model/FileModel'
import { MSIG_FILE_ATTACHMENT_PP, MSIG_FILE_ATTACHMENT_TT, MSIG_FILE_ATTACHMENT_CP } from 'erbecache/constant/MasterFile'


export class FormFileDocument extends Component {
    constructor(props) {
        super(props)
        this.state={
            isCapture: '',
            result: '',
            options: {
                format: "jpg",
                quality: 0.9,
                result: "base64"
            },
            filePp: '',
            fileTt: '',
            fileCp: ''
        }
    }

    async componentDidMount() {
        const { fileAttachment } = this.props

        const imgPp = await loadFile(fileAttachment.pemegangPolis)
        const imgTt = await loadFile(fileAttachment.tertanggung)
        const imgCp = await loadFile(fileAttachment.calonPembayar)

        await this.setState({ 
            filePp: imgPp === false ? '' : imgPp, 
            fileTt: imgTt === false ? '' : imgTt, 
            fileCp: imgCp === false ? '' : imgCp
        })
    }

    renderImageCapture(){
        const imgSource = { uri: `data:image/jpg;base64, ${this.state.result}` }
        return(
            <View style={{marginHorizontal: 10, padding: 10, borderWidth: 1, borderColor: Color.primary, backgroundColor: Color.greyLight}}>
                <Image source={imgSource} style={{width: null, height: 400, resizeMode: 'contain'}} />
            </View>
        )
    }

    renderFailedCapture() {
        return(
            <View>
                <ErbeText
                    value={'Maaf anda tidak bisa melanjutkan halaman ini'}
                />
                {this.renderImageCapture()}
                <ButtonElement
                    title="Ulangi Lagi"
                    type="solid"
                    onPress={ () => this.setState({isCapture: '', result: ''})}
                    theme="primary"
                />
            </View>
        )
    }
    
    /**
     * @param {String} uri base64 result of capture screen
     */
    captureViewShoot() {
        const { showErrorMessage, sagaNextScreenMsig, fileAttachment } = this.props
        try {
            this.refs.full.capture()
            .then( async uri => {
                // Test screenshoot
                // await this.setState({result: uri})
                // await this.setState({isCapture: 'success' })
                sagaNextScreenMsig('MsigFileUpload', 'MsigSubmitForm', fileAttachment, '', '', uri)
            })
        } catch (error) {
            this.setState({isCapture: 'failed'})
            showErrorMessage(error.message)
        }
    }

    renderButtonSubmit() {
        return(
            <ButtonElement
                title="Next"
                type="solid"
                onPress={ async () => await this.captureViewShoot()}
                theme="secondary"
            />
        )
    }

    renderTitleForm() {
        return(
            <TitleComponent
                value='Lampirkan Dokumen Pendukung'
            />
        )
    }

    async saveFile(imgbase64, type=undefined) {
        const { isTertanggung, fileAttachment, setFileAttachmentMsig } = this.props
        if(isTertanggung === true) {
            const md5Pp = await writeFile(fileAttachment.pemegangPolis, MSIG_FILE_ATTACHMENT_PP, imgbase64)
            setFileAttachmentMsig('pemegangPolis', md5Pp)
            const md5Tt = await writeFile(fileAttachment.tertanggung, MSIG_FILE_ATTACHMENT_TT, imgbase64)
            setFileAttachmentMsig('tertanggung', md5Tt)
            const md5Cp = await writeFile(fileAttachment.calonPembayar, MSIG_FILE_ATTACHMENT_CP, imgbase64)
            setFileAttachmentMsig('calonPembayar', md5Cp)
            await this.setState({ filePp: imgbase64, fileTt: imgbase64, fileCp: imgbase64 })
        } else {
            if(type == 'pp') {
                const md5Pp = await writeFile(fileAttachment.pemegangPolis, MSIG_FILE_ATTACHMENT_PP, imgbase64)
                setFileAttachmentMsig('pemegangPolis', md5Pp)
                const md5Cp = await writeFile(fileAttachment.calonPembayar, MSIG_FILE_ATTACHMENT_CP, imgbase64)
                setFileAttachmentMsig('calonPembayar', md5Cp)
                await this.setState({ filePp: imgbase64, fileCp: imgbase64 })
            }
            if(type == 'tt') {
                const md5Tt = await writeFile(fileAttachment.tertanggung, MSIG_FILE_ATTACHMENT_TT, imgbase64)
                setFileAttachmentMsig('tertanggung', md5Tt)
                await this.setState({ fileTt: imgbase64 })
            }
        }
        const list = await readRootDir()
        __DEV__ && console.log('List directory after saveFile', list)
    }

    renderFileUpload() {
        const { isTertanggung, fileAttachment } = this.props
        return(
            isTertanggung &&

            <ModalFileUpload
                title={'Upload KTP'}
                onSelectFiles={(ktp) => this.saveFile(ktp)}
                fileSelected = {this.state.filePp}
            />

            ||

            <View>
                <ModalFileUpload
                    title={'Upload KTP Pemegang Polis'}
                    onSelectFiles={(ktp) => this.saveFile(ktp, 'pp')}
                    fileSelected = {this.state.filePp}
                />

                <ModalFileUpload
                    title={'Upload KTP Tertanggung'}
                    onSelectFiles={(ktp) => this.saveFile(ktp, 'tt')}
                    fileSelected = {this.state.fileTt}
                />
            </View>
        )
    }

    render() {
        if(this.state.isCapture === 'failed' || this.state.result !== '') return this.renderFailedCapture()
        return(
            <ScrollView style={FormSpajStyle.container}>
                <ViewShot ref="full" options={this.state.options} style={FormSpajStyle.container}>
                    {this.renderTitleForm()}
                    {this.renderFileUpload()}
                    {this.renderButtonSubmit()}
                </ViewShot>
            </ScrollView>
        )
    }
}

const mtp = ({Msig}) => {
    const { isTertanggung, fileAttachment } = Msig
    return { isTertanggung, fileAttachment }
}

export default connect(mtp, { showErrorMessage, sagaNextScreenMsig, setFileAttachmentMsig })(FormFileDocument)
