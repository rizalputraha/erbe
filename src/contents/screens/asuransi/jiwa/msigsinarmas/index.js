//Import Screen Here
import ProposalCategory from './ProposalCategory'
import CacheFormSubmit from './CacheFormSubmit'
import ProposalDownline from './ProposalDownline'
import ProposalStarter from './ProposalStarter'
import ProposalPemegangPolis from "./ProposalPemegangPolis"
import ProposalTertanggung from "./ProposalTertanggung"
import ProposalProduk from "./ProposalProduk"
import FormPemegangPolis from './FormPemegangPolis'
import FormTertanggung from './FormTertanggung'
import FormCalonPembayar from './FormCalonPembayar'
import FormUsulanAsuransi from './FormUsulanAsuransi'
import FormPenerimaManfaat from './FormPenerimaManfaat'
import FormDetilAgen from './FormDetilAgen'
import FormKuisioner from './FormKuisioner'
import FormProfileResiko from './FormProfileResiko'
import FormPersetujuanNasabah from './FormPersetujuanNasabah'
import FormSignaturePemegangPolis from './FormSignaturePemegangPolis'
import FormSignatureTertanggung from './FormSignatureTertanggung'
import FormFileDocument from './FormFileDocument'
import FormSubmitSpaj from './FormSubmitSpaj'

module.exports = {
    ProposalCategory,
    CacheFormSubmit,
    ProposalDownline,
    ProposalStarter,
    ProposalPemegangPolis,
    ProposalTertanggung,
    ProposalProduk,
    FormPemegangPolis,
    FormTertanggung,
    FormCalonPembayar,
    FormUsulanAsuransi,
    FormPenerimaManfaat,
    FormDetilAgen,
    FormKuisioner,
    FormProfileResiko,
    FormPersetujuanNasabah,
    FormSignaturePemegangPolis,
    FormSignatureTertanggung,
    FormFileDocument,
    FormSubmitSpaj
}