import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import { Grid, Col } from 'react-native-easy-grid'
import { connect } from 'react-redux'
import { ErbeListMenu, ErbeText } from 'component/v3'
import { ButtonElement } from 'component/rnelement'
import Style from 'stylescreen/CacheFormSubmit'
import { setDateFormat } from 'erbevalidation/Helper'
import { sagaNextScreenMsig } from 'erberedux/actions'

class CacheFormSubmit extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ...this.props.navigation.state.params
        }
    }

    async componentDidMount() {
        __DEV__ && console.log('CacheFormSubmit', this.state)
    }

    setStateMsig(state) {
        //Navigate to MsigStarter and set state on saga
        const { sagaNextScreenMsig } = this.props
        sagaNextScreenMsig('MsigCache', 'MsigStarter', state)
    }

    renderItems(item) {
        return (
            <TouchableOpacity style={Style.containerItem} onPress={() => this.setStateMsig(item)} underlayColor='rgba(239,239,239,1,0.9)'>
                <Grid>
                    <Col size={50}>
                        <Text style={Style.dateTitle}>Create On</Text>
                    </Col>
                    <Col size={50}>
                        <Text style={Style.dateText}>{setDateFormat(item.dateCreate)}</Text>
                    </Col>
                </Grid>
                <Grid>
                    <Col size={50}>
                        <Text style={Style.title}>Pemegang Polis</Text>
                        <Text style={Style.name}>{item.proposal.pemegangPolis.namaPp}</Text>
                    </Col>
                    <Col size={50}>
                        <Text style={Style.title}>Tertanggung</Text>
                        <Text style={Style.name}>{item.proposal.tertanggung.namaTt}</Text>
                    </Col>
                </Grid>
                {
                    item.isPartner === true &&
                    <Grid>
                        <Col size={50}>
                            <Text style={Style.title}>Downline Username</Text>
                            <Text style={Style.name}>{item.partner.downlineUsername}</Text>
                        </Col>
                        <Col size={50}>
                            <Text style={Style.title}>Downline HP</Text>
                            <Text style={Style.name}>{item.partner.downlinePhone}</Text>
                        </Col>
                    </Grid>
                }
            </TouchableOpacity>
        )
    }

    renderHeader() {
        return (
            <View>
                <View style={Style.info}>
                    <Text style={Style.infoText}>{`Sudah ada ${this.state.data.length} data spaj dalam handphone anda.`}</Text>
                    <Text style={Style.infoText}>Anda bisa memilih daftar spaj yang telah di simpan atau membuat baru</Text>
                </View>
                <ErbeText value={'Daftar SPAJ Tersimpan'} style={'h3'} />
            </View>
        )
    }

    render() {
        const { sagaNextScreenMsig } = this.props
        return (
            this.state.data.length > 0 &&
            <View style={Style.container}>
                <ErbeListMenu
                    tipe="list"
                    data={this.state.data}
                    renderListItem={(item) => this.renderItems(item)}
                    keyExtractor={(item, index) => index.toString()}
                    ListHeaderComponent={() => this.renderHeader()}
                />
                <View style={Style.buttonArea}>
                    <ButtonElement
                        title="Buat SPAJ Baru"
                        type="solid"
                        onPress={() => sagaNextScreenMsig('MsigCache', 'MsigStarter')}
                        color="secondary"
                    />
                </View>
            </View>
        )
    }
}

const mtp = ({ Msig }) => {
    return { Msig }
}

export default connect(mtp, { sagaNextScreenMsig })(CacheFormSubmit)