import React, { Component } from 'react'
import { Text, View, ScrollView } from 'react-native'
import { ButtonElement } from "component/rnelement"
import { connect } from 'react-redux'
import { sagaNextScreenMsig, setStateProposalMsig} from 'erberedux/actions'
import { BirthdayPemegangPolis } from 'component/v3'
import { GenderOptions } from 'component/msig'

export class ProposalPemegangPolis extends Component {

    constructor(props) {
        super(props)
        this.state={}
    }

    submitForm() {
        const { isTertanggung, proposal, sagaNextScreenMsig } = this.props
        if(isTertanggung === true) {
            sagaNextScreenMsig('MsigProposalPp', 'MsigProduk', proposal.pemegangPolis)
        } else {
            sagaNextScreenMsig('MsigProposalPp', 'MsigProposalTt', proposal.pemegangPolis)
        }
    }

    _changeState(paramState, data){
        this.props.setStateProposalMsig('pemegangPolis', paramState, data)
    }

    renderNama() {
        const { proposal } = this.props
        return (
            <View style={{ padding: 15 }}>
                <Text style={{ fontWeight: 'bold', marginBottom: 10, color: 'grey' }}>Nama Pemegang Polis</Text>
                <Text>{proposal && proposal.pemegangPolis && proposal.pemegangPolis.namaPp}</Text>
            </View>
        )
    }

    renderBirthdayAndGender() {
        const { proposal } = this.props
        return(
            <View>
                <BirthdayPemegangPolis
                    setDob={(value) => this._changeState('dobPp', value)}
                    getDob={proposal.pemegangPolis.dobPp}
                    setUsia={(value) => this._changeState('usiaPp', value)}
                    getUsia={proposal.pemegangPolis.usiaPp}
                    backNav={() => this.props.navigation.goBack(null)}
                    minYear={18}
                    minDay={1}
                    minWeek={2}
                    maxYear={70}
                    titleButton={'Tanggal Lahir'}
                />
                <GenderOptions
                    selected={proposal.pemegangPolis.genderPp}
                    onChangeItem={(value) => this._changeState('genderPp', value)}
                />
            </View>
        )
    }

    render() {
        return (
            <ScrollView>
                {this.renderNama()}
                {this.renderBirthdayAndGender()}
                <ButtonElement
                    title="Next"
                    type="solid"
                    onPress={() => this.submitForm()}
                    color="secondary"
                />
            </ScrollView>
        )
    }
}

const mtp = ({Msig}) => {
    const {proposal, isTertanggung} = Msig
    return {proposal, isTertanggung}
}
export default connect(mtp, {sagaNextScreenMsig, setStateProposalMsig})(ProposalPemegangPolis)
