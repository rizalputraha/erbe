import React, { Component } from 'react'
import { ScrollView, View , Image, Text, Alert } from 'react-native'
import { CheckBox } from 'react-native-elements'
import ViewShot from 'react-native-view-shot'
import FormSpajStyle from 'stylescreen/FormSpajStyle'
import { connect } from 'react-redux'
import { showErrorMessage, sagaNextScreenMsig, setStateSpajQuestionerAnswerMsig } from 'erberedux/actions'
import { ButtonElement } from 'component/rnelement'
import { ErbeText, TitleComponent, InputComponent } from 'component/v3'
import { Grid, Col } from 'react-native-easy-grid'
import Color from 'style/Color'

export class FormKuisioner extends Component {
    constructor(props) {
        super(props)
        this.state={
            isCapture: '',
            result: '',
            options: {
                format: "jpg",
                quality: 0.9,
                result: "base64"
            }
        }
    }

    async componentDidMount() {

    }

    renderImageCapture(){
        const imgSource = { uri: `data:image/jpg;base64, ${this.state.result}` }
        return(
            <View style={{marginHorizontal: 10, padding: 10, borderWidth: 1, borderColor: Color.primary, backgroundColor: Color.greyLight}}>
                <Image source={imgSource} style={{width: null, height: 400, resizeMode: 'contain'}} />
            </View>
        )
    }

    renderFailedCapture() {
        return(
            <View>
                <ErbeText
                    value={'Maaf anda tidak bisa melanjutkan halaman ini'}
                />
                {this.renderImageCapture()}
                <ButtonElement
                    title="Ulangi Lagi"
                    type="solid"
                    onPress={ () => this.setState({isCapture: '', result: ''})}
                    theme="primary"
                />
            </View>
        )
    }
    
    /**
     * @param {String} uri base64 result of capture screen
     */
    captureViewShoot() {
        const { showErrorMessage, sagaNextScreenMsig, spaj } = this.props
        try {
            this.refs.full.capture()
            .then( async uri => {
                // Test screenshoot
                // await this.setState({result: uri})
                // await this.setState({isCapture: 'success' })

                sagaNextScreenMsig('MsigKuisioner', 'MsigProfileResiko', spaj.espaj.questioner, '', '', uri)
            })
        } catch (error) {
            this.setState({isCapture: 'failed'})
            showErrorMessage(error.message)
        }
    }

    renderButtonSubmit() {
        return(
            <ButtonElement
                title="Next"
                type="solid"
                onPress={ async () => await this.captureViewShoot()}
                theme="secondary"
            />
        )
    }

    renderTitleForm() {
        return(
            <TitleComponent
                value='Data Kuisioner Calon Tertanggung'
            />
        )
    }

    renderItemOptions(item, index) {
        const { setStateSpajQuestionerAnswerMsig } = this.props
        return(
            <Grid>
                <Col>
                    <CheckBox
                        title="Ya"
                        size={16}
                        textStyle={{fontSize: 14}}
                        checkedIcon="dot-circle-o"
                        uncheckedIcon="circle-o"
                        checked={item.answerOption.optionYa}
                        onPress={() => {
                            Alert.alert('Error', 'Aplikasi SPAJ tidak bisa kami proses')
                        }}
                        checkedColor="green"
                        containerStyle={{
                            backgroundColor: "white",
                            borderWidth: 1,
                            borderColor: Color.greyLight
                        }}
                    />
                </Col>
                <Col>
                    <CheckBox
                        title="Tidak"
                        size={16}
                        textStyle={{fontSize: 14}}
                        checkedIcon="dot-circle-o"
                        uncheckedIcon="circle-o"
                        checked={item.answerOption.optionTidak}
                        onPress={() => {
                            setStateSpajQuestionerAnswerMsig(index, item.isOption, 'optionTidak', !item.answerOption.optionTidak)
                        }}
                        checkedColor="green"
                        containerStyle={{
                            backgroundColor: "white",
                            borderWidth: 1,
                            borderColor: Color.greyLight
                        }}
                    />
                </Col>
            </Grid>
        )
    }

    renderItemText(item, index) {
        const { setStateSpajQuestionerAnswerMsig } = this.props
        return(
            <View>
                {
                    item.answerList.map((q, i) => {
                        return(
                            <InputComponent
                                key={i.toString()}
                                label={q.question}
                                placeholder={''}
                                value={q.answerText}
                                onChangeText={ (value) => {
                                    setStateSpajQuestionerAnswerMsig(index, item.isOption, '', '', i, value)
                                }}
                                validation={['required', 'length']}
                                minCharacter={1}
                                maxCharacter={30}
                            />
                        )
                    })
                }
            </View>
        )
    }

    renderItems(item, index) {
        return(
            <View key={index.toString()} style={{marginHorizontal: 15}}>
                <Text style={{marginTop: 15, marginBottom: 15}}>{item.label}</Text>
                {
                    item.isOption ? this.renderItemOptions(item, index) : this.renderItemText(item, index)
                }
            </View>
        )
    }

    renderQuestioner() {
        const { spaj } = this.props
        return(
            spaj &&
            <View>
                {
                    spaj.espaj.questioner.map((item, index) => {
                        return this.renderItems(item, index)
                    })
                }
            </View>
        )
    }

    render() {
        if(this.state.isCapture === 'failed' || this.state.result !== '') return this.renderFailedCapture()
        return(
            <ScrollView style={FormSpajStyle.container}>
                <ViewShot ref="full" options={this.state.options} style={FormSpajStyle.container}>
                    {this.renderTitleForm()}
                    {this.renderQuestioner()}
                    {this.renderButtonSubmit()}
                </ViewShot>
            </ScrollView>
        )
    }
}

const mtp = ({Msig}) => {
    const { spaj } = Msig
    return { spaj }
}

export default connect(mtp, { showErrorMessage, sagaNextScreenMsig, setStateSpajQuestionerAnswerMsig })(FormKuisioner)
