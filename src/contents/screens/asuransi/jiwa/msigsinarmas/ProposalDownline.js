import React, { Component } from 'react'
import { Text, View, ScrollView, TouchableOpacity } from 'react-native'
import { ErbeText, HeaderComponent, ErbeListMenu, BadgeLevel } from 'component/v3'
import { SearchElement } from 'component/rnelement'
import { loadCacheStorage } from 'erbecache/model/MasterModel'
import { STORAGE_DOWNLINE_MEMBER } from 'erbecache/constant/MasterAsuransi'
import { connect } from 'react-redux'
import Style from 'stylescreen/StyleProposalDownline'
import { sagaNextScreenMsig, navigatePop } from 'erberedux/actions'
import { getLevelAgen } from 'erbevalidation/Helper'

class ProposalDownline extends Component {
    constructor(props) {
        super(props)
        this.state = {
            downline: [],
            username: ''
        }
    }

    async componentDidMount() {
        const downline = await loadCacheStorage(STORAGE_DOWNLINE_MEMBER)
        await this.setState({downline})
        __DEV__ && console.log('Downline', this.state.downline)
    }

    async _searchData(value) {
        let searchKey = value.toLowerCase()
        this.setState({ username: searchKey })
        let data = await loadCacheStorage(STORAGE_DOWNLINE_MEMBER);
        if (this.state.username == '') {
            this.setState({
                downline: data
            });
        }
        data = data.filter(l => {
            return l.username.toLowerCase().match(searchKey);
        });
        this.setState({
            downline: data
        });
    }

    selectDownline(item) {
        const { sagaNextScreenMsig } = this.props;
        sagaNextScreenMsig('MsigDownline', 'MsigStarter', item);
    }

    renderItemDownline(item) {
        return (
            <TouchableOpacity onPress={() => this.selectDownline(item)} style={Style.containerItem}>
                <View style={{ flex: 1, margin: 10 }}>
                    <Text style={{ margin: 5, fontWeight: 'bold', fontSize: 16 }}>{item.username}</Text>
                    <Text style={{ margin: 5 }}>Nama: {item.nama}</Text>
                    <Text style={{ margin: 5 }}>Email: {item.email}</Text>
                    <Text style={{ margin: 5 }}>No Hp: {item.no_telp}</Text>
                </View>
                <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: 20 }}>
                    <BadgeLevel level={item.level} />
                    <Text style={{ fontWeight: 'bold' }}>{getLevelAgen(item.level)}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    render(){
        const { navigatePop } = this.props
        return(
            <View style={Style.container}>
                <View style={Style.containerSearch}>
                    <HeaderComponent
                        pushBackButtonHeader={() => navigatePop(1)}
                        title={'Pilih Downline Member'}
                    />
                    <SearchElement
                        lightTheme
                        placeholder="Search by Username"
                        showLoading={this.props.showGlobalLoading}
                        changeTextHandler={(value) => this._searchData(value)}
                        value={this.state.username}
                        containerStyle={Style.containerSearch}
                        onSubmitEditing={event => this._searchData(event.nativeEvent.text)}
                    />
                </View>
                <ErbeListMenu
                    tipe='list'
                    data={this.state.downline}
                    renderListItem={(item) => this.renderItemDownline(item)}
                />
            </View>
        )
    }
}

const mtp = ({Msig}) => {
    return {}
}

export default connect(mtp, { sagaNextScreenMsig, navigatePop })(ProposalDownline)