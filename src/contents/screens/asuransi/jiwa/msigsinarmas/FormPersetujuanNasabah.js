import React, { Component } from 'react'
import { ScrollView, View , Image, Alert } from 'react-native'
import { CheckBox } from 'react-native-elements'
import { WebView } from 'react-native-webview'
import ViewShot from 'react-native-view-shot'
import FormSpajStyle from 'stylescreen/FormSpajStyle'
import { connect } from 'react-redux'
import { setStateRootMsig, showErrorMessage, sagaNextScreenMsig } from 'erberedux/actions'
import { ButtonElement } from 'component/rnelement'
import { ErbeText, TitleComponent } from 'component/v3'
import Color from 'style/Color'

export class FormPersetujuanNasabah extends Component {
    constructor(props) {
        super(props)
        this.state={
            isCapture: '',
            result: '',
            options: {
                format: "jpg",
                quality: 0.9,
                result: "base64"
            }
        }
    }

    async componentDidMount() {
        
    }

    renderImageCapture(){
        const imgSource = { uri: `data:image/jpg;base64, ${this.state.result}` }
        return(
            <View style={{marginHorizontal: 10, padding: 10, borderWidth: 1, borderColor: Color.primary, backgroundColor: Color.greyLight}}>
                <Image source={imgSource} style={{width: null, height: 400, resizeMode: 'contain'}} />
            </View>
        )
    }

    renderFailedCapture() {
        return(
            <View>
                <ErbeText
                    value={'Maaf anda tidak bisa melanjutkan halaman ini'}
                />
                {this.renderImageCapture()}
                <ButtonElement
                    title="Ulangi Lagi"
                    type="solid"
                    onPress={ () => this.setState({isCapture: '', result: ''})}
                    theme="primary"
                />
            </View>
        )
    }
    
    /**
     * @param {String} uri base64 result of capture screen
     */
    captureViewShoot() {
        const { showErrorMessage, sagaNextScreenMsig, isAgreePernyataan } = this.props
        const state = {}
        if(isAgreePernyataan === false) {
            Alert.alert('Error', 'Mohon check kolom "Saya setuju"');
        } else {
            try {
                this.refs.full.capture()
                .then( async uri => {
                    // Test screenshoot
                    // await this.setState({result: uri})
                    // await this.setState({isCapture: 'success' })
    
                    sagaNextScreenMsig('MsigPersetujuanNasabah', 'MsigSignaturePp', state, '', '', uri)
                })
            } catch (error) {
                this.setState({isCapture: 'failed'})
                showErrorMessage(error.message)
            }
        }
    }

    renderButtonSubmit() {
        return(
            <ButtonElement
                title="Next"
                type="solid"
                onPress={ async () => await this.captureViewShoot()}
                theme="secondary"
            />
        )
    }

    renderTitleForm() {
        return(
            <TitleComponent
                value='Pernyataan Dan Persetujuan Nasabah'
            />
        )
    }

    renderPernyataan() {
        const data = `
        <ul style="margin: 30px; list-style-type: decimal">
            <li style="font-size: 36px; text-align: justify; margin-left: -5px; padding-bottom: 30px; border-bottom: 1px solid #d3d3d3;">
                Menyatakan bahwa semua pernyataan, keterangan dan jawaban pertanyaan di dalam Surat Permintaan Asuransi Jiwa ini,
                demikian juga yang akan saya berikan selanjutnya dalam rangka permintaan asuransi ini
                adalah yang sebenarnya dan semata-mata saya berikan dengan
                kesadaran penuh dan tanpa paksaan dari pihak manapun. Saya
                memahami bahwa semua pernyataan dan keterangan tersebut
                merupakan dasar asuransi yang tidak terpisahkan dari Surat Permintaan
                Asuransi Jiwa dan Polis yang diminta.
            </li>
            <li style="font-size: 36px; text-align: justify; margin-left: -5px; padding-bottom: 30px; border-bottom: 1px solid #d3d3d3; padding-top: 30px;">
                Mengerti bahwa PT. Asuransi Jiwa Sinarmas MSIG dalam menerima
                pertanggungan yang diminta ini berdasarkan semua pernyataan dan
                keterangan yang saya berikan, termasuk data pendukungnya (jika ada).
                Oleh karena itu saya mengerti dan menyetujui bahwa apabila
                dikemudian hari diketahui bahwa pernyataan/ keterangan/ data yang
                saya berikan ini keliru atau tidak benar atau yang saya ketahui tetapi
                tidak saya beritahukan kepada PT. Asuransi Jiwa Sinarmas MSIG bersama
                surat permintaan ini, maka mengakibatkan pertanggungan yang
                diadakan berdasarkan permintaan ini menjadi batal (Pasal 251 KUHD)
                dan PT. Asuransi Jiwa Sinarmas MSIG tidak berkewajiban membayar
                klaim yang diajukan atasnya.
            </li>
            <li style="font-size: 36px; text-align: justify; margin-left: -5px; padding-bottom: 30px; border-bottom: 1px solid #d3d3d3; padding-top: 30px;">
                Mengerti bahwa pertanggungan ini mulai berlaku pada tanggal yang
                dinyatakan dalam Polis dan Premi telah dibayar lunas dengan mengacu
                pada ketentuan Polis.
            </li>
            <li style="font-size: 36px; text-align: justify; margin-left: -5px; padding-bottom: 30px; border-bottom: 1px solid #d3d3d3; padding-top: 30px;">
                Memberi persetujuan kepada PT. Asuransi Jiwa Sinarmas MSIG untuk
                mencari keterangan mengenai kesehatan saya (Calon
                Tertanggung/Tertanggung) dari Dokter, Rumah Sakit, Klinik,
                Laboratorium, Puskesmas, Perusahaan Asuransi, Perorangan atau badan
                lainnya, yang mempunyai catatan dan informasi tentang keadaan
                kesehatan atau riwayat pemeriksaan atau perawatan saya baik sebelum/
                setelah penutupan pertanggungan maupun setelah saya meninggal
                dunia.
            </li>
            <li style="font-size: 36px; text-align: justify; margin-left: -5px; padding-bottom: 30px; border-bottom: 1px solid #d3d3d3; padding-top: 30px;">
                Menyatakan setuju dan meminta (menginstruksikan) kepada Dokter,
                Rumah Sakit, Klinik, Laboratorium, Puskesmas, Perusahaan Asuransi,
                Perorangan atau Badan lainnya, yang mempunyai catatan dan informasi
                tentang keadaan kesehatan atau riwayat pemeriksaan atau perawatan
                saya (Calon Tertanggung/Tertanggung) baik sebelum/ setelah penutupan
                pertanggungan ini maupun setelah saya meninggal dunia, untuk
                memperlihatkan dan memberikan catatan dan informasi tersebut kepada
                PT. Asuransi Jiwa Sinarmas MSIG.
            </li>
            <li style="font-size: 36px; text-align: justify; margin-left: -5px; padding-bottom: 30px; border-bottom: 1px solid #d3d3d3; padding-top: 30px;">
                Memberikan persetujuan kepada pihak-pihak yang berwenang untuk
                melakukan otopsi terhadap diri saya (Tertanggung), bila diperlukan,
                dalam hal terdapat ketidakwajaran dalam kematian saya dikemudian
                hari.
            </li>
            <li style="font-size: 36px; text-align: justify; margin-left: -5px; padding-bottom: 30px; border-bottom: 1px solid #d3d3d3; padding-top: 30px;">
                Mengerti dan menyetujui bahwa setiap pernyataan dan atau
                endorsemen dan atau perjanjian yang dibuat oleh PT. Asuransi Jiwa
                Sinarmas MSIG, yang merupakan bagian yang tidak terpisahkan dari Polis
                asuransi yang diminta, hanya akan berlaku apabila telah ditandatangani
                oleh Direksi atau pejabat yang ditunjuk dari PT. Asuransi Jiwa Sinarmas
                MSIG.
            </li>
            <li style="font-size: 36px; text-align: justify; margin-left: -5px; padding-bottom: 30px; border-bottom: 1px solid #d3d3d3; padding-top: 30px;">
                <ul style="margin: 0; margin-top: -43px; list-style-type: lower-alpha">
                    <li style="padding-bottom: 30px">
                        Menyetujui bahwa Harga Unit yang digunakan dalam penutupan Polis
                        ini berdasarkan Harga Unit pada tanggal H + 1, dimana H adalah tanggal
                        Surat Permintaan Asuransi Jiwa diterima dan Premi cair (Good Fund) di
                        rekening yang ditentukan PT. Asuransi Jiwa Sinarmas MSIG di Kantor
                        Pusat Jakarta.
                    </li>
                    <li style="padding-bottom: 30px">
                        Menyatakan telah mendapat penjelasan dan sepenuhnya mengerti hal-hal berikut:
                        <ul style="list-style-type: lower-roman">
                            <li style="padding-top: 30px">
                                Nilai dari masing-masing jenis Dana Investasi yang saya pilih dapat meningkat atau menurun tanpa ada
                                jaminan akan adanya batas minimal dan maksimal
                            </li>
                            <li style="padding-top: 30px">
                                Segala resiko pemilihan jenis Dana Investasi sepenuhnya menjadi tanggung jawab saya
                            </li>
                        </ul>
                    </li>
                    <li>
                        Menyetujui apabila saya membatalkan permintaan Asuransi Jiwa
                        ini atau permintaan Asuransi Jiwa ini dibatalkan oleh PT. Asuransi Jiwa
                        Sinarmas MSIG, maka yang akan dibayarkan adalah premi hasil investasi
                        (keuntungan atau kerugian) dikurangi biaya-biaya yang dibebankan oleh
                        pihak ketiga (bila ada).
                    </li>
                </ul>
            </li>
            <li style="font-size: 36px; text-align: justify; margin-left: -5px; padding-bottom: 30px; border-bottom: 1px solid #d3d3d3; padding-top: 30px;">
                Mengetahui bahwa semua pernyataan yang saya berikan dalam surat
                permintaan ini adalah merupakan hal yang esensi, yang tanpa pernyataan demikian
                PT. Asuransi Jiwa Sinarmas MSIG tidak akan menyetujui penutupan pertanggungan ini.
            </li>
            <li style="font-size: 36px; text-align: justify; margin-left: -5px; padding-bottom: 30px; border-bottom: 1px solid #d3d3d3; padding-top: 30px;">
                Menyatakan telah membaca dengan teliti dan menyetujui persyaratan,
                serta kondisi yang tercantum dalam Polis.    
            </li>
            <li style="font-size: 36px; text-align: justify; margin-left: -5px; padding-bottom: 30px; border-bottom: 1px solid #d3d3d3; padding-top: 30px;">
                Mengerti bahwa penerbitan dan atau penolakan Polis dan atau Klaim
                yang diajukan sepenuhnya merupakan tanggung jawab PT. Asuransi Jiwa
                Sinarmas MSIG. 
            </li>
        </ul>
        `
        return(
            <View style={{flex: 1}}>
                <WebView
                    originWhitelist={['*']}
                    source={{ html: data }}
                    style={{height: 1750}}
                />
            </View>
        )
    }

    renderCheck() {
        const { isAgreePernyataan, setStateRootMsig } = this.props
        return(
            <View style={{marginHorizontal: 5}}>
                <CheckBox
                    title='Saya setuju dengan pernyataan diatas.'
                    checked={isAgreePernyataan}
                    onPress={() => setStateRootMsig('isAgreePernyataan', !isAgreePernyataan)}
                    checkedColor={Color.secondary}
                />
            </View>
        )
    }

    render() {
        if(this.state.isCapture === 'failed' || this.state.result !== '') return this.renderFailedCapture()
        return(
            <ScrollView style={FormSpajStyle.container}>
                <ViewShot ref="full" options={this.state.options} style={FormSpajStyle.container}>
                    {this.renderTitleForm()}
                    {this.renderPernyataan()}
                    {this.renderCheck()}
                    {this.renderButtonSubmit()}
                </ViewShot>
            </ScrollView>
        )
    }
}

const mtp = ({Msig}) => {
    const { isAgreePernyataan } = Msig
    return { isAgreePernyataan }
}

export default connect(mtp, { setStateRootMsig, showErrorMessage, sagaNextScreenMsig })(FormPersetujuanNasabah)
