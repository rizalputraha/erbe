import React, { Component } from 'react'
import { Text, View, ScrollView } from 'react-native'
import { ButtonNasabah, ButtonAgen } from 'component/msig'
import { ErbeText } from 'component/v3'
import styles from 'stylescreen/ProposalCategory';
import { navigateToScreen, pushToScreen, sagaProposalCategoryMsig, hideLoadingBtnNasabahMsig, hideLoadingBtnAgenMsig } from 'erberedux/actions'
import { connect } from 'react-redux'

class ProposalCategory extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { hideLoadingBtnNasabahMsig, hideLoadingBtnAgenMsig } = this.props
        hideLoadingBtnNasabahMsig()
        hideLoadingBtnAgenMsig()
    }

    render() {
        const { sagaProposalCategoryMsig } = this.props
        return (
            <ScrollView style={styles.container}>
                <View style={styles.containerTitle}>
                    <Text style={styles.txtTitle}> Tentukan Pilihan pendaftaran anda </Text>
                </View>
                <ErbeText
                    value={ `Buat SPAJ untuk nasabah`}
                    style={'h3'}
                />
                <ErbeText
                    value={ `Anda dapat membuat SPAJ saat ini, dan share referal pada akun anda dapat digunakan ketika SPAJ anda telah aktif`}
                    style={'p'}
                />
                <ButtonNasabah
                    onPress={ () => sagaProposalCategoryMsig('nasabah')}
                />
                <ErbeText
                    value={ `Buat SPAJ untuk downline anda`}
                    style={'h3'}
                />
                <ErbeText
                    value={ `Apakah anda adalah salah satu agen partner dari ebe ? Jika ya, anda dapat mendaftarkan downline anda saat ini.`}
                    style={'p'}
                />
                <ButtonAgen
                    onPress={ () => sagaProposalCategoryMsig('agen')}
                />
            </ScrollView>
        )
    }
}

const mtp = ({}) => {
    return {}
}
export default connect(mtp, { navigateToScreen, pushToScreen, sagaProposalCategoryMsig, hideLoadingBtnNasabahMsig, hideLoadingBtnAgenMsig })(ProposalCategory)
