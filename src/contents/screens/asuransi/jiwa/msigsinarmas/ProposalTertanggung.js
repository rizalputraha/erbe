import React, { Component } from 'react'
import { View, ScrollView } from 'react-native'
import { ButtonElement } from "component/rnelement"
import { connect } from 'react-redux'
import { sagaNextScreenMsig, setStateProposalMsig, setStateSpajMsig } from 'erberedux/actions'
import { BirthdayTertanggung, InputComponent } from 'component/v3'
import { GenderOptions } from 'component/msig'

export class ProposalTertanggung extends Component {

    constructor(props) {
        super(props)
        this.state={}
    }

    submitForm() {
        const { proposal, sagaNextScreenMsig } = this.props
        sagaNextScreenMsig('MsigProposalTt', 'MsigProduk', proposal.tertanggung)
    }

    _changeState(paramState, data){
        this.props.setStateProposalMsig('tertanggung', paramState, data)
    }

    renderNama() {
        const { proposal, setStateSpajMsig } = this.props
        return (
            <InputComponent
                label={'Nama Tertanggung'}
                placeholder={''}
                value={proposal.tertanggung.namaTt}
                onChangeText={ (value) => {
                    this._changeState('namaTt', value),
                    setStateSpajMsig('tertanggung', 'aliasTt', value)
                }}
                validation={['required', 'length']}
                minCharacter={3}
                maxCharacter={30}
            />
        )
    }

    renderBirthdayAndGender() {
        const { proposal } = this.props
        return(
            <View>
                <BirthdayTertanggung
                    setDob={(value) => this._changeState('dobTt', value)}
                    getDob={proposal.tertanggung.dobTt}
                    setUsia={(value) => this._changeState('usiaTt', value)}
                    getUsia={proposal.tertanggung.usiaTt}
                    backNav={() => this.props.navigation.goBack(null)}
                    minYear={0}
                    maxYear={50}
                    minDay={1}
                    minWeek={2}
                    titleButton={'Tanggal Lahir'}
                />
                <GenderOptions
                    selected={proposal.tertanggung.genderTt}
                    onChangeItem={(value) => this._changeState('genderTt', value)}
                />
            </View>
        )
    }

    render() {
        return (
            <ScrollView>
                {this.renderNama()}
                {this.renderBirthdayAndGender()}
                <ButtonElement
                    title="Next"
                    type="solid"
                    onPress={() => this.submitForm()}
                    color="secondary"
                />
            </ScrollView>
        )
    }
}

const mtp = ({Msig}) => {
    const {proposal, isTertanggung} = Msig
    return {proposal, isTertanggung}
}
export default connect(mtp, {sagaNextScreenMsig, setStateProposalMsig, setStateSpajMsig})(ProposalTertanggung)
