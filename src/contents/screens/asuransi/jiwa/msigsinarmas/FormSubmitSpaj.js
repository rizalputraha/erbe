import React, { Component } from 'react'
import { ScrollView, View , Image, Text} from 'react-native'
import ViewShot from 'react-native-view-shot'
import FormSpajStyle from 'stylescreen/FormSpajStyle'
import { connect } from 'react-redux'
import { 
    sagaSubmitFormMsig,
    sagaSaveSubmitFormMsig,
    setStateRootMsig,
    setStateUriResponseMsig,
    hideButtonLoading,
    hideLoadingUploadDocumentMsig,
    hideLoadingGenerateProposalMsig,
    hideLoadingSubmitSpajMsig
} from 'erberedux/actions'
import { ButtonElement } from 'component/rnelement'
import { ErbeText, TitleComponent, DisconnectView } from 'component/v3'
import { InfoSpajSubmit } from 'component/msig'
import Color from 'style/Color'
import NetInfo, {NetInfoSubscription, NetInfoState} from "@react-native-community/netinfo"
import md5 from 'md5'
import moment from 'moment'
export class FormSubmitSpaj extends React.Component {

    static _subscription = NetInfoSubscription ? null : null;

    constructor(props) {
        super(props)
        this.state={
            isCapture: '',
            result: '',
            options: {
                format: "jpg",
                quality: 0.9,
                result: "base64"
            },
            isConnected: null
        }
    }

    componentDidMount() {
        const { setStateRootMsig, keyStorage, setStateUriResponseMsig, hideLoadingUploadDocumentMsig, hideLoadingGenerateProposalMsig, hideLoadingSubmitSpajMsig } = this.props
        const dateNow = moment().format('YYYYMMDD hhmmss');
        setStateRootMsig('dateCreate', dateNow)
        setStateRootMsig('successFileUpload', false)
        setStateRootMsig('successGenerateProposal', false)
        setStateRootMsig('successSubmitSpaj', false)
        if(keyStorage == '') {
            const keyStorage = md5('msig' + dateNow)
            setStateRootMsig('keyStorage', keyStorage)
        }
        this._subscription = NetInfo.addEventListener(
          this._handleConnectionInfoChange,
        );
        hideButtonLoading()
        hideLoadingUploadDocumentMsig()
        hideLoadingGenerateProposalMsig()
        hideLoadingSubmitSpajMsig()
        // setStateUriResponseMsig('imgFileAttachment', 'pemegangPolis', '')
        // setStateUriResponseMsig('imgFileAttachment', 'tertanggung', '')
        // setStateUriResponseMsig('imgFileAttachment', 'calonPembayar', '')
        // setStateUriResponseMsig('imgSignature', 'pemegangPolis', '')
        // setStateUriResponseMsig('imgSignature', 'tertanggung', '')
        // setStateUriResponseMsig('imgScreenShoot', 'screenPp', '')
        // setStateUriResponseMsig('imgScreenShoot', 'screenTt', '')
        // setStateUriResponseMsig('imgScreenShoot', 'screenCp', '')
        // setStateUriResponseMsig('imgScreenShoot', 'screenUa', '')
        // setStateUriResponseMsig('imgScreenShoot', 'screenDi', '')
        // setStateUriResponseMsig('imgScreenShoot', 'screenDa', '')
        // setStateUriResponseMsig('imgScreenShoot', 'screenQu', '')
        // setStateUriResponseMsig('imgScreenShoot', 'screenPr', '')
        // setStateUriResponseMsig('imgScreenShoot', 'screenPn', '')
        // setStateUriResponseMsig('imgScreenShoot', 'screenSp', '')
        // setStateUriResponseMsig('imgScreenShoot', 'screenSt', '')
        // setStateUriResponseMsig('imgScreenShoot', 'screenFd', '')
        setStateUriResponseMsig('pdfDocument', 'proposal', '')
        setStateUriResponseMsig('pdfDocument', 'spaj', '')
    }

    componentWillUnmount() {
        this._subscription && this._subscription();
    }

    _handleConnectionInfoChange = (state) => {
        console.log('connectionInfo', state)
        this.setState({isConnected: state.isConnected});
    };

    renderButtonSubmit() {
        const connect = this.state.isConnected
        const { sagaSubmitFormMsig, sagaSaveSubmitFormMsig } = this.props
        return(
            <View>
                {
                    connect &&
                    <ButtonElement
                        title={'Submit'}
                        type="solid"
                        onPress={ async () => sagaSubmitFormMsig()}
                        theme={'secondary'}
                    />
                    ||
                    <ButtonElement
                        title={'Save This Data'}
                        type="solid"
                        onPress={ async () => sagaSaveSubmitFormMsig()}
                        theme={'secondary'}
                    />
                }
            </View>
        )
    }

    renderTitleForm() {
        const { isPartner } = this.props
        return(
            <TitleComponent
                value={`Submit Data SPAJ ${isPartner ? 'Downline' : 'Nasabah'}`}
            />
        )
    }

    renderDisconnect() {
        return(
            <View>
                <DisconnectView />
                <InfoSpajSubmit isConnected={this.state.isConnected}/>
            </View>
        )
    }

    renderConnect() {
        return(
            <InfoSpajSubmit isConnected={this.state.isConnected}/>
        )
    }

    render() {
        if(this.state.isCapture === 'failed' || this.state.result !== '') return this.renderFailedCapture()
        return(
            <ScrollView style={FormSpajStyle.container}>
                <ViewShot ref="full" options={this.state.options} style={FormSpajStyle.container}>
                    {this.renderTitleForm()}
                    {this.state.isConnected ? this.renderConnect() : this.renderDisconnect()}
                    {this.renderButtonSubmit()}
                </ViewShot>
            </ScrollView>
        )
    }
}

const mtp = ({Msig}) => {
    const { isPartner, product, proposal, spaj, signature, fileAttachment, keyStorage } = Msig
    return { isPartner, product, proposal, spaj, signature, fileAttachment, keyStorage, Msig }
}

export default connect(mtp, { 
    sagaSubmitFormMsig,
    sagaSaveSubmitFormMsig,
    setStateRootMsig,
    setStateUriResponseMsig,
    hideLoadingUploadDocumentMsig,
    hideLoadingGenerateProposalMsig,
    hideLoadingSubmitSpajMsig
})(FormSubmitSpaj)
