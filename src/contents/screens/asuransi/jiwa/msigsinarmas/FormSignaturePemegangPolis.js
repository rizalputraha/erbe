import React, { Component } from 'react'
import { ScrollView, View , Image, Alert } from 'react-native'
import ViewShot from 'react-native-view-shot'
import FormSpajStyle from 'stylescreen/FormSpajStyle'
import { connect } from 'react-redux'
import { showErrorMessage, sagaNextScreenMsig, setStateSignatureMsig } from 'erberedux/actions'
import { ButtonElement } from 'component/rnelement'
import { ErbeText, TitleComponent, Signature } from 'component/v3'
import Color from 'style/Color'
import { writeFile, loadFile, deleteFile } from 'erbecache/model/FileModel'
import { MSIG_SIGNATURE_PP, MSIG_SIGNATURE_TT } from 'erbecache/constant/MasterFile'

export class FormSignaturePemegangPolis extends Component {
    constructor(props) {
        super(props)
        this.state={
            isCapture: '',
            result: '',
            options: {
                format: "jpg",
                quality: 0.9,
                result: "base64"
            },
            assign: ''
        }
    }

    async componentDidMount() {
        const { signature, setStateSignatureMsig } = this.props
        if(signature.pemegangPolis !== '') {
            const assignPp = await loadFile(signature.pemegangPolis)
            if(assignPp === false) {
                setStateSignatureMsig('pemegangPolis', '')
                setStateSignatureMsig('isAssignedPp', false)
            }
            await this.setState({assign: assignPp === false ? '' : assignPp})
        }
    }

    renderImageCapture(){
        const imgSource = { uri: `data:image/jpg;base64, ${this.state.result}` }
        return(
            <View style={{marginHorizontal: 10, padding: 10, borderWidth: 1, borderColor: Color.primary, backgroundColor: Color.greyLight}}>
                <Image source={imgSource} style={{width: null, height: 400, resizeMode: 'contain'}} />
            </View>
        )
    }

    renderFailedCapture() {
        return(
            <View>
                <ErbeText
                    value={'Maaf anda tidak bisa melanjutkan halaman ini'}
                />
                {this.renderImageCapture()}
                <ButtonElement
                    title="Ulangi Lagi"
                    type="solid"
                    onPress={ () => this.setState({isCapture: '', result: ''})}
                    theme="primary"
                />
            </View>
        )
    }
    
    /**
     * @param {String} uri base64 result of capture screen
     */
    captureViewShoot() {
        const { showErrorMessage, sagaNextScreenMsig, isTertanggung, signature } = this.props
        try {
            this.refs.full.capture()
            .then( async uri => {
                // Test screenshoot
                // await this.setState({result: uri})
                // await this.setState({isCapture: 'success' })

                if(isTertanggung === true) {
                    sagaNextScreenMsig('MsigSignaturePp', 'MsigFileUpload', signature, '', '', uri)
                } else {
                    sagaNextScreenMsig('MsigSignaturePp', 'MsigSignatureTt', signature, '', '', uri)
                }
                
            })
        } catch (error) {
            this.setState({isCapture: 'failed'})
            showErrorMessage(error.message)
        }
    }

    async savePenSignature(base64String) {
        const { signature, setStateSignatureMsig, isTertanggung } = this.props;
        if (isTertanggung == true) {
            const md5Pp = await writeFile(signature.pemegangPolis, MSIG_SIGNATURE_PP, base64String)
            const md5Tt = await writeFile(signature.tertanggung, MSIG_SIGNATURE_TT, base64String)
            await setStateSignatureMsig('pemegangPolis', md5Pp)
            await setStateSignatureMsig('tertanggung', md5Tt)
            await this.setState({assign: base64String})
        } else {
            const md5Pp = await writeFile(signature.pemegangPolis, MSIG_SIGNATURE_PP, base64String)
            await setStateSignatureMsig('pemegangPolis', md5Pp)
            await this.setState({assign: base64String})
        }
    }

    async resetPenSignature() {
        const { setStateSignatureMsig, isTertanggung, signature } = this.props
        if (isTertanggung == true) {
            if(signature.pemegangPolis !== '') {
                await deleteFile(signature.pemegangPolis)
                await deleteFile(signature.tertanggung)
                setStateSignatureMsig('pemegangPolis', '')
                setStateSignatureMsig('isAssignedPp', false)
                setStateSignatureMsig('tertanggung', '')
                setStateSignatureMsig('isAssignedTt', false)
            }
        } else {
            if(signature.pemegangPolis !== '')
            await deleteFile(signature.pemegangPolis)
            setStateSignatureMsig('pemegangPolis', '')
            setStateSignatureMsig('isAssignedPp', false)
        }
        await this.setState({assign: ''})
    }

    async dragPenSignature(event) {
        const { setStateSignatureMsig, isTertanggung, signature } = this.props
        if (isTertanggung == true) {
            setStateSignatureMsig('isAssignedPp', true)
            setStateSignatureMsig('isAssignedTt', true)
        } else {
            setStateSignatureMsig('isAssignedPp', true)
        }
        __DEV__ && console.log('dragPenSignature event', event)
    }

    renderButtonSubmit() {
        const { signature } = this.props
        if(signature.pemegangPolis !== '')
        return(
            <ButtonElement
                title="Next"
                type="solid"
                onPress={ async () => await this.captureViewShoot()}
                theme="secondary"
            />
        )
    }

    renderTitleForm() {
        return(
            <TitleComponent
                value='Tanda Tangan Pemegang Polis'
            />
        )
    }

    renderSignature() {
        const { signature, proposal } = this.props
        return(
            signature &&
            <Signature
                assign={this.state.assign}
                name={proposal.pemegangPolis.namaPp}
                onSaveSignature={(res) => this.savePenSignature(res)}
                onDragSignature={(e) => this.dragPenSignature(e)}
                onResetSignature={() => this.resetPenSignature()}
            />
        )
    }

    render() {
        if(this.state.isCapture === 'failed' || this.state.result !== '') return this.renderFailedCapture()
        return(
            <ScrollView style={FormSpajStyle.container}>
                <ViewShot ref="full" options={this.state.options} style={FormSpajStyle.container}>
                    {this.renderTitleForm()}
                    {this.renderSignature()}
                    {this.renderButtonSubmit()}
                </ViewShot>
            </ScrollView>
        )
    }
}

const mtp = ({Msig}) => {
    const { isTertanggung, signature, proposal } = Msig
    return { isTertanggung, signature, proposal }
}

export default connect(mtp, { showErrorMessage, sagaNextScreenMsig, setStateSignatureMsig })(FormSignaturePemegangPolis)
