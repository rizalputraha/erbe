import React, { Component } from 'react'
import { ScrollView, View , Image } from 'react-native'
import ViewShot from 'react-native-view-shot'
import FormSpajStyle from 'stylescreen/FormSpajStyle'
import { connect } from 'react-redux'
import { setStateSpajMsig, showErrorMessage, sagaNextScreenMsig, setStateSpajInvestasiMsig, setStateSpajAhliWarisMsig } from 'erberedux/actions'
import { ButtonElement } from 'component/rnelement'
import { GenderOptions, RelationAhliwarisOptions } from 'component/msig'
import { ErbeText, TitleComponent, InputComponent, BirthdayTertanggung } from 'component/v3'
import Color from 'style/Color'

export class FormPenerimaManfaat extends Component {
    constructor(props) {
        super(props)
        this.state={
            isCapture: '',
            result: '',
            options: {
                format: "jpg",
                quality: 0.9,
                result: "base64"
            }
        }
    }

    async componentDidMount() {
        const { product, spaj, setStateSpajInvestasiMsig } = this.props
        setStateSpajInvestasiMsig(product, spaj.espaj.pemegangPolis )
    }

    renderImageCapture(){
        const imgSource = { uri: `data:image/jpg;base64, ${this.state.result}` }
        return(
            <View style={{marginHorizontal: 10, padding: 10, borderWidth: 1, borderColor: Color.primary, backgroundColor: Color.greyLight}}>
                <Image source={imgSource} style={{width: null, height: 400, resizeMode: 'contain'}} />
            </View>
        )
    }

    renderFailedCapture() {
        return(
            <View>
                <ErbeText
                    value={'Maaf anda tidak bisa melanjutkan halaman ini'}
                />
                {this.renderImageCapture()}
                <ButtonElement
                    title="Ulangi Lagi"
                    type="solid"
                    onPress={ () => this.setState({isCapture: '', result: ''})}
                    theme="primary"
                />
            </View>
        )
    }
    
    /**
     * @param {String} uri base64 result of capture screen
     */
    captureViewShoot() {
        const { spaj, showErrorMessage, sagaNextScreenMsig } = this.props
        const { penerima_manfaat_di } = spaj.espaj.investasi
        try {
            this.refs.full.capture()
            .then( async uri => {
                // Test screenshoot
                // await this.setState({result: uri})
                // await this.setState({isCapture: 'success' })
                sagaNextScreenMsig('MsigPenerimaManfaat', 'MsigDetilAgen', {...penerima_manfaat_di[0]}, '', '', uri)
            })
        } catch (error) {
            this.setState({isCapture: 'failed'})
            showErrorMessage(error.message)
        }
    }

    renderButtonSubmit() {
        return(
            <ButtonElement
                title="Next"
                type="solid"
                onPress={ async () => await this.captureViewShoot()}
                theme="secondary"
            />
        )
    }

    renderTitleForm() {
        return(
            <TitleComponent
                value='Penerima Manfaat'
            />
        )
    }

    renderAhliWaris() {
        const { spaj, setStateSpajAhliWarisMsig } = this.props
        const { penerima_manfaat_di } = spaj.espaj.investasi
        return(
            <View>
                <InputComponent
                    label={'Nama Penerima Manfaat'}
                    placeholder={''}
                    value={penerima_manfaat_di[0].nama}
                    onChangeText={ (value) => {
                        setStateSpajAhliWarisMsig('nama', value)
                    }}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={30}
                />
                <GenderOptions
                    selected={penerima_manfaat_di[0].jekel}
                    onChangeItem={(value) => {
                        setStateSpajAhliWarisMsig('jekel', value)
                    }}
                />
                <InputComponent
                    label={'Kota Kelahiran'}
                    placeholder={''}
                    value={penerima_manfaat_di[0].kota_lahir}
                    onChangeText={ (value) => {
                        setStateSpajAhliWarisMsig('kota_lahir', value)
                    }}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={100}
                />
                <BirthdayTertanggung
                    setDob={(value) => setStateSpajAhliWarisMsig('ttl', value)}
                    getDob={penerima_manfaat_di[0].ttl}
                    setUsia={(value) => setStateSpajAhliWarisMsig('usia', value)}
                    getUsia={penerima_manfaat_di[0].usia}
                    backNav={() => this.props.navigation.goBack(null)}
                    minYear={0}
                    maxYear={50}
                    minDay={1}
                    minWeek={2}
                    titleButton={'Tanggal Lahir'}
                />
                <RelationAhliwarisOptions
                    selected={penerima_manfaat_di[0].hub_dgcalon_tt}
                    onChangeItem={(value) => {
                        setStateSpajAhliWarisMsig('hub_dgcalon_tt', value)
                    }}
                />
            </View>
        )
    }

    render() {
        if(this.state.isCapture === 'failed' || this.state.result !== '') return this.renderFailedCapture()
        return(
            <ScrollView style={FormSpajStyle.container}>
                <ViewShot ref="full" options={this.state.options} style={FormSpajStyle.container}>
                    {this.renderTitleForm()}
                    {this.renderAhliWaris()}
                    {this.renderButtonSubmit()}
                </ViewShot>
            </ScrollView>
        )
    }
}

const mtp = ({Msig}) => {
    const { spaj, product } = Msig
    return { spaj, product }
}

export default connect(mtp, { setStateSpajMsig, showErrorMessage, sagaNextScreenMsig, setStateSpajInvestasiMsig, setStateSpajAhliWarisMsig })(FormPenerimaManfaat)
