import React from 'react'
import { View, Text, ScrollView, ActivityIndicator } from 'react-native'
import { ButtonElement } from "component/rnelement";
import Styles from "stylescreen/ProposalStarter";
import { connect } from 'react-redux'
import { setStateProposalPPSimas, sagaNextScreenSimas } from "erberedux/actions";
import { OptionYaProposalStarter, OptionTidakProposalStarter } from 'component/simasjiwa'

class ProposalStarter extends React.Component {
    constructor(props) {
        super(props)
    }

    submitForm() {
        if (this.props.isPartner) {
            this.props.sagaNextScreenSimas('SimasStarter', 'SimasPP', this.props.partner, this.props.authUser);
        } else {
            this.props.sagaNextScreenSimas('SimasStarter', 'SimasPP', '', this.props.authUser);
        }
    }

    render() {
        return (
            this.props.showGlobalLoading && <ActivityIndicator size="large" color="#0000ff" /> ||
            <ScrollView style={Styles.container}>
                <View style={Styles.containerTitle}>
                    <Text style={Styles.txtTitle}>Apakah data tertanggung sama dengan pemegang polis ?</Text>
                </View>

                <View style={Styles.containerForm}>
                    <OptionYaProposalStarter />
                    <OptionTidakProposalStarter />
                </View>

                <ButtonElement
                    title="Next"
                    type="solid"
                    onPress={() => this.submitForm()}
                    color="secondary"
                />

            </ScrollView>
        )
    }
}

const mtp = ({ Auth, SimasJiwa }) => {
    const { authUser } = Auth
    const { proposal, isPartner, partner } = SimasJiwa
    return { proposal, authUser, isPartner, partner }
}

export default connect(mtp, { setStateProposalPPSimas, sagaNextScreenSimas })(ProposalStarter)