//Import Screen Here
import ProposalCategory from './ProposalCategory';
import ProposalCacheStarter from './ProposalCacheStarter'
import ProposalDownline from './ProposalDownline'
import ProposalStarter from './ProposalStarter'
import ProposalPemegangPolis from "./ProposalPemegangPolis";
import ProposalTertanggung from "./ProposalTertanggung";
import ProposalProduk from "./ProposalProduk";
import FormPemegangPolis from './FormPemegangPolis';
import FormTertanggung from './FormTertanggung';
import FormPenerimaManfaat from './FormPenerimaManfaat';
import FormKuisionerHealth from './FormKuisionerHealth';
import FormKuisionerResiko from './FormKuisionerResiko';
import FormSignaturePemegangPolis from './FormSignaturePemegangPolis';
import FormSignatureTertanggung from './FormSignatureTertanggung';
import FormFileDocument from './FormFileDocument';
import FormSubmitSpaj from './FormSubmitSpaj';

module.exports = {
    ProposalCategory,
    ProposalCacheStarter,
    ProposalDownline,
    ProposalStarter,
    ProposalPemegangPolis,
    ProposalTertanggung,
    ProposalProduk,
    FormPemegangPolis,
    FormTertanggung,
    FormPenerimaManfaat,
    FormSignaturePemegangPolis,
    FormSignatureTertanggung,
    FormFileDocument,
    FormKuisionerHealth,
    FormKuisionerResiko,
    FormSubmitSpaj
}