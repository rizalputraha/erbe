import React, { Component } from 'react'
import { Text, View, StyleSheet, SafeAreaView } from 'react-native'
import { TitleComponent, ErbeListMenu, InputComponent } from "component/v3";
import { CheckBox } from 'react-native-elements'
import { ButtonElement } from "component/rnelement";
import Color from 'style/Color'
import { push } from 'erbeservice/AppNavigation'
import { masterQuestioner } from "erbecache/model/SimasModel";
import { connect } from "react-redux";
import { sagaNextScreenSimas, loadStorageQuestionerSimas, setAnswerHealthQuestionerTextSimas, setAnswerHealthQuestionerOptionSimas } from "erberedux/actions";

export class FormKuisioner extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    renderTitleContent() {
        return (
            <View style={styles.content}>
                <TitleComponent
                    value={'Data Kuesioner Calon Tertanggung'}
                />
            </View>
        )
    }

    _getLabel(index, item) {
        return (
            <Text>{++index}. {item.label}</Text>
        )
    }

    renderItems(item, index) {
        const { spaj, setAnswerHealthQuestionerTextSimas, setAnswerHealthQuestionerOptionSimas } = this.props;
        return (
            <View style={{ margin: 10 }}>
                {
                    item.isOption == false ?
                        <View>
                            {this._getLabel(index, item)}
                            <InputComponent
                                label={'Jawaban'}
                                placeholder={'Jawaban'}
                                minCharacter={2}
                                maxCharacter={10}
                                keyboardType={'numeric'}
                                value={item.answerText}
                                onChangeText={(value) => setAnswerHealthQuestionerTextSimas('answerText', value, index)}
                                validation={['required', 'length']}
                            />
                        </View>
                        :
                        <View>
                            {this._getLabel(index, item)}
                            <CheckBox
                                title="Ya"
                                checkedIcon="dot-circle-o"
                                uncheckedIcon="circle-o"
                                checked={item.answerOptions.optionYa}
                                onPress={() => setAnswerHealthQuestionerOptionSimas('optionYa', !item.answerOptions.optionYa, index)}
                                checkedColor="green"
                            />
                            <CheckBox
                                title="Tidak"
                                checkedIcon="dot-circle-o"
                                uncheckedIcon="circle-o"
                                checked={item.answerOptions.optionTidak}
                                onPress={() => setAnswerHealthQuestionerOptionSimas('optionTidak', !item.answerOptions.optionTidak, index)}
                                checkedColor="green"
                            />
                        </View>
                }
            </View>
        )
    }

    renderHealth() {
        return (
            <ErbeListMenu
                tipe="list"
                data={this.props.spaj.questioner.healthTertanggung}
                renderListItem={(item, index) => this.renderItems(item, index)}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }

    renderButton() {
        const { sagaNextScreenSimas } = this.props;
        return (
            <ButtonElement
                title='Next'
                onPress={async () => sagaNextScreenSimas('SimasKuisionerHealth', 'SimasKuisionerResiko', '')}
            />
        )
    }

    render() {
        return (
            <SafeAreaView style={styles.container} ref="full">
                {this.renderTitleContent()}
                {this.renderHealth()}
                {this.renderButton()}
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white
    },
    content: {
        backgroundColor: Color.white
    }
})

const mtp = ({ SimasJiwa }) => {
    const { product, isTertanggung, spaj } = SimasJiwa
    return { product, isTertanggung, spaj }
}

export default connect(mtp, { loadStorageQuestionerSimas, setAnswerHealthQuestionerTextSimas, setAnswerHealthQuestionerOptionSimas, sagaNextScreenSimas })(FormKuisioner)
