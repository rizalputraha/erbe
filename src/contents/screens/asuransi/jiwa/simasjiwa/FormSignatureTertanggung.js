import React, { Component } from 'react'
import { ButtonElement } from "component/rnelement";
import { TitleComponent, Signature } from "component/v3";
import { connect } from "react-redux";
import { showErrorMessage, setStateSpajSignatureSimas, sagaNextScreenSimas } from "erberedux/actions";
import { writeFile, loadFile, deleteFile, readRootDir } from 'erbecache/model/FileModel';
import { SIMASJIWA_SIGNATURE_TERTANGGUNG } from 'erbecache/constant/MasterFile';
import { ScrollView } from 'react-native-gesture-handler';

class FormSignatureTertanggung extends Component {
    constructor(props) {
        super(props);
        this.state = {
            assign: '',
        }
    }

    async componentDidMount() {
        const list = await readRootDir();
        console.log('list file', list)
        const { signature, setStateSpajSignatureSimas } = this.props
        if (signature.tertanggung !== '') {
            const assignTt = await loadFile(signature.tertanggung)
            console.log('assignTt', assignTt)
            if (assignTt === false) {
                setStateSpajSignatureSimas('tertanggung', '')
                setStateSpajSignatureSimas('isAssignedTt', false)
            }
            await this.setState({ assign: assignTt === false ? '' : assignTt })
        }
    }

    async savePenSignature(base64String) {
        const { setStateSpajSignatureSimas, signature } = this.props;
        const md5File = await writeFile(signature.tertanggung, SIMASJIWA_SIGNATURE_TERTANGGUNG, base64String)
        await setStateSpajSignatureSimas('tertanggung', md5File)
        await this.setState({ assign: base64String })
    }

    async resetPenSignature() {
        const { setStateSpajSignatureSimas, signature } = this.props
        await deleteFile(signature.tertanggung)
        await setStateSpajSignatureSimas('tertanggung', '')
        await setStateSpajSignatureSimas('isAssignedTt', false)
        await this.setState({ assign: '' })

        await this.setState({ assign: '' })
    }

    dragPenSignature(event) {
        const { setStateSpajSignatureSimas } = this.props
        setStateSpajSignatureSimas('isAssignedTt', true)
        __DEV__ && console.log('dragPenSignature event', event)
    }

    renderTitleForm() {
        return (
            <TitleComponent
                value={'Tanda Tangan Tertanggung'}
            />
        )
    }

    renderSignature() {
        const { signature, proposal } = this.props;
        return (
            signature &&
            <Signature
                assign={this.state.assign}
                name={proposal.tertanggung.namaTt}
                onSaveSignature={(res) => this.savePenSignature(res)}
                onDragSignature={(e) => this.dragPenSignature(e)}
                onResetSignature={() => this.resetPenSignature()}
            />
        )
    }

    nextScreen() {
        if (this.props.signature.isAssignedTt == false) {
            this.props.showErrorMessage('Error', 'Silahkan Tanda Tangan Terlebih Dahulu!')
        } else {
            this.props.sagaNextScreenSimas('SimasSignatureTT', 'SimasFileUpload', '');
        }
    }

    renderButtonSubmit() {
        const { signature } = this.props
        if (signature.tertanggung !== '')
            return (
                <ButtonElement
                    title="Next"
                    type="solid"
                    onPress={async () => this.nextScreen()}
                    theme="secondary"
                />
            )
    }

    render() {
        return (
            <ScrollView>
                {this.renderTitleForm()}
                {this.renderSignature()}
                {this.renderButtonSubmit()}
            </ScrollView>
        )
    }


}

const mtp = ({ Auth, SimasJiwa, ErrorHandling }) => {
    const { authUser } = Auth
    const { errorMessage, showGlobalLoading } = ErrorHandling
    const { signature, proposal, isTertanggung } = SimasJiwa
    return { authUser, errorMessage, showGlobalLoading, signature, proposal, isTertanggung }
}

export default connect(mtp, { showErrorMessage, setStateSpajSignatureSimas, sagaNextScreenSimas })(FormSignatureTertanggung)
