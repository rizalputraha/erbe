import React, { Component } from 'react'
import { Dimensions, ScrollView } from 'react-native'
import { ButtonElement } from "component/rnelement";
import { TitleComponent, ErbeText, Signature } from "component/v3";
import { connect } from "react-redux";
import { showErrorMessage, setStateSpajSignatureSimas, sagaNextScreenSimas } from "erberedux/actions";
import { writeFile, loadFile, deleteFile, readRootDir } from 'erbecache/model/FileModel';
import { SIMASJIWA_SIGNATURE_PEMEGANGPOLIS, SIMASJIWA_SIGNATURE_TERTANGGUNG } from 'erbecache/constant/MasterFile';

class FormSignaturePemegangPolis extends Component {
    constructor(props) {
        super(props);
        this.state = {
            assign: '',
        }
    }

    async componentDidMount() {
        const list = await readRootDir();
        console.log('list Dir', list);
        const { signature, setStateSpajSignatureSimas } = this.props
        if (signature.pemegangPolis !== '') {
            const assignPp = await loadFile(signature.pemegangPolis)
            console.log('assignPp', assignPp)
            if (assignPp === false) {
                setStateSpajSignatureSimas('pemegangPolis', '')
                setStateSpajSignatureSimas('isAssignedPp', false)
            }
            await this.setState({ assign: assignPp === false ? '' : assignPp })
        }
    }

    async savePenSignature(base64String) {
        const { signature, setStateSpajSignatureSimas, isTertanggung } = this.props;
        if (isTertanggung == true) {
            const md5Pp = await writeFile(signature.pemegangPolis, SIMASJIWA_SIGNATURE_PEMEGANGPOLIS, base64String)
            const md5Tt = await writeFile(signature.tertanggung, SIMASJIWA_SIGNATURE_TERTANGGUNG, base64String)
            await setStateSpajSignatureSimas('pemegangPolis', md5Pp)
            await setStateSpajSignatureSimas('tertanggung', md5Tt)
            await this.setState({ assign: base64String })
        } else {
            const md5Pp = await writeFile(signature.pemegangPolis, SIMASJIWA_SIGNATURE_PEMEGANGPOLIS, base64String)
            await setStateSpajSignatureSimas('pemegangPolis', md5Pp)
            await this.setState({ assign: base64String })
        }
    }

    async resetPenSignature() {
        const { setStateSpajSignatureSimas, isTertanggung, signature } = this.props
        if (isTertanggung == true) {
            if (signature.pemegangPolis !== '') {
                await deleteFile(signature.pemegangPolis)
                await deleteFile(signature.tertanggung)
                setStateSpajSignatureSimas('pemegangPolis', '')
                setStateSpajSignatureSimas('isAssignedPp', false)
                setStateSpajSignatureSimas('tertanggung', '')
                setStateSpajSignatureSimas('isAssignedTt', false)
            }
        } else {
            if (signature.pemegangPolis !== '')
                await deleteFile(signature.pemegangPolis)
            setStateSpajSignatureSimas('pemegangPolis', '')
            setStateSpajSignatureSimas('isAssignedPp', false)
        }
        await this.setState({ assign: '' })
    }

    dragPenSignature(event) {
        const { setStateSpajSignatureSimas, isTertanggung, signature } = this.props
        if (isTertanggung == true) {
            setStateSpajSignatureSimas('isAssignedPp', true)
            setStateSpajSignatureSimas('isAssignedTt', true)
        } else {
            setStateSpajSignatureSimas('isAssignedPp', true)
        }
        __DEV__ && console.log('dragPenSignature event', event)
    }

    renderTitleForm() {
        return (
            <TitleComponent
                value={'Tanda Tangan Pemegang Polis'}
            />
        )
    }

    renderSignature() {
        const { signature, proposal } = this.props;
        return (
            signature &&
            <Signature
                assign={this.state.assign}
                name={proposal.pemegangPolis.namaPp}
                onSaveSignature={(res) => this.savePenSignature(res)}
                onDragSignature={(e) => this.dragPenSignature(e)}
                onResetSignature={() => this.resetPenSignature()}
            />
        )
    }

    nextScreen() {
        if (this.props.signature.isAssignedPp == false) {
            this.props.showErrorMessage('Error', 'Silahkan Tanda Tangan Terlebih Dahulu!')
        } else {
            this.props.sagaNextScreenSimas('SimasSignaturePP', 'SimasSignatureTT', '');
        }
    }

    renderButtonSubmit() {
        const { signature } = this.props
        if (signature.pemegangPolis !== '')
            return (
                <ButtonElement
                    title="Next"
                    type="solid"
                    onPress={async () => this.nextScreen()}
                    theme="secondary"
                />
            )
    }

    render() {
        return (
            <ScrollView>
                {this.renderTitleForm()}
                {this.renderSignature()}
                {this.renderButtonSubmit()}
            </ScrollView>
        )
    }


}

const mtp = ({ Auth, SimasJiwa, ErrorHandling }) => {
    const { authUser } = Auth
    const { errorMessage, showGlobalLoading } = ErrorHandling
    const { signature, proposal, isTertanggung } = SimasJiwa
    return { authUser, errorMessage, showGlobalLoading, signature, proposal, isTertanggung, }
}

export default connect(mtp, { showErrorMessage, setStateSpajSignatureSimas, sagaNextScreenSimas })(FormSignaturePemegangPolis)
