import React, { Component } from 'react'
import { SafeAreaView, View, StyleSheet, TouchableOpacity, Text } from 'react-native'
import { connect } from 'react-redux'
import { ErbeListMenu, BadgeLevel, HeaderComponent } from "component/v3";
import { STORAGE_DOWNLINE_MEMBER } from 'erbecache/constant/MasterAsuransi'
import { loadCacheSimas } from 'erbecache/model/MasterModel'
import { ListItem } from 'react-native-elements';
import { SearchElement } from 'component/rnelement'
import Color from 'style/Color'
import { setStateDownlineSimas, sagaNextScreenSimas } from 'erberedux/actions'
import { goBack } from 'erbeservice/AppNavigation'
import { getLevelAgen } from 'erbevalidation/Helper'

class ProposalDownline extends Component {
    constructor(props) {
        super(props);
        this.state = {
            downline: [],
        }
    }

    async componentDidMount() {
        const downline = await loadCacheSimas(STORAGE_DOWNLINE_MEMBER);
        this.setState({
            downline
        })
        console.log(this.state.downline);

    }

    async _searchData(value) {
        this.setState({ username: value })
        let data = await loadCacheSimas(STORAGE_DOWNLINE_MEMBER);
        if (this.state.username == '') {
            this.setState({
                downline: data
            });
        }
        data = data.filter(l => {
            return l.username.toLowerCase().match(value);
        });
        this.setState({
            downline: data
        });
    }

    selectDownline(item) {
        const { sagaNextScreenSimas } = this.props;
        sagaNextScreenSimas('SimasDownline', 'SimasStarter', item);
    }

    renderItemDownline(item) {
        return (
            <TouchableOpacity onPress={() => this.selectDownline(item)} style={{ padding: 5, margin: 5, borderRadius: 5, elevation: 2, flexDirection: 'row', alignItems: 'center' }}>
                <View style={{ flex: 1, margin: 10 }}>
                    <Text style={{ margin: 5, fontWeight: 'bold', fontSize: 16 }}>{item.username}</Text>
                    <Text style={{ margin: 5 }}>Nama: {item.nama}</Text>
                    <Text style={{ margin: 5 }}>Email: {item.email}</Text>
                    <Text style={{ margin: 5 }}>No Hp: {item.no_telp}</Text>
                </View>
                <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: 20 }}>
                    <BadgeLevel level={item.level} />
                    <Text style={{ fontWeight: 'bold' }}>{getLevelAgen(item.level)}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <HeaderComponent
                        pushBackButtonHeader={() => goBack()}
                        title={'Pilih Downline Member'}
                    />
                    <SearchElement
                        lightTheme
                        placeholder="Username"
                        showLoading={this.props.showGlobalLoading}
                        changeTextHandler={(value) => this._searchData(value)}
                        value={this.state.username}
                        containerStyle={{ backgroundColor: Color.primary, borderTopWidth: 0 }}
                        onSubmitEditing={event => this.onSubmitUname(event.nativeEvent.text)}
                    />
                </View>
                <ErbeListMenu
                    tipe='list'
                    data={this.state.downline}
                    renderListItem={(item) => this.renderItemDownline(item)}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    content: {
        backgroundColor: 'white'
    }
})

const mtp = ({ SimasJiwa }) => {
    const { proposal, isTertanggung } = SimasJiwa
    return { proposal, isTertanggung }
}

export default connect(mtp, { setStateDownlineSimas, sagaNextScreenSimas })(ProposalDownline)