import React, { Component } from 'react'
import { View, Text, ScrollView, Alert } from 'react-native'
import { ButtonElement } from 'component/rnelement'
import { InputComponent, BirthdayTertanggung } from "component/v3";
import { connect } from 'react-redux'
import { showErrorMessage, setStateProposalTTSimas, sagaNextScreenSimas } from 'erberedux/actions'
import { GenderOptions, RelationOptions, BirthdayProposal } from 'component/simasjiwa'
import Style from 'stylescreen/ProposalTertanggung'

export class ProposalTertanggung extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    componentDidMount() {
        console.log('ProposalPemegangPolis', { props: this.props, state: this.state })
    }

    submitForm() {
        const { sagaNextScreenSimas, proposal } = this.props;

        sagaNextScreenSimas('SimasTT', 'SimasProduk', proposal.tertanggung)
    }

    renderForm() {
        const { proposal, setStateProposalTTSimas } = this.props;
        return (
            <View>
                <View style={Style.containerTitle}>
                    <Text style={Style.txtTitle}>Lengkapi data tertanggung berikut ini</Text>
                </View>
                <InputComponent
                    label={'Nama Lengkap Sesuai KTP'}
                    placeholder={''}
                    value={proposal.tertanggung.namaTt}
                    onChangeText={(value) => {
                        setStateProposalTTSimas('namaTt', value)
                    }}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={100}
                />
                <BirthdayProposal
                    setDob={(value) => setStateProposalTTSimas('dobTt', value)}
                    getDob={proposal.tertanggung.dobTt}
                    setUsia={(value) => setStateProposalTTSimas('usiaTt', value)}
                    getUsia={proposal.tertanggung.usiaTt}
                    backNav={() => this.props.navigation.goBack(null)}
                    minYear={0}
                    maxYear={50}
                    minDay={1}
                    minWeek={2}
                    titleButton={'Tanggal Lahir'}
                />

                <GenderOptions
                    selected={proposal.tertanggung.genderTt}
                    onChangeItem={(value) => setStateProposalTTSimas('genderTt', value)}
                />

                <ButtonElement
                    title="Next"
                    onPress={() => this.submitForm()}
                />
            </View>
        )
    }

    render() {
        return (
            <ScrollView>
                {this.renderForm()}
            </ScrollView>
        )
    }
}

const mtp = ({ SimasJiwa }) => {
    const { proposal } = SimasJiwa
    return { proposal }
}

export default connect(mtp, { showErrorMessage, setStateProposalTTSimas, sagaNextScreenSimas })(ProposalTertanggung)