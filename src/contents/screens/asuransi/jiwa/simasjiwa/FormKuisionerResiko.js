import React, { Component } from 'react'
import { Text, View, StyleSheet, SafeAreaView } from 'react-native'
import { TitleComponent, ErbeListMenu, PickerItemValidation } from "component/v3";
import { CheckBox } from 'react-native-elements'
import { ButtonElement } from "component/rnelement";
import Color from 'style/Color'
import { push } from 'erbeservice/AppNavigation'
import { masterQuestioner } from "erbecache/model/SimasModel";
import { connect } from "react-redux";
import { loadStorageQuestionerSimas, setAnswerHealthQuestionerTextSimas, setAnswerHealthQuestionerOptionSimas } from "erberedux/actions";


export class FormKuisionerResiko extends Component {
    constructor(props) {
        super(props)
    }

    renderTitleContent() {
        return (
            <View style={styles.content}>
                <TitleComponent
                    value={'Data Kuesioner Pemegang polis'}
                />
            </View>
        )
    }

    renderItemsResiko(data, index) {
        return (
            <View style={{ margin: 10 }}>
                <Text>{++index}. {data.label}</Text>
                <View>
                    <PickerItemValidation
                        title={'Jawaban'}
                        titleEmptyData={'Silahkan Pilih Jawaban'}
                        titleForm={true}
                        paramSelected={'pilihan'}
                        paramView={'label'}
                        data={data.listAcceptedAnswers}
                        selected={data.answerDefault}
                        onChangeItem={(value) => this.props.setAnswerProfileResiko(value)}
                    />
                </View>
            </View>
        )
    }

    renderProfileResiko() {
        return (
            <ErbeListMenu
                tipe="list"
                data={this.props.spaj.questioner.profileResikoPemegangPolis}
                renderListItem={(item, index) => this.renderItemsResiko(item, index)}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }

    renderButton() {
        return (
            <ButtonElement
                title='Next'
                onPress={async () => await push('SimasSignaturePP')}
            />
        )
    }

    render() {
        return (
            <SafeAreaView style={styles.container} ref="full">
                {this.renderTitleContent()}
                {this.renderProfileResiko()}
                {this.renderButton()}
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white
    },
    content: {
        backgroundColor: Color.white
    }
})

const mtp = ({ SimasJiwa }) => {
    const { spaj, product } = SimasJiwa;
    return { spaj, product }
}

export default connect(mtp, { loadStorageQuestionerSimas })(FormKuisionerResiko)
