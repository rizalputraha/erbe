import React, { Component } from 'react'
import { Text, View, ScrollView, StyleSheet, Alert } from 'react-native'
import { ButtonElement } from 'component/rnelement';
import {
    InputComponent,
    ItemComponent,
    TitleComponent
} from "component/v3";
import Color from '../../../../../styles/Color';
import { navigate } from "erbeservice/AppNavigation";
import Styles from "stylescreen/FormPemegangPolis";
import { submitFormTT, formatPhoneNumber } from "erbevalidation/SimasJiwa";
import { connect } from 'react-redux'
import { showErrorMessage, setStateSpajTTSimas } from 'erberedux/actions'
import { ReligionOptions, StatusOptions, WilayahOptions, GraduatedOptions, IdentityOptions } from 'component/simasjiwa'

export class FormTertanggung extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    async _changeState(value, parentState) {
        await this.setState({ [parentState]: value });
    }

    _setValidationTelephon(value, parentState) {
        this.setState({ [parentState]: value })
        const check = formatPhoneNumber(this.state[parentState])
        console.log("Check Format Number", check);
        if (check === false) {
            this.setState({ [parentState]: '' })
        }
    }

    async onSubmitPress() {
        const validation = await submitFormTT(this.props.spaj.tertanggung);
        console.log(validation);
        if (validation.error == 101) {
            Alert.alert('Error', validation.message);
        } else {
            await navigate('SimasPenerimaManfaat');
        }
    }

    renderTitleContent() {
        return (
            <View style={styles.content}>
                <TitleComponent
                    value={'Data Calon Tertanggung'}
                />
            </View>
        )
    }

    renderDataTT() {
        const { proposal, spaj, setStateSpajTTSimas } = this.props;
        return (
            <View style={styles.content}>
                <InputComponent
                    label={'Nama Tertanggung'}
                    placeholder={''}
                    value={spaj.tertanggung.namaTt}
                    onChangeText={(value) => setStateSpajTTSimas('namaTt', value)}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={200}
                    editable={false}
                />
                <InputComponent
                    label={'Nama Ibu'}
                    placeholder={''}
                    value={spaj.tertanggung.namaIbuTt}
                    onChangeText={(value) => setStateSpajTTSimas('namaIbuTt', value)}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={200}
                />
                <InputComponent
                    label={'Kota Kelahiran'}
                    placeholder={''}
                    value={spaj.tertanggung.tptLahirTt}
                    onChangeText={(value) => setStateSpajTTSimas('tptLahirTt', value)}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={30}
                />
                <ItemComponent
                    label={'Tanggal Lahir'}
                    value={spaj.tertanggung.dobTt}
                />
                <ItemComponent
                    label={'Usia'}
                    value={spaj.tertanggung.usiaTt}
                />
                <ReligionOptions
                    selected={spaj.tertanggung.agamaTt}
                    onChangeItem={(value) => {
                        console.log('FormPemegangPolis ReligionOptions onChangeItem', value),
                            setStateSpajTTSimas('agamaTt', value)
                    }}
                    onChangeText={(value) => {
                        console.log('FormPemegangPolis ReligionOptions onChangeText', value),
                            setStateSpajTTSimas('agamaLainTt', value)
                    }}
                />
                <ItemComponent
                    label={'Jenis Kelamin'}
                    value={spaj.tertanggung.genderTt == 0 ? 'WANITA' : 'PRIA'}
                />
                <StatusOptions
                    selected={spaj.tertanggung.statusTt}
                    onChangeItem={(value) => {
                        console.log('FormPemegangPolis StatusOptions onChangeItem', value),
                            setStateSpajTTSimas('statusTt', value)
                    }}
                />
            </View >
        )
    }

    renderTempatTinggal() {
        const { setStateSpajTTSimas, spaj } = this.props;
        return (
            <View style={Styles.content}>
                <WilayahOptions
                    selectedProvinsi={spaj.tertanggung.provinsiTt}
                    selectedKota={spaj.tertanggung.kotaTt}
                    onChangeProvinsi={(value) => setStateSpajTTSimas('provinsiTt', value)}
                    onChangeKota={(value) => setStateSpajTTSimas('kotaTt', value)}
                />

                <InputComponent
                    label={'Alamat Rumah'}
                    placeholder={''}
                    value={spaj.tertanggung.addressTt}
                    onChangeText={(value) => setStateSpajTTSimas('addressTt', value)}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={200}
                />

                <InputComponent
                    label={'Kode Pos'}
                    placeholder={'Kode Pos'}
                    value={spaj.tertanggung.kdPosTt}
                    onChangeText={(value) => setStateSpajTTSimas('kdPosTt', value)}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={200}
                />
            </View>
        );
    }

    renderPendidikan() {
        const { setStateSpajTTSimas, spaj } = this.props;

        return (
            <View>
                <GraduatedOptions
                    selected={spaj.tertanggung.pendidikanTt}
                    onChangeItem={(value) => {
                        console.log('FormPemegangPolis GraduatedOptions onChangeItem', value),
                            setStateSpajTTSimas('pendidikanTt', value)
                    }}
                />

                <InputComponent
                    label={'Gelar'}
                    placeholder={'Dr.'}
                    value={spaj.tertanggung.gelarTt}
                    onChangeText={(value) => setStateSpajTTSimas('gelarTt', value)}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={50}
                />
            </View>
        )
    }

    renderIdentitas() {
        const { setStateSpajTTSimas, spaj } = this.props;
        return (
            <View>
                <InputComponent
                    label={'Nomor HP'}
                    placeholder={''}
                    value={spaj.tertanggung.noHpTt}
                    onChangeText={(value) => {
                        setStateSpajTTSimas('noHpTt', value)
                    }}
                    validation={['phone', 'required', 'length']}
                    minCharacter={10}
                    maxCharacter={20}
                    keyboardType={'numeric'}
                />

                <InputComponent
                    label={'Email'}
                    placeholder={''}
                    value={spaj.tertanggung.emailTt}
                    onChangeText={(value) => setStateSpajTTSimas('emailTt', value)}
                    validation={['required', 'length', 'email']}
                    minCharacter={3}
                    maxCharacter={15}
                    keyboardType={'email-address'}
                    callback={(value) => this._changeState(value, 'isValidEmail')}
                />
                <IdentityOptions
                    selected={spaj.tertanggung.buktiIdentitasTt}
                    onChangeItem={(value) => {
                        console.log('FormPemegangPolis ReligionOptions onChangeItem', value),
                            setStateSpajTTSimas('buktiIdentitasTt', value)
                    }}
                />
                <InputComponent
                    label={'No Bukti Identitas'}
                    placeholder={''}
                    value={spaj.tertanggung.identitasTt}
                    onChangeText={(value) => setStateSpajTTSimas('identitasTt', value)}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={50}
                    keyboardType={spaj.pemegangPolis.identitasTt == 1 || spaj.pemegangPolis.identitasTt == 9 ? 'numeric' : 'default'}
                />
            </View>
        )
    }

    renderLembagaKerja() {
        const { setStateSpajTTSimas, spaj } = this.props;

        return (
            <View style={styles.content}>
                <InputComponent
                    label={'Pekerjaan'}
                    placeholder={''}
                    value={spaj.tertanggung.jobDescTt}
                    onChangeText={(value) => {
                        setStateSpajTTSimas('jobDescTt', value)
                    }}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={50}
                />
                <InputComponent
                    label={'Penghasilan'}
                    placeholder={''}
                    value={spaj.tertanggung.penghasilanTt}
                    onChangeText={(value) => {
                        setStateSpajTTSimas('penghasilanTt', value)
                    }}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={50}
                />
            </View>
        )
    }

    renderButton() {
        return (
            <ButtonElement
                title={'Next'}
                onPress={() => this.onSubmitPress()}
            />
        )
    }

    render() {
        return (
            <ScrollView>
                {this.renderTitleContent()}
                {this.renderDataTT()}
                {this.renderTempatTinggal()}
                {this.renderPendidikan()}
                {this.renderIdentitas()}
                {this.renderLembagaKerja()}
                {this.renderButton()}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white
    },
    content: {
        backgroundColor: Color.white
    }
})

const mtp = ({ SimasJiwa }) => {
    const { spaj, proposal, isTertanggung } = SimasJiwa
    return { spaj, proposal, isTertanggung }
}

export default connect(mtp, { showErrorMessage, setStateSpajTTSimas })(FormTertanggung)
