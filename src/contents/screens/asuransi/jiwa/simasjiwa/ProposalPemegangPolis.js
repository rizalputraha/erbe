import React, { Component } from 'react'
import { Text, View, ScrollView, Alert, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
import { sagaNextScreenSimas, setStateProposalPPSimas, setProposalPPtoTTSimas } from "erberedux/actions";
import { BirthdayPemegangPolis } from "component/v3";
import { ButtonElement } from "component/rnelement";
import { push } from 'erbeservice/AppNavigation';
import { submitProposalPemegangPolis } from 'erbevalidation/SimasJiwa'
import { GenderOptions, WilayahOptions, BirthdayProposal } from 'component/simasjiwa'

class ProposalPemegangPolis extends Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        console.log('ProposalPemegangPolis', { props: this.props, state: this.state })
    }

    async onSubmitForm() {
        const { isTertanggung, sagaNextScreenSimas, proposal } = this.props;
        if (isTertanggung === true) {
            sagaNextScreenSimas('SimasPP', 'SimasProduk', proposal.pemegangPolis)
        } else {
            sagaNextScreenSimas('SimasPP', 'SimasTT', proposal.pemegangPolis)
        }
    }

    renderNama() {
        const { proposal } = this.props;
        return (
            <View style={{ padding: 10 }}>
                <Text style={{ fontWeight: 'bold', marginBottom: 10, color: 'grey' }}>Nama Pemegang Polis</Text>
                <Text>{proposal && proposal.pemegangPolis && proposal.pemegangPolis.namaPp}</Text>
            </View>
        )
    }

    renderButtonSubmit() {
        return (
            <ButtonElement
                onPress={() => { this.onSubmitForm() }}
                type="solid"
                title="Next" />
        )
    }


    renderFormDataProposal() {
        const { proposal, setStateProposalPPSimas } = this.props;
        return (
            <View>
                <BirthdayProposal
                    setDob={(value) => setStateProposalPPSimas('dobPp', value)}
                    getDob={proposal.pemegangPolis.dobPp}
                    setUsia={(value) => setStateProposalPPSimas('usiaPp', value)}
                    getUsia={proposal.pemegangPolis.usiaPp}
                    backNav={() => this.props.navigation.goBack(null)}
                    minYear={18}
                    minDay={1}
                    minWeek={2}
                    maxYear={70}
                    titleButton={'Tanggal Lahir'}
                />
                <GenderOptions
                    selected={proposal.pemegangPolis.genderPp}
                    onChangeItem={(value) => setStateProposalPPSimas('genderPp', value)}
                />
                {this.renderButtonSubmit()}
            </View>
        )
    }

    render() {
        return (this.props.showGlobalLoading ? <ActivityIndicator size="large" color="#0000ff" /> : <ScrollView>
            {this.renderNama()}
            {this.renderFormDataProposal()}
        </ScrollView>)

    }
}


const mtp = ({ SimasJiwa }) => {
    const { proposal, isTertanggung } = SimasJiwa
    return { proposal, isTertanggung }
}

export default connect(mtp, { setStateProposalPPSimas, setProposalPPtoTTSimas, sagaNextScreenSimas })(ProposalPemegangPolis)