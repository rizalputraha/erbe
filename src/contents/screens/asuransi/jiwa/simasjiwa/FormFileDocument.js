import React, { Component } from 'react'
import { ScrollView, Image, Dimensions, View } from 'react-native'
import { ButtonElement } from "component/rnelement";
import { connect } from "react-redux";
import { navigate } from 'erbeservice/AppNavigation'
import { setStateFileAttachmentSimas, sagaNextScreenSimas } from 'erberedux/actions'
import { ModalFileUpload, TitleComponent } from 'component/v3';
import { writeFile, loadFile, readRootDir } from 'erbecache/model/FileModel';
import { SIMASJIWA_FILE_PEMEGANGPOLIS, SIMASJIWA_FILE_TERTANGGUNG } from 'erbecache/constant/MasterFile';

class FormFileDocument extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filePp: '',
            fileTt: '',
        }
    }

    async componentDidMount() {
        const list = await readRootDir();
        console.log('list file', list);
        const { fileAttachment } = this.props

        const imgPp = await loadFile(fileAttachment.pemegangPolis)
        const imgTt = await loadFile(fileAttachment.tertanggung)

        await this.setState({
            filePp: imgPp === false ? '' : imgPp,
            fileTt: imgTt === false ? '' : imgTt
        })

    }

    async saveFile(imgbase64, type = undefined) {
        const { isTertanggung, fileAttachment, setStateFileAttachmentSimas } = this.props
        if (isTertanggung === true) {
            const md5Pp = await writeFile(fileAttachment.pemegangPolis, SIMASJIWA_FILE_PEMEGANGPOLIS, imgbase64)
            setStateFileAttachmentSimas('pemegangPolis', md5Pp)
            const md5Tt = await writeFile(fileAttachment.tertanggung, SIMASJIWA_FILE_TERTANGGUNG, imgbase64)
            setStateFileAttachmentSimas('tertanggung', md5Tt)
            await this.setState({ filePp: imgbase64, fileTt: imgbase64 })
        } else {
            if (type == 'pp') {
                const md5Pp = await writeFile(fileAttachment.pemegangPolis, SIMASJIWA_FILE_PEMEGANGPOLIS, imgbase64)
                setStateFileAttachmentSimas('pemegangPolis', md5Pp)
                await this.setState({ filePp: imgbase64 })
            }
            if (type == 'tt') {
                const md5Tt = await writeFile(fileAttachment.tertanggung, SIMASJIWA_FILE_TERTANGGUNG, imgbase64)
                setStateFileAttachmentSimas('tertanggung', md5Tt)
                await this.setState({ fileTt: imgbase64 })
            }
        }
        const list = await readRootDir()
        __DEV__ && console.log('List directory after saveFile', list)
    }

    renderTitle() {
        return (
            <TitleComponent
                value={'Tanda Tangan Pemegang Polis'}
            />
        )
    }

    renderFileUpload() {
        const { isTertanggung, fileAttachment } = this.props
        return (
            isTertanggung &&

            <ModalFileUpload
                title={'Upload KTP'}
                onSelectFiles={(ktp) => this.saveFile(ktp)}
                fileSelected={this.state.filePp}
            />

            ||

            <View>
                <ModalFileUpload
                    title={'Upload KTP Pemegang Polis'}
                    onSelectFiles={(ktp) => this.saveFile(ktp, 'pp')}
                    fileSelected={this.state.filePp}
                />

                <ModalFileUpload
                    title={'Upload KTP Tertanggung'}
                    onSelectFiles={(ktp) => this.saveFile(ktp, 'tt')}
                    fileSelected={this.state.fileTt}
                />
            </View>
        )
    }

    renderButtonSubmit() {
        return (
            <View style={{ flex: 1 }}>
                <ButtonElement title='Next' onPress={() => this.props.sagaNextScreenSimas('SimasFileUpload', 'SimasFormSubmit', '')} />
            </View>
        )
    }

    render() {
        return (
            <ScrollView>
                {this.renderTitle()}
                {this.renderFileUpload()}
                {this.renderButtonSubmit()}
            </ScrollView>
        )
    }
}

const mtp = ({ Auth, SimasJiwa, ErrorHandling }) => {
    const { authUser } = Auth
    const { errorMessage, showGlobalLoading } = ErrorHandling
    const { proposal, fileAttachment, isTertanggung } = SimasJiwa
    return { authUser, errorMessage, showGlobalLoading, proposal, fileAttachment, isTertanggung }
}

export default connect(mtp, { setStateFileAttachmentSimas, sagaNextScreenSimas })(FormFileDocument)