import React, { Component } from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import { Grid, Col } from 'react-native-easy-grid'
import { connect } from 'react-redux'
import { ErbeListMenu, ErbeText } from 'component/v3'
import { ButtonElement } from 'component/rnelement'
import Style from 'stylescreen/CacheFormSubmit'
import { setDateFormat } from 'erbevalidation/Helper'
import { sagaNextScreenSimas } from 'erberedux/actions'

class ProposalCacheStarter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ...this.props.navigation.state.params
        }
    }

    async componentDidMount() {
        __DEV__ && console.log('CacheFormSubmit', this.state);
    }

    setStateSimas(state) {
        const { sagaNextScreenSimas } = this.props
        sagaNextScreenSimas('SimasCache', 'SimasStarter', state);
    }

    renderItems(item) {
        return (
            <TouchableOpacity style={Style.containerItem} onPress={() => this.setStateSimas(item)} underlayColor='rgba(239,239,239,1,0.9)'>
                <Grid>
                    <Col size={50}>
                        <Text style={Style.dateTitle}>Create On</Text>
                    </Col>
                    <Col size={50}>
                        <Text style={Style.dateText}>{setDateFormat(item.dateCreate)}</Text>
                    </Col>
                </Grid>
                <Grid>
                    <Col size={50}>
                        <Text style={Style.title}>Pemegang Polis</Text>
                        <Text style={Style.name}>{item.proposal.pemegangPolis.namaPp}</Text>
                    </Col>
                    <Col size={50}>
                        <Text style={Style.title}>Tertanggung</Text>
                        <Text style={Style.name}>{item.proposal.tertanggung.namaTt}</Text>
                    </Col>
                </Grid>
                {
                    item.isPartner === true &&
                    <Grid>
                        <Col size={50}>
                            <Text style={Style.title}>Downline Username</Text>
                            <Text style={Style.name}>{item.partner.downlineUsername}</Text>
                        </Col>
                        <Col size={50}>
                            <Text style={Style.title}>Downline HP</Text>
                            <Text style={Style.name}>{item.partner.downlinePhone}</Text>
                        </Col>
                    </Grid>
                }
            </TouchableOpacity>
        )
    }

    renderHeader() {
        return (
            <View>
                <View style={Style.info}>
                    <Text style={Style.infoText}>{`Sudah ada ${this.state.data.length} data spaj dalam handphone anda.`}</Text>
                    <Text style={Style.infoText}>Anda bisa memilih daftar spaj yang telah di simpan atau membuat baru</Text>
                </View>
                <ErbeText value={'Daftar SPAJ Tersimpan'} style={'h3'} />
            </View>
        )
    }

    render() {
        const { sagaNextScreenSimas } = this.props
        return (
            this.state.data.length > 0 &&
            <View>
                <ErbeListMenu
                    tipe="list"
                    data={this.state.data}
                    renderListItem={(item) => this.renderItems(item)}
                    keyExtractor={(item, index) => index.toString()}
                    ListHeaderComponent={() => this.renderHeader()}
                />
                <View style={Style.buttonArea}>
                    <ButtonElement
                        title="Buat SPAJ Baru"
                        type="solid"
                        onPress={() => sagaNextScreenSimas('SimasCache', 'SimasStarter')}
                        color="secondary"
                    />
                </View>
            </View>
        )
    }
}

const mtp = ({ SimasJiwa }) => {
    return { SimasJiwa }
}

export default connect(mtp, { sagaNextScreenSimas })(ProposalCacheStarter)