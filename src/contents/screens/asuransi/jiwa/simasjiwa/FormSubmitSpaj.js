import React, { Component } from 'react'
import { ScrollView, Text, View } from 'react-native'
import { InfoSpajSubmit } from 'component/simasjiwa'
import FormSpajStyle from 'stylescreen/FormSpajStyle'
import { connect } from 'react-redux'
import { sagaSubmitFormSimas, sagaSaveSubmitFormSimas, setKeyRootSimas, hideLoadingGenerateProposalSimas, hideLoadingUploadDocumentSimas, hideLoadingSubmitSpajSimas, hideButtonLoading } from 'erberedux/actions'
import { ButtonElement } from 'component/rnelement'
import { ErbeText, TitleComponent, DisconnectView } from 'component/v3'
import Color from 'style/Color'
import NetInfo, { NetInfoSubscription, NetInfoState } from "@react-native-community/netinfo"
import md5 from 'md5'
import moment from 'moment'

export class FormSubmitSpaj extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isConnected: null,
        }
    }

    _handleConnectionInfoChange = (state) => {
        console.log('connectionInfo', state)
        this.setState({ isConnected: state.isConnected });
    };

    componentDidMount() {
        const { setKeyRootSimas, keyStorage, hideButtonLoading, hideLoadingSubmitSpajSimas, hideLoadingGenerateProposalSimas, hideLoadingUploadDocumentSimas } = this.props
        const dateNow = moment().format('YYYYMMDD hhmmss');
        setKeyRootSimas('dateCreate', dateNow)
        setKeyRootSimas('successFileUpload', false)
        setKeyRootSimas('successGenerateProposal', false)
        setKeyRootSimas('successSubmitSpaj', false)
        if (keyStorage == '') {
            const keyStorage = md5('simas' + moment().format('DDMMYYYYhmss'))
            setKeyRootSimas('keyStorage', keyStorage)
        }
        this._subscription = NetInfo.addEventListener(
            this._handleConnectionInfoChange,
        );
        hideButtonLoading()
        hideLoadingUploadDocumentSimas()
        hideLoadingGenerateProposalSimas()
        hideLoadingSubmitSpajSimas()
    }

    componentWillUnmount() {
        this._subscription && this._subscription();
    }

    renderTitle() {
        const { isPartner } = this.props
        return (
            <TitleComponent
                value={`Submit Data SPAJ ${isPartner ? 'Downline' : 'Nasabah'}`}
            />
        )
    }

    renderDisconnect() {
        return (
            <View>
                <DisconnectView />
                <InfoSpajSubmit isConnected={this.state.isConnected} />
            </View>
        )
    }

    renderConnect() {
        return (
            <InfoSpajSubmit isConnected={this.state.isConnected} />
        )
    }

    renderButtonSubmit() {
        const connect = this.state.isConnected
        const { sagaSubmitFormSimas, sagaSaveSubmitFormSimas } = this.props
        return (
            <View>
                {
                    connect &&
                    <ButtonElement
                        title={'Submit'}
                        type="solid"
                        onPress={async () => sagaSubmitFormSimas()}
                        theme={'secondary'}
                    />
                    ||
                    <ButtonElement
                        title={'Save This Data'}
                        type="solid"
                        onPress={async () => sagaSaveSubmitFormSimas()}
                        theme={'secondary'}
                    />
                }
            </View>
        )
    }

    render() {
        return (
            <ScrollView style={FormSpajStyle.container}>
                {this.renderTitle()}
                {this.state.isConnected ? this.renderConnect() : this.renderDisconnect()}
                {this.renderButtonSubmit()}
            </ScrollView>
        )
    }
}

const mtp = ({ SimasJiwa }) => {
    const { isPartner, product, proposal, spaj, signature, fileAttachment, keyStorage } = SimasJiwa
    return { isPartner, product, proposal, spaj, signature, fileAttachment, keyStorage, SimasJiwa }
}

export default connect(mtp, { sagaSubmitFormSimas, sagaSaveSubmitFormSimas, setKeyRootSimas, hideButtonLoading, hideLoadingGenerateProposalSimas, hideLoadingUploadDocumentSimas, hideLoadingSubmitSpajSimas })(FormSubmitSpaj)
