import React from 'react'
import { ScrollView, View, StyleSheet, Alert } from 'react-native'
import { ButtonElement } from "component/rnelement";
import { TitleComponent } from 'component/v3';
import { captureRef } from 'react-native-view-shot'
import Color from 'style/Color'
import { navigate } from 'erbeservice/AppNavigation';

export default class BaseSimasJiwa extends React.Component {
    constructor(props) {
        super(props)
        this.labelButtonSubmit = 'Next';
        this.state = {}
    }

    async componentDidMount() {
        console.log('componentDidMount BaseSceenMsig');
    }

    renderTitleContent() {
        return (
            <View style={styles.content}>
                <TitleComponent
                    value={'Data Calon Pemegang Polis'}
                />
            </View>
        )
    }

    renderFormSpaj() {
        return (
            <View style={styles.content}>

            </View>
        )
    }

    renderButton() {
        return (
            <ButtonElement
                title={this.labelButtonSubmit}
                onPress={() => this.onSubmitForm()}
                style={'secondary'}
            />
        );
    }

    renderFormSimasJiwa() {
        return (
            <ScrollView style={styles.container} ref="full">
                {this.renderTitleContent()}
                {this.renderFormSpaj()}
                {this.renderButton()}
            </ScrollView>
        )
    }

    render() {
        if (this.state.isLoading === true) return this.renderLoading();
        return this.renderFormSimasJiwa()
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white
    },
    content: {
        backgroundColor: Color.white
    }
})