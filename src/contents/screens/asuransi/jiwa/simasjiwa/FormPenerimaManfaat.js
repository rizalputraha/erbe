import React, { Component } from 'react'
import { View, ScrollView, StyleSheet, Alert } from 'react-native'
import { ButtonElement } from 'component/rnelement'
import {
    BirthdayPenerimaManfaat,
    InputComponent,
    TitleComponent,
} from 'component/v3'
import Color from 'style/Color'
import { GenderOptions, RelationManfaatOptions } from 'component/simasjiwa'
import { navigate } from "erbeservice/AppNavigation";
import { submitFormPenerimaManfaat } from 'erbevalidation/SimasJiwa'
import { connect } from 'react-redux'
import { showErrorMessage, setStateSpajPMSimas, sagaNextScreenSimas } from "erberedux/actions";


export class FormPenerimaManfaat extends Component {

    constructor(props) {
        super(props);
        this.state = {
            nama_pm: '',
            jk_pm: undefined,
            kota_pm: '',
            tgl_lahir_pm: '',
            usia_pm: '',
            hubungan_pm: false,
        }
    }

    async _changeState(value, parentState) {
        await this.setState({ [parentState]: value });
    }

    async onSubmit() {
        const { spaj, sagaNextScreenSimas } = this.props;
        sagaNextScreenSimas('SimasPenerimaManfaat', 'SimasKuisionerHealth', spaj.investasi.ahliwaris[0]);
    }

    renderTitleContent() {
        return (
            <View style={styles.content}>
                <TitleComponent
                    value={'Data Penerima Manfaat'}
                />
            </View>
        )
    }

    renderPenerimaManfaat() {
        const { spaj, setStateSpajPMSimas } = this.props;
        return (
            <View style={styles.content}>
                <InputComponent
                    label={'Nama Penerima Manfaat'}
                    placeholder={''}
                    value={spaj.investasi.ahliwaris[0].nama}
                    onChangeText={(value) => setStateSpajPMSimas('nama', value)}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={100}
                />

                <BirthdayPenerimaManfaat
                    setDob={(value) => setStateSpajPMSimas('dobAhliWaris', value)}
                    getDob={spaj.investasi.ahliwaris[0].dobAhliWaris}
                    minYear={18}
                    minDay={1}
                    minWeek={2}
                    maxYear={70}
                    titleButton={'Tanggal Lahir'}
                />

                <GenderOptions
                    selected={spaj.investasi.ahliwaris[0].genderAhliWaris}
                    onChangeItem={(value) => setStateSpajPMSimas('genderAhliWaris', value)}
                />

                <InputComponent
                    label={'Kota Tempat Lahir'}
                    placeholder={''}
                    value={spaj.investasi.ahliwaris[0].tptLahir}
                    onChangeText={(value) => setStateSpajPMSimas('tptLahir', value)}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={100}
                />

                <RelationManfaatOptions
                    selected={spaj.investasi.ahliwaris[0].hubunganAhliWaris}
                    onChangeItem={(value) => setStateSpajPMSimas('hubunganAhliWaris', value)}
                />

            </View>
        );
    }

    renderButton() {
        return (
            <ButtonElement
                title={'Next'}
                style={'secondary'}
                onPress={() => this.onSubmit()}
            />
        );
    }

    render() {
        return (
            <ScrollView style={styles.container} ref="full">
                {this.renderTitleContent()}
                {this.renderPenerimaManfaat()}
                {this.renderButton()}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Color.white
    },
    content: {
        backgroundColor: Color.white
    }
})

const mtp = ({ SimasJiwa }) => {
    const { isTertanggung, spaj } = SimasJiwa
    return { spaj, isTertanggung }
}

export default connect(mtp, { showErrorMessage, setStateSpajPMSimas, sagaNextScreenSimas })(FormPenerimaManfaat)