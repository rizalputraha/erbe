import React, { Component } from 'react'
import { View, ScrollView, Alert } from 'react-native'
import { ListItem, CheckBox } from 'react-native-elements';
import {
    setStateProposalProductSimas,
    setKeyRootSimas,
    setStateProposalTTSimas,
    setStateSpajPPSimas,
    sagaNextScreenSimas
} from "erberedux/actions";
import { connect } from 'react-redux';
import { ModalListItemArray, ProductAsuransiJiwa } from "component/v3";
import { ButtonElement } from "component/rnelement";
import { push } from 'erbeservice/AppNavigation';
import { formatCurrency } from "erbevalidation/Helper";
import Color from "style/Color";
import { RelationOptions } from 'component/simasjiwa'

export class ProposalProduk extends Component {
    constructor(props) {
        super(props);
        this.state = {
            agreeProposal: false,
        }
    }

    async submitForm() {
        const { sagaNextScreenSimas, isAgreeProduk } = this.props;
        if (isAgreeProduk == false) {
            await Alert.alert('Error', 'Mohon check kolom "Saya setuju"');
        } else {
            if (this.props.product == false) {
                await Alert.alert('Error', 'Mohon pilih Produk');
            } else {
                sagaNextScreenSimas('SimasProduk', 'SimasFormPP', '')
            }
        }
    }

    renderRider() {
        const { product } = this.props
        return (
            product.rider &&
            <View>
                <ListItem
                    containerStyle={{ backgroundColor: Color.lambo }}
                    titleStyle={{ color: Color.white }}
                    title='Rider Investasi'
                    bottomDivider />
                {product.rider.map((item, index) => {
                    return (
                        <ListItem
                            key={index}
                            title={item.rider_name}
                            bottomDivider />
                    )
                })}
            </View>
        )
    }

    renderFund() {
        const { product } = this.props;
        return (
            product.fund &&
            <View>
                <ListItem
                    containerStyle={{ backgroundColor: Color.lambo }}
                    titleStyle={{ color: Color.white }}
                    title='Investasi Asuransi'
                    bottomDivider />
                {product.fund.map((item, index) => {
                    return (
                        <ListItem
                            key={index}
                            title={item.fund_name}
                            bottomDivider />
                    )
                })}
            </View>
        )
    }



    rendeInfoPackage() {
        const { periodeBayar, product, proposal, setKeyRootSimas, isAgreeProduk } = this.props;
        let masa_pertanggungan = 100 - proposal.pemegangPolis.usiaPp;
        return (
            (product.periode_bayar && periodeBayar.name) &&
            <View>
                <ListItem
                    containerStyle={{ backgroundColor: Color.lambo }}
                    titleStyle={{ color: Color.white }}
                    title='Package Product Utama'
                    bottomDivider />
                <ListItem
                    title={product.product_name}
                    bottomDivider />
                <ListItem
                    title={'Cara bayar ' + periodeBayar.name}
                    bottomDivider />
                <ListItem
                    title={'Masa Pertanggungan ' + masa_pertanggungan + ' Tahun'}
                    bottomDivider />
                <ListItem
                    title={'Total Premi ' + formatCurrency(product.premi)}
                    bottomDivider />
                <ListItem
                    title={'Premi Pokok ' + formatCurrency(product.premi_pokok)}
                    bottomDivider />
                <ListItem
                    title={'Premi Topup ' + formatCurrency(product.premi_topup)}
                    bottomDivider />
                <ListItem
                    title={'Cuti Premi setelah tahun ke ' + product.thn_cuti_premi + ' Tahun'}
                    bottomDivider />
                <ListItem
                    title={'Masa Pembayaran Premi ' + product.thn_masa_bayar + ' Tahun'}
                    bottomDivider />
                <ListItem
                    title={'Masa Asuransi ' + masa_pertanggungan + ' Tahun'}
                    bottomDivider />
                <ListItem
                    title={'Uang Pertanggungan ' + formatCurrency(product.up)}
                    bottomDivider />

                {this.renderRider()}

                {this.renderFund()}

                <CheckBox
                    title='Saya setuju dengan product tersebut diatas.'
                    checked={isAgreeProduk}
                    onPress={() => setKeyRootSimas('isAgreeProduk', !isAgreeProduk)}
                />
            </View>
            ||
            <View></View>
        )
    }

    renderRelationOption() {
        const { periodeBayar, proposal, setStateProposalTTSimas, setStateSpajPPSimas } = this.props
        return (
            periodeBayar &&
            <View>
                <ListItem
                    containerStyle={{ backgroundColor: Color.lambo }}
                    titleStyle={{ color: Color.white }}
                    title='Tentukan Hubungan Dengan Pemegang Polis'
                    bottomDivider />
                <RelationOptions
                    selected={proposal.tertanggung.relationId}
                    onChangeItem={(value) => {
                        console.log('renderRelationOption', { res: value, prop: this.props }),
                            setStateProposalTTSimas('relationId', value),
                            setStateSpajPPSimas('hubunganPp', value)
                    }}
                />
            </View>
        )
    }

    renderPeriodeBayar() {
        const { product, periodeBayar, setKeyRootSimas } = this.props;
        return (
            product.periode_bayar
            &&
            <View>
                <ListItem
                    containerStyle={{ backgroundColor: Color.lambo }}
                    titleStyle={{ color: Color.white }}
                    title='Tentukan Cara Bayar'
                    bottomDivider />
                <ModalListItemArray
                    data={product.periode_bayar}
                    labelItem={'name'}
                    title={'Cara Bayar'}
                    titleOptions={'Pilih Cara Bayar'}
                    onChangeItem={(value) => setKeyRootSimas('periodeBayar', { ...value })}
                    defaultSelected={periodeBayar.name}
                />
            </View>
        )
    }

    renderProduct() {
        const { productProposal, setKeyRootSimas, proposal, product } = this.props;
        return (
            productProposal &&
            <ProductAsuransiJiwa
                proposal={proposal}
                value={product.product_sub}
                data={productProposal}
                title={'Product'}
                labelItem={'product_sub'}
                titleOptions={'Pilih Product'}
                onChangeItem={(value) => {
                    setKeyRootSimas('product', { ...value })
                }}
                defaultSelected={false} />
        )
    }



    renderButtonSubmit() {
        return (
            <ButtonElement onPress={() => this.submitForm()} title="Next" type="solid" theme="secondary" />
        );
    }

    render() {
        return (
            <ScrollView>
                <ListItem
                    containerStyle={{ backgroundColor: Color.lambo }}
                    titleStyle={{ color: Color.white }}
                    title='Tentukan Pilihan Produk'
                    bottomDivider
                />
                {this.renderProduct()}
                {this.renderPeriodeBayar()}
                {this.renderRelationOption()}
                {this.rendeInfoPackage()}
                {this.renderButtonSubmit()}
            </ScrollView>
        )
    }
}

const mtp = ({ Insurance, SimasJiwa }) => {
    const { productProposal } = Insurance
    const { proposal, product, periodeBayar, isAgreeProduk } = SimasJiwa
    return { productProposal, proposal, product, periodeBayar, isAgreeProduk }
}

export default connect(mtp, { sagaNextScreenSimas, setKeyRootSimas, setStateProposalTTSimas, setStateProposalProductSimas, setStateSpajPPSimas })(ProposalProduk)