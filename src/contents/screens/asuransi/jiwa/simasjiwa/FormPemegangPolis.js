import React, { Component } from 'react'
import { Text, View, ScrollView, Alert } from 'react-native'
import {
    InputComponent,
    ItemComponent,
} from 'component/v3'
import Styles from "stylescreen/FormPemegangPolis";
import Color from 'style/Color';
import { formatPhoneNumber, submitFormPP } from 'erbevalidation/SimasJiwa'
import BaseSimasJiwa from './BaseSimasJiwa';
import { connect } from 'react-redux'
import { masterQuestioner } from 'erbecache/model/SimasModel'
import { setStateSpajPPSimas, setSpajPPtoTTSimas, sagaNextScreenSimas, setStateSpajCacheStorageSimas, loadStorageQuestionerSimas } from 'erberedux/actions'
import { NpwpOptionPemegangPolis, BankOptions, ReligionOptions, StatusOptions, RelationOptions, GraduatedOptions, IdentityOptions, WilayahOptions } from 'component/simasjiwa'

class FormPemegangPolis extends BaseSimasJiwa {
    constructor(props) {
        super(props);
        this.state = {}
    }

    async componentDidMount() {
        const { spaj, product, loadStorageQuestionerSimas } = this.props
        if (spaj.questioner.length == 0) {
            /**
             * Set questioner and profileResiko state reducer from AsynStorage data
             * Used to list of question and answer to users
             */
            const questionerCache = await masterQuestioner()
            loadStorageQuestionerSimas(questionerCache, product.product_sub_code);
        }
    }

    async _changeState(value, parentState) {
        await this.setState({ [parentState]: value });
    }

    _setValidationTelephon(value, parentState) {
        this.setState({ [parentState]: value })
        const check = formatPhoneNumber(this.state[parentState])
        if (check === false) {
            this.setState({ [parentState]: '' })
        }
    }

    async onSubmitForm() {
        const { spaj, sagaNextScreenSimas } = this.props
        if (this.props.isTertanggung === true) {
            sagaNextScreenSimas('SimasFormPP', 'SimasPenerimaManfaat', spaj.pemegangPolis)
        } else {
            sagaNextScreenSimas('SimasFormPP', 'SimasFormTT', spaj.pemegangPolis)
        }
    }

    renderDataPP() {
        const { setStateSpajPPSimas, spaj, proposal } = this.props;
        return (
            <View style={Styles.content}>
                <InputComponent
                    label={'Nama Pemegang Polis'}
                    placeholder={''}
                    value={spaj.pemegangPolis.namaPp}
                    onChangeText={(value) => setStateSpajPPSimas('namaPp', value)}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={200}
                    editable={false}
                />
                <InputComponent
                    label={'Nama Ibu'}
                    placeholder={''}
                    value={spaj.pemegangPolis.namaIbuPp}
                    onChangeText={(value) => setStateSpajPPSimas('namaIbuPp', value)}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={200}
                />

                <InputComponent
                    label={'Kota Kelahiran'}
                    placeholder={'Masukkan Kota Kelahiran'}
                    value={spaj.pemegangPolis.tptLahirPp}
                    onChangeText={(value) => setStateSpajPPSimas('tptLahirPp', value)}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={30}
                />

                <ItemComponent
                    label={'Tanggal Lahir'}
                    value={spaj.pemegangPolis.dobPp}
                />

                <ItemComponent
                    label={'Usia'}
                    value={spaj.pemegangPolis.usiaPp}
                />

                <RelationOptions
                    selected={spaj.pemegangPolis.hubunganPp}
                    onChangeItem={(value) => {
                        console.log('FormPemegangPolis ReligionOptions onChangeItem', value),
                            setStateSpajPPSimas('hubunganPp', value)
                    }}
                    onChangeText={(value) => {
                        console.log('FormPemegangPolis ReligionOptions onChangeText', value),
                            setStateSpajPPSimas('hubunganLainPp', value)
                    }}
                />

                <ReligionOptions
                    selected={spaj.pemegangPolis.agamaPp}
                    onChangeItem={(value) => {
                        console.log('FormPemegangPolis ReligionOptions onChangeItem', value),
                            setStateSpajPPSimas('agamaPp', value)
                    }}
                    onChangeText={(value) => {
                        console.log('FormPemegangPolis ReligionOptions onChangeText', value),
                            setStateSpajPPSimas('agamaLainPp', value)
                    }}
                />

                <ItemComponent
                    label={'Jenis Kelamin'}
                    value={proposal.pemegangPolis.genderPp == 0 ? 'WANITA' : 'PRIA'}
                />

                <StatusOptions
                    selected={spaj.pemegangPolis.statusPp}
                    onChangeItem={(value) => {
                        console.log('FormPemegangPolis ReligionOptions onChangeItem', value),
                            setStateSpajPPSimas('statusPp', value)
                    }}
                />
            </View>
        )
    }

    renderPendidikan() {
        const { setStateSpajPPSimas, spaj } = this.props;

        return (
            <View>
                <GraduatedOptions
                    selected={spaj.pemegangPolis.pendidikanPp}
                    onChangeItem={(value) => {
                        console.log('FormPemegangPolis ReligionOptions onChangeItem', value),
                            setStateSpajPPSimas('pendidikanPp', value)
                    }}
                />

                <InputComponent
                    label={'Gelar'}
                    placeholder={'Dr.'}
                    value={spaj.pemegangPolis.gelarPp}
                    onChangeText={(value) => setStateSpajPPSimas('gelarPp', value)}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={50}
                />
            </View>
        )
    }

    renderIdentitas() {
        const { setStateSpajPPSimas, spaj } = this.props;
        return (
            <View>
                <ItemComponent
                    label={'No Hp'}
                    value={spaj.pemegangPolis.noHpPp}
                />

                <ItemComponent
                    label={'Email'}
                    value={spaj.pemegangPolis.emailPp}
                />

                <IdentityOptions
                    selected={spaj.pemegangPolis.buktiIdentitasPp}
                    onChangeItem={(value) => {
                        console.log('FormPemegangPolis ReligionOptions onChangeItem', value),
                            setStateSpajPPSimas('buktiIdentitasPp', value)
                    }}
                />
                <InputComponent
                    label={'No Bukti Identitas'}
                    placeholder={''}
                    value={spaj.pemegangPolis.identitasPp}
                    onChangeText={(value) => setStateSpajPPSimas('identitasPp', value)}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={50}
                    keyboardType={spaj.pemegangPolis.identitasPp == 1 || spaj.pemegangPolis.identitasPp == 9 ? 'numeric' : 'default'}
                />
            </View>
        )
    }

    renderLembagaKerja() {
        const { setStateSpajPPSimas, spaj } = this.props
        return (
            <View >
                <InputComponent
                    label={'Pekerjaan'}
                    placeholder={''}
                    value={spaj.pemegangPolis.jobDescPp}
                    onChangeText={(value) => {
                        setStateSpajPPSimas('jobDescPp', value)
                    }}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={50}
                />

                <InputComponent
                    label={'Penghasilan'}
                    placeholder={''}
                    value={spaj.pemegangPolis.penghasilanPp}
                    onChangeText={(value) => {
                        setStateSpajPPSimas('penghasilanPp', value)
                    }}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={50}
                />
            </View>
        )
    }

    renderTempatTinggal() {
        const { setStateSpajPPSimas, spaj } = this.props;
        return (
            <View style={Styles.content}>

                <WilayahOptions
                    selectedProvinsi={spaj.pemegangPolis.provinsiPp}
                    selectedKota={spaj.pemegangPolis.kotaPp}
                    onChangeProvinsi={(value) => setStateSpajPPSimas('provinsiPp', value)}
                    onChangeKota={(value) => setStateSpajPPSimas('kotaPp', value)}
                />

                <InputComponent
                    label={'Alamat Rumah (jika sesuai KTP)'}
                    placeholder={''}
                    value={spaj.pemegangPolis.addressPp}
                    onChangeText={(value) => setStateSpajPPSimas('addressPp', value)}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={200}
                />

                <InputComponent
                    label={'Kode Pos'}
                    placeholder={'Kode Pos'}
                    value={spaj.pemegangPolis.kdPosPp}
                    onChangeText={(value) => setStateSpajPPSimas('kdPosPp', value)}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={200}
                />
            </View>
        );
    }

    renderBankPemegangPolis() {
        const { setStateSpajPPSimas, spaj } = this.props;
        return (
            <View style={Styles.content}>
                <NpwpOptionPemegangPolis />
                <InputComponent
                    label={'Nama Rekening Bank Pemegang Polis'}
                    placeholder={''}
                    value={spaj.pemegangPolis.namaRekeningPp}
                    onChangeText={(value) => setStateSpajPPSimas('namaRekeningPp', value)}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={100}
                />
                <InputComponent
                    label={'Kota Rekening'}
                    placeholder={''}
                    value={spaj.pemegangPolis.cityRekPp}
                    onChangeText={(value) => setStateSpajPPSimas('cityRekPp', value)}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={100}
                />
                <BankOptions
                    selectedBank={spaj.pemegangPolis.bankIdPp}
                    selectedBranch={spaj.pemegangPolis.branchIdPp}
                    onChangeBank={(value) => setStateSpajPPSimas('bankIdPp', value)}
                    onChangeBranch={(value) => setStateSpajPPSimas('branchIdPp', value)}
                />
                <InputComponent
                    label={'Nomor Rekening'}
                    placeholder={''}
                    value={spaj.pemegangPolis.noRekPp}
                    onChangeText={async (value) => {
                        if (spaj.pemegangPolis.bankIdPp == '') {
                            Alert.alert('Pilih bank terlebih dahulu !');
                        } else {
                            await setStateSpajPPSimas('noRekPp', value)
                        }
                    }}
                    validation={['required', 'length']}
                    minCharacter={3}
                    maxCharacter={16}
                    keyboardType={'numeric'}
                />
            </View>
        )
    }

    renderInfoPolis() {
        return (
            <View style={Styles.content}>
                <View style={Styles.title}>
                    <Text style={Styles.titleText}>Polis dikirim melalui</Text>
                </View>
                <Text style={Styles.infoPolis}>Softcopy Polis dikirim ke alamat email Ikhtisar Polis dikirim melalui Kurir/ POS</Text>
            </View>
        )
    }

    renderFormSpaj() {
        return (
            <ScrollView style={{ backgroundColor: Color.white }}>
                {this.renderDataPP()}
                {this.renderTempatTinggal()}
                {this.renderPendidikan()}
                {this.renderIdentitas()}
                {this.renderLembagaKerja()}
                {this.renderBankPemegangPolis()}
                {this.renderInfoPolis()}
            </ScrollView>
        )
    }
}

const mtp = ({ SimasJiwa }) => {
    const { spaj, proposal, isTertanggung, product } = SimasJiwa
    return { spaj, proposal, isTertanggung, product }
}

export default connect(mtp, { setStateSpajPPSimas, setSpajPPtoTTSimas, sagaNextScreenSimas, setStateSpajCacheStorageSimas, loadStorageQuestionerSimas })(FormPemegangPolis)