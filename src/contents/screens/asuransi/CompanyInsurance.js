import React from 'react'
import { View, Text, Animated, TouchableOpacity } from 'react-native'
import { ButtonElement, CardElement } from 'component/rnelement'
import { popToTop } from 'erbeservice/AppNavigation'
import { connect } from 'react-redux'
import { sagaInsuranceCompany,pushToScreen } from 'erberedux/actions'
import Style from 'stylescreen/Insurance'
import Color from 'style/Color'
import MainStyle from 'style/MainStyle'
import { Card } from 'react-native-elements'
import { getCompanyInsuranceImages } from 'erbevalidation/Helper'
import { push } from 'erbeservice/AppNavigation'
import { sagaSelectedPerusahaan } from '../../../redux/actions'

class CompanyInsurance extends React.Component {

    constructor(props) {
        super(props)
        this.state={
            opacity: new Animated.Value(0.5),
        }
    }

    animateLoading() {
        Animated.loop(Animated.timing(this.state.opacity, {
            toValue: 1,
            duration: 2000
        })).start()
    }
    

    componentDidMount() {
        this.animateLoading();
    }

    renderLoading() {
        return(
            <Animated.View style={{width: '100%', opacity: this.state.opacity, height: 100}}>
                <Card>
                    <View style={{width: '100%', height: 100, backgroundColor: Color.greyLight }}></View>
                    <View style={{width: '100%', height: 20, backgroundColor: Color.greyLight, marginTop:  MainStyle.margin}}></View>
                </Card>
                <Card>
                    <View style={{width: '100%', height: 100, backgroundColor: Color.greyLight }}></View>
                    <View style={{width: '100%', height: 20, backgroundColor: Color.greyLight, marginTop:  MainStyle.margin}}></View>
                </Card>
            </Animated.View>
        )
    }

    daftar(item) {
        this.props.sagaSelectedPerusahaan(item);
    }

    renderData() {        
        if(this.props.company.length > 0) {
            return this.props.company.map((item, index) => {
                return(
                    <TouchableOpacity key={item.id} underlayColor="rgba(0, 0, 0, 0)" onPress={() => this.daftar(item)}>
                        <Card
                            title={item.name}
                            image={getCompanyInsuranceImages(item.logo)}
                        >
                        </Card>
                    </TouchableOpacity>
                )
            })
        } else {
            return(
                <View>
                    <Text>Data Tidak Ada</Text>
                </View>
            )
        }
    }

    renderListCompany() {
        if(this.props.showGlobalLoading === true) return this.renderLoading()
        return this.renderData()
    }

    render() {
        return(
            <View style={Style.container}>
                {this.renderListCompany()}
            </View>
        )
    }
}

const mtp = ({Auth, Insurance, ErrorHandling}) => {
    const { authUser } = Auth
    const { company,selectedCompany } = Insurance
    const { showGlobalLoading } = ErrorHandling
    return { authUser, company, showGlobalLoading }
}

export default connect(mtp, {sagaInsuranceCompany,sagaSelectedPerusahaan,pushToScreen})(CompanyInsurance)