import React from 'react'
import { ScrollView } from 'react-native'
import { createAppContainer } from 'react-navigation'
import { SafeAreaProvider } from 'react-native-safe-area-context'
import SafeAreaView from 'react-native-safe-area-view'
import { createDrawerNavigator, DrawerNavigatorItems } from 'react-navigation-drawer'
import { createStackNavigator } from 'react-navigation-stack'
import { Icon } from 'react-native-elements'
import Style from 'style/stylescreen/Home'
import LoginNavigation from './LoginNavigation'
import RegisterNavigation from './RegisterNavigation'
import ForgotPasswordNavigation from './ForgotPasswordNavigation'
import MainStyle from 'style/MainStyle'
import {
    WhoWeAre,
    WhyUs,
    OurJob,
    ContactUs
} from 'screen/auth'

const CustomDrawerContentComponent = props => (
    <ScrollView>
        <SafeAreaProvider>
            <SafeAreaView
                style={Style.container}
                forceInset={{ top: 'always', horizontal: 'never' }}
            >
                <DrawerNavigatorItems {...props} />
            </SafeAreaView>
        </SafeAreaProvider>
    </ScrollView>
);

const AuthNavigator = createDrawerNavigator(
    {
        Login : {
            screen: LoginNavigation,
            navigationOptions: {
                title: 'Login Pengguna'
            }
        },
        Register : {
            screen: RegisterNavigation,
            navigationOptions: {
                title: 'Daftar Akun Baru'
            }
        },
        ForgotPassword: {
            screen: ForgotPasswordNavigation,
            navigationOptions: {
                title: 'Lupa Password'
            }
        },
        WhoWeAre: {
            screen: WhoWeAre,
            navigationOptions: {
                title: 'Tentang Erbe'
            }
        },
        WhyUs: {
            screen: WhyUs,
            navigationOptions: {
                title: 'Kenapa Erbenetwork'
            }
        },
        OurJob: {
            screen: OurJob,
            navigationOptions: {
                title: 'Info Job'
            }
        },
        ContactUs: {
            screen: ContactUs,
            navigationOptions: {
                title: 'Hubungi Kami'
            }
        }
    },
    { 
        contentComponent: CustomDrawerContentComponent,
    }
)

const InitNavigator = createStackNavigator({
    Intro: {
        screen: AuthNavigator,
        headerMode: 'float',
        navigationOptions: ({ navigation }) => ({
            headerStyle: { ...MainStyle.header },
            headerLeft: <Icon 
                            containerStyle={{ paddingLeft: 10 }} 
                            name="menu" color="#fff" size={30} 
                            underlayColor={'rgba(198, 40, 40, 1)'} 
                            onPress={() => navigation.toggleDrawer()}
                        />
        })
    }
})

export default createAppContainer(InitNavigator)