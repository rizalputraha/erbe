import React from 'react'
import { createStackNavigator } from 'react-navigation-stack'
import { Report } from 'screen/dashboard'
import { ReportReferal,ReportKomisiBonus } from 'screen/report'
import Color from 'style/Color'

const RouteConfigs = {
    Report: {
        screen: Report,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },
    ReportReferal: {
        screen: ReportReferal,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    ReportKomisiBonus: {
        screen: ReportKomisiBonus,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    }
}

const StackNavigatorConfig = {

}

const ReportNavigation = createStackNavigator(RouteConfigs, StackNavigatorConfig)

ReportNavigation.navigationOptions = ({ navigation }) => {
    if(navigation.state.index==0){
      return {
          tabBarVisible: true,
      };
    }
    return {
        tabBarVisible: false,
    };
};

export default ReportNavigation