import React from 'react'
import { createStackNavigator } from 'react-navigation-stack'
import { Invoice } from 'screen/dashboard'
import { 
    InvoiceCategory,
    InvoicePembayaran,
    InvoiceStatusPembayaran,
    InvoiceSpajSelect,
    InvoiceStatus,
    InvoicePdf,
    InvoicePay
} from 'screen/invoice'

const RouteConfigs = {
    Invoice: {
        screen: Invoice,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },
    InvoiceCategory,
    InvoicePembayaran: {
        screen: InvoicePembayaran,
        navigationOptions: ({navigation}) => ({
            title: 'Daftar Pembayaran'
        })
    },
    InvoiceStatusPembayaran: {
        screen: InvoiceStatusPembayaran,
        navigationOptions: ({navigation}) => ({
            title: 'Report Status Pembayaran'
        })
    },
    InvoiceSpajSelect: {
        screen: InvoiceSpajSelect,
        navigationOptions: ({navigation}) => ({
            title: 'Pembayaran Premi'
        })
    },
    InvoiceStatus: {
        screen: InvoiceStatus,
        navigationOptions: ({navigation}) => ({
            title: 'Status Pembayaran'
        })
    },
    InvoicePdf: {
        screen: InvoicePdf,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    InvoicePaySpaj: {
        screen: InvoicePay,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    }
}

const StackNavigatorConfig = {

}

const InvoiceNavigation = createStackNavigator(RouteConfigs, StackNavigatorConfig)

InvoiceNavigation.navigationOptions = ({ navigation }) => {
    if(navigation.state.index==0){
      return {
          tabBarVisible: true,
      };
    }
    return {
        tabBarVisible: false,
    };
};

export default InvoiceNavigation