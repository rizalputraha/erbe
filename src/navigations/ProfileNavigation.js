import React from 'react'
import { createStackNavigator } from 'react-navigation-stack'
import { Profile } from 'screen/dashboard'
import { UserProfile } from 'screen/profile'

const RouteConfigs = {
    Profile: {
        screen: Profile,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },
    UserProfile
}

const StackNavigatorConfig = {

}

const ProfileNavigation = createStackNavigator(RouteConfigs, StackNavigatorConfig)

ProfileNavigation.navigationOptions = ({ navigation }) => {
    if(navigation.state.index==0){
      return {
          tabBarVisible: true,
      };
    }
    return {
        tabBarVisible: false,
    };
};

export default ProfileNavigation