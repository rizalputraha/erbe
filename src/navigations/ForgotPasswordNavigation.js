import React from 'react'
import { createStackNavigator } from 'react-navigation-stack'
import { ForgotPassword, ForgotPasswordVerify } from 'screen/auth'

const RouteConfigs = {
    ForgotPassword: {
        screen: ForgotPassword,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },
    ForgotPasswordVerify: {
        screen: ForgotPasswordVerify,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    }
}

const StackNavigatorConfig = {

}

const ForgotPasswordNavigation = createStackNavigator(RouteConfigs, StackNavigatorConfig)

ForgotPasswordNavigation.navigationOptions = ({ navigation }) => {
    let drawerLockMode = 'unlocked';
    if (navigation.state.index > 0) {
        drawerLockMode = 'locked-closed';
    }

    return {
        drawerLockMode,
    };
};

export default ForgotPasswordNavigation