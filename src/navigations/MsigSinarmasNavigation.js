import React from 'react'
import { createStackNavigator, HeaderBackButton } from 'react-navigation-stack'
import {
    ProposalCategory,
    ProposalStarter,
    CacheFormSubmit,
    ProposalDownline,
    ProposalPemegangPolis,
    ProposalTertanggung,
    ProposalProduk,
    FormPemegangPolis,
    FormTertanggung,
    FormCalonPembayar,
    FormUsulanAsuransi,
    FormPenerimaManfaat,
    FormDetilAgen,
    FormKuisioner,
    FormProfileResiko,
    FormPersetujuanNasabah,
    FormSignaturePemegangPolis,
    FormSignatureTertanggung,
    FormFileDocument,
    FormSubmitSpaj
} from 'asuransijiwa/msigsinarmas'

const RouteConfigs = {
    MsigSinarmas: {
        // screen: ProposalCategory,
        screen: FormSubmitSpaj,
        navigationOptions: ({ navigation }) => ({
            headerTitle: 'Sinarmas MSIG Life',
            headerLeft: <HeaderBackButton onPress={() => navigation.goBack(null)} />
        })
    },
    MsigCache: {
        screen: CacheFormSubmit
    },
    MsigDownline: {
        screen: ProposalDownline,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    },
    MsigStarter: {
        screen: ProposalStarter
    },
    MsigProposalPp: {
        screen: ProposalPemegangPolis,
        navigationOptions: ({ navigation }) => ({
            headerTitle: 'Pemegang Polis'
        })
    },
    MsigProposalTt: {
        screen: ProposalTertanggung,
        navigationOptions: ({ navigation }) => ({
            headerTitle: 'Tertanggung'
        })
    },
    MsigProduk: {
        screen: ProposalProduk,
        navigationOptions: ({ navigation }) => ({
            headerTitle: 'Produk Msig'
        })
    },
    MsigPemegangPolis: {
        screen: FormPemegangPolis
    },
    MsigTertanggung: {
        screen: FormTertanggung
    },
    MsigCalonPembayar: {
        screen: FormCalonPembayar
    },
    MsigUsulanAsuransi: {
        screen: FormUsulanAsuransi
    },
    MsigPenerimaManfaat: {
        screen: FormPenerimaManfaat
    },
    MsigDetilAgen: {
        screen: FormDetilAgen
    },
    MsigKuisioner: {
        screen: FormKuisioner
    },
    MsigProfileResiko: {
        screen: FormProfileResiko
    },
    MsigPersetujuanNasabah: {
        screen: FormPersetujuanNasabah
    },
    MsigSignaturePp: {
        screen: FormSignaturePemegangPolis
    },
    MsigSignatureTt: {
        screen: FormSignatureTertanggung
    },
    MsigFileUpload: {
        screen: FormFileDocument
    },
    MsigSubmitForm: {
        screen: FormSubmitSpaj
    }

}

const StackNavigatorConfig = {
    defaultNavigationOptions: {
        headerTitle: 'Sinarmas MSIG Life'
    }
}

const MsigSinarmasNavigation = createStackNavigator(RouteConfigs, StackNavigatorConfig)

MsigSinarmasNavigation.navigationOptions = ({ navigation }) => {
    if (navigation.state.index == 0) {
        return {
            tabBarVisible: true,
        };
    }
    return {
        tabBarVisible: false,
    };
};

export default MsigSinarmasNavigation