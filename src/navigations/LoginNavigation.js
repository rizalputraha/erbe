import React from 'react'
import { createStackNavigator } from 'react-navigation-stack'
import { Login, LoginVerify } from 'screen/auth'

const RouteConfigs = {
    Login: {
        screen: Login,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },
    LoginVerify: {
        screen: LoginVerify,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    }
}

const StackNavigatorConfig = {

}

const LoginNavigation = createStackNavigator(RouteConfigs, StackNavigatorConfig)

LoginNavigation.navigationOptions = ({ navigation }) => {
    let drawerLockMode = 'unlocked';
    if (navigation.state.index > 0) {
        drawerLockMode = 'locked-closed';
    }

    return {
        drawerLockMode,
    };
};

export default LoginNavigation