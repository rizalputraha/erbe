import React from 'react'
import { createAppContainer } from 'react-navigation'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import Color from 'style/Color'
import Style from 'stylenavigation/StyleMainNavigation'
import { NextgIcon } from 'component/v3'
import HomeNavigation from './HomeNavigation'
import InvoiceNavigation from './InvoiceNavigation'
import ReportNavigation from './ReportNavigation'
import ProfileNavigation from './ProfileNavigation'

const RouteConfigs = {
    Home: {
        screen: HomeNavigation,
        navigationOptions: ({ navigation }) => ({
            tabBarLabel: 'Home',
            tabBarIcon: ({ tintColor }) => {
                return(
                    <NextgIcon 
                        name={'home-2'} 
                        size={20} 
                        color={tintColor}
                        style={Style.BottomTabNavigator}
                    />
                )
            }
        })
    },
    Invoice: {
        screen: InvoiceNavigation,
        navigationOptions: ({ navigation }) => ({
            tabBarLabel: 'Invoice',
            tabBarIcon: ({ tintColor }) => {
                return(
                    <NextgIcon 
                        name={'invoice-2'} 
                        size={20} 
                        color={tintColor}
                        style={Style.BottomTabNavigator}
                    />
                )
            }
        })
    },
    Report: {
        screen: ReportNavigation,
        navigationOptions: ({ navigation }) => ({
            tabBarLabel: 'Report',
            tabBarIcon: ({ tintColor }) => {
                return(
                    <NextgIcon 
                        name={'report-2'} 
                        size={20} 
                        color={tintColor}
                        style={Style.BottomTabNavigator}
                    />
                )
            }
        })
    },
    Profile: {
        screen: ProfileNavigation,
        navigationOptions: ({ navigation }) => ({
            tabBarLabel: 'Profile',
            tabBarIcon: ({ tintColor }) => {
                return(
                    <NextgIcon 
                        name={'profile-2'} 
                        size={20} 
                        color={tintColor}
                        style={Style.BottomTabNavigator}
                    />
                )
            }
        })
    }
}

const TabNavigatorConfig = {
    tabBarOptions: {
        activeTintColor: Color.activeTintColor,
        inactiveTintColor: Color.inactiveTintColor,
        style: { backgroundColor: Color.primary, paddingBottom: 10, height: Platform.OS=='ios'?100:70 },
        showIcon: true,
        showLabel: true,
        labelStyle: {
            fontSize: 14,
            fontFamily: 'DroidSans'
        },
        indicatorStyle: { backgroundColor: Color.inactiveTintColor },
    }
}

const BottomTabNavigator = createBottomTabNavigator(
    RouteConfigs, TabNavigatorConfig
)

export default createAppContainer(BottomTabNavigator)