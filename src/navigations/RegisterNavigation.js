import React from 'react'
import { createStackNavigator } from 'react-navigation-stack'
import { Register, RegisterUser, RegisterVerify } from 'screen/auth'

const RouteConfigs = {
    Register: {
        screen: Register,
        // params: { referal: '92mnvaar'},
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },
    RegisterUser: {
        screen: RegisterUser,
        navigationOptions: ({ navigation }) => ({
            title: 'Register',
        })
    },
    RegisterVerify: {
        screen: RegisterVerify,
        navigationOptions: ({ navigation }) => ({
            title: 'Verifikasi Registrasi',
        })
    }
}

const StackNavigatorConfig = {

}

const RegisterNavigation = createStackNavigator(RouteConfigs, StackNavigatorConfig)

RegisterNavigation.navigationOptions = ({ navigation }) => {
    let drawerLockMode = 'unlocked';
    if (navigation.state.index > 0) {
        drawerLockMode = 'locked-closed';
    }

    return {
        drawerLockMode,
    };
};

export default RegisterNavigation