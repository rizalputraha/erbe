import React from 'react'
import { createStackNavigator, HeaderBackButton } from 'react-navigation-stack'
import {
    ProposalDownline,
    ProposalCacheStarter,
    ProposalCategory,
    ProposalStarter,
    ProposalPemegangPolis,
    ProposalTertanggung,
    ProposalProduk,
    FormPemegangPolis,
    FormTertanggung,
    FormPenerimaManfaat,
    FormKuisionerHealth,
    FormKuisionerResiko,
    FormSignaturePemegangPolis,
    FormSignatureTertanggung,
    FormFileDocument,
    FormSubmitSpaj
} from 'asuransijiwa/simasjiwa'

const RouteConfigs = {
    SimasJiwa: {
        screen: ProposalCategory,
        navigationOptions: ({ navigation }) => ({
            headerTitle: 'Simas Jiwa',
            headerLeft: <HeaderBackButton onPress={() => navigation.goBack(null)} />
        })
    },
    SimasCache: {
        screen: ProposalCacheStarter
    },
    SimasDownline: {
        screen: ProposalDownline,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },
    SimasStarter: {
        screen: ProposalStarter,
    },
    SimasPP: {
        screen: ProposalPemegangPolis,
    },
    SimasTT: {
        screen: ProposalTertanggung,
    },
    SimasProduk: {
        screen: ProposalProduk,
    },
    SimasFormPP: {
        screen: FormPemegangPolis,
    },
    SimasFormTT: {
        screen: FormTertanggung,
    },
    SimasPenerimaManfaat: {
        screen: FormPenerimaManfaat,
    },
    SimasKuisionerHealth: {
        screen: FormKuisionerHealth,
    },
    SimasKuisionerResiko: {
        screen: FormKuisionerResiko,
    },
    SimasSignaturePP: {
        screen: FormSignaturePemegangPolis,
    },
    SimasSignatureTT: {
        screen: FormSignatureTertanggung,
    },
    SimasFileUpload: {
        screen: FormFileDocument,
    },
    SimasFormSubmit: {
        screen: FormSubmitSpaj,
    }
}

const StackNavigatorConfig = {
    defaultNavigationOptions: {
        headerTitle: 'Simas Jiwa'
    }
}

const SimasJiwaNavigation = createStackNavigator(RouteConfigs, StackNavigatorConfig)

SimasJiwaNavigation.navigationOptions = ({ navigation }) => {
    if (navigation.state.index == 0) {
        return {
            tabBarVisible: true,
        };
    }
    return {
        tabBarVisible: false,
    };

};

export default SimasJiwaNavigation