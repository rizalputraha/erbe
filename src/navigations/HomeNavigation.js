import React from 'react'
import { createStackNavigator } from 'react-navigation-stack'
import { Home } from 'screen/dashboard'
import CompanyInsurance from 'screen/asuransi/CompanyInsurance'
import MsigSinarmasNavigation from './MsigSinarmasNavigation'
import SimasJiwaNavigation from './SimasJiwaNavigation'

const RouteConfigs = {
    Home: {
        screen: Home,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },
    CompanyInsurance: {
        screen: CompanyInsurance,
        navigationOptions: ({ navigation }) => ({
            title: 'Perusahaan Asuransi'
        })
    },
    MsigSinarmas: {
        screen: MsigSinarmasNavigation,
        navigationOptions: ({ navigation }) => ({
            title: navigation.state.params.name,
            header: null
        })
    },
    SimasJiwa: {
        screen: SimasJiwaNavigation,
        navigationOptions: ({ navigation }) => ({
            title: navigation.state.params.name,
            header: null
        })
    }
}

const StackNavigatorConfig = {

}

const HomeNavigation = createStackNavigator(RouteConfigs, StackNavigatorConfig)

HomeNavigation.navigationOptions = ({ navigation }) => {
    if(navigation.state.index==0){
      return {
          tabBarVisible: true,
      };
    }
    return {
        tabBarVisible: false,
    };
};

export default HomeNavigation